<?php

/**
 * redis 缓存设置基类
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2016/1/27
 * Time: 15:41
 */
abstract class CacheRedisBase {
	private static $master;
	private static $slave;

	private static function getMaster() {
		if (!self::$master) {
			self::$master = RedisClient::getInstanceMaster();
		}
		return self::$master;
	}

	private static function getSlave() {
		if (!self::$slave) {
			self::$slave = RedisClient::getInstanceSlave();
		}
		return self::$slave;
	}

	protected static function getCacheKey($keyConfig, $append) {
		if (!self::isLiteral($append)) {
			trigger_error('键类型错误', E_USER_ERROR);
		}
		return $keyConfig['version'] . '-' . $keyConfig['name'] . $append;
	}

	protected static function isLiteral($data) {
		return is_string($data) || is_numeric($data) || !$data;
	}

	/**
	 * 统一设置缓存
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $keyConfig
	 * @param $appendStr
	 * @param string|array $data 为空删除缓存
	 *
	 * @return bool|int
	 */
	protected static function set($keyConfig, $appendStr, $data) {
		$cacheKey = self::getCacheKey($keyConfig, $appendStr);
		if ($data) {
			$res      = self::getMaster()->set($cacheKey, self::isLiteral($data) ? $data : json_encode($data));
			self::setExpire($cacheKey, $keyConfig);
			return $res;
		} else {
			return self::getMaster()->del($cacheKey);
		}
	}

	/**
	 * 统一获取缓存
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $cacheKey
	 * @param int $decode 1返回数组 0返回字符
	 *
	 * @return bool|mixed|string
	 */
	protected static function get($cacheKey, $decode) {
		if (!RedisKeyConfig::getCacheEnabled()) {
			return false;
		}
		$r = self::getSlave()->get($cacheKey);
		if ($r && $decode) {
			return json_decode($r, 1);
		}
		return $r;
	}

	protected static function hset($cacheKey, $field, $data) {
		if ($data) {
			return self::getMaster()->hSet($cacheKey, $field, self::isLiteral($data) ? $data : json_encode($data));
		} else {
			return self::getMaster()->hDel($cacheKey, $field);
		}
	}

	protected static function hget($cacheKey, $field, $decode) {
		if (!RedisKeyConfig::getCacheEnabled()) {
			return false;
		}
		$r = self::getSlave()->hGet($cacheKey, $field);
		if ($r && $decode) {
			return json_decode($r, 1);
		}
		return $r;
	}

	protected static function setExpire($cacheKey, $keyConfig) {
		if (!isset($keyConfig['expire'])) {
			trigger_error('过期时间错误', E_USER_ERROR);
		}
		if($keyConfig['expire']) {
			return self::getMaster()->expire($cacheKey, $keyConfig['expire']);
		}
		return false;
	}

	protected static function incr($cacheKey, $count = 1) {
		if($count < 0) return false;
		if ($count == 1) {
			return self::getMaster()->incr($cacheKey);
		}
		return self::getMaster()->incrBy($cacheKey, $count);
	}

	protected static function decr($cacheKey, $count = 1) {
		if($count < 0) return false;
		if ($count == 1) {
			return self::getMaster()->decr($cacheKey);
		}
		return self::getMaster()->decrBy($cacheKey, $count);
	}
}