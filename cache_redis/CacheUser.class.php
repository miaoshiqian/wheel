<?php
/**
 * 用户信息缓存
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2016/1/27
 * Time: 18:59
 */

class CacheUser extends CacheRedisBase {

	// 用户头像
	public static function setUserAvatar($userId, $avatar){
		return self::set(RedisKeyConfig::$user_avatar, $userId, $avatar);
	}

	// 获取用户头像
	public static function getUserAvatar($userId){
		$cacheKey = self::getCacheKey(RedisKeyConfig::$user_avatar, $userId);
		return self::get($cacheKey, false);
	}

	// 用户是否关注了老师
	public static function checkIsFollowTeacher($userId, $teacherId) {
		$cacheKey = self::getCacheKey(RedisKeyConfig::$follow_user_teacher, $userId.'_'.$teacherId);
		return self::get($cacheKey, false);
	}

	// 关注老师
	public static function followTeacher($userId, $teacherId) {
		return self::set(RedisKeyConfig::$follow_user_teacher, $userId.'_'.$teacherId, 1);
	}

	// 取消关注老师
	public static function unFollowTeacher($userId, $teacherId) {
		return self::set(RedisKeyConfig::$follow_user_teacher, $userId.'_'.$teacherId, null);
	}
}