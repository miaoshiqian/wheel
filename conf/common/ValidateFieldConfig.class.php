<?php
/**
 *  表单验证中的字段配置
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/12/9
 * Time: 13:33
 */

class ValidateFieldConfig {

	const MOBILE_CONTEXT = 'mobileContext_'; // 短信验证成功后 返回的

	public static function captcha($name = 'captcha'){
		return array(
			'name' => $name,
			'display' => '图形验证码',
			'focusMessage' => '请填写图形验证码',
			'rules' => array(
				array(
					'mode' => RuleConfig::REQUIRED,
					'errorMessage' => '请输入右侧图片中的验证码',
				),
				array(
					'mode' => RuleConfig::CUSTOMIZE,
					'errorMessage' => '图形验证码错误',
					'phpCallback' => function($value){
						return VerCodeInterface::verifyCaptchaCode(array('captchaCode' => $value));
					},
				),
				array(
					'mode' => RuleConfig::AJAX,
					'errorMessage' => '图形验证码错误',
					'url' => '/ajax/verifyCaptcha',
				),
			),
		);
	}

	public static function mobile($name = 'mobile') {
		return array(
			'name'         => $name,
			'display'      => '手机号码',
			'focusMessage' => '请填写手机号码',
			'rules'        => array(
				array(
					'mode'         => RuleConfig::REQUIRED,
					'errorMessage' => '手机号码不可以为空',
				),
				array(
					'mode'         => RuleConfig::REG_EXP,
					'pattern'      => RegExpRuleConfig::PHONE,
					'errorMessage' => '手机号码不正确，请重新输入',
				),
			),
		);
	}

	public static function smsCode($mobile, $name = 'vcode') {
		return array(
			'name'         => $name,
			'display'      => '短信验证码',
			'focusMessage' => '请填写短信验证码',
			'rules'        => array(
				array(
					'mode'         => RuleConfig::REQUIRED,
					'errorMessage' => '请输入手机短信验证码',
				),
				array(
					'mode'         => RuleConfig::REG_EXP,
					'pattern'      => RegExpRuleConfig::CODE,
					'errorMessage' => '短信验证码格式错误',
				),
				array(
					'mode'         => RuleConfig::CUSTOMIZE,
					'errorMessage' => '短信验证码错误，请重新输入',
					'phpCallback'  => function ($value) use ($mobile) {
						try {
							$result = VerCodeInterface::verifySmsCode(array(
								'mobile'  => $mobile,
								'verCode' => $value
							));
						} catch (Exception $e) {
							Log::fatal('user.register.verifySmsCode', $e);
							$result = array(
								'code'    => '999',
								'message' => '服务器繁忙'
							);
						}
						if ($result['code']) {
							return false;
						} else {
							$mobileContext = $result['data']['mobileContext'];
							Session::setValue(self::MOBILE_CONTEXT.$mobile, $mobileContext); // 存储下
							return true;
						}
					},
				),
			),
		);
	}

	public static function getMobileContext($mobile){
		return Session::getValue(self::MOBILE_CONTEXT.$mobile);
	}

	public static function password($name = 'password'){
		return array(
			'name'         => 'password',
			'display'      => '登录密码',
			'focusMessage' => '请填写登录密码',
			'rules'        => array(
				array(
					'mode'         => RuleConfig::REQUIRED,
					'errorMessage' => '登录密码不可以为空',
				),
				array(
					'mode'         => RuleConfig::CUSTOMIZE,
					'errorMessage' => '密码强度过低',
				),
			),
		);
	}
}