<?php
/**
 * @author XXX
 * Yac key的配置文件
 * @date 15/6/4
 * @time 下午5:40
 */

/*
 * 1. key的长度最大不能超过48个字符. (我想这个应该是能满足大家的需求的, 如果你非要用长Key, 可以MD5以后再存)
 * 2. Value的最大长度不能超过64M, 压缩后的长度不能超过1M.
 * 3. 当内存不够的时候, Yac会有比较明显的踢出率, 所以如果要使用Yac, 那么尽量多给点内存吧.
 */
class YacKeyConfig {
    // 后台管理员权限
    const ADMIN_USER_RULE_CODE_PREFIX = 'bc_u_r_c_'; // 实际用时前面会有版本号vx_

}