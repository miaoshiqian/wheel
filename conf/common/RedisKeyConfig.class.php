<?php
/**
 * @author XXX
 * Redis key的配置文件
 * @date 15/6/4
 * @time 下午5:40
 */
class RedisKeyConfig {

	/** 是否启用缓存 */
	public static function getCacheEnabled() {
		if (GlobalConfig::inOnline()) {
			return true;
		}
		return true;
	}


	// 用户头像(_id)
	public static $user_avatar = array(
		'name' => 'u_ava_',
		'version' => 1,
		'expire' => 864000,
	);
}