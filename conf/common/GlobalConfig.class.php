<?php
/**
 *  全局配置 不区分环境，可定义一些常量
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/17
 * Time: 16:44
 */
require_once __DIR__.'/../conf/BaseGlobalConfig.class.php';

class GlobalConfig extends BaseGlobalConfig{
	/**
	 * 当前是否是线上
	 * @author 李登科<lidengke@xiongying.com>
	 * @return bool
	 */
	public static function inOnline(){
		return self::ENV === 'online';
	}

	const ADMIN_DEFAULT_PASSWORD = '123456';

	/** 主站域名 */
	const MS_DOMAIN = 'http://www.wheelsrnr.com';

	/** 主站cookie域 */
	const MS_COOKIE_DOMAIN = 'wheelsrnr.com';

    /** 公用数据库status枚举 */
    const STATUS_NORMAL = 1; //正常状态
    const STATUS_INIT   = 0; //初始的默认值
    const STATUS_USER_DESTROY  = -5; //用户注销
    const STATUS_USER_DELETE   = -10; //用户删除
    const STATUS_ADMIN_DISABLE = -15; //管理员禁止
    const STATUS_ADMIN_DELETE  = -20; //管理员删除

    public static $COMMON_STATUS_TEXT = array(
        self::STATUS_NORMAL => '正常',
        self::STATUS_INIT   => '初始',
        self::STATUS_USER_DESTROY => '用户注销',
        self::STATUS_USER_DELETE  => '用户删除',
        self::STATUS_ADMIN_DISABLE => '管理员禁用',
        self::STATUS_ADMIN_DELETE  => '管理员删除',
    );

	/** 主站cookie过期时间 */
	public static function MS_COOKIE_EXPIRE(){
		return time() + 604800;
	}

	/** 后台cookie过期时间 */
	public static function BC_COOKIE_EXPIRE(){
		return time() + 86400; //保存一天过期
	}

    public static $SERVICE_TYPE = array(
        1 => '技术开发',
        2 => '法律人事',
        3 => '企业运营',
        4 => '融资咨询',
        5 => '行业分析',
        6 => '团队管理',
        7 => '创业辅导',
        8 => 'UI 设计',
        9 => '创业指导',
        10 => '找合伙人',
        11 => '产品规划',
        12 => '股权设计',
        13 => '工商财税'
    );

    //技术、产品、运营、设计。映射到 服务类型上面
    const KEY_JISHU   = 1; //技术
    const KEY_CHANPIN = 2; //产品
    const KEY_YUNYING = 3; //运营
    const KEY_SHEJI   = 4; //设计
    public static $KEYWORD_VALUE = array(
        self::KEY_JISHU   => array('技术'),
        self::KEY_CHANPIN => array('产品'),
        self::KEY_YUNYING => array('运营'),
        self::KEY_SHEJI   => array('设计'),
    );
    public static $KEY_TO_TYPE = array(
        self::KEY_JISHU   => array(1), //数组中的值、为service_type的值
        self::KEY_CHANPIN => array(5,6,7,9),
        self::KEY_YUNYING => array(3),
        self::KEY_SHEJI   => array(8),
    );

    /**
     * @brief 点赞类型
     */
    const PRAISE_SERVICE = 0; //对服务进行点赞
    const PRAISE_USER    = 1; //对用户进行点赞

    /**
     * @brief 服务价格单位配置
     */
    public static $SERVICE_UNIT = array(
        '1' => '小时',
        '2' => '天',
        '3' => '周',
        '4' => '月',
    );
}