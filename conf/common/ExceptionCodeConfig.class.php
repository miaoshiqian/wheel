<?php
/**
 *  统一放异常编号
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/18
 * Time: 13:23
 */

/*
 * 编号段说明，999为自定义语句
 * 其他编号从1000开始，以1000为步长 定义各个业务段
 *
 *
 */

class ExceptionCodeConfig{
	const CUSTOM = 999; // 自定义,可展示给用户看的
	const SYS_CUSTOM = 1000; // 自定义,系统错误，不给用户看的，

	/** 公共方法里抛的异常 2000 ~ 2999 */
	const ERR_PARAMS_NOT_EXIST = 2000; // 参数不存在
	const ERR_PARAMS_EMPTY = 2001; // 参数不能为空
	const ERR_FILE_NOT_EXIST = 2002; // 文件不存在
}