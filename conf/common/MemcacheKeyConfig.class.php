<?php
class MemcacheKeyConfig {

    //省份memcache索引
    const LOCATION_PROVINCE = 'MEM_LOCATION_PROVINCE';

    //城市缓存前缀
    const LOCATION_CITY_ID = 'MEM_LOCATION_CITY_';

    //区域缓存前缀
    const LOCATION_DISTRICT_ID = 'MEM_LOCATION_DISTRICT_';

    //街道缓存前缀
    const LOCATION_STREET_ID = 'MEM_LOCATION_STREET_';

}