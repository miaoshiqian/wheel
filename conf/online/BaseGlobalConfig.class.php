<?php
/**
 *  全局配置 区分环境
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/17
 * Time: 16:44
 */

class BaseGlobalConfig{
	/** 当前运行环境 */
	const ENV = 'online';

    /** 前端静态文件域名 */
    const JS_DOMAIN = 'bc.wheelsrnr.com';

    /** sql日志写入目录 */
    const SQL_LOG_DIR = '/data/log/php_program_debug';

    /** 后台cookie域 */
    const BC_COOKIE_DOMAIN = 'bc.wheelsrnr.com';

    /** 后台域名 */
    const BC_DOMAIN = 'bc.wheelsrnr.com';
}