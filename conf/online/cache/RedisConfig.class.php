<?php

class RedisConfig {

	// 业务配置
	public static $REDIS_DEFAULT; // 默认配置
	public static $REDIS_QUEUE; // redis队列配置

    public static function init() {
        self::$REDIS_DEFAULT = array(
            'master'    => array(self::$MS_1),
            'slaves'    => array(self::$SL_1),
        );
        self::$REDIS_QUEUE = array(
            'master'    => array(self::$MS_Q1),
            'slaves'    => array(),
        );
    }

    // 主库
    private static $MS_1 = array(
        'host' => '127.0.0.1',
        'port'     => 6379,
        'timeout'  => 0,
        'password' => '',   // 预留
        'db'       => 0,    // 预留
        'weight'   => 1,    // 预留
    );

	// 从库
	private static $SL_1 = array(
		'host' => '127.0.0.1',
		'port'     => 6379,
		'timeout'  => 0,
		'password' => '',   // 预留
		'db'       => 0,    // 预留
		'weight'   => 1,    // 预留
	);

	// 队列
    private static $MS_Q1 = array(
        'host' => '127.0.0.1',
        'port'     => 6379,
        'timeout'  => 0,
        'password' => '',   // 预留
        'db'       => 1,    // db1
        'weight'   => 1,    // 预留
    );

}
RedisConfig::init();
