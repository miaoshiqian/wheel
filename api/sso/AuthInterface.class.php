<?php
/**
 *  后台权限接口
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/19
 * Time: 14:43
 */

require_once __DIR__.'/impl/AuthImpl.class.php';

class AuthInterface {

	public static function hashPassword($password, $salt){
		return md5(md5($password).$salt);
	}

	/**
	 * 根据用户user_id获取组编号
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $params['user_id']
	 *
	 * @return array|bool 组编号集合 没有返回false
	 * @throws Exception
	 */
	public static function getGroupCodesByUserId($params){
		Util::checkParamsExist(array('user_id'), $params);
		return AuthImpl::getGroupCodesByUserId($params['user_id']);
	}

	/**
	 * 检查用户权限
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $params['token']
	 * @param $params['user_id']
	 * @param $params['rule_group']
	 *
	 * @return true 权限通过, -1 token 无效, false 权限不通过
	 * @throws Exception
	 */
	public static function checkAuth($params) {
		Util::checkParamsExist(array('token', 'user_id', 'rule_group'), $params);
		return AuthImpl::checkAuth($params['token'], $params['user_id'], $params['rule_group']);
	}

	/**
	 * 登录接口
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string $params['username']
	 * @param string $params['password']
	 *
	 * @return bool|int 失败|用户id
	 * @throws Exception
	 */
	public static function login($params){
		Util::checkParamsExist(array('username', 'password'), $params);
		return AuthImpl::login($params['username'], $params['password']);
	}
}