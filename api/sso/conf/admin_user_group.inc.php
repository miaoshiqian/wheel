<?php
/**
 *  用户组表
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 10:41
 */

// 组:以admin(admin是管理员组,拥有最大权限)为基础派生,派生的组名越长权限越细化,权限也越小.组名为*的代表所有未登录用户均有此组权限
// 组命名规则：admin[-后台频道号[-项目操作[-数字编号]]]
// 后台频道号:子系统 比如main,log,sso.项目操作:自定义. 数字编号:一般约定 1 查看 2 编辑 3 删除,数字大的拥有数字小的权限
// 有编辑权限的肯定得有查看权限,查看权限比较低,这样给用户分配组时就不用写那么繁琐了
// 组的设计看两点:1.继承关系:比较短的组名自动拥有以此组派生出的组的权限.例如admin-main 拥有 admin-main-video下的所有权限,这也是为啥 admin有最高权限了
// 2. 层级关系：组名中除去数字编号剩余部分相同的组,以数字编号大小来比较,数字大的拥有数字小的权限;
// 子目录会拥有上层目录的查看(为1)权限,这样是为了用户拥有权限树底层的一个权限,要自动拥有上层的访问权限,不然进不了底层,权限没意义.
// 例如 检查 admin-main-1时，用户有 admin-main-video admin-main-video-1时是可以访问的
class AdminGroupConfig {
	private static $GROUP_NAME =  array(
		'admin' => '超级',
		'admin-log' => '日志',
		'admin-main-comment' => '评价',
		'admin-main-feedback' => '用户反馈',
		'admin-main-user' => '用户',
		'admin-sso' => '管理员',
	);

	public static function getConfig() {
		$files = array(
			'bc/conf/rule.inc.php',
			'bc/modules/apidoc/conf/rule.inc.php',
			'bc/modules/doc/conf/rule.inc.php',
			'bc/modules/log/conf/rule.inc.php',
			'bc/modules/sso/conf/rule.inc.php',
		);
		$data = array();
		foreach ($files as $file) {
			$data = array_merge($data, include __DIR__ . '/../../../' . $file);
		}
		$list = array();
		foreach ($data as $val) {
			if (is_string($val)) {
				$val = preg_replace("/-\d/", "", $val);
				$list[$val] = '';
			} else {
				$val['admin_group'] = preg_replace("/-\d/", "", $val['admin_group']);
				$list[$val['admin_group']] = '';
			}
		}

		ksort($list);
		$ret = array();

		foreach ($list as $key => $val) {
			if ($key == '*') {
				continue;
			}
			if ($key == 'admin') {
				$ret['admin'] = array(
					'text' => self::$GROUP_NAME[$key] . '管理员',
					'parent' => '',
					'level' => 0,
				);
//				$ret['admin-1'] = array(
//					'text' => '后台查看',
//					'parent' => 'admin',
//					'level' => 1,
//				);
			} else {
				if (isset(self::$GROUP_NAME[$key])) {
					$prefix = self::$GROUP_NAME[$key];
					$name1 = $prefix . '管理员';
					$name2 = $prefix . '查看';
					$name3 = $prefix . '编辑';
				} else {
					$name1 = $key;
					$name2 = $key . '-1';
					$name3 = $key . '-2';
				}
				$parentKey = preg_replace("/-\w+$/", "", $key);
				$ret[$key] = array(
					'text' => $name1,
					'parent' => $parentKey,
					'level' => isset($ret[$parentKey]) ? $ret[$parentKey]['level'] + 1 : 1,
				);
				$ret[$key . '-1'] = array(
					'text' => $name2,
					'parent' => $key,
					'level' => $ret[$key]['level'] + 1,
				);
				$ret[$key . '-2'] = array(
					'text' => $name3,
					'parent' => $key,
					'level' => $ret[$key]['level'] + 1,
				);
			}
		}

		return $ret;
	}
}

return AdminGroupConfig::getConfig();