<?php

/**
 * admin_user表 model
 *
 * @author model自动生成模板
 * @date   2015-12-18 11:52:22
 */
class AdminUserModel extends BaseModel {

	protected function init() {
        $this->dbName         = \DBConfig::DB_COMMON;
		$this->tableName      = 'admin_user';
		$this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
		$this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
		$this->fieldTypes     = array(
			'id'              => 'int', // id
			'account'         => 'varchar', // 账号
			'password'        => 'varchar', // 密码
			'salt'            => 'varchar', // 密码混淆字符
			'name'            => 'varchar', // 姓名
			'phone'           => 'char', // 手机号
			'email'           => 'varchar', // 邮箱
			'avatar_url'      => 'varchar', // 头像url
			'create_user_id'  => 'int', // 添加人id
			'create_time'     => 'int', // 添加时间
			'last_login_time' => 'int', // 最后登录时间
            'status' => 'tinyint', // 0新创建 1正常 -2管理员禁止 -3管理员删除
		);
	}


	public function getByAccount($account) {
		return $this->getRow('*', array(
			array('account', '=', $account)
		));
	}

	public function updateLoginTime($userId, $loginTime) {
		return $this->updateById($userId, array(
			'last_login_time' => $loginTime
		));
	}


	public function forbidById($id) {
		return $this->updateById($id, array(
			'status' => -50
		));
	}

	public function unForbidById($id) {
		return $this->updateById($id, array(
			'status' => 1
		));
	}


	public function getPageList($url, $pageNum, $pageSize, $filed) {

	}

}