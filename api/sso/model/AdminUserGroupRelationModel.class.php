<?php

/**
 * admin_user_group_relation表 model
 *
 * @author model自动生成模板
 * @date   2015-12-18 11:52:22
 */
class AdminUserGroupRelationModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_COMMON;
        $this->tableName      = 'admin_user_group_relation';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // id
            'user_id' => 'int', // admin_user_id
            'group_id' => 'varchar', // 组编号
        );
    }

	/**
	 * 检查用户是否在指定组内
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $userId
	 * @param $groupId
	 *
	 * @return bool|int false不存在，否则返回关联id
	 */
	public function checkUserGroupExist($userId, $groupId) {
		if ($userId && $groupId) {
			$check = $this->getRow('id', array(
				array('user_id', '=', $userId),
				array('group_id', '=', trim($groupId)),
			));
			if ($check) {
				return $check['id'];
			}
		}
		return false;
	}

	/**
	 * 添加用户组
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $userId
	 * @param $groupId
	 *
	 * @return bool|false|int
	 */
	public function addUserGroup($userId, $groupId) {
		if ($userId && $groupId) {
			$insertId = $this->insert(array(
				'user_id'  => $userId,
				'group_id' => $groupId
			));
			return $insertId;
		}
		return false;
	}

	/**
	 * 删除用户组
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $userId
	 *
	 * @return bool|int
	 */
	public function removeUserGroup($userId){
		if($userId) {
			return $this->delete(array(
				array('user_id', '=', $userId)
			));
		}
		return false;
	}

	/**
	 * 根据用户找组
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $userId
	 *
	 * @return array|bool
	 */
	public function getGroupByUserId($userId) {
		if ($userId) {
			$list = $this->getAll('group_id', array(
				array('user_id', '=', $userId)
			));
			return array_column($list, 'group_id');
		}
		return false;
	}

	/**
	 * 根据组找用户
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $groupId
	 *
	 * @return array|bool
	 */
	public function getUserByGroupId($groupId) {
		if ($groupId) {
			$list = $this->getAll('user_id', array(
				array('group_id', '=', $groupId)
			));
			return array_column($list, 'user_id');
		}
		return false;
	}

	public function getGroupList(){
		static $groupList;
		if($groupList) {
			return $groupList;
		}
		$groupList = include __DIR__.'/../conf/admin_user_group.inc.php';
		return $groupList;
	}

}