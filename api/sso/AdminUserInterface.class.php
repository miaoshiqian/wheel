<?php
/**
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/12/18
 * Time: 14:28
 */

include_once __DIR__ . '/model/AdminUserModel.class.php';
include_once __DIR__ . '/model/AdminUserGroupRelationModel.class.php';

class AdminUserInterface {

	private static $adminUserModel;
	private static $userGroupModel;

	private static function modelAdminUser() {
		if (!self::$adminUserModel) {
			self::$adminUserModel = new AdminUserModel();
		}
		return self::$adminUserModel;
	}

	private static function modelUserGroup() {
		if (!self::$userGroupModel) {
			self::$userGroupModel = new AdminUserGroupRelationModel();
		}
		return self::$userGroupModel;
	}

	/**
	 * 添加管理员
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $params
	 *
	 * @return false|int
	 * @throws UserException
	 */
	public static function addUser($params) {
		if (empty($params['account'])) {
			throw new UserException('账号不能为空');
		}
		if (empty($params['password'])) {
			throw new UserException('密码不能为空');
		}
		if (empty($params['name'])) {
			throw new UserException('姓名不能为空');
		}
		$params['salt']        = mt_rand(10000, 99999);
		$params['password']    = AuthInterface::hashPassword($params['password'], $params['salt']);
		$params['create_time'] = time();
		$params['status']      = 1;

		$model = self::modelAdminUser();
		return $model->insert($params);
	}

	/**
	 * 修改管理员
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $id
	 * @param $field
	 *
	 * @return bool|int
	 */
	public static function updateUser($id, $field) {
		return self::modelAdminUser()->updateById($id, array(
			'account' => Util::getFromArray('account', $field),
			'name'    => Util::getFromArray('name', $field),
			'phone'   => Util::getFromArray('phone', $field),
			'email'   => Util::getFromArray('email', $field),
		));
	}

	/**
	 * 禁止管理员
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $ids
	 *
	 * @return bool|int
	 */
	public static function forBidUser($ids) {
		return self::modelAdminUser()->disableByAdmin($ids);
	}

	/**
	 * 解除禁止
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $ids
	 *
	 * @return bool|int
	 */
	public static function unForBidUser($ids) {
		return self::modelAdminUser()->enableByAdmin($ids);
	}

	public static function deleteByAdmin($ids) {
		if ($ids && is_array($ids)) {
			return self::modelAdminUser()->deleteByAdmin($ids);
		}
		return false;
	}

	/**
	 * 修改密码
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $userId
	 * @param $password
	 *
	 * @return bool|int
	 */
	public static function changePassword($userId, $password) {
		$userInfo = self::modelAdminUser()->getById($userId, 'salt');
		if (!empty($userInfo['salt'])) {
			return self::modelAdminUser()->updateById($userId, array(
				'password' => AuthInterface::hashPassword($password, $userInfo['salt'])
			));
		}

		return false;
	}

	/**
	 * 管理员列表
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $fields
	 * @param $pageParams
	 * @param $conditions
	 *
	 * @return array
	 */
	public static function userList($fields, $pageParams, $conditions) {
		$searchCondition = array();
		$orderBy         = array('id' => 'DESC');
		$model           = self::modelAdminUser();

		if ($conditions) {
			if (!empty($conditions['kw']) && !empty($conditions['search_field'])) {
				$searchCondition[] = array($conditions['search_field'], '=', $conditions['kw']);
			}
			if (!empty($conditions['only_disable'])) {
				$searchCondition[] = array('status', '=', -50);
			}
		}
		$searchCondition[] = array('status', '>', -100); // 未被删除的

		$pageSize = empty($pageParams['pageSize']) ? 20 : $pageParams['pageSize'];
		$pageNum  = empty($pageParams['pageNum']) ? 1 : $pageParams['pageNum'];
		$pageUrl  = empty($pageParams['pageUrl']) ? '' : $pageParams['pageUrl'];

		$totalCount = $model->getCount($searchCondition);
		$offset     = Pager::genPageOffset($pageNum, $pageSize, 1);
		$dataList   = $model->getAll($fields, $searchCondition, $orderBy, $pageSize, $offset);

		return Pager::getPageList($totalCount, $pageNum, $pageSize, $dataList, $pageUrl);
	}

	/**
	 * 管理员详细信息
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param        $id
	 * @param string $field
	 *
	 * @return array|bool|false
	 */
	public static function userDetail($id, $field = '*') {
		$model = self::modelAdminUser();
		return $model->getById($id, $field);
	}


	/**
	 * 编辑用户和组的关系
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param int   $userId
	 * @param array $groupIdList 组id array
	 *
	 * @return bool
	 */
	public static function editUserGroup($userId, $groupIdList) {
		$modelRelation = self::modelUserGroup();

		$modelRelation->removeUserGroup($userId);

		if ($groupIdList) {
			sort($groupIdList);
			foreach ($groupIdList as $groupId) {
				$groupIdArr = explode("-", $groupId);
				$continue = false;
				while($groupIdArr) {
					array_pop($groupIdArr);
					if (in_array(implode("-", $groupIdArr), $groupIdList)) {
						$continue = true;
						break;
					}
				}
				if ($continue) continue;
				$modelRelation->addUserGroup($userId, $groupId);
			}
		}
		return true;
	}

	public static function getGroupList() {
		return self::modelUserGroup()->getGroupList();
	}

	public static function getGroupByUserId($userId) {
		return self::modelUserGroup()->getGroupByUserId($userId);
	}

	public static function getUserByGroupId($groupId) {
		return self::modelUserGroup()->getUserByGroupId($groupId);
	}
}