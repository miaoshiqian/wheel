<?php

/**
 *  用户权限验证实现
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/19
 * Time: 14:54
 */

// 后台用户权限说明:
// 权限控制是以组为基础的,先找到人所在组1(一个用户可能会有N个组),再找到权限对的组2(一个权限只能对应一个组),再比较组1和组2的关系
// 组:以admin(admin是管理员组,拥有最大权限)为基础派生,派生的组名越长权限越细化,权限也越小.组名为*的代表所有未登录用户均有此组权限
// 组命名规则：admin[-后台频道号[-项目操作[-数字编号]]]
// 后台频道号:子系统 比如main,log,sso.项目操作:自定义. 数字编号:一般约定 1 查看 2 编辑 3 删除,数字大的拥有数字小的权限
// 有编辑权限的肯定得有查看权限,查看权限比较低,这样给用户分配组时就不用写那么繁琐了
// 组的设计看两点:1.继承关系:比较短的组名自动拥有以此组派生出的组的权限.例如admin-main 拥有 admin-main-video下的所有权限,这也是为啥 admin有最高权限了
// 2. 层级关系：组名中除去数字编号剩余部分相同的组,以数字编号大小来比较,数字大的拥有数字小的权限;
// 子目录会拥有上层目录的查看(为1)权限,这样是为了用户拥有权限树底层的一个权限,要自动拥有上层的访问权限,不然进不了底层,权限没意义.
// 例如 检查 admin-main-1时，用户有 admin-main-video admin-main-video-1时是可以访问的

require_once __DIR__.'/../model/AdminUserModel.class.php';

class AuthImpl {

	public static function hashPassword($password, $salt){
		return md5(md5($password).$salt);
	}

	/**
	 * 根据用户获取组编号s
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param int $userId
	 *
	 * @return array
	 * @throws Exception
	 */
	public static function getGroupCodesByUserId($userId) {
		Util::checkEmpty($userId);
		static $userAllGroups = array();
		if (isset($userAllGroups[$userId])) {
			return $userAllGroups[$userId];
		}

		$groups = AdminUserInterface::getGroupByUserId($userId);
		$userAllGroups[$userId] = $groups;
		if ($groups) {
			return $groups;
		}

		return array();
	}

	/**
	 * 检查用户权限
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string $token
	 * @param int    $userId
	 * @param string $ruleGroup 权限所在组
	 *
	 * @return true 权限通过, -1 token 无效, false 权限不通过
	 * @throws Exception
	 */
	public static function checkAuth($token, $userId, $ruleGroup) {
		Util::checkEmpty(array($ruleGroup, $userId));
		$userGroups = self::getGroupCodesByUserId($userId);
		// 检查token有效性
		static $checkToken = null; // token 是否有效
        $checkToken = true; //改于 2016-9-7 不用校验token，允许一个账号多人登陆。
		/*if($checkToken === null){
			$getToken = self::getTokenByUser($userId, $userGroups);
			if ($token !== $getToken) {
				$checkToken = false;
			} else {
				$checkToken = true;
			}
		}*/
		if (!$checkToken) {
			return -1;
		}

		static $userAllowGroups = array();
		if (isset($userAllowGroups[$userId . '_' . $ruleGroup])) {
			return true;
		}

		if (in_array($ruleGroup, $userGroups)) {

			$userAllowGroups[$userId . '_' . $ruleGroup] = 1;
			return true;
		}

		$ruleGroupArr = explode('-', $ruleGroup);
		$ruleGroupNum = array_pop($ruleGroupArr);
		foreach ($userGroups as $_userGroup) {

			// 继承关系
			if (false !== strpos($ruleGroup, $_userGroup)) { // 无数字的
				$userAllowGroups[$userId . '_' . $ruleGroup] = 1;
				return true;
			}

			$_userGroupArr = explode('-', $_userGroup);
			$_userGroupNum = array_pop($_userGroupArr);
			if (is_numeric($ruleGroupNum) && is_numeric($_userGroupNum)) {
				// 层极关系，最后一位是数字的，大数字的权限包含小数字的 -3的包括 -1的
				if (join('-', $ruleGroupArr) === join('-', $_userGroupArr) && $_userGroupNum >= $ruleGroupNum) {

					$userAllowGroups[$userId . '_' . $ruleGroup] = 1;
					return true;
				}
			}
			// 子目录要拥有上层目录的查看(为1)权限 eg 检查 admin-main-1时，用户有 admin-main-video admin-main-video-1时是可以访问的
			if ($ruleGroupNum == 1 && false !== strpos(join('-', $_userGroupArr), join('-', $ruleGroupArr))) {
				$userAllowGroups[$userId . '_' . $ruleGroup] = 1;
				return true;
			}
		}
		return false;
	}

	/**
	 * 登录，成功后返回 userId
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $userName
	 * @param $password
	 *
	 * @return array|bool
	 * @throws SysException
	 * @throws UserException
	 */
	public static function login($userName, $password) {
		Util::checkEmpty(array($userName, $password));
		$model = new AdminUserModel();
		$userInfo = $model->getByAccount($userName);
		if (!$userInfo && $userName == 'admin') {
			if ($password != GlobalConfig::ADMIN_DEFAULT_PASSWORD) {
				throw new UserException('用户名或密码错误');
			}
			$userId = AdminUserInterface::addUser(array(
				'account' => 'admin',
				'name' => '超级管理员',
				'password' => GlobalConfig::ADMIN_DEFAULT_PASSWORD,
			));
			if ($userId) {
				AdminUserInterface::editUserGroup($userId, array('admin'));
			}
			$userInfo = $model->getByAccount($userName);
		}
		if ($userInfo) {
			$now = time();
			$userId = $userInfo['id'];
			$dbPassword = $userInfo['password'];
			if ($dbPassword != self::hashPassword($password, $userInfo['salt'])) {
				throw new UserException('用户名或密码错误');
			}
			if ($userInfo['status'] != 1) {
				throw new UserException('已被禁用，禁止登录');
			}

			// 成功
			$model->updateLoginTime($userId, $now);
			return array(
				'id' => $userId,
				'group' => $userGroup = self::getGroupCodesByUserId($userId),
				'username' => $userInfo['name'],
				'token' => self::makeToken($userId, $userInfo['name'], $userGroup, $now),
			);
		}

		return false;
	}

	public static function getTokenByUser($userId, $userGroups){
		$userInfo = self::getUserInfoById($userId);
		if($userInfo){
			$loginTime = $userInfo['last_login_time'];
			return self::makeToken($userId, $userInfo['name'], $userGroups, $loginTime);
		}
		return false;
	}

	public static function getUserInfoById($userId){
		$model = new AdminUserModel();
		return $model->getById($userId);
	}

	public static function makeToken($userId, $userName, $userGroups, $loginTime){
		if(!is_array($userGroups)){
			$userGroups = array($userGroups);
		}
		ksort($userGroups);
		$hashCode = '^&_(@]ki1';
		return md5($userId.'_'.$userName.'_'.join("|", $userGroups).'_'.$loginTime.'_'.$hashCode);
	}
}