<?php

/**
 * 用户反馈接口
 * User: 龙卫国<longweiguo@xiongying.com>
 * Date: 2015/12/16
 * Time: 19:10
 */
class FeedbackInterface {

	public static function getData($fields, $pageParams, $conditions) {
		$searchCondition = array();
		if ($conditions) {
			if (!empty($conditions['kw'])) {
				if (in_array($conditions['search_field'], array('user_id', 'reply_user_id'))) {
					$searchCondition[] = array($conditions['search_field'], '=', $conditions['kw']);
				} else if ($conditions['search_field'] == 'content') {
					$searchCondition[] = array('content', 'like', "%{$conditions['kw']}%");
				}
			}
			// 只显示未处理
			if (!empty($conditions['enable_only'])) {
				$searchCondition[] = array('status', '=', 1);
			}
		}
		$searchCondition[] = array('status', '>', -100);

		$orderBy = array('id' => 'DESC');

		$pageSize = empty($pageParams['pageSize']) ? 30 : $pageParams['pageSize'];
		$pageNum  = empty($pageParams['pageNum']) ? 1 : $pageParams['pageNum'];
		$pageUrl  = empty($pageParams['pageUrl']) ? '' : $pageParams['pageUrl'];

		$model      = FeedbackModel::getInstance();
		$totalCount = $model->getCount($searchCondition);
		$offset     = Pager::genPageOffset($pageNum, $pageSize, 1);
		$dataList   = $model->getAll($fields, $searchCondition, $orderBy, $pageSize, $offset);

		return Pager::getPageList($totalCount, $pageNum, $pageSize, $dataList, $pageUrl);
	}

	public static function enableByAdmin($ids) {
		if ($ids && is_array($ids)) {
			return FeedbackModel::getInstance()->enableByAdmin($ids);
		}
		return false;
	}

	public static function disableByAdmin($ids) {
		if ($ids && is_array($ids)) {
			return FeedbackModel::getInstance()->disableByAdmin($ids);
		}
		return false;
	}

	public static function ignoreByAdmin($ids, $data) {
		if ($ids && is_array($ids)) {
			return FeedbackModel::getInstance()->disableByAdmin($ids, $data);
		}
		return false;
	}

	public static function add($data) {
		$userId       = (int) Util::getFromArray('user_id', $data);
		$mobile       = (int) Util::getFromArray('mobile', $data);
		$content      = Util::getFromArray('content', $data);
		$platFormType = (int) Util::getFromArray('platform_type', $data);
		if ($userId < 1 || empty($content)) {
			throw new UserException('参数错误！');
		}
		if ($platFormType > 0 && $platFormType != 1 && $platFormType != 2) {
			throw new UserException('参数错误！');
		}
		$image_url = !empty($data['image_url']) && is_string($data['image_url']) ? $data['image_url'] : '';
		return FeedbackModel::getInstance()->insert(array(
			'user_id'        => $userId,
			'mobile'         => $mobile,
			'content'        => $content,
			'image_url'      => $image_url,
			'platform_type'  => $platFormType,
		));
	}

	public static function replyById($id, $data) {
		return FeedbackModel::getInstance()->replyById($id, $data);
	}

	public static function getInfoById($id) {
		return FeedbackModel::getInstance()->getById($id);
	}

}