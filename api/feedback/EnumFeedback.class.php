<?php
/**
 * @author 缪石乾<miaoshiqian@haoche51.com>
 * @date 15/12/27
 * @time 下午4:00
 */

class EnumFeedback {
    /**
     * @brief 反馈状态
     */
    const STATUS_WAIT_DEAL = 1;//待处理
    const STATUS_HAS_DEAL  = 2;//已处理
    public static $STATUS_TEXT = array(
        self::STATUS_WAIT_DEAL => '待处理',
        self::STATUS_HAS_DEAL  => '已处理',
    );
}