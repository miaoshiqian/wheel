<?php
/**
 * feedback表 model
 * @date 2016-02-25 11:29:26
 */

class FeedbackModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_QCB;
        $this->tableName      = 'feedback';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 自增id
            'user_id' => 'int', // 反馈人id
            'mobile' => 'int', // 手机
            'content' => 'varchar', // 反馈内容
	    'image_url'        => 'varchar', // 图片路径
            'create_time' => 'int', // 创建时间
            'reply_content' => 'varchar', // 回复内容
            'reply_user_id' => 'int', // 回复人
            'reply_time' => 'int', // 回复时间
            'status' => 'int', // 1显示，2已回复，-1用户删除，-2管理员禁用，-3管理员删除
        );
    }

	public function disableByAdmin($ids, $columnsAndValues = array()) {
		return parent::disableByAdmin($ids, array(
			'handle_user_id' => Util::getFromArray('reply_user_id', $columnsAndValues),
			'handle_time'    => time(),
		));
	}

	public function replyById($id, $columnsAndValues = array()) {
		return parent::updateById($id, array(
			'reply_content'  => Util::getFromArray('reply_content', $columnsAndValues),
			'handle_user_id' => Util::getFromArray('reply_user_id', $columnsAndValues),
			'handle_time'    => time(),
			'status'         => 2,
		));
	}

	public function insert($columnsAndValues) {
		$columnsAndValues = array_merge($columnsAndValues, array(
			'create_time' => time(),
			'status'      => 1,
		));
		return parent::insert($columnsAndValues);
	}
}