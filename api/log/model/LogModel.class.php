<?php
/**
 * model自动生成模板
 * @author 缪石乾<miaoshiqian@iyuesao.com>
 * @date 2015年1月13日 下午5:38:00
 */

class LogModel extends BaseModel {

    protected function init() {
        $this->dbName         = DBConfig::DB_LOG;
        $this->tableName      = 'log';
        $this->dbMasterConfig = DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 
            'level' => 'int', // 日志等级：1，普通信息；2，调试信息，3，警告；4，致命错误；
            'data' => 'varchar', // 详细信息
            'tag' => 'varchar', // 日志tag
            'url' => 'varchar', // 产生日志URL
            'trace' => 'varchar', // 执行轨迹
            'create_time' => 'int', // 创建时间
        );
    }
    
    public function update($updateArr, $params) {
        return false;
    }
}