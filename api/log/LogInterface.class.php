<?php

class LogInterface {

	/**
	 * 直接调用Impl记录日志
	 * @param $params
	 * @return bool
	 */
	public static function add($params) {
		Util::checkParamsExist(array('level', 'tag', 'data', 'url', 'trace'), $params);
		
		$addParams = [
			'level'       => (int) $params['level'],
			'data'        => $params['data'],
			'tag'         => $params['tag'],
			'url'         => $params['url'],
			'trace'       => $params['trace'],
			'create_time' => time(),
		];
		$logModel  = new LogModel();
		$ret       = $logModel->insert($addParams);
		return $ret;		
	}

    /**
     * 获取日志列表
     */
	public static function logList($pageParams, $conditions) {
		$searchCondition = array();
		$orderBy         = array('id' => 'DESC');
		$model           = new LogModel();

		if ($conditions) {
			if (!empty($conditions['level'])) {
				$searchCondition[] = array('level', '=', $conditions['level']);
			}
			if (!empty($conditions['kw']) && !empty($conditions['search_field'])) {
				$searchCondition[] = array($conditions['search_field'], '=', $conditions['kw']);
			}
		}

		$pageSize = empty($pageParams['pageSize']) ? 50 : $pageParams['pageSize'];
		$pageNum  = empty($pageParams['pageNum']) ? 1 : $pageParams['pageNum'];
		$pageUrl  = empty($pageParams['pageUrl']) ? '' : $pageParams['pageUrl'];

		$totalCount = $model->getCount($searchCondition);
		$offset     = Pager::genPageOffset($pageNum, $pageSize, 1);
		$dataList   = $model->getAll('*', $searchCondition, $orderBy, $pageSize, $offset);

		return Pager::getPageList($totalCount, $pageNum, $pageSize, $dataList, $pageUrl);
	}

}