<?php
/**
 * 验证码相关、短信、图形
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/12/8
 * Time: 19:54
 */

require_once __DIR__ . '/impl/VerCodeImpl.class.php';

class VerCodeInterface {

	/**
	 * 验证短信验证码
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $params [mobile]
	 * @param $params [verCode]
	 *
	 * @return array
	 * @throws SysException
	 */
	public static function verifySmsCode($params) {
		Util::checkParamsExist(array('mobile', 'verCode'), $params);
		return VerCodeImpl::verifySmsCode($params['mobile'], $params['verCode']);
	}

	/**
	 * 验证图形
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $params [captchaCode]
	 *
	 * @return bool
	 */
	public static function verifyCaptchaCode($params) {
		Util::checkParamsExist(array('captchaCode'), $params);
		return VerCodeImpl::verifyCaptchaCode($params['captchaCode']);
	}

	/**
	 * 生成图形验证码
	 * @author 李登科<lidengke@xiongying.com>
	 */
	public static function getCaptcha() {
		VerCodeImpl::getCaptcha();
	}

	/**
	 * 注册或者登录后老用户绑定时获取验证码
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $params [mobile]
	 *
	 * @return array
	 * @throws SysException
	 */
	public static function getBindMobileVerCode($params) {
		Util::checkParamsExist(array('mobile'), $params);
		return VerCodeImpl::getBindMobileVerCode($params['mobile']);
	}

	/**
	 * 重置密码时获取验证码
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $params[mobile]
	 *
	 * @return array
	 * @throws SysException
	 */
	public static function getPasswordVerCode($params) {
		Util::checkParamsExist(array('mobile'), $params);
		return VerCodeImpl::getPasswordVerCode($params['mobile']);
	}
}