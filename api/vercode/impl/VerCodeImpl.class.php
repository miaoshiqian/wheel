<?php
/**
 *
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/12/8
 * Time: 19:58
 */

class VerCodeImpl {

	/**
	 * 统一返回成功信息
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param mixed $data
	 *
	 * @return array
	 */
	private static function outputOk($data = ''){
		return array(
			'code' => 0,
			'data' => $data,
		);
	}

	/**
	 * 统一返回失败信息
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param array $codeMap
	 * @param array $responseArr
	 * @param string $defaultMessage
	 *
	 * @return array
	 */
	private static function outputFail($codeMap, $responseArr, $defaultMessage){
		$errCode = isset($responseArr['MsgCode']) ? $responseArr['MsgCode'] : null;
		$message = isset($codeMap[$errCode]) ? $codeMap[$errCode] : $defaultMessage;
		$errCode === null && $errCode = 999; // 未响应时返回999
		$errCode === 0 && $errCode = -1; // 接口返回0时代表失败，转成统一的-1

		return array(
			'code' => $errCode,
			'message' => $message
		);
	}

	public function aaa(){}
	/**
	 * 验证手机验证码（通用的）
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $mobile
	 * @param $verCode
	 *
	 * @return array
	 * @throws SysException
	 */
	public static function verifySmsCode($mobile, $verCode){
		Util::checkEmpty(array($mobile, $verCode));
		$url    = '/Mobile/VerifyVerCode';
		$params = array(
			'Mobile' => $mobile,
			'VerCode' => $verCode,
		);
		$codeMap = array(
			1 => '成功',
			0 => '验证失败',
			-200 => '验证失败', // 失败，服务器未知错误
			-11001 => '验证码错误', // 失败，Msg=服务器无有效验证码
			-11002 => '验证码错误', // 失败，Msg=验证码有误
			-11003 => '验证码已过期', // 失败，Msg=验证码过期
		);
		$response = NetApi::query($url, $params);
		$data = NetApi::parseResponse($response);

		if(NetApi::isRequestOk($data)){
			return self::outputOk(array(
				'mobileContext' => $data['MobileContext'],
			));
		} else {
			Log::fatal('vercode.verifySmsCode', $response);
			return self::outputFail($codeMap, $data, '验证失败');
		}
	}

	/**
	 * 验证图形验证码
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $code
	 *
	 * @return bool
	 */
	public static function verifyCaptchaCode($code){
		$codeStore = Session::getValue('captchaCode');
		if(!$codeStore) {
			return false;
		}
		if(strtolower($codeStore['code']) != strtolower($code)) {
			return false;
		}
		if($codeStore['expire'] < time()){
			return false;
		}
		return true;
	}

	/**
	 * 获取图形验证码
	 * @author 李登科<lidengke@xiongying.com>
	 * @throws Exception
	 */
	public static function getCaptcha(){
		$captcha = new Captcha();
		$code = $captcha->getCode();
		Session::setValue('captchaCode', array(
			'code' => $code,
			'expire' => time() + 60 * 10
		));
		$captcha->show($code);
	}

	/**
	 * 重置密码时获取验证码
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param int $mobile 手机号码
	 *
	 * @return array
	 * @throws SysException
	 */
	public static function getPasswordVerCode($mobile){
		Util::checkEmpty(array($mobile));
		$url    = '/Mobile/GetPasswordVerCode';
		$params = array(
			'Mobile' => $mobile,
		);
		$codeMap = array(
			1 => '成功',
			0 => '获取失败',
			-200 => '获取失败', // 失败，服务器未知错误
			-13001 => '已经连续申请5次', // 失败，Msg=已经连续申请5次
			-13002 => '每天只能申请24次', // 失败，Msg=每天只能申请24次
		);
		$response = NetApi::query($url, $params);
		$data = NetApi::parseResponse($response);

		if(NetApi::isRequestOk($data)){
			return self::outputOk();
		} else {
			Log::fatal('user.getPasswordVerCode', $response);
			return self::outputFail($codeMap, $data, '获取失败');
		}
	}

	/**
	 * 注册或者登录后老用户绑定时获取验证码
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $mobile
	 *
	 * @return array
	 * @throws SysException
	 */
	public static function getBindMobileVerCode($mobile){
		Util::checkEmpty(array($mobile));
		$url    = '/Mobile/GetBindMobileVerCode';
		$params = array(
			'Mobile' => $mobile,
		);
		$codeMap = array(
			1 => '成功',
			0 => '获取失败',
			-200 => '获取失败', // 失败，服务器未知错误
			-12001 => '手机号已经被注册', // 失败，Msg=手机号已经被注册,与第一个获取验证码不同之处在得到此值需要提示用户是否登录。
			-13001 => '已经连续申请5次', // 失败，Msg=每天只能申请24次
			-13002 => '每天只能申请24次', // 失败，Msg=每天只能申请24次
		);
		$response = NetApi::query($url, $params);
		$data = NetApi::parseResponse($response);

		if(NetApi::isRequestOk($data)){
			return self::outputOk();
		} else {
			Log::fatal('user.getBindMobileVerCode', $response);
			return self::outputFail($codeMap, $data, '获取失败');
		}
	}
}