<?php
/**
 * auto_brand表 model
 * @date 2016-04-18 21:00:50
 */

class AutoBrandModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_CAR;
        $this->tableName      = 'auto_brand';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 主键id
            'name' => 'varchar', // 品牌名称
            'en_name' => 'varchar', // 英文名称
            'pinyin' => 'varchar', // 拼音
            'first_char' => 'varchar', // 拼音首字母
            'img_url' => 'varchar', // 图片url
            'show_status' => 'int', // 默认是否显示:0-不显示 1-显示
        );
    }
}