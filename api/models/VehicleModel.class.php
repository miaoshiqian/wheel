<?php
/**
 * vehicle表 model
 * @date 2016-10-22 22:59:51
 */

class VehicleModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_WHEEL;
        $this->tableName      = 'vehicle';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 
            'first_name' => 'varchar', // 
            'last_name' => 'varchar', // 
            'address' => 'varchar', // number and street
            'more_address' => 'varchar', // 更多详细地址
            'province_id' => 'int', // state id
            'city_id' => 'int', // city id
            'email_address' => 'varchar', // 邮箱地址
            'zip_code' => 'int', // 邮编
            'title' => 'varchar', // 标题
            'phone1' => 'varchar', // 
            'phone2' => 'varchar', // 
            'make' => 'varchar', // 
            'vehicle_year' => 'int', // 
            'model_name' => 'varchar', // 
            'model_trim' => 'int', // 车款id
            'model_trim_name' => 'varchar', // 车款名称
            'listing_price' => 'double', // listing price 单位 $
            'mile' => 'float', // 行驶里程
            'status' => 'int', // 车源当前状态  1：上线、-1：下线、5：已售出
            'exterior_color' => 'varchar', // 
            'interior_color' => 'varchar', // 
            'transmission' => 'int', // 
            'title_status' => 'int', // 
            'body_style' => 'int', // 车身结构
            'vin_code' => 'varchar', // vin code
            'highlights' => 'varchar', // 
            'create_time' => 'int', // 
            'create_user_id' => 'int', // 
            'update_time' => 'int', // 
            'offline_time' => 'int', // 下线时间
            'offline_reason' => 'varchar', // 下线理由
        );
    }
}