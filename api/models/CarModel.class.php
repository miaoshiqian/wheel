<?php
/**
 * car表 model
 * @date 2016-08-03 17:41:32
 */

class CarModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_CAR;
        $this->tableName      = 'car';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 
            'brand_id' => 'int', // 车辆品牌id
            'brand_name' => 'varchar', // 品牌名称
            'class_id' => 'int', // 车系id
            'class_name' => 'varchar', // 车系名称
            'vehicle_year' => 'int', // 车款的年份
            'model_id' => 'int', // 该车做在车型表中的id
            'model_name' => 'varchar', // 车款名称
            'province_id' => 'int', // 省份id
            'city_id' => 'int', // 车源所在城市id
            'email_num' => 'varchar', // 邮编
            'create_user_id' => 'int', // 添加车源的客服id
            'create_user_name' => 'varchar', // 添加车源的客服姓名
            'seller_phone' => 'varchar', // 车主电话
            'seller_name' => 'varchar', // 车主姓名
            'seller_price' => 'float', // 卖家定价(万元)
            'title' => 'varchar', // 车源信息
            'plate_number' => 'varchar', // 车牌号
            'plate_time' => 'int', // 上牌时间
            'year_check_time' => 'int', // 年检到期时间
            'insurance_time' => 'int', // 交强险到期时间
            'color' => 'int', // 车身颜色枚举
            'address' => 'varchar', // 地址
            'emission' => 'varchar', // 排量
            'geerbox' => 'int', // 变速箱类型 0:未知 1:手动 2:自动 3:双离合 4:手自一体 5:无级变速
            'structure' => 'int', // 车身结构，0未知 1两厢 2三厢 3SUV 4MPV 5旅行车 6跑车 7皮卡 8面包车
            'miles' => 'float', // 行驶里程数(万公里)
            'status' => 'int', // 车源当前状态  1：上线、-1：下线、5：已售出
            'offline_time' => 'int', // 车源下线时间(unix timestamp)
            'offline_reason' => 'varchar', // 下架原因
            'sold_time' => 'int', // 车源售出时间(unix timestamp)
            'vin_code' => 'varchar', // VIN码
            'high_lights' => 'varchar', // 亮点配置数组的json字符串
            'create_time' => 'int', // 车源信息录入时间(unix timestamp)
            'update_time' => 'timestamp', // 更新时间
        );
    }
}