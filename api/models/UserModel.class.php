<?php
/**
 * user表 model
 * @date 2016-03-18 11:34:48
 */

class UserModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_CAR;
        $this->tableName      = 'user';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 自增id
            'real_name' => 'varchar', // 姓名
            'province_id' => 'int', // 省份id
            'city_id' => 'int', // 城市id
            'district_id' => 'int', // 区域id
            'street_id' => 'int', // 街道id
            'mobile' => 'varchar', // 手机号
            'username' => 'varchar', // 用户名
            'mail' => 'varchar', // 邮箱
            'passwd' => 'varchar', // 密码
            'last_login_time' => 'int', // 最后登录时间
            'create_time' => 'int', // 创建时间
            'update_time' => 'timestamp', // 更新时间
        );
    }
}