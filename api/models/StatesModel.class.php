<?php
/**
 * states表 model
 * @date 2016-08-03 17:23:09
 */

class StatesModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_CAR;
        $this->tableName      = 'states';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 
            'state' => 'varchar', // 
            'state_code' => 'char', // 
        );
    }
}