<?php
/**
 * car_images表 model
 * @date 2016-04-12 14:47:09
 */

class CarImagesModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_CAR;
        $this->tableName      = 'car_images';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 
            'car_id' => 'int', // 车源id
            'url' => 'varchar', // 图片url
            'status' => 'int', // 图片状态 0:无意义、1：正常显示、-1:删除
            'type' => 'int', // 图片类型。0：默认类型、1：封面图
            'create_time' => 'int', // 添加时间
        );
    }
}