<?php
/**
 * car_models表 model
 * @date 2016-10-20 17:03:52
 */

class CarModelsModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_WHEEL;
        $this->tableName      = 'car_models';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'model_id' => 'int', // 
            'model_make_id' => 'varchar', // 
            'model_name' => 'varchar', // 
            'model_trim' => 'varchar', // 
            'model_year' => 'int', // 
            'model_body' => 'varchar', // 
            'model_engine_position' => 'varchar', // 
            'model_engine_cc' => 'int', // 
            'model_engine_cyl' => 'int', // 
            'model_engine_type' => 'varchar', // 
            'model_engine_valves_per_cyl' => 'int', // 
            'model_engine_power_ps' => 'int', // 
            'model_engine_power_rpm' => 'int', // 
            'model_engine_torque_nm' => 'int', // 
            'model_engine_torque_rpm' => 'int', // 
            'model_engine_bore_mm' => 'decimal', // 
            'model_engine_stroke_mm' => 'decimal', // 
            'model_engine_compression' => 'varchar', // 
            'model_engine_fuel' => 'varchar', // 
            'model_top_speed_kph' => 'int', // 
            'model_0_to_100_kph' => 'decimal', // 
            'model_drive' => 'varchar', // 
            'model_transmission_type' => 'varchar', // 
            'model_seats' => 'int', // 
            'model_doors' => 'int', // 
            'model_weight_kg' => 'int', // 
            'model_length_mm' => 'int', // 
            'model_width_mm' => 'int', // 
            'model_height_mm' => 'int', // 
            'model_wheelbase_mm' => 'int', // 
            'model_lkm_hwy' => 'decimal', // 
            'model_lkm_mixed' => 'decimal', // 
            'model_lkm_city' => 'decimal', // 
            'model_fuel_cap_l' => 'int', // 
            'model_sold_in_us' => 'tinyint', // 
            'model_co2' => 'int', // 
            'model_make_display' => 'varchar', // 
        );
    }
}