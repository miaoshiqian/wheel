<?php
/**
 * cities表 model
 * @date 2016-08-03 17:23:14
 */

class CitiesModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_CAR;
        $this->tableName      = 'cities';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 
            'city' => 'varchar', // 
            'state_code' => 'char', // 
            'state_id' => 'int', // 
        );
    }
}