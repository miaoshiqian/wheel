<?php
/**
 * car_lead表 model
 * @date 2016-03-24 11:55:36
 */

class CarLeadModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_CAR;
        $this->tableName      = 'car_lead';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 主键id
            'brand_id' => 'int', // 品牌id
            'class_id' => 'int', // 车系id
            'year' => 'varchar', // 年份
            'model_id' => 'int', // 车型id
            'brand_name' => 'varchar', // 品牌名称
            'class_name' => 'varchar', // 车系名称
            'model_name' => 'varchar', // 车型名称
            'mile' => 'float', // 行驶里程，单位“万公里”
            'plate_time' => 'int', // 上牌时间
            'province_id' => 'int', // 省份id
            'city_id' => 'int', // 城市id
            'district_id' => 'int', // 区域id
            'seller_phone' => 'varchar', // 车主手机号
            'status' => 'int', // 处理状态. 0:无意义、1：待处理、5：已处理
            'remark' => 'varchar', // 备注
            'create_time' => 'int', // 添加时间
            'update_time' => 'timestamp', // 
        );
    }
}