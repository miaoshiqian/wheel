<?php
/**
 * auto_series表 model
 * @date 2016-04-18 21:03:07
 */

class AutoSeriesModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_CAR;
        $this->tableName      = 'auto_series';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 
            'name' => 'varchar', // 车系名称
            'en_name' => 'varchar', // 英文车系名称
            'short_name' => 'varchar', // 车系短名称
            'pinyin' => 'varchar', // 拼音
            'brand_pinyin' => 'varchar', // 品牌拼音
            'brand_id' => 'int', // 品牌id
            'brand_name' => 'varchar', // 
            'en_brand_name' => 'varchar', // 英文品牌名称
            'subbrand_id' => 'int', // 子品牌id
            'subbrand_name' => 'varchar', // 子品牌名
            'auto_type' => 'varchar', // 车型级别，A00微型车/A0小型车，A紧凑型车，B中型车，C高级车，D豪华车，SUP跑车，SUVSUV，MPVMPV，PK 皮卡，MB 微面，SY 商用车
            'apperance' => 'float', // 外观评分
            'average_score' => 'float', // 综合评分
            'cost_efficient' => 'float', // 性价比评分
            'internal' => 'float', // 内饰评分
            'maneuverability' => 'float', // 操控性评分
            'oil_consumption' => 'float', // 油耗评分
            'power' => 'float', // 动力评分
            'space' => 'float', // 空间评分
            'comfortableness' => 'float', // 舒适性评分
            'oil_consumption_info' => 'varchar', // 油耗信息
            'img_url' => 'varchar', // 图片url
        );
    }
}