<?php
/**
 * @brief 简介：model自动生成模板
 * @author 缪石乾<miaoshiqian@iyuesao.com>
 * @date 2015年1月13日 下午5:38:00
 */

class DistrictModel extends BaseModel {

    protected function init() {
        $this->dbName         = DBConfig::DB_COMMON;
        $this->tableName      = 'district';
        $this->dbMasterConfig = DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 区域ID
            'name' => 'varchar', // 区域名称
            'short_name' => 'varchar', // 简称
            'pinyin' => 'varchar', // 拼音
            'location' => 'varchar', // 百度经纬度
            'city_id' => 'int', // 城市ID
            'display_order' => 'int', // 排序权重
        );
    }
    
    public function add($params) {
        $r = $this->insert($params);
        return $r;
    }
    
    public function edit($updateArr, $params) {
        $r = $this->update($updateArr, $params);
        return $r;
    }
    
    public function get($fields, $params) {
        $fields = isset($fields) ? $fields : '*';
        $r = $this->getRow($fields, $params, '', 0, true);
        return $r;
    }
    
    public function getList($field = '*', $params = array(), $orderBy = '', $limit = 0, $offset = 0, $fromMaster = false) {
        $r = $this->getAll($field, $params, $orderBy, $limit, $offset);
        return $r;
    }
    
    public function getCount($params) {
        return $this->getOne('count(*)' , $params);
    }
}