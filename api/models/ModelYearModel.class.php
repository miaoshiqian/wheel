<?php
/**
 * model_year表 model
 * @date 2016-07-30 12:09:04
 */

class ModelYearModel extends BaseModel {

    protected function init() {
        $this->dbName         = \DBConfig::DB_CAR;
        $this->tableName      = 'model_year';
        $this->dbMasterConfig = \DBConfig::$SERVER_MASTER;
        $this->dbSlaveConfig  = \DBConfig::$SERVER_SLAVE;
        $this->fieldTypes     = array(
            'id' => 'int', // 
            'year' => 'int', // 
            'make' => 'varchar', // 
            'model' => 'varchar', // 
            'color' => 'varchar', // 车辆颜色
            'geearbox' => 'varchar', // 变速箱
            'door' => 'int', // 车门数
            'seat' => 'int', // 座位数
            'length' => 'int', // 长度，单位mm 毫米
            'height' => 'int', // 高度单位毫米mm
            'width' => 'int', // 宽度单位毫米mm
            'volume' => 'int', // 行李箱容积单位 升L
            'hub' => 'varchar', // 轮毂材料
            'wheelbase' => 'int', // 轴距 单位毫米mm
            'quality' => 'int', // 整备质量单位kg
            'oil_100' => 'int', // 百公里油耗单位升L
            'displacement' => 'varchar', // 排量字符串，填写例如 2.0L、2.0T
            'oil_volume' => 'int', // 邮箱容积单位升L
            'inlet_style' => 'varchar', // 进气形式 如：涡轮增压
            'torque' => 'int', // 最大扭矩单位（n.m)
            'oil_type' => 'varchar', // 燃油，例如：汽油
            'oil_mark' => 'varchar', // 燃油标号 例如：97号
            'emission' => 'varchar', // 排放标准 例如：国IV
            'power' => 'int', // 功率单位千瓦 kw
            'high_speed' => 'int', // 最高车速单位 km/h
            'acceleration' => 'int', // 百公里加速度单位 秒
            'drive_type' => 'varchar', // 驱动方式 例如：前置前驱
            'steering_system' => 'varchar', // 转向系统 例如：电动助力
            'pre_suspension' => 'varchar', // 前悬挂 例如：麦弗逊式独立悬架
            'af_suspension' => 'varchar', // 后悬架
            'pre_braking' => 'varchar', // 前制动 例如：通风盘式
            'af_braking' => 'varchar', // 后制动
            'pre_tyre' => 'varchar', // 前轮胎规格 例如：15/55 R16
            'af_tyre' => 'varchar', // 后轮胎规格 例如：215/55 R16
        );
    }
}