<?php
/**
 * @brief 美国城市接口
 */
class UsLocationInterface {
    /**
     * @brief 获取洲列表
     * @return array
     */
    public static function getStateList($params = []) {
        return StatesModel::getInstance()->getAll('*');
    }

    /**
     * @brief 根据state ID获取城市列表
     * @param state_id  洲ID
     * @return array
     */
    public static function getCityByStateId($params = []) {
        if (empty($params['state_id'])) {
            return array();
        }
        return CitiesModel::getInstance()->getAll('*', array(array('state_id', '=', $params['state_id'])));
    }

    //参数：$params['id'] //state_id
    public static function getStateNameById($params = []) {
        if (empty($params['id'])) {
            return '';
        }
        return StatesModel::getInstance()->getOne('state', array(array('id', '=', $params['id'])));
    }

    //参数：$params['id'] //city_id
    public static function getCityNameById($params = []) {
        if (empty($params['id'])) {
            return '';
        }
        return CitiesModel::getInstance()->getOne('city', array(array('id', '=', $params['id'])));
    }
}