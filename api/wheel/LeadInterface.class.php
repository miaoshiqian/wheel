<?php

class LeadInterface{

    /**
     * @brief 添加客户接口
     * $params = array(
     *     'brand_id' => int, //品牌id
     *     'brand_name' => int, //品牌名称
     *     'class_id' => int, //车系id
     *     'class_name' => string, //车系名称
     *     'model_id' => int, //车型id
     *     'model_name' => int, //车型名称
     *     'year' => int, //年款 例如 2013
     *     'mile' => int, //行驶里程
     *     'plate_time' => int, //上牌时间
     *     'province_id' => int, //省份id
     *     'city_id' => int, //城市id
     *     'district_id' => int, //区域id
     *     'seller_phone' => string, //车主电话
     *     'remark' => string, //备注
     * )
     */
    public static function add($params) {
        $info = array(
            'brand_id'      => Util::getFromArray('brand_id', $params, 0),
            'brand_name'    => Util::getFromArray('brand_name', $params, ''),
            'class_id'      => Util::getFromArray('class_id', $params, 0),
            'class_name'    => Util::getFromArray('class_name', $params, ''),
            'model_id'      => Util::getFromArray('model_id', $params, 0),
            'model_name'    => Util::getFromArray('model_name', $params, ''),
            'year'          => Util::getFromArray('year', $params, ''),
            'mile'          => Util::getFromArray('mile', $params, 0),
            'plate_time'    => Util::getFromArray('plate_time', $params, 0),
            'province_id'   => Util::getFromArray('province_id', $params, 0),
            'district_id'   => Util::getFromArray('district_id', $params, 0),
            'city_id'       => Util::getFromArray('city_id', $params, 0),
            'seller_phone'  => Util::getFromArray('seller_phone', $params, ''),
            'remark'        => Util::getFromArray('remark', $params, ''),
            'status'        => EnumLead::STATUS_WAIT,
            'create_time'   => time(),
        );
        $leadId = CarLeadModel::getInstance()->insert($info);
        if ($leadId === false) {
            throw new Exception('添加线索失败');
        }
        return $leadId;
    }

    /**
     * @brief 获取列表
     * @param $params['filter'] 查询条件
     * @param limit/offset/order/field   记录数、开始查找位置、排序、获取的字段
     */
    public static function getList($params) {
        $field  = Util::getFromArray('field', $params, '*');
        $filter = Util::getFromArray('filters', $params, array());
        $limit  = Util::getFromArray('limit', $params, 0);
        $offset = Util::getFromArray('offset', $params, 0);
        $order  = Util::getFromArray('order', $params, '');
        return CarLeadModel::getInstance()->getAll($field, $filter, $order, $limit, $offset);
    }

    /**
     * @brief 通过用户id获取信息
     * @param $params['id'] //用户id
     */
    public static function getById($params) {
        Util::checkParamsExist(array('id'), $params);
        return CarLeadModel::getInstance()->getRow('*', array(array('id', '=', $params['id'])));
    }

    /**
     * @brief 获取符合filter条件的记录数
     * @param $params['filters'] 查询条件
     */
    public static function getCount($params) {
        Util::checkParamsExist(array('filters'), $params);
        $filter = Util::getFromArray('filters', $params, array());
        return CarLeadModel::getInstance()->getCount($filter);
    }

    /**
     * @brief 标记为已处理
     * @param $params['id']
     */
    public static function deal($params) {
        Util::checkParamsExist(array('id'), $params);
        $ret = CarLeadModel::getInstance()->updateById($params['id'], array('status' => EnumLead::STATUS_DONE));
        if ($ret === false) {
            throw new Exception('更新失败');
        }
    }
}