<?php

class VehicleModelInterface{
    public static function getBrandList() {
        $brandList = CarModelsModel::getInstance()->queryAll('select model_make_id as make from rric_new.car_models group by model_make_id;');
        /*$brandList = AutoBrandModel::getInstance()->getAll('id,name,en_name,pinyin');
        if ($brandList && $english) {
            foreach($brandList as &$brand) {
                if ($english && $brand['en_name']) {
                    $brand['name'] = $brand['en_name'];
                }
            }
        }*/
        return $brandList;
    }

    public static function getModelsByFilter($params) {
        $filter = array();
        if (isset($params['brand'])) {
            $filter[] = array('model_make_id','=',$params['brand']);
        }
        if (isset($params['series'])) {
            $filter[] = array('model_name','=',$params['series']);
        }
        return CarModelsModel::getInstance()->getAll('model_id', $filter);
    }

    public static function getSeriesList($brand) {
        $brand = addslashes($brand);
        $seriesList = CarModelsModel::getInstance()->queryAll("select model_name as model from car_models where model_make_id='{$brand}' group by model_name;");
        foreach($seriesList as &$series) {
            $series['name'] = $series['model'];
        }
        return $seriesList;
    }

    public static function getModelById($modelId) {
        $model = CarModelsModel::getInstance()->getRow('*', array(
            array('model_id', '=' ,$modelId)
        ));
        return $model;
    }
}