<?php
/**
 * 用户接口
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/16
 * Time: 19:10
 */

require_once __DIR__.'/impl/UserImpl.class.php';

class UserInterface{

    /**
     * @brief 添加客户接口
     * $params = array(
     *     'real_name' => int, //姓名
     *     'mobile' => int, //手机
     *     'username' => int, //用户名
     *     'passwd' => string, //登陆密码 MD5
     *     'province_id' => int, //省份
     *     'city_id' => int, //城市
     *     'district_id' => int, //区域
     *     'steet_id' => int, //街道id
     * )
     */
    public static function add($params) {
        Util::checkParamsExist(array('real_name', 'mobile', 'username', 'passwd'), $params);
        return UserImpl::add($params);
    }

    /**
     * @brief 更新完善客户信息, 传入需要更新的字段
     * $params = array(
     *     'real_name' => int, //姓名
     *     'mobile' => int, //手机
     *     'username' => int, //用户名
     *     'passwd' => string, //登陆密码 MD5
     *     'province_id' => int, //省份
     *     'city_id' => int, //城市
     *     'district_id' => int, //区域
     *     'steet_id' => int, //街道id
     * )
     */
    public static function upUser($params) {
        Util::checkParamsExist(array('user_id'), $params);
        return UserImpl::upUser($params);
    }

    /**
     * @brief 获取用户列表
     * @param $params['filter'] 查询条件
     * @param limit/offset/order/field   记录数、开始查找位置、排序、获取的字段
     */
    public static function getList($params) {
        return UserImpl::getList($params);
    }

    /**
     * @brief 通过用户id获取用户信息
     * @param $params['id'] //用户id
     */
    public static function getById($params) {
        Util::checkParamsExist(array('id'), $params);
        return UserImpl::getById($params);
    }

    /**
     * @brief 通过用户mobile获取用户信息
     * @param $params['mobile'] //用户id
     */
    public static function getByMobile($params) {
        Util::checkParamsExist(array('mobile'), $params);
        return UserImpl::getByMobile($params);
    }

    /**
     * @brief 通过用户mobile获取用户信息
     * @param $params['username'] //用户id
     */
    public static function getByUsername($params) {
        Util::checkParamsExist(array('username'), $params);
        return UserImpl::getByUsername($params);
    }

    /**
     * @brief 通过用户mail获取用户信息
     * @param $params['mail'] //用户邮箱
     */
    public static function getByMail($params) {
        Util::checkParamsExist(array('mail'), $params);
        return UserImpl::getByMail($params);
    }

    /**
     * @brief 通过微信id获取用户信息
     * @param $params['wx_id']
     */
    public static function getByWxId($params) {
        Util::checkParamsExist(array('wx_id'), $params);
        return UserImpl::getByWxId($params);
    }

    /**
     * @brief 获取符合filter条件的记录数
     * @param $params['filters'] 查询条件
     */
    public static function getCount($params) {
        Util::checkParamsExist(array('filters'), $params);
        return UserImpl::getCount($params);
    }

    /**
     * @brief 账号加密
     * @return 加密后的m_AUTH_STRING
     */
    public static function encodeSsoTicket($params) {
        Util::checkParamsExist(array('username', 'password'), $params);
        return UserImpl::encodeSsoTicket($params['username'], $params['password']);
    }

    /**
     * @brief 对帐号解密，方式暂时和老业管保持一致
     * @param
     * @return 账号信息
     */
    public static function decodeSsoTicket($params) {
        Util::checkParamsExist(array('passport'), $params);
        return UserImpl::decodeSsoTicket($params['passport']);
    }

}