<?php

class UserImpl {

    /**
     * @brief 添加客户接口
     * $params = array(
     *     'real_name' => int, //姓名
     *     'mobile' => int, //手机
     *     'username' => int, //用户名
     *     'passwd' => string, //登陆密码 MD5
     *     'province_id' => int, //省份
     *     'city_id' => int, //城市
     *     'district_id' => int, //区域
     *     'steet_id' => int, //街道id
     * )
     */
    public static function add($params) {
        $info = array(
            'real_name'   => Util::getFromArray('real_name', $params, ''),
            'mobile'      => Util::getFromArray('mobile', $params, ''),
            'username'    => Util::getFromArray('username', $params, ''),
            'mail'        => Util::getFromArray('mail', $params, ''),
            'passwd'      => Util::getFromArray('passwd', $params, ''),
            'province_id' => Util::getFromArray('province_id', $params, 0),
            'city_id'     => Util::getFromArray('city_id', $params, 0),
            'district_id' => Util::getFromArray('district_id', $params, 0),
            'street_id'   => Util::getFromArray('street_id', $params, 0),
            'create_time' => time(),
        );
        $userId = UserModel::getInstance()->insert($info);
        if ($userId === false) {
            throw new Exception('添加用户失败');
        }
        return $userId;
    }

    /**
     * @brief 添加客户接口
     * $params = array(
     *     'real_name' => int, //姓名
     *     'mobile' => int, //手机
     *     'username' => int, //用户名
     *     'passwd' => string, //登陆密码 MD5
     *     'province_id' => int, //省份
     *     'city_id' => int, //城市
     *     'district_id' => int, //区域
     *     'steet_id' => int, //街道id
     * )
     */
    public static function upUser($params) {
        $userId = Util::getFromArray('user_id', $params, 0);
        if (empty($userId)) {
            throw new Exception('缺少用户id参数');
        }
        $userInfo  = UserModel::getInstance()->getRow('*', array(array('id', '=', $userId)));
        if (empty($userInfo)) {
            throw new Exception('用户id参数无效、没有找到该用户');
        }
        $info = array(
            'real_name'   => Util::getFromArray('real_name', $params, $userInfo['real_name']),
            'mobile'      => Util::getFromArray('mobile', $params, $userInfo['mobile']),
            'username'    => Util::getFromArray('username', $params, $userInfo['username']),
            'passwd'      => Util::getFromArray('passwd', $params, $userInfo['passwd']),
            'province_id' => Util::getFromArray('province_id', $params, $userInfo['province_id']),
            'city_id'     => Util::getFromArray('city_id', $params, $userInfo['city_id']),
            'district_id' => Util::getFromArray('district_id', $params, $userInfo['district_id']),
            'street_id'   => Util::getFromArray('street_id', $params, $userInfo['street_id']),
        );
        $ret = UserModel::getInstance()->update($info, array(array('id', '=', $userId)));
        if ($ret === false) {
            throw new Exception('更新用户信息失败');
        }
    }

    /**
     * @brief 获取用户列表
     */
    public static function getList($params) {
        $field  = Util::getFromArray('field', $params, '*');
        $filter = Util::getFromArray('filters', $params, array());
        $limit  = Util::getFromArray('limit', $params, 0);
        $offset = Util::getFromArray('offset', $params, 0);
        $order  = Util::getFromArray('order', $params, '');
        return UserModel::getInstance()->getAll($field, $filter, $order, $limit, $offset);
    }

    /**
     * @brief 通过用户id获取用户信息
     * @param $params['id'] //用户id
     */
    public static function getById($params) {
        $userId = Util::getFromArray('id', $params, 0);
        if (empty($userId)) {
            throw new Exception('缺少用户id');
        }
        return UserModel::getInstance()->getRow('*', array(array('id', '=', $userId)));
    }

    /**
     * @brief 通过手机号获取用户信息
     * @param $params['mobile']
     */
    public static function getByMobile($params) {
        $mobile = Util::getFromArray('mobile', $params, 0);
        if (empty($mobile)) {
            throw new Exception('缺少mobile');
        }
        return UserModel::getInstance()->getRow('*', array(array('mobile', '=', $mobile)));
    }

    /**
     * @brief 通过用户名号获取用户信息
     * @param $params['username']
     */
    public static function getByUsername($params) {
        $username = Util::getFromArray('username', $params, 0);
        if (empty($username)) {
            throw new Exception('缺少username');
        }
        return UserModel::getInstance()->getRow('*', array(array('username', '=', $username)));
    }

    /**
     * @brief 通过邮箱获取用户信息
     * @param $params['mail']
     */
    public static function getByMail($params) {
        $mail = Util::getFromArray('mail', $params, 0);
        if (empty($mail)) {
            throw new Exception('缺少mail');
        }
        return UserModel::getInstance()->getRow('*', array(array('mail', '=', $mail)));
    }

    //查询记录数
    public static function getCount($params) {
        $filter = Util::getFromArray('filters', $params, array());
        $userModel = new UserModel();
        return $userModel->getCount($filter);
    }

    /**
     * @brief 账号加密
     * @return 加密后的m_AUTH_STRING
     */
    public static function encodeSsoTicket($username, $passwd) {
        include_once FRAMEWORK_PATH . '/util/des/DesUtil.class.php';
        $key        = '1d387351b3023e0e';
        $iv         = 'dfh^&(89';
        $passwd     = substr($passwd, 0, 16);
        $createTime = time();
        $checkTime  = $createTime;
        $text = json_encode([$username,$passwd,$createTime,$checkTime]);
        $encrypt    = DesUtil::create(DesUtil::MODE_3DES, $key, $iv);
        return urlencode($encrypt->encrypt($text));
    }

    /**
     * @brief 对帐号解密，方式暂时和老业管保持一致
     * @param
     * @return 账号信息
     */
    public static function decodeSsoTicket($passport) {
        $passport = urldecode($passport);
        include_once FRAMEWORK_PATH . '/util/des/DesUtil.class.php';
        $key     = '1d387351b3023e0e';
        $iv      = 'dfh^&(89';
        $encrypt = DesUtil::create(DesUtil::MODE_3DES, $key, $iv);
        $text    = $encrypt->decrypt($passport);
        $info    = json_decode($text, true);
        return $info;
    }
}