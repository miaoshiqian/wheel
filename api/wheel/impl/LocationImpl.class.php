<?php
class LocationImpl {
    /**
     * @brief 获取省份列表
     */
    public static function getProvince($params = []) {
        $memHandle = CacheNamespace::createCache(CacheNamespace::MODE_MEMCACHE, MemcacheConfig::$GROUP_DEFAULT);
        $memKey = MemcacheKeyConfig::LOCATION_PROVINCE;
        $provinceListFormat = $memHandle->read($memKey);
        $provinceList = unserialize($provinceListFormat);
        if(!empty($provinceList)) {
            return $provinceList;
        }
//        Log::logInfo([
//            'tag' => 'locationImpl.getProvince.memcache',
//            'data' => 'memcache缓存失效,重新获取',
//        ]);

        $field   = Util::getFromArray('field', $params, '*');
        $orderBy = Util::getFromArray('order_by', $params, ['display_order' => 'asc']);
        $limit   = Util::getFromArray('limit', $params, 0);
        $offset  = Util::getFromArray('offset', $params, 0);

        $provinceModel = new ProvinceModel();
        $provinceList =  $provinceModel->getList($field, array(), $orderBy, $limit, $offset);
        $provinceListFormat = serialize($provinceList);
        $memHandle->write($memKey, $provinceListFormat, 86400);
        return $provinceList;
    }

    /**
     * @brief 根据省份ID获取城市列表
     */
    public static function getCityByProvinceId($params) {
        $provinceId= Util::getFromArray('province_id', $params, 0);
        if(empty($provinceId)) {
            return false;
        }

        $filter[] = ['province_id', '=', $provinceId];

        $field   = Util::getFromArray('field', $params, '*');
        $orderBy = Util::getFromArray('order_by', $params, ['display_order' => 'asc']);
        $limit   = Util::getFromArray('limit', $params, 0);
        $offset  = Util::getFromArray('offset', $params, 0);

        $cityModel = new CityModel();
        $cityInfo =  $cityModel->getList($field, $filter, $orderBy, $limit, $offset);
        return $cityInfo;
    }

    /**
     * @brief 根据城市ID获取区域列表
     */
    public static function getDistrictByCityId($params) {
        $cityId= Util::getFromArray('city_id', $params, 0);
        if(empty($cityId)) {
            return false;
        }
        $filter[] = ['city_id', '=', $cityId];

        $field   = Util::getFromArray('field', $params, '*');
        $orderBy = Util::getFromArray('order_by', $params, ['display_order' => 'asc']);
        $limit   = Util::getFromArray('limit', $params, 0);
        $offset  = Util::getFromArray('offset', $params, 0);

        $districtModel = new DistrictModel();
        return $districtModel->getList($field, $filter, $orderBy, $limit, $offset);
    }

    /**
     * @brief 根据区域ID获取街道列表
     */
    public static function getStreetByDistrictId($params) {
        $districtId= Util::getFromArray('district_id', $params, 0);
        if(empty($districtId)) {
            return false;
        }
        $filter[] = ['district_id', '=', $districtId];

        $field   = Util::getFromArray('field', $params, '*');
        $orderBy = Util::getFromArray('order_by', $params, ['display_order' => 'asc']);
        $limit   = Util::getFromArray('limit', $params, 0);
        $offset  = Util::getFromArray('offset', $params, 0);

        $streetModel = new StreetModel();
        return $streetModel->getList($field, $filter, $orderBy, $limit, $offset);
    }

    /**
     * @brief 根据省份ID获取省份名称
     */
    public static function getProvinceNameById($params) {
        $id = Util::getFromArray('id', $params, 0);
        if(empty($id)) {
            return '';
        }
        $filter[] = ['id','=', $id];
        $field = 'short_name';
        $provinceModel = new ProvinceModel();
        $name = $provinceModel->getOne($field, $filter);
        return $name;
    }

    /**
     * @brief 根据城市ID获取城市名称
     */
    public static function getCityNameById($params) {
        $id = Util::getFromArray('id', $params, 0);
        if(empty($id)) {
            return '';
        }
        $filter[] = ['id','=', $id];
        $field = 'short_name';
        $cityModel = new CityModel();
        $name = $cityModel->getOne($field, $filter);
        return $name;
    }

    /**
     * @brief 根据城市ID获取城市
     *
     * @param id        城市ID
     *
     * @return string   城市名称
     */
    public static function getCityById($params) {
        $field = Util::getFromArray('field', $params, '*');
        $id = Util::getFromArray('id', $params, 0);
        if(empty($id)) {
            return '';
        }
        $filter[] = ['id','=', $id];
        $cityModel = new CityModel();
        $name = $cityModel->getRow($field, $filter);
        return $name;
    }

    /**
     * @brief 根据区域ID获取区域名称
     */
    public static function getDistrictNameById($params) {
        $id = Util::getFromArray('id', $params, 0);
        if(empty($id)) {
            return '';
        }
        $filter[] = ['id','=', $id];
        $field = 'name';
        $districtModel = new DistrictModel();
        $name = $districtModel->getOne($field, $filter);
        return $name;
    }

    /**
     * @brief 根据街道ID获取街道名称
     */
    public static function getStreetNameById($params) {
        $id = Util::getFromArray('id', $params, 0);
        if(empty($id)) {
            return '';
        }
        $filter[] = ['id','=', $id];
        $field = 'name';
        $streetModel = new StreetModel();
        $name = $streetModel->getOne($field, $filter);
        return $name;
    }

    /**
     * @brief 根据街道ID获取街道详情
     */
    public static function getStreetById($params) {
        $id = Util::getFromArray('id', $params, 0);
        if(empty($id)) {
            return '';
        }
        $filter[] = ['id','=', $id];
        $field = '*';
        $streetModel = new StreetModel();
        $info = $streetModel->getRow($field, $filter);
        return $info;
    }

    /**
     * @breif 根据各个id、返回地址字符串
     * @param $params('province_id'=>, 'city_id' => , 'district_id'=>, 'street_id'=>)
     */
    public static function makeAddress($params) {
        $provinceNmae = $cityName = '';
        if (!empty($params['province_id'])) {
            $provinceNmae = self::getProvinceNameById(array('id' => $params['province_id']));
        }
        if (!empty($params['city_id'])) {
            $cityName .= self::getCityNameById(array('id' => $params['city_id']));
        }
        if($provinceNmae == $cityName) {
            $address = $cityName;
        } else {
            $address = $provinceNmae . $cityName;
        }

        if (!empty($params['district_id'])) {
            $address .= self::getDistrictNameById(array('id' => $params['district_id']));
            $address .= '区';
        }
        if (!empty($params['street_id'])) {
            $address .= self::getStreetNameById(array('id' => $params['street_id']));
        }
        return $address;
    }

    /**
     * @brief 根据城市名称获取城市信息
     */
    public static function getCityByName($params) {
        $cityName = Util::getFromArray('city_name', $params, '');
        if(empty($cityName)) {
            throw new Exception('参数city_name不能为空');
        }
        $filter[] = ['short_name', '=', $cityName];
        $field = Util::getFromArray('field', $params,'*');
        $cityModel = new CityModel();
        $info = $cityModel->getRow($field, $filter);

        //如果查不到,去full_name查询
        if(empty($info)) {
            $filter[] = ['name', '=', $cityName];
            $info = $cityModel->getRow($field, $filter);
        }
        return $info;
    }
}