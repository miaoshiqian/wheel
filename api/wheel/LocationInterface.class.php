<?php
/**
 * @brief 赶集网的地标
 */
class LocationInterface {
    /**
     * @brief 获取省份列表
     *
     * @param field     查询字段
     * @param order_by  排序字段
     * @param limit
     * @param offset
     *
     * @return array
     */
    public static function getProvince($params = []) {
        return LocationImpl::getProvince($params);
    }

    /**
     * @brief 根据省份ID获取城市列表
     *
     * @param province_id  省份ID
     * @param field        查询字段
     * @param order_by     排序字段
     * @param limit
     * @param offset
     *
     * @return array
     */
    public static function getCityByProvinceId($params) {
        return LocationImpl::getCityByProvinceId($params);
    }

    /**
     * @brief 根据城市ID获取区域列表
     *
     * @param city_id   城市ID
     * @param field     查询字段
     * @param order_by  排序字段
     * @param limit
     * @param offset
     *
     * @return array
     */
    public static function getDistrictByCityId($params) {
        return LocationImpl::getDistrictByCityId($params);
    }

    /**
     * @brief 根据区域ID获取街道列表
     *
     * $param district_id   区域ID
     * @param field         查询字段
     * @param order_by      排序字段
     * @param limit
     * @param offset
     *
     * @return array
     */
    public static function getStreetByDistrictId($params) {
        return LocationImpl::getStreetByDistrictId($params);
    }

    /**
     * @brief 根据省份ID获取省份名称
     *
     * @param id        省份ID
     *
     * @return string   省份名称
     */
    public static function getProvinceNameById($params) {
        return LocationImpl::getProvinceNameById($params);
    }

    /**
     * @brief 根据城市ID获取城市名称
     *
     * @param id        城市ID
     *
     * @return string   城市名称
     */
    public static function getCityNameById($params) {
        return LocationImpl::getCityNameById($params);
    }

    /**
     * @brief 根据城市ID获取城市
     *
     * @param id        城市ID
     *
     * @return string   城市名称
     */
    public static function getCityById($params) {
        return LocationImpl::getCityById($params);
    }

    /**
     * @brief 根据区域ID获取区域名称
     *
     * @param id        区域ID
     *
     * @return string   区域名称
     */
    public static function getDistrictNameById($params) {
        return LocationImpl::getDistrictNameById($params);
    }

    /**
     * @brief 根据街道ID获取街道名称
     *
     * @param id        街道ID
     *
     * @return string   街道名称
     */
    public static function getStreetNameById($params) {
        return LocationImpl::getStreetNameById($params);
    }

    /**
     * @brief 根据街道ID获取街道名称
     *
     * @param id        街道ID
     *
     * @return array   街道信息
     */
    public static function getStreetById($params) {
        return LocationImpl::getStreetById($params);
    }


    /**
     * @breif 根据各个id、返回地址字符串
     * @param $params('province_id'=>, 'city_id' => , 'district_id'=>, 'street_id'=>)
     */
    public static function makeAddress($params) {
        return LocationImpl::makeAddress($params);
    }

    /**
     * @brief 根据城市名称获取城市信息
     */
    public static function getCityByName($params) {
        return LocationImpl::getCityByName($params);
    }
}