<?php

class CarInterface{

    /**
     * @brief 添加车源接口
     */
    public static function add($params) {
        $info = array(
            'brand_id'      => Util::getFromArray('brand_id', $params, 0),
            'brand_name'    => Util::getFromArray('brand_name', $params, ''),
            'class_id'      => Util::getFromArray('class_id', $params, 0),
            'class_name'    => Util::getFromArray('class_name', $params, ''),
            'model_id'      => Util::getFromArray('model_id', $params, 0),
            'model_name'    => Util::getFromArray('model_name', $params, ''),
            'vehicle_year'  => Util::getFromArray('vehicle_year', $params, ''),
            'miles'         => Util::getFromArray('mile', $params, 0),
            'plate_number'  => Util::getFromArray('plate_number', $params, 0),
            'plate_time'    => Util::getFromArray('plate_time', $params, 0),
            'plate_city'    => Util::getFromArray('plate_city', $params, 0),
            'province_id'   => Util::getFromArray('province_id', $params, 0),
            'city_id'       => Util::getFromArray('city_id', $params, 0),
            'seller_phone'  => Util::getFromArray('seller_phone', $params, ''),
            'seller_name'   => Util::getFromArray('seller_name', $params, ''),
            'create_user_id'=> Util::getFromArray('create_user_id', $params, 0),
            'create_user_name' => Util::getFromArray('create_user_name', $params, ''),
            'seller_price'  => Util::getFromArray('seller_price', $params, 0),
            'year_check_time' => Util::getFromArray('year_check_time', $params, 0),
            'insurance_time'=> Util::getFromArray('insurance_time', $params, 0), //交强险到期时间
            'color'         => Util::getFromArray('color', $params, 0), //颜色
            'address'       => Util::getFromArray('address', $params, ''), //看车地址
            'emission'      => Util::getFromArray('emission', $params, ''), //排量 字符串. 2.0T
            'geerbox'       => Util::getFromArray('geerbox', $params, 0),
            'structure'     => Util::getFromArray('structure', $params, 0),
            'vin_code'      => Util::getFromArray('vin_code', $params, ''),
            'status'        => EnumCar::STATUS_ONLINE,
            'create_time'   => time(),
        );
        //title,
        $carId = CarModel::getInstance()->insert($info);
        if ($carId === false) {
            throw new Exception('添加车源信息失败');
        }
        return $carId;
    }

    /**
     * @brief 获取列表
     * @param $params['filter'] 查询条件
     * @param limit/offset/order/field   记录数、开始查找位置、排序、获取的字段
     */
    public static function getList($params) {
        $field  = Util::getFromArray('field', $params, '*');
        $filter = Util::getFromArray('filters', $params, array());
        $limit  = Util::getFromArray('limit', $params, 0);
        $offset = Util::getFromArray('offset', $params, 0);
        $order  = Util::getFromArray('order', $params, '');
        return CarModel::getInstance()->getAll($field, $filter, $order, $limit, $offset);
    }

    /**
     * @brief 通过用户id获取信息
     * @param $params['id'] //用户id
     */
    public static function getById($params) {
        Util::checkParamsExist(array('id'), $params);
        return CarLeadModel::getInstance()->getRow('*', array(array('id', '=', $params['id'])));
    }

    /**
     * @brief 获取符合filter条件的记录数
     * @param $params['filters'] 查询条件
     */
    public static function getCount($params) {
        Util::checkParamsExist(array('filters'), $params);
        $filter = Util::getFromArray('filters', $params, array());
        return CarLeadModel::getInstance()->getCount($filter);
    }

    /**
     * @brief 标记为已处理
     * @param $params['id']
     */
    public static function deal($params) {
        Util::checkParamsExist(array('id'), $params);
        $ret = CarLeadModel::getInstance()->updateById($params['id'], array('status' => EnumLead::STATUS_DONE));
        if ($ret === false) {
            throw new Exception('更新失败');
        }
    }
}