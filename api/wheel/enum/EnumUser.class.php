<?php
/**
 * @author 缪石乾<miaoshiqian@haoche51.com>
 * @brief 简介：User接口的相关枚举常量配置
 * @date 15/12/27
 * @time 下午4:00
 */

class EnumUser {
    /**
     * @brief 用户状态
     */
    const USER_STATUS_NORMAL = 1; //正常
    const USER_STATUS_DEL    = -1; //删除
    const USER_STATUS_FROZEN = -2; //冻结
    public static $USER_STATUS = array(
        self::USER_STATUS_NORMAL => '正常',
        self::USER_STATUS_DEL    => '已删除',
        self::USER_STATUS_FROZEN => '已冻结',
    );

    //性别
    const SEX_BOY = 1;
    const SEX_GIRL = 2;
    public static $SEX_TEXT = array(
        self::SEX_BOY  => '男',
        self::SEX_GIRL => '女',
    );
}