<?php

class EnumCar {
    /**
     * @brief 车源状态
     */
    const STATUS_ONLINE = 1; //在线
    const STATUS_SOLD   = 5; //售出
    const STATUS_OFFLINE = -1; //下线
    public static $STATUS_TEXT = array(
        self::STATUS_ONLINE    => '在线',
        self::STATUS_SOLD      => '已售',
        self::STATUS_OFFLINE   => '下线',
    );

    /**
     * @brief 车源图片状态枚举配置
     */
    const IMG_STATUS_NORMAL = 1; //正常显示
    const IMG_STATUS_DEL    = -1; //删除

    //车辆颜色配置
    const Black                      = 1; //黑色
    const White                      = 2; //白色
    const Silver                     = 3; //银灰色
    const Darkgrey                   = 4; //深灰色
    const Red                        = 5; //红色
    const Blue                       = 6; //蓝色
    const Yellow                     = 7; //黄色
    const Orange                     = 8; //橙色
    const Golden                     = 9; //金色/棕色
    const Green                      = 10; //绿色
    const Other                      = 11; //其他
    public static $COLOR_TEXT = array(
        self::Black                      =>'黑色',
        self::White                      =>'白色',
        self::Silver                     =>'银灰色',
        self::Darkgrey                   =>'深灰色',
        self::Red                        =>'红色',
        self::Blue                       =>'蓝色',
        self::Yellow                     =>'黄色',
        self::Orange                     =>'橙色',
        self::Golden                     =>'金色/棕色',
        self::Green                      =>'绿色',
        self::Other                      =>'其它',
    );

    //排放标准配置
    const Emission2 = 0;//国二
    const Emission3 = 1;//国三
    const Emission4 = 2;//国四
    const Emission5 = 3;//国五
    public static $EMISSION_TEXT = array(
        self::Emission2 => "国二",
        self::Emission3 => '国三',
        self::Emission4 => '国四',
        self::Emission5 => '国五',
    );

    const Unknown = 0;
    const Manual = 1;
    const Auto = 2;
    const DSG = 3; //Direct Shift Gearbox
    const AutoManual = 4;
    const CVT = 5; //Continuously Variable Transmission
    public static $GEARBOX_TEXT = array(
        self::Unknown => '未知',
        self::Manual => '手动',
        self::Auto => '自动',
        self::DSG => '双离合',
        self::AutoManual => '手自一体',
        self::CVT => '无级变速',
    );

    //车身结构配置
    const Hatchback = 1;//两厢
    const Sedan = 2;//三厢
    const SUV = 3;//SUV
    const MPV = 4;//MPV
    const Wagon = 5;//旅行车
    const Sports = 6;//跑车
    const PickUp = 7;//皮卡
    const Minivan = 8;//面包车
    public static $STRUCTURE_TEXT = array(
        self::Hatchback => '两厢',
        self::Sedan => '三厢',
        self::SUV => 'SUV',
        self::MPV => 'MPV',
        self::Wagon => '旅行车',
        self::Sports => '跑车',
        self::PickUp => '皮卡',
        self::Minivan => '面包车',
    );

    /**
     * @brief 亮点配置
     */
    public static $HIGH_LIGHT = array(
        '1' => 'Inch Chrome Alloy Wheels, Premium Wheels',
        '2' => 'Inch Painted Alloy Wheels, Premium Wheels',
        '3' => 'Active Cruise Control, Adaptive Cruise Control',
        '4' => 'AWD/4WD',
        '5' => 'Bluetooth',
        '6' => 'Bang & Olufsen Speakers, Premium Sound, Surround Sound Audio',
        '7' => 'Bose Speakers, Premium Sound, Surround Sound Audio',
        '8' => 'Bose Speakers, Premium Sound, Subwoofer, Surround Sound Audio',
        '9' => 'Harman/Kardon Audio, Harmon/Kardon Speakers, Premium Sound',
        '10' => 'Heated Seats, Rear Heated Seats',
        '11' => 'Keyless Entry',
        '12' => 'Leather, Premium Leather, Simulated Suede Seating',
        '13' => 'Navigation, Voice Activated Navigation System',
        '14' => 'Paddle Shifters, Steering Wheel Transmission Controls',
        '15' => 'Front and Rear Parking Sensors, Parking Aid, Rear View Camera',
        '16' => 'Premium Lights, Xenon High Intensity Discharge Headlights',
        '17' => 'Premium Wheels',
        '18' => 'Steering Wheel Controls',
        '19' => 'Sunroof',
        '20' => 'Extended Sunroof, Fixed Glass Third Row Sunroof, Power Glass Rear Sunroof, Power Glass Sunroof, Sunroof',
        '21' => 'Twin Turbocharger',
        '22' => 'Super Charger',
        '23' => 'Turbo, Tubocharger',
        '24' => 'USB',
        '25' => 'Ventilated Front Seats',
        '26' => 'In-Car Wireless Internet',
        '27' => 'Cooled and Heated Cupholders',
        '28' => 'Blind Spot Detection',
        '29' => 'Capacitive-Touch Controls',
        '30' => 'Ambient Lighting',
        '31' => 'Rear Entertainment Systems',
    );

    //==================新配置===========
    //body style 配置
    public static $BODY_STYLE = array(
        '1' => 'Sedan',
        '2' => 'Luxury Car',
        '3' => 'SUV',
        '4' => 'Crossover',
        '5' => 'Wagon/ Hatchback',
        '6' => 'Coupe',
        '7' => 'Green Car/ Hybrid',
        '8' => 'Mini-van',
        '9' => 'Convertible',
        '10' => 'Pickup Truck',
        '11' => 'Sports Car',
        '12' => 'Others',
    );

    //Transmission 配置
    public static $TRANSMISSION = array(
        '1' => 'Automatic',
        '2' => 'Manual',
        '3' => 'Other',
    );

    //title status 配置
    public static $TITLE_STATUS = array(
        '1' => 'Clean',
        '2' => 'Salvage',
        '3' => 'Rebuilt',
    );
}