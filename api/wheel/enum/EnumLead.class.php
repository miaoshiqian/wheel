<?php

class EnumLead {
    /**
     * @brief 用户状态
     */
    const STATUS_NONE = 0; //无意义
    const STATUS_WAIT = 1; //待处理
    const STATUS_DONE = 5; //已处理
    public static $STATUS_TEXT = array(
        self::STATUS_WAIT    => '待处理',
        self::STATUS_DONE => '已处理',
    );
}