<?php

class EnumCarImage {
    /**
     * @brief 车源图片状态
     */
    const STATUS_NORMAL = 1; //正常
    const STATUS_DELETE = -1; //删除
}