<?php

class BrandInterface {

    /**deprecated
     *  @brief  获取车辆品牌列表
     *  @param string $first_char 品牌首字母
     *  @param int $show_status 显示状态，-1查询所有
     *  @return array
     */
    public static function getBrandList($first_char = '', $show_status=1) {
        $filters = array();
        if ($first_char != '') {
            $filters[] = array('first_char', '=', $first_char);
        }
        if ($show_status != -1) {
            $filters[] = array('show_status', '=', $show_status);
        }
        $ret = AutoBrandModel::getInstance()->getAll('*', $filters, 'first_char asc');
        return $ret;
    }

    /**
     * 获取品牌列表,转成json
     */
    public static function getBrandListToJson($show_status=1) {
        $filters = array();
        if ($show_status != -1) {
            $filters[] = array('show_status', '=', $show_status);
        }
        $ret = AutoBrandModel::getInstance()->getAll('id, name, first_char', $filters);
        $info = array();
        foreach ((array)$ret as $brand_info) {
            $info[$brand_info['first_char']][] = array(
                'n' => $brand_info['name'],
                'v' => $brand_info['id'],
            );
        }
        $ret = @json_encode($info);
        return $ret;
    }

    /** deprecated
     *  @brief  获取车系列表
     *  @param string $brand_id 品牌id
     *  @return array
     */
    public static function getClassList($brand_id, $fields='*') {
        $filters = array();
        $filters[] = is_array($brand_id) ? array('brand_id', 'in', $brand_id) : array('brand_id', '=', $brand_id);
        $ret = AutoSeriesModel::getInstance()->getAll($fields, $filters);
        return $ret;
    }

    /**
     * ID获取车系
     */
    public static function getClassById($class_id){
        return AutoSeriesModel::getInstance()->getRow(array(array('id', '=', $class_id)));
    }

    public static function getClassListToJson($brand_id) {
        $ret = AutoSeriesModel::getInstance()->getAll('id, name, brand_name', array(array('brand_id', '=', $brand_id)));

        $info = array();
        foreach ($ret as $class_info) {
            $info[$class_info['brand_name']][] = array(
                'n' => $class_info['name'],
                'v' => $class_info['id'],
            );
        }

        $ret = @json_encode($info);
        return $ret;
    }

    /**deprecated
     *  @brief  获取具体的车型列表
     *  @param string $class_id 车系id
     *  @param string $year 年份
     *  @param string $emisions 排量
     *  @return array
     */
    public static function getVehicleList($class_id, $year = -1, $emissions = '') {
        $class_id = intval($class_id);
        $year = intval($year);
        $filter = array(array('series_id', '=', $class_id));
        if (intval($year) > 0) $filter[] = array('year', '=', $year);
        if ($emissions != '') $filter[] = array('emissions', '=', $emissions);
        $field = 'id, model_short_name as name,model_name as fullname, year'; //用short name 替换 name
        $ret =AutoModelModel::getInstance()->getAll($field, $filter,'year desc');
        return $ret;
    }

    public static function getVehicleListToJson($class_id) {
        $ret = BizDataCache::getInstance()->getVehicleListToJson($class_id);
        if ($ret === false) {
            $where = array(
                'series_id' => $class_id,
            );
            $field = 'id, model_name, year'; //用short name 替换 name
            $orderby = 'year desc';
            $ret = Dao_AutoModel::getInstance()->getList($field, $where, $orderby);
            $info = array();
            foreach ($ret as $vehicle_info) {
                $info[$vehicle_info['year']][] = array(
                    'n' => $vehicle_info['model_name'],
                    'v' => $vehicle_info['id'],
                );
            }

            $ret = @json_encode($info);
            BizDataCache::getInstance()->setVehicleListJson($ret, $class_id);
        }
        return $ret;
    }

    public static function getVehicleYearList($class_id, $emissions = '') {
        $where = array(
            array('series_id', '=', $class_id),
        );
        if ($emissions != '') $where[] = array('emissions', '=', $emissions);
        $field = 'distinct year,series_id';
        $ret = AutoModelModel::getInstance()->getAll($field, $where, 'year desc');
        return $ret;
    }

    public static function getVehicleEmissionsList($class_id, $year = 0) {
        $where = array(
            'series_id' => $class_id,
        );
        if ($year != 0) $where['year'] = $year;
        $field = 'distinct emissions_l as emissions';
        $ret = Dao_AutoModel::getInstance()->getList($field, $where);
    }

    /**
     *@brief 获得车辆的信息信息
     */
    public static function getVehicleMessage($id, $field) {
        $result = Dao_AutoModel::getInstance()->getRow(array('id'=>$id), $field);
        return $result;
    }

    /**
     *  @brief  获取城市列表
     *  @return array
     */
    public static function getCityList() {
        //get the cache
        $ret = BizDataCache::getInstance()->getCityList();
        if ($ret === false) {
            $field = 'city_id, city_name';
            $ret = Dao_City::getInstance()->getList($field);
            //set the cache
            BizDataCache::getInstance()->setCityList($ret);
        }
        return $ret;

    }

    /**
     *  @brief  获取区域列表
     *  @param int $cityid 城市id
     *  @return array
     */
    public static function getDistrictList($cityid) {
        //get the cache
        $ret = BizDataCache::getInstance()->getDistrictList($cityid);
        if ($ret === false) {
            $where = array('city_id' => $cityid);
            $field = 'district_id,district_name';
            $ret = Dao_District::getInstance()->getList($field, $where);
            //set the cache
            BizDataCache::getInstance()->setDistrictList($ret, $cityid);
        }
        return $ret;
    }

    /**
     *  @brief  获取街道列表
     *  @param int $district_id 区域id
     *  @return array
     */
    public static function getStreetList($district_id) {
        $ret = BizDataCache::getInstance()->getStreetList($district_id);
        if ($ret === false) {
            $where = array('district_id' => $district_id);
            $field = 'street_id, street_name';
            $ret = Dao_Street::getInstance()->getList($field, $where,'pinyin');
            //set the cache
            BizDataCache::getInstance()->setStreetList($ret, $district_id);
        }
        return $ret;
    }

    /**
     * 通过街道ID获得区域信息
     * @param  int  $street_id 街道ID
     * @return array | false
     */
    public static function getDistrictInfoByStreetId($street_id){
        $street_info = Dao_Street::getInstance()->getRow(array("street_id"=>$street_id));//街道信息
        if(!empty($street_info)){
            $district_info = Dao_District::getInstance()->getRow(array('district_id' => $street_info['district_id']));
            return $district_info;
        }else{
            return false;
        }

    }

    /**
     * 通过区域ID获得城市信息
     * @param  int  $district_id 区域ID
     * @return array | false
     */
    public static function getCityInfoByDistrictId($district_id){
        $district_info = Dao_District::getInstance()->getRow(array('district_id' => $district_id));
        if(!empty($district_info)){
            $city_info = Dao_City::getInstance()->getRow(array('city_id'=>$district_info['city_id']));
            return $city_info;
        }else{
            return false;
        }
    }

    //获取ip所在的城市的domain
    public static function getIpDomain($ip_str) {
        $ipv4address = sprintf("%u", ip2long($ip_str));
        $where = "start <= {$ipv4address} and end >= {$ipv4address}";
        return Dao_IpDomainList::getInstance()->getOne('domain', $where);
    }

    /*
    * @brief 获取车款的完整名称
    * @param brand_name 品牌
    * @param class_name 车系
    * @param short_model_name 车型（不含年份）
    * @param year 年份
    */
    public static function getCompleteVehicleName($brand_name, $class_name, $short_model_name, $year, $flag = '款') {

        $replace_array = array(' ', '.', '-','(',')','·');

        $class_name = str_replace($replace_array, '', $class_name);
        $brand_name = str_replace($replace_array, '', $brand_name);

        if ($class_name && strpos($class_name, $brand_name) !== false) {
            $complete_class_name = $class_name;
        } else {
            $complete_class_name = $brand_name . ' ' . $class_name;
        }

        $vehicle_name = "{$complete_class_name} {$year}{$flag} {$short_model_name}";

        return $vehicle_name;
    }

    /*
    * @brief 获取车款的完整名称
    * @param brand_name 品牌
    * @param class_name 车系
    * @param short_model_name 车型（不含年份）
    * @param year 年份
    */
    public static function makeVehicleTitle($year, $make_name, $model_name, $trim_name) {

        return $year . ' ' . $make_name . ' ' . $model_name . ' ' . $trim_name;
    }


    public static function decorate(&$list, $need=array()) {
        if(!is_array($list) || empty($list)) {
            return;
        }
        if(in_array('brand_id', $need)) {
            $brand_list = self::getBrandList();
            foreach($brand_list as $v) {
                $brand[$v['id']] = $v;
            }
            if(in_array('series_id', $need)) {
                $brand_ids = array();
                foreach($list as $item) {
                    if(isset($item['brand_id'])) {
                        $brand_ids[] = $item['brand_id'];
                    }
                }
                $series_list = self::getClassList($brand_ids);
                foreach($series_list as $v) {
                    $series[$v['id']] = $v;
                }
                if(in_array('model_id', $need)) {
                    $series_ids = array();
                    foreach($list as $item) {
                        if(isset($item['series_id'])) {
                            $series_ids[] = $item['series_id'];
                        }
                    }
                    $model_list = self::getVehicleList($series_ids);
                    foreach($model_list as $v) {
                        $model[$v['id']] = $v;
                    }
                }
            }
        }
        foreach($list as $k => $v) {
            if(isset($v['brand_id']) && isset($brand[$v['brand_id']])) {
                $v['brand_name'] = $brand[$v['brand_id']]['name'];
            }
            if(isset($v['series_id']) && isset($series[$v['series_id']])) {
                $v['series_name'] = $series[$v['series_id']]['name'];
            }
            if(isset($v['model_id']) && isset($model[$v['model_id']])) {
                $v['model_name'] = $model[$v['model_id']]['name'];
            }
            $list[$k] = $v;
        }
    }

    /**
     * @用品牌id去获取品牌名称
     */
    public static function getBrandName($brand_id) {
        return $brand_id ? Dao_AutoBrand::getInstance()->getOne('name', array('id' => $brand_id)) : '';
    }

    /**
     * @用车系id去获取车系名称
     */
    public static function getClassName($class_id) {
        return $class_id ? Dao_AutoSeries::getInstance()->getOne('name', array('id' => $class_id)) : '';
    }
}