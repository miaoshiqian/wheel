<?php

/**
 *  发送短信
 */

class SmsImpl {

    /**发送短信
     * @param $params['conf']模板配置
     * @param 。。。其他模板变量
     * @param $params['mobile']接收人手机号
     * @return bool|string
     * @throws Exception
     */
    public static function send($params) {
        $SMSparams['conf']      = Util::getFromArray('conf', $params);
        $needParams             = Util::getFromArray('need_params', $SMSparams['conf'], []);
        $SMSparams['tpl_value'] = '';
        if (empty($params['mobile'])) {
            throw new Exception('MessageImpl.send 参数异常');
        }
        $SMSparams['mobile'] = $params['mobile'];
        $SMSparams['conf'] = $params['conf'];
        if (!isset($params['company']) || empty($params['company'])) {
            $params['company'] = 'wheelsrnr';
        }
        foreach ($needParams as $key => $value) {
            $need = Util::getFromArray($key, $params, 0);
            if (empty($need)) {
                throw new Exception('MessageImpl.send 参数异常,检查config中的参数和传递的参数是否匹配');
            }
            $SMSparams['tpl_value'] .= "#$key#=$need&";
        }
        include_once FRAMEWORK_PATH . '/util/msg/SDK/YunPianService.class.php';
        $handle = new YunPianServcie();
        $ret = $handle->sendMessage($SMSparams);
        return $ret;
    }
}