<?php

include_once __DIR__ . '/impl/SmsImpl.class.php';

class SmsInterface {

    public static function send($params) {
        return SmsImpl::send($params);
    }
}