$(function(){			
	$(document).bind("click",function(e){ 
			var target = $(e.target);  //判断点击的目标对象
			
			//点击目标 标题栏(.select dt)时,收回全部选项表,展开当前选项表
			if(target.closest(".select dt").length != 0){  
				//$(".option").hide();
				//target.next('div').show();				
				if(target.next('div').is(":hidden")){
					$(".option").hide();
					target.next('div').show();
				}else{
					$(".option").hide();
					target.next('div').hide();
				}
			}
			//点击目标 选项(.select dd)时,改变选值到标题栏(dt),存储选值到title,收回选项表
			if(target.closest(".option dd").length != 0){
				target.parent('div').prev('dt').text(target.text());			
				if(target.attr('value')){
				target.parent('div').prev('dt').attr('title',target.attr('value'));
				}else{
				target.parent('div').prev('dt').attr('title',target.text());
				}
				target.parent('div').hide();
			}
			//点击空白处时收回选项
			if(target.closest(".option").length == 0&&target.closest(".select dt").length == 0){ 
				$(".option").hide(); 
			}
		}) 	
})