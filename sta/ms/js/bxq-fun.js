var bxq = {
    over: function () {
        $(".bxq-ci-list li").click(function () {
            var index = $(".bxq-ci-list li").index(this);
            $(this).addClass("active").siblings("li").removeClass("active");
            var top=$("#bxq-cim-"+index).offset().top-42;
            $("html,body").scrollTop(top);
        })

        $(".bxq-tsr-mn li").click(function () {
            var index = $(".bxq-tsr-mn li").index(this);
            $(this).addClass("active").siblings("li").removeClass("active");
            $(".bxq-tsr-mbox").eq(index).show().siblings(".bxq-tsr-mbox").hide();
        })
        if($(".bxq-ci-mn").length){
            var cur_top = $(".bxq-ci-mn").offset().top; //元素相对当前视口相对位置
            $(window).scroll(function () {
                var scrollTop = $(this).scrollTop(); //滚动条滚动距离
                var scrollHeight = $(document).height(); //内容可视区域高度+滚动距离
                var windowHeight = $(this).height(); //内容可视区域高度　

                var fun_box=$(".bxq-ci-list li");

                if (scrollTop > cur_top) {
                    $(".bxq-ci-mn").addClass('fixed');
                } else {
                    $(".bxq-ci-mn").removeClass('fixed');

                    fun_box.eq(0).addClass('active').siblings('li').removeClass('active');

                }
                for (var i = 0; i < fun_box.length; i++) {

                    var live_cinfo = ($("#bxq-cim-"+i).offset().top) - 42;
                    if (scrollTop >= live_cinfo) {
                        fun_box.eq(i).addClass('active').siblings('li').removeClass('active');
                    }

                }

                if (scrollTop + windowHeight == scrollHeight) { //到达底部
                    fun_box.eq(fun_box.length - 1).addClass('cur').siblings('li').removeClass('cur');
                }
                if(scrollTop>($(".bxq-like").offset().top) - 42){				
                    $(".bxq-ci-mn").removeClass('fixed');
                };

            });

        }

    },
}
$(function () {
    bxq.over();
})