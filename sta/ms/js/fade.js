var Fade = {
	fade:function(itm){
		var banner = $(itm),
            // banner
            pics_s = banner.find(".slideTop-box"),
            pics   = pics_s.children(),
            curr_p = pics.eq(0).stop().fadeIn(300),
            // 下标
            idxs_s = banner.find(".itemT"),
            idxs   = idxs_s.children(),
            curr_i = idxs.eq(0).addClass("on"),
            
			// 按钮            
            left   = $(".prevT"),
            right  = $(".nextT"),
			
            // 记录
            len    = pics.length,
            idx    = 0,
            idx_m  = len - 1,
            prev_i = -1,
            // 自动切换
            tt     = 0,
            delay  = 2500;

        
            // 使下标点屏幕居中
            //idxs_s.css('margin-left', -(idxs_s.width()-parseInt(curr_i.css('margin-right')))/2);
            
            // 下标点 点击 控制 初始化
            idxs.mouseover(function() {
				var index=idxs.index(this);				
				fade(index);               
            });
			
            // 左右按钮 点击控制 初始化
            left.bind("click", function(e) {
                fade(idx===0? idx=idx_m: --idx);
            });
            right.bind("click", function(e) {
                fade(idx===idx_m? idx=0: ++idx);
            });
			
            // 自动切换
            auto();
       


        function auto() {
            tt = setTimeout(function() {				
                fade(idx===idx_m? idx=0: ++idx);
            }, delay);
        }

        // 切换函数
        function fade(i) {
            if (i === prev_i) return;
            clearTimeout(tt);
            //prev_i = i;
			idx=i;
            curr_p.stop(true, true).fadeOut(500);
            curr_p = pics.eq(i).stop(true, true).fadeIn(500);
            curr_i.removeClass("on");
            curr_i = idxs.eq(i).addClass("on");
			//b_text_li.eq(i).show().siblings('li').hide();
            auto();
        } 
	},
};
$(function(){
	var fade=Fade.fade("#top");	
})