var inp_hide = {
	init:function(itm){
		$(itm).keydown(function(){		
		var df_vl=this.defaultValue;
		var now_vl=$(this).val();
		if(now_vl==df_vl){
			$(this).val('');
			}		
		}).blur(function(){
			var df_vl=this.defaultValue;
			var now_vl=$(this).val();
			if(now_vl==''){				  
				$(this).val(df_vl);
				}
		})
	},
};
var buy_page={
	over:function(){
		$(".dr-fea-item .list a").click(function(){			
			$(this).parent().find('a').removeClass("active");
			$(this).addClass("active");
		})
	},
	handle:function(){
		$(".advanced-tip").click(function(){
			$(".buy-tHid").toggle();
			$(this).toggleClass("active");
		})
	}
}
var bxq={
    over:function(){
        $(".bxq-ci-list li").click(function(){
            var index=$(".bxq-ci-list li").index(this);
            $(this).addClass("active").siblings("li").removeClass("active");
        })
        $(".bxq-tsr-mn li").click(function(){
            $(this).addClass("active").siblings("li").removeClass("active");
        })
    },
}
$(function(){
	inp_hide.init(".sousuo .sel2");
	buy_page.over();
	buy_page.handle();
    bxq.over();
})