var input_hide = {
	init:function(itm){
		$(itm).keydown(function(){		
		var df_vl=this.defaultValue;
		var now_vl=$(this).val();
		if(now_vl==df_vl){
			$(this).val('');
			}		
		}).blur(function(){
			var df_vl=this.defaultValue;
			var now_vl=$(this).val();
			if(now_vl==''){				  
				$(this).val(df_vl);
				}
		})
	},
};
$(function(){
	input_hide.init("input[type='text']");	
})