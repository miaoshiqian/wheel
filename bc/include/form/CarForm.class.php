<?php
/**
 * @author 缪石乾<miaoshiqian@anxin365.com>
 * @brief 简介：添加车源表单类
 * @date 16/3/27
 * @time 下午3:40
 */
class CarForm {
    /**
     * @param array() 表单域的值
     */
    private $_fieldData = array();

    public function __construct($data = array()) {
        $this->_fieldData = $data;
    }

    private function _setField($form, $data) {
        $firstNameField = array(
            'name'         => 'first_name',
            'postValue'    => !empty($data['first_name']) ? $data['first_name'] : 'null',
            'tipTarget' => '#first_name_tip',
            'rules'        => array(
            ),
        );

        $lastNameField = array(
            'name'         => 'last_name',
            'postValue'    => !empty($data['last_name']) ? $data['last_name'] : 'null',
            'tipTarget'    => '#last_name_tip',
            'rules'        => array(
            ),
        );

        $addressField = array(
            'name'         => 'address',
            'postValue'    => !empty($data['address']) ? $data['address'] : 'null',
            'tipTarget'    => '#address_tip',
            'rules'        => array(
            ),
        );

        $moreAddressField = array(
            'name'         => 'more_address',
            'postValue'    => !empty($data['more_address']) ? $data['more_address'] : 'null',
            'tipTarget'    => '#more_address_tip',
            'rules'        => array(
            ),
        );

        $emailAddressField = array(
            'name'         => 'email_address',
            'postValue'    => !empty($data['email_address']) ? $data['email_address'] : 'null',
            'tipTarget'    => '#email_address_tip',
            'rules'        => array(
            ),
        );

        $zipCodeField = array(
            'name'         => 'zip_code',
            'postValue'    => !empty($data['zip_code']) ? $data['zip_code'] : 'null',
            'tipTarget'    => '#zip_code_tip',
            'rules'        => array(
            ),
        );

        //客户手机
        $phone1Field = array(
            'name'         => 'phone1',
            'postValue'    => !empty($data['phone1']) ? $data['phone1'] : 'null',
            'tipTarget'    => '#phone1_tip',
            'rules'        => array(
            ),
        );

        //客户备用手机
        $phone2Field = array(
            'name'         => 'phone2',
            'postValue'    => !empty($data['phone2']) ? $data['phone2'] : 'null',
            'tipTarget'    => '#phone2_tip',
            'rules'        => array(
            ),
        );

        //品牌
        $brandField = array(
            'name'      => 'make',
            'tipTarget' => '#make_tip',
            'postValue' => !empty($data['make_name']) ? $data['make_name'] : 'null',
            'next' => 'model_name',
            'rules'     => array(
            ),
        );

        //年份
        $yearField = array(
            'name'      => 'vehicle_year',
            'tipTarget' => '#make_tip',
            'postValue' => !empty($data['vehicle_year']) ? $data['vehicle_year'] : 'null',
            'next' => 'make',
            'rules'     => array(
            ),
        );

        //车型
        $modelField = array(
            'name'      => 'model_name',
            'tipTarget' => '#make_tip',
            'postValue' => !empty($data['model_name']) ? $data['model_name'] : 'null',
            'next' => 'model_trim',
            'rules'     => array(
            ),
        );

        //车款
        $trimField = array(
            'name'      => 'model_trim',
            'tipTarget' => '#make_tip',
            'postValue' => !empty($data['model_trim']) ? $data['model_trim'] : 'null',
            'rules'     => array(
            ),
        );

        //listing price
        $listPriceField = array(
            'name' => 'listing_price',
            'tipTarget' => '#listing_price_tip',
            'postValue' => !empty($data['listing_price']) ? $data['listing_price'] : 'null',
            'rules'     => array(
            ),
        );

        //Mileage
        $mileField = array(
            'name' => 'mile',
            'tipTarget' => '#mile_tip',
            'postValue' => !empty($data['mile']) ? $data['mile'] : 'null',
            'rules'     => array(
            ),
        );

        //Exterior color
        $exteriorColorField = array(
            'name' => 'exterior_color',
            'tipTarget' => '#exterior_color_tip',
            'postValue' => !empty($data['exterior_color']) ? $data['exterior_color'] : 'null',
            'rules'     => array(
            ),
        );

        //Interior Color
        $interiorColorField = array(
            'name' => 'interior_color',
            'tipTarget' => '#interior_color_tip',
            'postValue' => !empty($data['interior_color']) ? $data['interior_color'] : 'null',
            'rules'     => array(
            ),
        );

        //transmission
        $transmissionField = array(
            'name' => 'transmission',
            'tipTarget' => '#transmission_tip',
            'postValue' => !empty($data['transmission']) ? $data['transmission'] : 'null',
            'rules'     => array(
            ),
        );

        //title status
        $titleStatusField = array(
            'name' => 'title_status',
            'tipTarget' => '#title_status_tip',
            'postValue' => !empty($data['title_status']) ? $data['title_status'] : 'null',
            'rules'     => array(
            ),
        );

        //title status
        $bodyStyleField = array(
            'name' => 'body_style',
            'tipTarget' => '#body_style_tip',
            'postValue' => !empty($data['body_style']) ? $data['body_style'] : 'null',
            'rules'     => array(
            ),
        );

        //vin code
        $vinField = array(
            'name' => 'vin_code',
            'tipTarget' => '#vin_code_tip',
            'postValue' => !empty($data['vin_code']) ? $data['vin_code'] : 'null',
            'rules'     => array(
            ),
        );

        //省份
        $provinceField = array(
            'name'      => 'province_id',
            'tipTarget' => '#province_id_tip',
            'postValue' => !empty($data['province_id']) ? $data['province_id'] : 'null',
            'next' => 'city_id',
            'rules'     => array(
            ),
        );

        //城市
        $cityField = array(
            'name'      => 'city_id',
            'tipTarget' => '#province_id_tip',
            'postValue' => !empty($data['city_id']) ? $data['city_id'] : 'null',
            'rules'     => array(
            ),
        );


        //隐藏表单域
        $brandNameField = array(//年款名称
            'name'         => 'year_name',
            'postValue'    => !empty($data['vehicle_year']) ? $data['vehicle_year'] : 'null',
            'rules'        => array(
            ),
        );
        $classNameField = array(//车系名称
            'name'         => 'make_name',
            'postValue'    => !empty($data['make']) ? $data['make'] : 'null',
            'rules'        => array(
            ),
        );
        $modelNameField = array(//车型名称
            'name'         => 'model_name',
            'postValue'    => !empty($data['model_name']) ? $data['model_name'] : 'null',
            'rules'        => array(
            ),
        );
        $modelTrimField = array(//车型名称
            'name'         => 'model_trim',
            'postValue'    => !empty($data['model_trim']) ? $data['model_trim'] : 'null',
            'rules'        => array(
            ),
        );
        $modelTrimNameField = array(//车型名称
            'name'         => 'model_trim_name',
            'postValue'    => !empty($data['model_trim_name']) ? $data['model_trim_name'] : 'null',
            'rules'        => array(
            ),
        );
        $idField = array(//id
            'name'         => 'id',
            'postValue'    => !empty($data['id']) ? $data['id'] : 'null',
            'rules'        => array(
            ),
        );

        $form->addFields(array(
            $firstNameField,
            $lastNameField,
            $addressField,
            $moreAddressField,
            $emailAddressField,
            $zipCodeField,
            $trimField,
            $listPriceField,
            $mileField,
            $exteriorColorField,
            $interiorColorField,
            $transmissionField,
            $titleStatusField,
            $bodyStyleField,
            $provinceField,
            $cityField,
            $brandField,
            $yearField,
            $modelField,
            $vinField,
            $phone1Field,
            $phone2Field,
            $brandNameField,
            $classNameField,
            $modelNameField,
            $modelTrimField,
            $modelTrimNameField,
            $idField,
        ));
    }

    /**
     * @return 返回form对象
     */
    public function getForm() {
        $formObj = new Form();
        $this->_setField($formObj, $this->_fieldData);
        return $formObj;
    }
}