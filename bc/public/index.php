<?php
require_once dirname(__FILE__) . '/../conf/config.inc.php';
require_once FRAMEWORK_PATH.'/common/AutoLoader.class.php'; // 自动加载类
require_once FRAMEWORK_PATH . '/mvc/bc/bootstrap.php';

//设置该模块自动加载的目录,可以设置多个,如果多个目录中有同一个类的话，先设置哪个目录，就自动加载这个目录下面的类
AutoLoader::setAutoDir(FRAMEWORK_PATH . "/util/");//工具类
AutoLoader::setAutoDir(CONF_PATH);                //平台配置类
AutoLoader::setAutoDir(CONF_COMMON_PATH); // 项目总配置公共文件
AutoLoader::setAutoDir(PROJECT_PATH . "/conf/");  //配置类
AutoLoader::setAutoDir(PROJECT_PATH . "/common/");//项目公共类
AutoLoader::setAutoDir(PROJECT_PATH . "/include/");//项目公共类
AutoLoader::setAutoDir(API_PATH); // Api接口


//注册自定义的自动加载方法
spl_autoload_register (['AutoLoader', 'autoLoad']);

Dispatch::run(Router::getInfo());