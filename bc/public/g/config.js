(function () {
    //var now = parseInt(Date.now() / 1000, 10);
    //var defaultVersion = now - (now % 604800);
    G.config({
        baseUrl: '/build/',
        map: [
            [/^(.*\/\/)((.*)\.(js|css|tpl))$/, function (url, server, path, filename, ext) {
                return server + filename + '.' + ext;
            }]
        ]
    });
})();

(function () {
    var config = {
        alias: {
            //"jquery": "lib/jquery/jquery-1.8.2.js",
            "jquery": "lib/jquery/jquery-2.1.4.js",
            "underscore": "lib/underscore/underscore.js",
            "events": "lib/event/event.js",
            "widget": "lib/widget/widget.js",
            "uploader": "widget/imageUploader/image_uploader.cmb.js",
            "validator": "widget/form/form.cmb.js",
            "util": "core/util.js",
            //追加
            "rangSlider": "com/lte/widget/ionslider-2.12/js/ion.rangeSlider.js",
            "rangSliderH5": "com/lte/widget/ionslider-2.12/css/ion.rangeSlider.skinHTML5.css",
            "tableSort": "com/backend/js/widget/jquery.table_sort.js"
        }
    };

    if (window.location.href.indexOf('NG_ENABLE_LOCALSTORAGE') !== -1) {
        config.enableLocalstorage = true;
    }

    G.config(config);

    if (/\.wheel\.com$/.test(window.location.host)) {
        document.domain = 'wheel.com';
    }
})();