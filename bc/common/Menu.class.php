<?php
/**
 *  菜单处理
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 16:05
 */

class Menu{

    /**
     * 获取可显示的菜单
     * @author 李登科<lidengke@xiongying.com>
     *
     * @param string $token
     * @param int $userId
     * @param string $module
     * @param string $checkedUrl 要选中的结点链接
     *
     * @return mixed
     * @throws Exception
     */
    public static function getMenuData($token, $userId, $module = '', $checkedUrl = ''){
        $menuData = self::_getMenuData($module);
        $currKeys = array();
        self::setMenuItemShow($token, $userId, $module, $menuData, $menuData, array(), $checkedUrl, $currKeys);
        return array($menuData, $currKeys);
    }

    public static function getMenuInfoByKeys($keys, $module = ''){
        if(!$keys){
            return array();
        }
        $menuData = self::_getMenuData($module);
        foreach ($keys as $index => $key) {
            if($index === 0){
                $menuData = $menuData[$key];
            } else {
                $menuData = $menuData['child'][$key];
            }
        }
        return $menuData;
    }

    private static function _getMenuData($module = ''){
        static $menuData;
        if(!$menuData){
            if($module){
                $menuFile =  __DIR__.'/../modules/'.$module.'/conf/menu.inc.php';
            } else {
                $menuFile =   __DIR__.'/../conf/menu.inc.php';
            }

            if(!file_exists($menuFile)){
                throw new Exception('菜单配置文件不存在', ExceptionCodeConfig::ERR_FILE_NOT_EXIST);
            }
            $menuData = include $menuFile;
        }
        return $menuData;
    }

    private static function setMenuItemShow($token, $userId, $module, &$menuData, &$menuChild, $parentMenu = array(), $checkedUrl, &$currKeys){
        foreach ($menuChild as $key => &$menu) {
            $keys = (!empty($parentMenu)) ? $parentMenu['key'] : array();
            $keys[] = $key;
            $menu['key'] = $keys;
            $url = $menu['url'];
            if(empty($url)){
                $menu['show'] = false;
            } else {
                $check = Auth::checkAuth($token, $userId, $url, $module);
//				echo join('|', array($userId, $url, $module, $check))."\n";
                if(!$check){
                    unset($menuChild[$key]);
                } else {
                    $isOpen = $checkedUrl == $url;  // 是否展开
                    $isOpen && $menu['open'] = true; // 选中当前结点
                    $isOpen && $currKeys = $keys;
                    self::setParentShow($menuData, $keys, $isOpen);
                }
            }

            if(!empty($menu['child'])) {
                self::setMenuItemShow($token, $userId, $module, $menuData, $menu['child'], $menu, $checkedUrl, $currKeys);
            }
        }
    }

    private static function setParentShow(&$menuData, $keys, $open = false){
        $key = array_shift($keys);
        $menuData[$key]['show'] = true;
        if(empty($menuData[$key]['url']) && empty($menuData[$key]['open']) && $open){ // 打开当前节点
            $menuData[$key]['open'] = true;
        }
        if(count($keys) > 0 && !empty($menuData[$key]['child'])){
            self::setParentShow($menuData[$key]['child'], $keys, $open);
        }
    }

}