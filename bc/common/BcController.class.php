<?php

/**
 * Class BcController
 *
 * 后台公共控制器
 * @author 李登科<lidengke@xiongying.com>
 */
class BcController extends BaseController {
    protected $pageSize = 20;
    protected $maxSize = 6000;
    protected $currentPage = 1;

//	public function __construct() {
//		parent::__construct();
//	}

    public function init() {
        $this->view->assign(array(
            'globalUserName' => $this->getUserName(),
            'menuWidth' => 170, //菜单导航栏宽度
        ));

        $this->checkAuth();

        // 初始化后处理一些公共事情
        $this->createMenu();

        // 模块菜单
        $this->createModuleMenu();
    }

    public function checkAuth() {
        $userId = $this->getUserId();
        $checkAuth = Auth::checkAuth($this->getToken(), $userId, $this->getCurrentUrl(), $this->getCurrentModule());
        if ($checkAuth === true) {
            return true;
        } else if ($checkAuth === -1) {
            Response::redirect($this->createUrl('user', 'login', array(), 'sso', array(
                'backUrl' => Request::getCurrentUrl(),
            )));
        } else {
            $refer = Request::getRefer();
            $this->jumpToFail('权限不够!', $refer ? $refer : '/');
        }
    }

    public function turnTo($url) {
        $ret = Router::getInfo($url);
        Dispatch::run($ret);
    }

    public function jumpToFail($message, $url = '') {
        Response::redirect($this->createUrl('index', 'error', array(), '', array(
            'message' => $message,
            'code'    => 1,
            'url'     => $url,
        )));
    }

    public function jumpToSuccess($message, $url = '') {
        Response::redirect($this->createUrl('index', 'success', array(), '', array(
            'message' => $message,
            'code'    => 0,
            'url'     => $url,
        )));
    }

    public function outputJson($code = 0, $message = '', $extraData = array()) {
        $params = array(
            'code'    => $code,
            'message' => $message,
            'data'    => $extraData,
        );
        Response::outputJson($params);
        exit;
    }

    /**
     * 后台页面标题
     * @author 李登科<lidengke@xiongying.com>
     *
     * @param string $title
     */
    public function setPageTitle($title = '') {
        $this->view->assign('pageTitle', $title);
    }

    /**
     * 给模板赋值
     * @author 李登科<lidengke@xiongying.com>
     *
     * @param string|array $key
     * @param null         $value
     */
    public function assign($key, $value = null) {
        $this->view->assign($key, $value);
    }

    /**
     * 渲染模板
     * @author 李登科<lidengke@xiongying.com>
     *
     * @param string $tplFile 模板文件
     * @param array  $params
     * @param string  $mainTpl 框架主体模板文件
     */
    public function render($tplFile, $params = array(), $mainTpl = 'include/tpl_main.php') {
        if ($mainTpl) {
            $this->assign('globalRenderFile', $tplFile); // 渲染的模板文件
            $content = $this->view->fetch($mainTpl, $params);
        } else {
            $content = $this->view->fetch($tplFile, $params);
        }
        Response::output($content);
    }

    /**
     * 获取模板内容
     * @author 李登科<lidengke@xiongying.com>
     *
     * @param string $tplFile
     * @param array  $params
     *
     * @return string
     */
    public function fetch($tplFile, $params = array()) {
        return $this->view->fetch($tplFile, $params);
    }

    /**
     * 获取当前页面url
     * @author 龙卫国<longweiguo@xiongying.com>
     *
     * @param bool|false $includeParams 是否包含controller和action以外的参数
     *
     * @return string
     */
    public function getCurrentUrl($includeParams = false) {
        $controller = Router::getController();
        $action     = Router::getAction();
        if ($includeParams) {
            $params = Router::getParams();
            return Router::createUrl($controller, $action, $params);
        } else {
            return Router::createUrl($controller, $action);
        }
    }

    public function createUrl($controller = 'index', $action = 'default', $params = array(), $module = '', $get = array()) {
        return Router::createUrl($controller, $action, $params, $module, $get);
    }

    public function getUserId() {
        return $this->getUserInfo('id');
    }

    public function getToken() {
        return $this->getUserInfo('token');
    }

    public function getUserName() {
        return $this->getUserInfo('username');
    }

    public function getUserInfo($filed = null) {
        static $userInfo;
        if (!$userInfo) {
            $userInfo = Cookie::get('bcUserInfo');
            $userInfo && $userInfo = json_decode($userInfo, 1);
        }
        if($userInfo && $filed){
            return isset($userInfo[$filed]) ? $userInfo[$filed] : null;
        }
        return $userInfo;
    }

    public function isLogin() {
        return !!$this->getUserInfo();
    }


    public function getCurrentModule() {
        return Router::getModule();
    }


    public function createMenu() {
        $allMenu = Menu::getMenuData($this->getToken(), $this->getUserId(), $this->getCurrentModule(), $this->getCurrentUrl(true));
        $currentMenuInfo = Menu::getMenuInfoByKeys($allMenu[1], $this->getCurrentModule());
        $this->assign(array(
            'globalAllMenu' => $allMenu[0],
            'globalCurrentMenuInfo' => $currentMenuInfo,
        ));

        $this->setPageTitle(isset($currentMenuInfo['text']) ? $currentMenuInfo['text'] : '');
    }

    public function createModuleMenu() {
        $allMenu = ModuleMenu::getMenuData($this->getToken(), $this->getUserId(), $this->getCurrentModule());
        $this->assign(array(
            'globalModuleMenu' => $allMenu[0],
            'globalCurrentModuleInfo' => $allMenu[1],
        ));
    }

    public function errorAction($data = array('msg' => '对不起！操作失败，请稍后再试。', 'code' => 1, 'url' => '/')) {
        $this->setPageTitle('错误提示');
        $message = Util::getFromArray('msg', $data, '');
        $code    = Util::getFromArray('code', $data, 1);
        $jumpUrl = Util::getFromArray('url', $data, '/');
        if (Request::isAjax()) {
            Response::outputJson(array(
                'message' => $message,
                'code'    => $code
            ));
        } else if (in_array('?', $_GET)) {
            $key = array_search('?', $_GET);
            Response::outputJsonp($message, $key);
        } else {
            $this->assign(array());
            $this->render('error.php', array(
                'jumpUrl' => $jumpUrl,
                'message' => $message,
            ), 'include/tpl_simple.php');
        }
        exit;
    }

    public function successAction() {
        $this->setPageTitle('操作成功');
        $message = empty($_GET['message']) ? '操作成功' : $_GET['message'];
        $code    = 0;
        $jumpUrl = empty($_GET['url']) ? '/' : $_GET['url'];
        if (Request::isAjax()) {
            Response::outputJson(array(
                'message' => $message,
                'code'    => $code
            ));
        } else if (in_array('?', $_GET)) {
            $key = array_search('?', $_GET);
            Response::outputJsonp($message, $key);
        } else {
            $this->assign(array());
            $this->render('success.php', array(
                'jumpUrl' => $jumpUrl,
                'message' => $message,
            ), 'include/tpl_simple.php');
        }
        exit;
    }

    /**
     * @desc 拼接GET参数
     * @return string
     */
    public function getQueryString() {
        $url = parse_url($_SERVER['REQUEST_URI']);
        $queryString = isset($url['query']) ? $url['query'] : '';
        parse_str($queryString,$params);
        return http_build_query($params);
    }
}