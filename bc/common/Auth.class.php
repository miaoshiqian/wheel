<?php
/**
 *
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 15:21
 */

class Auth{

    /**
     * 获取用户是否有对应url的访问权限
     * @author 李登科<lidengke@xiongying.com>
     *
     * @param string $token
     * @param int $userId
     * @param string $url
     * @param string $module
     *
     * @return true 权限通过, -1 token 无效, false 权限不通过
     * @throws Exception
     */
    public static function checkAuth($token, $userId, $url, $module = ''){
        $groupArr = self::getGroupsByUrl($url, $module);
        if($groupArr) foreach ($groupArr as $group) {
            if($group === '*'){ // 未登录、登录都可以
                return true;
            }
            if (!$userId) { // 未登录
                return -1;
            }
            $ret = AuthInterface::checkAuth(array('token' => $token, 'user_id' => $userId, 'rule_group' => $group));
            if ($ret === true) {
                return true;
            } else {
                return $ret;
            }
        }
        return false;
    }

    /**
     * 获取url所对应的组集合
     * @author 李登科<lidengke@xiongying.com>
     *
     * @param string $url
     * @param string $module
     *
     * @return array|bool
     * @throws Exception
     */
    public static function getGroupsByUrl($url, $module = ''){
        static $groups = array();
        if (empty($module)) {
            $module = 'default';
        }
        if(empty($groups[$module])){
            if($module != 'default'){
                $ruleFile =  __DIR__.'/../modules/'.$module.'/conf/rule.inc.php';
            } else {
                $ruleFile =   __DIR__.'/../conf/rule.inc.php';
            }
            if(!file_exists($ruleFile)){
                throw new SysException('组权限配置文件'.$ruleFile.'不存在');
            }
            $groups[$module] = include $ruleFile;
        }
        $group = $groups[$module];
        if($group){
            $config = array();
            if ($url == '/') {
                $url = '/index/default/';
            }
            $url = '/' . trim($url, '/') . '/';
            if (isset($group[$url])) {
                $config = $group[$url];
            } else {
                $urls = explode('/', $url);
                $url = '/' . $urls[1] . '/' . $urls[2] . '/';
                if (isset($group[$url])) {
                    $config = $group[$url];
                }
            }
            if(!empty($config['admin_group'])){
                return Util::split2array($config['admin_group'], ',');
            } else {
                //如果没设置admin_group、默认需要admin权限才能返回
                $config['admin_group'] = 'admin';
                return Util::split2array($config['admin_group'], ',');
            }
        }
        return false;
    }
}