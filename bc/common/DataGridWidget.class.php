<?PHP
/**
 * @author 缪石乾<miaoshiqian@haoche51.com>
 * @brief 简介：异步生成表格数据组件封装
 * @date 15/12/15
 * @time 下午2:17
 */

require_once __DIR__ . '/Pager.class.php';

class DataGridWidget {
    protected $_col            = array();
    protected $_data           = array();
    protected $_pager          = array();
    protected $_mutilRowAction = array(); // 多行操作
    protected $_callback       = null;
    protected $_callbaclParams = array(0 => null, 1 => null);
    public $url = '';
    public $id  = 'datagrid-1';

    public function setUrl($url) {
        $this->url = $url;
    }

    public function setFormatCallback($callback, $params = array()) {
        $this->_callback = $callback;
        $i = 2;
        foreach ($params as $params) {
            $this->_callbaclParams[$i++] = $params;
        }
    }

    /**
     * @brief 设置表格头
     * @fieldx 仅仅是个名称、可以自定义。只需要和 ajaxGetData中的 数据key对上就ok了
     * @text 表头显示的名称内容
     * @style 表头中需要显示 style样式的时候
     * @class 表头中需要添加class的时候
     * @data 表头中需要存储 data-xxx数据的时候 例如: array('a' => 100) dom上面为：data-a="100"
     */
    public function setCol($field) {
        $this->_col = !empty($field) && is_array($field) ? $field : array();
    }

    public function setData($data) {
        $this->_data = $data ? $data : array();
    }

    /*
    array(
        "text" => "删除",
        "icon" => "icon-trash",
        "action" => "datagrid-multi-row-delete",
        "url"   => "/default/list/ajaxDelete"
    )*/
    public function addMultiRowAction($action) {
        $this->_mutilRowAction[] = $action;
    }

    public function setPager($count, $current, $size, $maxPage = null) {
        $this->_pager = Pager::getPager($count, $current, $size, $maxPage);
    }

    public function getData() {
        $ret = array(
            'cols'     => $this->_col,
            'rows'     => $this->_getFormatRowData(),
            'pager'    => $this->_pager,
            'url'      => $this->url,
            'multiRowAction' => $this->_mutilRowAction,
        );
        if (!count($this->_pager)) {
            $ret['pager'] = Null;
        }
        return $ret;
    }

    /**
     * @param string $params['id'] 生成的div的id名称. 默认 datagrid-1
     * @param string $params['class'] 要给该div添加的class名称
     * @return string
     */
    public function toHTML($params = array()) {
        if (!empty($params['id'])) {
            $this->id = $params['id'];
        }
        $class = !empty($params['class']) ? $params['class'] : '';

        $html = '
<div class="ui-datagrid '.$class.'" data-show-fields=\''.json_encode(array_keys($this->_col)).'\' data-widget="datagrid"'.(!empty($this->id) ? " id='".$this->id."'" : "").' data-url="'.$this->url.'">
    <table class="table table-bordered tablesorter table-striped">
        <thead style="background-color:rgba(60, 141, 188, 0.75);color:#fff;">
            <tr>';
        if (count($this->_mutilRowAction)) {
            $html .= '<th><input data-action-type="datagrid-checkall" type="checkbox"></th>';
        }
        foreach($this->_col as $name => $col) {
            if (empty($col)) {
                trigger_error('this col not found:'.$name);
            }
            $html .= '<th ';
            if (!empty($col['data']) && is_array($col['data'])) { //赋值th上面的data-xxx
                foreach ($col['data'] as $k_name => $k_val) {
                    $html .= 'data-' . $k_name . '="' . $k_val .'" ';
                }
            }
            if (isset($col['class'])) {
                $html .= 'class="'.$col['class'].'"';
            }
            $html .= ' data-field-name="'.$name.'" style="height:30px;line-height:30px;'.$col['style'].'"';
            /*if ($col['orderMode']) {
                $html .= ' data-action-type="datagrid-sort-'.$col['orderMode'].'"';
            }*/
            $html .= '>'.$col['text'];
            /*if ($col['orderMode']) {
                $html .= "<a href='#' class='pull-right' data-action-type='datagrid-sort-".$col['orderMode']."'><i class='icon-sort-".$col['orderMode']."'></i></a>";
            }*/
            $html .= '</th>';
        }
        $html .= '
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div class="btn-group pull-left">

    </div>
    <div class="pagination pull-right">

    </div>
</div>
';
        return $html;
    }

    protected function _getFormatRowData() {
        foreach ((array)$this->_data as $key => $value) {
            foreach ($this->_col as $name => $col) {
                $this->_callbaclParams[0] = $name;
                $this->_callbaclParams[1] = $value;
                //$this->_data[$key][$name] = call_user_func_array($this->_callback, $this->_callbaclParams);
            }
        }
        return $this->_data;
    }
}