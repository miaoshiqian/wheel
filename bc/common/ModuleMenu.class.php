<?php
/**
 * 简介: 模块菜单处理
 * User: 龙卫国<longweiguo@xiongying.com>
 * Date: 2015/11/20
 * Time: 16:05
 * Copyright (c) 2015, mingtian.com
 */

class ModuleMenu{

	/**
	 * @brief  简介: 获取可显示的菜单
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string $token
	 * @param int $userId
	 * @param string $module
	 * @param string $checkedUrl 要选中的结点链接
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public static function getMenuData($token, $userId, $module){
		$menuData = self::_getMenuData();
		$ret = array();
		$curr = array();
		foreach ($menuData as $key => $menu) {
			if(empty($menu['url'])){
				continue;
			}
			$check = Auth::checkAuth($token, $userId, $menu['url'], $menu['module']);
			if(!$check){
				continue;
			}
			if ($menu['module'] == $module) {
				$menu['active'] = 1;
				$curr = $menu;
			}
			$ret[] = $menu;
		}

		return array($ret, $curr);
	}

	private static function _getMenuData($module = ''){
		static $menuData;
		if(!$menuData){
			$menuFile =   __DIR__ . '/../conf/menu_module.inc.php';

			if(!file_exists($menuFile)){
				throw new Exception('模块菜单配置文件不存在', ExceptionCodeConfig::ERR_FILE_NOT_EXIST);
			}
			$menuData = include $menuFile;
		}
		return $menuData;
	}

}