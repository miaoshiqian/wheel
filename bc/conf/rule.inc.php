<?php
/**
 *  权限表
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 11:22
 */

/*
 *  url => array(),
 *  admin_group为*的所有人都可访问. 不设置 admin_group 默认需要admin权限才能反问
 *
 *  1. 查看
 *  2. 添加 修改 审核 禁用 删除
 */

return array(
    '/index/error/' => array(
        'text' => '公共错误跳转页',
        'admin_group' => '*',
    ),
    '/index/success/' => array(
        'text' => '公共成功跳转页',
        'admin_group' => '*',
    ),
    '/index/default/' => array(
        'text' => '首页',
    ),
    //客户管理
    '/customer/default/' => array(
        'text' => '客户列表',
    ),
    //车源管理
    '/car/add/' => array(
        'text' => '车源发布',
    ),
    '/lead/default/' => array(
        'text' => '卖车意向列表',
    ),
    '/model/default/' => array(
        'text' => '品牌车系管理',
    ),
);