<?php
/**
 * 简介: 模块配置
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 11:21
 * Copyright (c) 2015, mingtian.com
 */

return array(
	array(
		'module' => 'log',
		'url'    => '/',
		'text'   => '日志查看',
		'icon'   => 'fa-exclamation-circle',
	),
	array(
		'module' => 'doc',
		'url'    => '/api/default/',
		'text'   => 'Interface接口文档',
		'icon'   => 'fa-folder',
	),
	array(
		'module' => 'apidoc',
		'url'    => '/',
		'text'   => 'API接口文档',
		'icon'   => 'fa-folder-open',
	),
	array(
		'module' => 'sso',
		'url'    => '/',
		'text'   => '人员与权限管理',
		'icon'   => 'fa-lock',
	),
);