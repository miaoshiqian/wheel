<?php
/**
 *  主站菜单配置
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 11:21
 */

return array(
    array(
        'url'   => '', // 访问url
        'text'  => '客户管理',       // 显示
        'ico'  => 'fa fa-fw fa-users',     // 菜单图标
        'child' => array(
            array(
                'url'   => '/customer/default/', // 访问url
                'text'  => '客户列表',       // 显示
                'ico'  => '',     // 菜单图标
            ),
        ),
    ),
    array(
        'url'   => '', // 访问url
        'text'  => '车源管理',       // 显示
        'ico'  => 'fa fa-fw fa-th-large',     // 菜单图标
        'child' => array(
            array(
                'url'   => '/car/publish/', // 访问url
                'text'  => '发布车源',       // 显示
                'ico'  => '',     // 菜单图标
            ),
            array(
                'url'   => '/car/list/', // 访问url
                'text'  => '车源列表',       // 显示
                'ico'  => '',     // 菜单图标
            ),
            array(
                'url'   => '/car/lead/', // 访问url
                'text'  => '卖车意向列表',       // 显示
                'ico'  => '',     // 菜单图标
            ),
            array(
                'url'   => '/model/default/', // 访问url
                'text'  => '品牌车系管理',       // 显示
                'ico'  => '',     // 菜单图标
            ),
            array(
                'url'   => '/modelOut/default/', // 访问url
                'text'  => '国外车系管理',       // 显示
                'ico'  => '',     // 菜单图标
            ),
        ),
    ),
    array(
        'url'   => '', // 访问url
        'text'  => '订单管理',       // 显示
        'ico'  => 'fa fa-fw fa-exclamation-circle',     // 菜单图标
        'child' => array(
            array(
                'url'   => '/feedback/default/', // 访问url
                'text'  => '订单列表',       // 显示
                'ico'  => '',     // 菜单图标
            ),
        ),
    ),
    /*array(
        'url'   => '', // 访问url
        'text'  => '动态管理',       // 显示
        'ico'  => 'fa fa-fw fa-bookmark',     // 菜单图标
        'child' => array(
            array(
                'url'   => '/dynamic/default/', // 访问url
                'text'  => '动态列表',       // 显示
                'ico'  => '',     // 菜单图标
            ),
        ),
    ),
    array(
        'url'   => '', // 访问url
        'text'  => '求贤管理',       // 显示
        'ico'  => 'fa fa-fw fa-bookmark',     // 菜单图标
        'child' => array(
            array(
                'url'   => '/ability/default/', // 访问url
                'text'  => '求贤需求列表',       // 显示
                'ico'  => '',     // 菜单图标
            ),
        ),
    ),
    array(
        'url'   => '', // 访问url
        'text'  => '轻合伙管理',       // 显示
        'ico'  => 'fa fa-fw fa-bookmark',     // 菜单图标
        'child' => array(
            array(
                'url'   => '/partner/default/', // 访问url
                'text'  => '合伙需求列表',       // 显示
                'ico'  => '',     // 菜单图标
            ),
            array(
                'url'   => '/partner/member/', // 访问url
                'text'  => '合伙人列表',       // 显示
                'ico'  => '',     // 菜单图标
            ),
        ),
    ),*/
);