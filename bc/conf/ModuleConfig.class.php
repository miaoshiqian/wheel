<?php

/**
 * 模块配置
 *
 * @author 龙卫国<longweiguo@xiongying.com>
 */

class ModuleConfig {
	public static $MODULES = array(
		'log' => array(
			'url' => '/',
			'text' => '日志查看',
			'icon'   => 'fa-exclamation-circle',
			'dir' => '',
		),
		'doc' => array(
			'url' => '/api/default/',
			'text' => '开发文档',
			'icon'   => 'fa-folder',
			'dir' => '',
		),
		'apidoc' => array(
			'url' => '/',
			'text' => 'API接口文档',
			'icon'   => 'fa-folder-open',
			'dir' => '',
		),
		'sso' => array(
			'url' => '/',
			'text' => '人员与权限管理',
			'icon'   => 'fa-lock',
			'dir' => '',
		),
//		'sysmanager' => array(
//			'url' => '/',
//			'text' => '系统管理',
//			'icon'   => 'fa-lock',
//			'dir' => FRAMEWORK_PATH . '/../../sysmanager',
//		),
	);

	/**
	 * 获取模块所在目录，空模块时目录为bc目录，有配置dir时用指定dir目录，否则为bc下的modules目录下的子目录
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string $module
	 *
	 * @return string
	 */
	public static function getModuleDir($module = '') {
		if (empty($module) || $module == 'default' || empty(self::$MODULES[$module])) {
			return __DIR__ . '/..';
		}
		if (!empty(self::$MODULES[$module]['dir'])) {
				return self::$MODULES[$module]['dir'];
		} else {
			return __DIR__ . '/../modules/'.$module;
		}
	}
	
}