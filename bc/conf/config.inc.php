<?php
/**
 * 主站常量定义
 */
define('PROJECT_PATH', dirname(dirname(__FILE__)));
define('ROOT_PATH', dirname(PROJECT_PATH));
define('API_PATH', ROOT_PATH . '/api');
define('FRAMEWORK_PATH', ROOT_PATH . '/framework');
define('CONF_PATH', ROOT_PATH . '/conf/conf');
define('CONF_COMMON_PATH', ROOT_PATH . '/conf/common');
define('DATA_PATH', ROOT_PATH . '/data/');

require_once CONF_COMMON_PATH.'/GlobalConfig.class.php';
$_SERVER['DEBUG'] = !GlobalConfig::inOnline();