<?php
/**
 * @brief 牛人控制器
 * @author 缪石乾<miaoshiqian@gmail.com>
 * @since 2015-12-20
 */

class ServiceController extends BcController{

    public function defaultAction() {
        $this->IndexAction();
    }

    public function IndexAction(){
        $this->view->assign('pageTitle', '服务列表');

        $this->render('service/list.php', array(
            'datagrid' => $this->_getDataGrid(),
        ));
    }

    /**
     * @brief 服务详情页
     */
    public function detailAction() {
        try {
            $id = Request::getGET('id');
            $this->view->assign('pageTitle', '服务详情-' . $id);
            $info = ServiceInterface::getDetail(array('id' => $id));
            $userInfo = UserInterface::getById(array('id' => $info['user_id']));
            $this->render('service/detail.php', array(
                'data'     => $info,
                'userInfo' => $userInfo,
            ));
        } catch (Exception $e) {
            $this->errorAction(array('msg' => $e->getMessage()));
        }
    }

    /**
     * @brief 设置表格头
     * @fieldx 仅仅是个名称、可以自定义。只需要和 ajaxGetData中的 数据key对上就ok了
     * @text 表头显示的名称内容
     * @style 表头中需要显示 style样式的时候
     * @class 表头中需要添加class的时候
     * @data 表头中需要存储 data-xxx数据的时候 例如: array('a' => 100) dom上面为：data-a="100"
     */
    private function _getDataGrid() {
        $dataGrid = new DataGridWidget();
        $dataGrid->setUrl('/Service/ajaxGetData/?' . $this->getQueryString());
        $field = array(
            'field1' => array('text' => '姓名', 'style' => 'width:10%'),
            'field2' => array('text' => '名称/状态', 'style' => 'width:20%'),
            'field3' => array('text' => '价格', 'style' => 'width:10%'),
            'field4' => array('text' => '描述', 'style' => 'width:25%'),
            'field5' => array('text' => '类型', 'style' => 'width:10%'),
            'field6' => array('text' => '时间/城市', 'style' => 'width:15%'),
            'field7' => array('text' => '操作', 'style' => 'width:10%'),
        );
        $dataGrid->setCol($field);
        return $dataGrid;
    }

    //ajax获取表格的数据
    public function ajaxGetDataAction() {
        $dataGrid = $this->_getDataGrid();
        $this->currentPage = (int) Request::getGET('page', 1); //默认第一页
        $offset  = ($this->currentPage - 1) * $this->pageSize;

        //查询条件
        $name = Request::getGET('name', '');
        $name = urldecode($name); //汉字记得 urldecode
        $status    = Request::getGET('status', 0);
        $type    = Request::getGET('type', 0);

        $filters = array();
        if (!empty($status)) {
            $filters[] = array('status', '=', $status);
        }
        if (!empty($name)) {
            $filters[] = array('name', 'like', '%' . $name . '%');
        }
        if (!empty($type)) {
            $filters[] = array('type', '=', $type);
        }
        $params = array(
            'filters' => $filters,
            'limit'   => $this->pageSize,
            'offset'  => $offset,
        );
        $allCount = ServiceInterface::getCount($params);
        $data = ServiceInterface::getList($params);

        $list = array();
        $userIds = !empty($data) ? array_column($data, 'user_id') : array();
        $userIds = array_unique($userIds);
        $userInfo = !empty($userIds) ? UserInterface::getList(array('filter' => array('id', 'in', $userIds))) : array();
        if (!empty($userInfo)) {
            $ids = array_column($userInfo, 'id');
            $userInfo = array_combine($ids, $userInfo);
        }
        foreach ((array)$data as $row) {
            $timeCity = '空闲时间：' . $row['free_time'] . '<br>服务城市：' . $row['service_city'];
            $l = array(
                'field1' => '<a href="/customer/detail/?id='.$row['user_id'].'" target="_blank">' . (isset($userInfo[$row['user_id']]) ? Util::getFromArray('real_name', $userInfo[$row['user_id']], '') : '') . '</a>',
                'field2' => '<a href="/service/detail/?id='.$row['id'].'" target="_blank">' . Util::getFromArray('name', $row, '')
                    . '</a><br>(' . Util::getFromArray($row['status'], EnumService::$SERVICE_STATUS, '') . ')',
                'field3' => '报价：' . $row['price'] / 100 . '元' . '<br>股权：' . $row['stake_price'] / 100 . '元',
                'field4' => Util::getFromArray('desc', $row, ''),
                'field5' => Util::getFromArray($row['type'], EnumService::$SERVICE_TYPE, ''),
                'field6' => $timeCity,
                'field7' => '',
            );
            if ($row['status'] == EnumService::SERVICE_STATUS_NORMAL) {
                $l['field7'] .= '<a href="javascript:;" data-id="'.$row['id'].'" data-action-type="frozen_op">冻结</a>';
            } else if ($row['status'] == EnumService::SERVICE_STATUS_FROZEN) {
                $l['field7'] .= '<a href="javascript:;" data-id="'.$row['id'].'" data-action-type="frozen_op">解冻</a>';
            }
            if ($row['status'] != EnumService::SERVICE_STATUS_DEL) {
                $l['field7'] .= ' | <a href="javascript:;" data-id="'.$row['id'].'" data-action-type="del">删除</a>';
            }
            $list[] = $l;
        }

        $dataGrid->setData($list);
        //设置分页信息
        $maxSize = $allCount > $this->maxSize ? $this->maxSize : $allCount;
        $dataGrid->setPager($maxSize, $this->currentPage, $this->pageSize);

        $data = $dataGrid->getData();
        echo json_encode(array('data' => $data));
    }

    /**
     * @brief 冻结和解冻服务
     */
    public function ajaxFrozenOpAction() {
        $id = Request::getPOST('service_id');
        if (empty($id)) {
            Response::outputJson(array('errorCode' => 1, 'msg' => '缺少用户id'));
        }
        try {
            ServiceInterface::frozenOp(array('service_id' => $id));
        } catch (Exception $e) {
            Response::outputJson(array('errorCode' => 1, 'msg' => $e->getMessage()));
        }

        Response::outputJson(array('errorCode' => 0, 'msg' => '操作成功'));
    }

    /**
     * @brief 删除服务
     */
    public function ajaxDelServiceAction() {
        $id = Request::getPOST('service_id');
        if (empty($id)) {
            Response::outputJson(array('errorCode' => 1, 'msg' => '缺少用户id'));
        }
        try {
            ServiceInterface::del(array('service_id' => $id));
        } catch (Exception $e) {
            Response::outputJson(array('errorCode' => 1, 'msg' => $e->getMessage()));
        }

        Response::outputJson(array('errorCode' => 0, 'msg' => '删除成功'));
    }
}