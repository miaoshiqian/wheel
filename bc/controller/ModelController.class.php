<?php
/**
 * @brief 品牌车系管理控制器
 * @author 缪石乾<miaoshiqian@gmail.com>
 * @since 2015-12-20
 */

class ModelController extends BcController{

    public function defaultAction() {
        $this->IndexAction();
    }

    public function IndexAction(){
        $this->view->assign('pageTitle', '车型列表');

        $this->render('model/list.php', array(
            'datagrid' => $this->_getDataGrid(),
        ));
    }

    /**
     * @brief 设置表格头
     * @fieldx 仅仅是个名称、可以自定义。只需要和 ajaxGetData中的 数据key对上就ok了
     * @text 表头显示的名称内容
     * @style 表头中需要显示 style样式的时候
     * @class 表头中需要添加class的时候
     * @data 表头中需要存储 data-xxx数据的时候 例如: array('a' => 100) dom上面为：data-a="100"
     */
    private function _getDataGrid() {
        $dataGrid = new DataGridWidget();
        $dataGrid->setUrl('/Model/ajaxGetData/?' . $this->getQueryString());
        $field = array(
            'field1' => array('text' => '车型ID', 'style' => 'width:10%'),
            'field2' => array('text' => '品牌名称', 'style' => 'width:15%'),
            'field3' => array('text' => '车系名称', 'style' => 'width:15%'),
            'field4' => array('text' => '年款', 'style' => 'width:10%'),
            'field5' => array('text' => '车型名称', 'style' => 'width:30%'),
            'field6' => array('text' => '操作', 'style' => 'width:20%'),
        );
        $dataGrid->setCol($field);
        return $dataGrid;
    }

    //ajax获取表格的数据
    public function ajaxGetDataAction() {
        $dataGrid = $this->_getDataGrid();
        $this->maxSize = 30000;
        $this->currentPage = (int) Request::getGET('page', 1); //默认第一页
        $offset  = ($this->currentPage - 1) * $this->pageSize;

        //查询条件
        $brandId = Request::getGET('brand_id', 0);
        $classId = Request::getGET('class_id', 0);
        $year    = Request::getGET('year', 0);
        $modelId = Request::getGET('model_id', 0);
        $name    = Request::getGET('model_name', ''); //车型名称模糊查询

        $filters = array();
        if (!empty($modelId)) {
            $filters[] = array('id', '=', $modelId);
        } else if (!empty($classId)) {
            $filters[] = array('series_id', '=', $classId);
            !empty($year) && $filters[] = array('year', '=', $year);
        } else if (!empty($brandId)) {
            $classIds = BrandInterface::getClassList($brandId, 'id');
            !empty($classIds) && $filters[] = array('series_id', 'in', $classIds);
        }

        if (!empty($name)) {
            $filters[] = array('model_name', 'like', '%' . urldecode($name) . '%');
        }

        $allCount = AutoModelModel::getInstance()->getCount($filters);
        $field = 'id, model_short_name as name,en_model_short_name as en_name,model_name as fullname,en_model_name as en_fullname, year, series_id';
        $data = AutoModelModel::getInstance()->getAll($field, $filters, array('id' => 'asc'), $this->pageSize, $offset);
        $seriesIds = !empty($data) ? array_column($data, 'series_id') : array();
        $classInfo = !empty($seriesIds) ? AutoSeriesModel::getInstance()->getAll('id,brand_name,name,en_name,en_brand_name', array(array('id', 'in', $seriesIds))) : array();
        if (!empty($classInfo)) {
            $keys = !empty($classInfo) ? array_column($classInfo, 'id') : array();
            $classInfo = array_combine($keys, $classInfo);
        }

        $list = array();
        foreach ((array)$data as $row) {
            $operate = '<a data-action-type="operate" data-type="iframe" data-width="450" data-height="550" data-url="/model/iframeEdit/?id='.$row['id'].'" data-title="车型编辑或翻译" href="javascript:;">编辑或翻译</a>';
            $l = array(
                'field1' => Util::getFromArray('id', $row, 0),
                'field2' => '中文：' . (!empty($classInfo[$row['series_id']]) ? Util::getFromArray('brand_name', $classInfo[$row['series_id']], '') : '')
                         . '<br>英文：' . (!empty($classInfo[$row['series_id']]) ? Util::getFromArray('en_brand_name', $classInfo[$row['series_id']], '') : ''),
                'field3' => '中文：' . (!empty($classInfo[$row['series_id']]) ? Util::getFromArray('name', $classInfo[$row['series_id']], '') : '')
                         . '<br>英文：' . (!empty($classInfo[$row['series_id']]) ? Util::getFromArray('en_name', $classInfo[$row['series_id']], '') : ''),
                'field4' => Util::getFromArray('year', $row, ''),
                'field5' => '中文：' . Util::getFromArray('name', $row, '')
                         . '<br>英文：' . Util::getFromArray('en_name', $row, '')
                         . '<br>中文全称：' . Util::getFromArray('fullname', $row, '')
                         . '<br>英文全称：' . Util::getFromArray('en_fullname', $row, ''),
                'field6' => $operate,
            );
            $list[] = $l;
        }

        $dataGrid->setData($list);
        //设置分页信息
        $maxSize = $allCount > $this->maxSize ? $this->maxSize : $allCount;
        $dataGrid->setPager($maxSize, $this->currentPage, $this->pageSize);

        $data = $dataGrid->getData();
        echo json_encode(array('data' => $data));
    }

    //添加新的品牌车型
    public function iframeAddAction() {
        $brandList    = BrandInterface::getBrandList();
        $brandData    = array();
        foreach((array)$brandList as $row) {
            $brandData[$row['first_char']][] = array(
                'id'   => $row['id'],
                'name' => $row['name'],
            );
        }
        !empty($brandData) && ksort($brandData);

        $this->render('model/iframe_add.php', array(
            'brand' => $brandData
        ), 'include/tpl_simple.php');
    }

    /**
     * @brief 添加品牌车型 提交处理
     */
    public function ajaxSubAddAction() {
        $type = Request::getPOST('type');
        if (empty($type)) {
            return Response::outputJson(array('errorCode' => 1, 'msg' => '缺少添加类型'));
        }
        if ($type == 1) {//加品牌
            $brandName = Request::getPOST('brand_name', '');
            $enBrand = Request::getPOST('en_brand_name', '');
            if (empty($brandName) || empty($enBrand)) {
                return Response::outputJson(array('errorCode' => 1, 'msg' => '品牌名称不能为空'));
            }
            $brandPinyin = Pinyin1::getPinyin($brandName);
            $data = array(
                'name' => $brandName,
                'en_name' => $enBrand,
                'pinyin' => $brandPinyin,
                'first_char' => strtoupper(mb_substr($brandPinyin, 0, 1)),
            );
            $ret = AutoBrandModel::getInstance()->insert($data);
        } else if ($type == 2) {//加车系
            $brandId = Request::getPOST('brand_id', '');
            $className = Request::getPOST('class_name', '');
            $enClass = Request::getPOST('en_class_name', '');
            if (empty($brandId)) {
                return Response::outputJson(array('errorCode' => 1, 'msg' => '品牌必须选择'));
            }
            if (empty($className) || empty($enClass)) {
                return Response::outputJson(array('errorCode' => 1, 'msg' => '车系名称不能为空'));
            }
            $brandInfo = AutoBrandModel::getInstance()->getById($brandId);
            $data = array(
                'name' => $className,
                'en_name' => $enClass,
                'brand_id' => $brandId,
                'brand_name' => $brandInfo['name'],
                'en_brand_name' => $brandInfo['en_name'],
                'brand_pinyin' => $brandInfo['pinyin'],
                'pinyin' => Pinyin1::getPinyin($className),
            );
            $ret = AutoSeriesModel::getInstance()->insert($data);
        } else if ($type == 3) {//加车型
            $brandId = Request::getPOST('brand_id', '');
            $classId = Request::getPOST('class_id', '');
            if (empty($brandId) || empty($classId)) {
                return Response::outputJson(array('errorCode' => 1, 'msg' => '品牌车系必须选择'));
            }
            $year = Request::getPOST('year', 0);
            $modelShortName = Request::getPOST('model_short_name', '');
            $enModel = Request::getPOST('en_model_short_name', '');
            if (empty($year) || empty($modelShortName) || empty($enModel)) {
                return Response::outputJson(array('errorCode' => 1, 'msg' => '年款车型名称不能为空'));
            }
            $classInfo = AutoSeriesModel::getInstance()->getById($classId);
            $data = array(
                'series_id' => $classId,
                'year' => $year,
                'model_short_name' => $modelShortName,
                'en_model_short_name' => $enModel,
                'model_name' => BrandInterface::getCompleteVehicleName($classInfo['brand_name'], $classInfo['name'], $modelShortName, $year),
                'en_model_name' => BrandInterface::getCompleteVehicleName($classInfo['en_brand_name'], $classInfo['en_name'], $enModel, $year, ' '),
                'emission_standard' => -1,
                'ems' => 0,
            );
            $ret = AutoModelModel::getInstance()->insert($data);
        }

        if ($ret === false) {
            return Response::outputJson(array('errorCode' => 1, 'msg' => '添加失败'.print_r($data,true)));
        }

        Response::outputJson(array('errorCode' => 0, 'msg' => '添加成功'));
    }

    //编辑品牌车型
    public function iframeEditAction() {
        $modelId = Request::getGET('id', 0);
        if (empty($modelId)) {
            return $this->render('include/iframe_error.php', array(
                'errorMsg' => '缺少参数',
            ), 'include/tpl_simple.php');
        }

        $modelInfo = AutoModelModel::getInstance()->getById($modelId, 'series_id,model_short_name,en_model_short_name,year');
        $classInfo = !empty($modelInfo['series_id']) ? AutoSeriesModel::getInstance()->getById($modelInfo['series_id'], 'id,brand_name,en_brand_name,name,en_name') : array();

        $this->render('model/iframe_edit.php', array(
            'id' => $modelId,
            'brand_name' => Util::getFromArray('brand_name', $classInfo, ''),
            'en_brand_name' => Util::getFromArray('en_brand_name', $classInfo, ''),
            'class_name' => Util::getFromArray('name', $classInfo, ''),
            'en_class_name' => Util::getFromArray('en_name', $classInfo, ''),
            'year' => Util::getFromArray('year', $modelInfo, ''),
            'model_short_name' => Util::getFromArray('model_short_name', $modelInfo, ''),
            'en_model_short_name' => Util::getFromArray('en_model_short_name', $modelInfo, ''),
        ), 'include/tpl_simple.php');
    }

    /**
     * @brief 提交编辑或翻译
     */
    public function ajaxSubEditAction() {
        $id = Request::getPOST('id');
        $brandName = Request::getPOST('brand_name', '');
        $className = Request::getPOST('class_name', '');
        $year = Request::getPOST('year', 0);
        $modelShortName = Request::getPOST('model_short_name', '');

        $enBrand = Request::getPOST('en_brand_name', '');
        $enClass = Request::getPOST('en_class_name', '');
        $enModel = Request::getPOST('en_model_short_name', '');

        if (empty($id) || empty($brandName) || empty($year) || empty($className) || empty($modelShortName)) {
            return Response::outputJson(array('errorCode' => 1, 'msg' => '内容必须填写完整'));
        }
        $modelInfo = AutoModelModel::getInstance()->getById($id);
        if (empty($modelInfo)) {
            return Response::outputJson(array('errorCode' => 1, 'msg' => '获取旧车型信息失败'));
        }
        $classInfo = !empty($modelInfo['series_id']) ? AutoSeriesModel::getInstance()->getById($modelInfo['series_id']) : array();
        if (empty($classInfo)) {
            return Response::outputJson(array('errorCode' => 1, 'msg' => '获取旧车系信息失败'));
        }
        $brandPinyin = Pinyin1::getPinyin($brandName);
        //更新车系
        $classData = array(
            'name' => $className,
            'en_name' => $enClass,
            'brand_name' => $brandName,
            'en_brand_name' => $enBrand,
            'brand_pinyin' => $brandPinyin,
            'pinyin' => Pinyin1::getPinyin($className),
        );
        $ret = AutoSeriesModel::getInstance()->updateById($classInfo['id'], $classData);
        if ($ret === false) {
            return Response::outputJson(array('errorCode' => 1, 'msg' => '更新车系信息失败'));
        }
        //更新品牌
        $brandData = array(
            'name' => $brandName,
            'en_name' => $enBrand,
            'pinyin' => $brandPinyin,
            'first_char' => strtoupper(mb_substr($brandPinyin, 0, 1)),
        );
        $ret = AutoBrandModel::getInstance()->updateById($classInfo['brand_id'], $brandData);
        if ($ret === false) {
            return Response::outputJson(array('errorCode' => 1, 'msg' => '更新品牌信息失败'));
        }
        //更新车型信息
        $modelData = array(
            'year' => $year,
            'model_short_name' => $modelShortName,
            'en_model_short_name' => $enModel,
            'model_name' => BrandInterface::getCompleteVehicleName($brandName, $className, $modelShortName, $year),
            'en_model_name' => BrandInterface::getCompleteVehicleName($enBrand, $enClass, $enModel, $year, ' '),
        );
        $ret = AutoModelModel::getInstance()->updateById($id, $modelData);
        if ($ret === false) {
            return Response::outputJson(array('errorCode' => 1, 'msg' => '更新品牌信息失败'));
        }

        Response::outputJson(array('errorCode' => 0, 'msg' => '操作成功'));
    }

    /**
     * @brief 删除用户
     */
    public function ajaxDelUserAction() {
        $id = Request::getPOST('user_id');
        if (empty($id)) {
            Response::outputJson(array('errorCode' => 1, 'msg' => '缺少用户id'));
        }
        try {
            UserInterface::del(array('user_id' => $id));
        } catch (Exception $e) {
            Response::outputJson(array('errorCode' => 1, 'msg' => $e->getMessage()));
        }

        Response::outputJson(array('errorCode' => 0, 'msg' => '删除成功'));
    }
}