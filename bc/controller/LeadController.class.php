<?php

class LeadController extends BcController{

    public function defaultAction() {
        $this->IndexAction();
    }

    public function IndexAction(){
        $this->view->assign('pageTitle', '卖车意向列表');

        $this->render('lead/list.php', array(
            'datagrid' => $this->_getDataGrid(),
        ));
    }

    /**
     * @brief 详情页
     */
    public function detailAction() {
        try {
            $id = Request::getGET('id');
            $this->view->assign('pageTitle', '客户详情-' . $id);
            $userInfo = UserInterface::getById(array('id' => $id));
            $userInfo['head_url'] = QiNiu::formatImageUrl(Util::getFromArray('head_url', $userInfo, ''), array('width' => 80, 'height' => 80, 'mode' => 1));
            $provinceName = LocationInterface::getProvinceNameById(array('id' => $userInfo['province']));
            $cityName = LocationInterface::getCityNameById(array('id' => $userInfo['city']));
            $userInfo['province_city'] = $provinceName . '-' . $cityName;
            $this->render('customer/detail.php', array(
                'data'     => $userInfo,
            ));
        } catch (Exception $e) {
            $this->errorAction(array('msg' => $e->getMessage()));
        }
    }

    /**
     * @brief 设置表格头
     * @fieldx 仅仅是个名称、可以自定义。只需要和 ajaxGetData中的 数据key对上就ok了
     * @text 表头显示的名称内容
     * @style 表头中需要显示 style样式的时候
     * @class 表头中需要添加class的时候
     * @data 表头中需要存储 data-xxx数据的时候 例如: array('a' => 100) dom上面为：data-a="100"
     */
    private function _getDataGrid() {
        $dataGrid = new DataGridWidget();
        $dataGrid->setUrl('/Lead/ajaxGetData/?' . $this->getQueryString());
        $field = array(
            'field1' => array('text' => '品牌-车系', 'style' => 'width:18%'),
            'field2' => array('text' => '联系电话', 'style' => 'width:10%'),
            'field3' => array('text' => '上牌时间', 'style' => 'width:10%'),
            'field4' => array('text' => '省份-城市', 'style' => 'width:10%'),
            'field5' => array('text' => '备注', 'style' => 'width:20%'),
            'field6' => array('text' => '添加时间', 'style' => 'width:15%'),
            'field8' => array('text' => '状态', 'style' => 'width:10%'),
            'field7' => array('text' => '操作', 'style' => 'width:10%'),
        );
        $dataGrid->setCol($field);
        return $dataGrid;
    }

    //ajax获取表格的数据
    public function ajaxGetDataAction() {
        $dataGrid = $this->_getDataGrid();
        $this->currentPage = (int) Request::getGET('page', 1); //默认第一页
        $offset  = ($this->currentPage - 1) * $this->pageSize;

        //查询条件
        $mobile    = Request::getGET('mobile', '');
        $status = Request::getGET('status', 0);
        $filters = array();
        if (!empty($mobile)) {
            $filters[] = array('seller_phone', '=', $mobile);
        }
        if (!empty($status)) {
            $filters[] = array('status', '=', $status);
        }
        $params = array(
            'filters' => $filters,
            'limit'   => $this->pageSize,
            'offset'  => $offset,
        );
        $allCount = LeadInterface::getCount($params);
        $data = LeadInterface::getList($params);

        $list = array();
        foreach ((array)$data as $row) {
            $provinceName = LocationInterface::getProvinceNameById(array('id' => $row['province_id']));
            $cityName = LocationInterface::getCityNameById(array('id' => $row['city_id']));
            $operate = '';
            if ($row['status'] == EnumLead::STATUS_WAIT) {
                $operate .= '<a href="javascript:;" data-action-type="deal" data-id="'.$row['id'].'">标为已处理</a>';
            }
            $l = array(
                'field1' => $row['brand_name'] . ' ' . $row['class_name'],
                'field2' => Util::getFromArray('seller_phone', $row, ''),
                'field3' => !empty($row['plate_time']) ? date('Y-m-d', $row['plate_time']) : '',
                'field4' => $provinceName . ' - ' . $cityName,
                'field5' => Util::getFromArray('remark', $row, ''),
                'field6' => !empty($row['create_time']) ? date('Y-m-d H:i', $row['create_time']) : '',
                'field7' => $operate,
                'field8' => Util::getFromArray($row['status'], EnumLead::$STATUS_TEXT, '无意义'),
            );
            $list[] = $l;
        }

        $dataGrid->setData($list);
        //设置分页信息
        $maxSize = $allCount > $this->maxSize ? $this->maxSize : $allCount;
        $dataGrid->setPager($maxSize, $this->currentPage, $this->pageSize);

        $data = $dataGrid->getData();
        echo json_encode(array('data' => $data));
    }

    /**
     * @brief 标记为已处理
     */
    public function ajaxDealAction() {
        $id = Request::getPOST('id');
        if (empty($id)) {
            Response::outputJson(array('errorCode' => 1, 'msg' => '缺少id'));
        }
        try {
            LeadInterface::deal(array('id' => $id));
        } catch (Exception $e) {
            Response::outputJson(array('errorCode' => 1, 'msg' => $e->getMessage()));
        }

        Response::outputJson(array('errorCode' => 0, 'msg' => '操作成功'));
    }
}