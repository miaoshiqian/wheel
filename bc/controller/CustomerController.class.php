<?php
/**
 * @brief 牛人控制器
 * @author 缪石乾<miaoshiqian@gmail.com>
 * @since 2015-12-20
 */

class CustomerController extends BcController{

    public function defaultAction() {
        $this->IndexAction();
    }

    public function IndexAction(){
        $this->view->assign('pageTitle', '项目列表');

        $this->render('customer/list.php', array(
            'datagrid' => $this->_getDataGrid(),
        ));
    }

    /**
     * @brief 客户详情页
     */
    public function detailAction() {
        try {
            $id = Request::getGET('id');
            $this->view->assign('pageTitle', '客户详情-' . $id);
            $userInfo = UserInterface::getById(array('id' => $id));
            $userInfo['head_url'] = QiNiu::formatImageUrl(Util::getFromArray('head_url', $userInfo, ''), array('width' => 80, 'height' => 80, 'mode' => 1));
            $provinceName = LocationInterface::getProvinceNameById(array('id' => $userInfo['province']));
            $cityName = LocationInterface::getCityNameById(array('id' => $userInfo['city']));
            $userInfo['province_city'] = $provinceName . '-' . $cityName;
            $this->render('customer/detail.php', array(
                'data'     => $userInfo,
            ));
        } catch (Exception $e) {
            $this->errorAction(array('msg' => $e->getMessage()));
        }
    }

    /**
     * @brief 设置表格头
     * @fieldx 仅仅是个名称、可以自定义。只需要和 ajaxGetData中的 数据key对上就ok了
     * @text 表头显示的名称内容
     * @style 表头中需要显示 style样式的时候
     * @class 表头中需要添加class的时候
     * @data 表头中需要存储 data-xxx数据的时候 例如: array('a' => 100) dom上面为：data-a="100"
     */
    private function _getDataGrid() {
        $dataGrid = new DataGridWidget();
        $dataGrid->setUrl('/Customer/ajaxGetData/?' . $this->getQueryString());
        $field = array(
            'field1' => array('text' => '姓名', 'style' => 'width:20%'),
            'field2' => array('text' => '用户名', 'style' => 'width:20%'),
            'field3' => array('text' => '手机', 'style' => 'width:20%'),
            'field4' => array('text' => '省份-城市', 'style' => 'width:20%'),
            'field5' => array('text' => '注册时间', 'style' => 'width:20%'),
        );
        $dataGrid->setCol($field);
        return $dataGrid;
    }

    //ajax获取表格的数据
    public function ajaxGetDataAction() {
        $dataGrid = $this->_getDataGrid();
        $this->currentPage = (int) Request::getGET('page', 1); //默认第一页
        $offset  = ($this->currentPage - 1) * $this->pageSize;

        //查询条件
        $real_name = Request::getGET('real_name', '');
        $real_name = urldecode($real_name); //汉字记得 urldecode
        $mobile    = Request::getGET('mobile', '');

        $filters = array();
        if (!empty($real_name)) {
            $filters[] = array('real_name', '=', $real_name);
        }
        if (!empty($mobile)) {
            $filters[] = array('mobile', '=', $mobile);
        }
        $params = array(
            'filters' => $filters,
            'limit'   => $this->pageSize,
            'offset'  => $offset,
        );
        $allCount = UserInterface::getCount($params);
        $data = UserInterface::getList($params);

        $list = array();
        foreach ((array)$data as $row) {
            $provinceName = LocationInterface::getProvinceNameById(array('id' => $row['province_id']));
            $cityName = LocationInterface::getCityNameById(array('id' => $row['city_id']));
            $l = array(
                'field1' => Util::getFromArray('real_name', $row, ''),
                'field2' => Util::getFromArray('username', $row, ''),
                'field3' => Util::getFromArray('mobile', $row, ''),
                'field4' => $provinceName . ' - ' . $cityName,
                'field5' => !empty($row['create_time']) ? date('Y-m-d H:i', $row['create_time']) : '',
            );
            $list[] = $l;
        }

        $dataGrid->setData($list);
        //设置分页信息
        $maxSize = $allCount > $this->maxSize ? $this->maxSize : $allCount;
        $dataGrid->setPager($maxSize, $this->currentPage, $this->pageSize);

        $data = $dataGrid->getData();
        echo json_encode(array('data' => $data));
    }

    /**
     * @brief 冻结和解冻用户
     */
    public function ajaxFrozenOpAction() {
        $id = Request::getPOST('user_id');
        if (empty($id)) {
            Response::outputJson(array('errorCode' => 1, 'msg' => '缺少用户id'));
        }
        try {
            UserInterface::frozenOp(array('user_id' => $id));
        } catch (Exception $e) {
            Response::outputJson(array('errorCode' => 1, 'msg' => $e->getMessage()));
        }

        Response::outputJson(array('errorCode' => 0, 'msg' => '操作成功'));
    }

    /**
     * @brief 删除用户
     */
    public function ajaxDelUserAction() {
        $id = Request::getPOST('user_id');
        if (empty($id)) {
            Response::outputJson(array('errorCode' => 1, 'msg' => '缺少用户id'));
        }
        try {
            UserInterface::del(array('user_id' => $id));
        } catch (Exception $e) {
            Response::outputJson(array('errorCode' => 1, 'msg' => $e->getMessage()));
        }

        Response::outputJson(array('errorCode' => 0, 'msg' => '删除成功'));
    }
}