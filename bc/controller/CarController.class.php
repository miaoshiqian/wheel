<?php
/**
 * @author 缪石乾<miaoshiqian@haoche51.com>
 * @brief 简介：
 * @date 16/3/29
 * @time 下午4:25
 */
class CarController extends BcController{

    public function defaultAction() {
        $this->addAction();
    }

    //添加或者编辑车源
    public function addAction() {
        $id   = Request::getGET('id', 0);
        $info = !empty($id) ? CarModel::getInstance()->getRow('*', array(array('id', '=', $id))) : array();
        $modelInfo = !empty($info['model_id']) ? ModelYearModel::getInstance()->getById($info['model_id']) : array();
        $info['model_name'] = Util::getFromArray('model', $modelInfo, '');
        $info['year_name'] = $info['vehicle_year'] = Util::getFromArray('year', $modelInfo, 0);
        $info['make_name'] = $info['make'] = Util::getFromArray('model_make_id', $modelInfo, '');
        $info['high_lights'] = !empty($info['high_lights']) ? json_decode($info['high_lights']) : array();
        $formObj = new CarForm($info);
        $provinceList = LocationInterface::getProvince();

        $data = array(
            'province' => $provinceList,
            'form'     => $formObj->getForm(),
            'editInfo' => $info,
            'isEdit'   => $id ? 1 : 0,
            'states'   => UsLocationInterface::getStateList(),
        );
        if ($data['isEdit'] && !empty($modelInfo)) {
            $data['makeList'] = ModelYearModel::getInstance()->getAll('distinct make', array(array('year', '=', $info['year_name'])));
            $data['modelList'] = ModelYearModel::getInstance()->getAll('id,model', array(array('year', '=', $info['year_name']), array('make', '=', $info['make_name'])));
        }

        if ($data['isEdit'] && !empty($info['province_id'])) {
            $data['cityList'] = UsLocationInterface::getCityByStateId(array('state_id' => $info['province_id']));
        }

        if (!empty($id)) { //编辑、去获取图片信息
            $imgInfo = CarImagesModel::getInstance()->getAll('*', array(array('car_id', '=', $id)), 'type desc');
            $images = array();
            foreach ((array)$imgInfo as $img) {
                $orgUrl = QiNiu::formatImageUrl($img['url']);
                $images[] = array(
                    'url'     => $orgUrl,
                    'org_url' => $orgUrl,
                    'key'     => $img['url'] ? $img['url'] : '',
                );
            }
            //json格式化
            $data['images'] = !empty($images) ? json_encode($images) : '';
        }

        $this->render('car/add.php', $data);
    }

    /**
     * @brief 提交车源发布处理
     */
    public function submitAction() {
        $id     = Request::getPost('id'); //如果是编辑车源的话，会从表单获得车源id
        $images = json_decode(Request::getPost('images'), 1);  //车源图片

        $formObj  = new CarForm(array());
        $form     = $formObj->getForm();
        $validate = $form->validate(); //执行表单的后端验证, 草稿去掉后端验证
        $user     = $this->getUserInfo();

        if (!$validate) { //表单后端验证失败
            $this->_backForm($form);
        }

        $needField = array(
        );
        $postInfo  = $form->getValue();

        $data = array(
            'first_name'         => Util::getFromArray('first_name', $postInfo, ''),
            'province_id'      => Util::getFromArray('province_id', $postInfo, 0),
            'city_id'          => Util::getFromArray('city_id', $postInfo, 0),
            'last_name'     => Util::getFromArray('last_name', $postInfo, ''),
            'address'     => Util::getFromArray('address', $postInfo, ''),
            'more_address'     => Util::getFromArray('more_address', $postInfo, ''),
            'email_address'     => Util::getFromArray('email_address', $postInfo, ''),
            'zip_code'     => Util::getFromArray('zip_code', $postInfo, ''),
            'phone1'      => Util::getFromArray('phone1', $postInfo, ''),
            'phone2'     => Util::getFromArray('phone2', $postInfo, ''),
            'make'     => Util::getFromArray('make_name', $postInfo, ''),
            'vehicle_year'     => Util::getFromArray('year_name', $postInfo, ''),
            'model_name'     => Util::getFromArray('model_name', $postInfo, ''),
            'model_trim'     => Util::getFromArray('model_trim', $postInfo, 0),
            'model_trim_name'     => Util::getFromArray('model_trim_name', $postInfo, ''),
            'listing_price'     => Util::getFromArray('listing_price', $postInfo, 0),
            'mile'     => Util::getFromArray('mile', $postInfo, 0),
            'exterior_color'     => Util::getFromArray('exterior_color', $postInfo, ''),
            'interior_color'     => Util::getFromArray('interior_color', $postInfo, ''),
            'transmission'     => Util::getFromArray('transmission', $postInfo, 0),
            'title_status'     => Util::getFromArray('title_status', $postInfo, 0),
            'body_style'     => Util::getFromArray('body_style', $postInfo, 0),
            'vin_code'            => Util::getFromArray('vin_code', $postInfo, ''),
            'title'            => BrandInterface::makeVehicleTitle($postInfo['year_name'], $postInfo['make_name'], $postInfo['model_name'], $postInfo['model_trim_name']),
        );
        $highLights = Util::getFromArray('high_lights', $_POST, '');
        $data['highlights'] = !empty($highLights) ? json_encode($highLights) : '';

        foreach ((array)$needField as $field) {
            if (empty($data[$field])) {
                $this->_backForm($form, '缺少参数：' . $field);
            }
        }

        if (empty($id)) { //新增
            $data['create_user_id']   = Util::getFromArray('id', $user, 0);
            $data['create_time']      = time();
            $data['update_time']      = time();
            $data['status']           = EnumCar::STATUS_ONLINE;

            $carId = VehicleModel::getInstance()->insert($data);
            if (!$carId) {
                $this->_backForm($form, '添加车源失败');
            }

        } else {
            $data['update_time'] = time();
            $ret = VehicleModel::getInstance()->updateById($id, $data);
            if ($ret === false) {
                $this->_backForm($form, '更新车源失败');
            }

            //更新先删除旧的车源图片
            CarImagesModel::getInstance()->delete(array(array('car_id', '=', $id)));

            $carId = $id;
        }

        //添加车源图片
        foreach ((array)$images as $key => $img) {
            $row = array(
                'car_id' => $carId,
                'url'    => $img['key'],
                'status' => EnumCar::IMG_STATUS_NORMAL,
                'type'   => $key == 0 ? 1 : 0,
                'create_time' => time(),
            );
            $ret = CarImagesModel::getInstance()->insert($row);
            if (!$ret) {
                $this->_backForm($form, '添加车源图片失败');
            }
        }

        Response::redirect('/car/list/');
    }

    //添加或者编辑车源
    public function publishAction() {
        $id   = Request::getGET('id', 0);
        $info = !empty($id) ? VehicleModel::getInstance()->getRow('*', array(array('id', '=', $id))) : array();
        $modelInfo = !empty($info['model_trim']) ? CarModelsModel::getInstance()->getRow('*', array(array('model_id', '=', $info['model_trim']))) : array();
        $info['model_name'] = Util::getFromArray('model_name', $modelInfo, '');
        $info['year_name'] = $info['vehicle_year'] = Util::getFromArray('model_year', $modelInfo, '');
        $info['make_name'] = $info['make'] = Util::getFromArray('model_make_id', $modelInfo, '');
        $info['model_trim_name'] = Util::getFromArray('model_trim', $modelInfo, '');
        $info['high_lights'] = !empty($info['highlights']) ? json_decode($info['highlights']) : array();
        $formObj = new CarForm($info);
        $provinceList = LocationInterface::getProvince();

        $data = array(
            'province' => $provinceList,
            'form'     => $formObj->getForm(),
            'editInfo' => $info,
            'isEdit'   => $id ? 1 : 0,
            'states'   => UsLocationInterface::getStateList(),
        );
        if ($data['isEdit'] && !empty($modelInfo)) {
            $data['makeList'] = CarModelsModel::getInstance()->getAll('distinct model_make_id as make', array(array('model_year', '=', $info['vehicle_year'])));
            $data['modelList'] = CarModelsModel::getInstance()->getAll('model_id,model_name', array(array('model_year', '=', $info['vehicle_year']), array('model_make_id', '=', $info['make_name'])));
            $data['trimList'] = CarModelsModel::getInstance()->getAll('model_id,model_trim', array(array('model_year', '=', $info['vehicle_year']), array('model_make_id', '=', $info['make_name']), array('model_name', '=', $info['model_name'])));
        }

        if ($data['isEdit'] && !empty($info['province_id'])) {
            $data['cityList'] = UsLocationInterface::getCityByStateId(array('state_id' => $info['province_id']));
        }

        if (!empty($id)) { //编辑、去获取图片信息
            $imgInfo = CarImagesModel::getInstance()->getAll('*', array(array('car_id', '=', $id)), 'type desc');
            $images = array();
            foreach ((array)$imgInfo as $img) {
                $orgUrl = QiNiu::formatImageUrl($img['url']);
                $images[] = array(
                    'url'     => $orgUrl,
                    'org_url' => $orgUrl,
                    'key'     => $img['url'] ? $img['url'] : '',
                );
            }
            //json格式化
            $data['images'] = !empty($images) ? json_encode($images) : '';
        }

        $this->render('car/publish.php', $data);
    }

    /**
     * 车源列表页
     */
    public function listAction(){
        $this->view->assign('pageTitle', '车源列表');

        $this->render('car/list.php', array(
            'datagrid' => $this->_getDataGrid(),
        ));
    }

    /**
     * @brief 设置表格头
     * @fieldx 仅仅是个名称、可以自定义。只需要和 ajaxGetData中的 数据key对上就ok了
     * @text 表头显示的名称内容
     * @style 表头中需要显示 style样式的时候
     * @class 表头中需要添加class的时候
     * @data 表头中需要存储 data-xxx数据的时候 例如: array('a' => 100) dom上面为：data-a="100"
     */
    private function _getDataGrid() {
        $dataGrid = new DataGridWidget();
        $dataGrid->setUrl('/car/ajaxGetData/?' . $this->getQueryString());
        $field = array(
            'field1' => array('text' => 'ID', 'style' => 'width:8%'),
            'field2' => array('text' => '车源标题', 'style' => 'width:25%'),
            'field3' => array('text' => '车主信息', 'style' => 'width:15%'),
            'field4' => array('text' => '报价', 'style' => 'width:10%'),
            'field5' => array('text' => '地区', 'style' => 'width:12%'),
            'field6' => array('text' => '状态', 'style' => 'width:10%'),
            'field7' => array('text' => '操作', 'style' => 'width:20%'),
        );
        $dataGrid->setCol($field);
        return $dataGrid;
    }

    //ajax获取表格的数据
    public function ajaxGetDataAction() {
        $dataGrid = $this->_getDataGrid();
        $this->currentPage = (int) Request::getGET('page', 1); //默认第一页
        $offset  = ($this->currentPage - 1) * $this->pageSize;

        //查询条件
        $sellerName = Request::getGET('seller_name', '');
        $sellerName = urldecode($sellerName); //汉字记得 urldecode
        $phone      = Request::getGET('seller_phone', '');

        $filters = array();
        if (!empty($sellerName)) {
            $filters[] = array('last_name', '=', $sellerName);
        }
        if (!empty($phone)) {
            $filters[] = array('phone1', '=', $phone);
        }
        $carList = VehicleModel::getInstance()->getAll('*', $filters, array('create_time' => 'desc'), $this->pageSize, $offset);
        $allCount = VehicleModel::getInstance()->getCount($filters);

        $list = array();
        foreach ((array)$carList as $row) {
            $provinceName = UsLocationInterface::getStateNameById(array('id' => $row['province_id']));
            $cityName     = UsLocationInterface::getCityNameById(array('id' => $row['city_id']));
            $operate = '<a href="/car/detail/?id='.$row['id'].'" target="_blank">查看</a>';
            $operate .= ' | <a href="/car/publish/?id='.$row['id'].'" target="_self">编辑</a>';
            $operate .= ' | <a data-action-type="operate" data-width="300" data-height="200" data-type="iframe" href="javascript:;" data-title="车源下线" data-url="/car/iframeOff/?id='.$row['id'].'" target="_self">下线</a>';
            $l = array(
                'field1' => Util::getFromArray('id', $row, 0),
                'field2' => Util::getFromArray('title', $row, ''),
                'field3' => $row['last_name'] . ' ' . $row['first_name'] . '<br/>(' . Util::getFromArray('phone1', $row, '') . ')',
                'field4' => '<font style="color:orangered">' . $row['listing_price']. '</font>' . ' $',
                'field5' => $provinceName . ' - ' . $cityName,
                'field6' => Util::getFromArray($row['status'], EnumCar::$STATUS_TEXT, ''),
                'field7' => $operate,
            );
            $list[] = $l;
        }

        $dataGrid->setData($list);
        //设置分页信息
        $maxSize = $allCount > $this->maxSize ? $this->maxSize : $allCount;
        $dataGrid->setPager($maxSize, $this->currentPage, $this->pageSize);

        $data = $dataGrid->getData();
        echo json_encode(array('data' => $data));
    }

    /**
     * @param 车源详情页
     */
    public function detailAction() {
        $id = Request::getInt('id', 0);
        if (empty($id)) {
            $this->errorAction(array('msg' => '缺少车源编号、无法查看车源信息'));
        }

        $info = VehicleModel::getInstance()->getById($id);

        $provinceName = UsLocationInterface::getStateNameById(array('id' => $info['province_id']));
        $cityName     = UsLocationInterface::getCityNameById(array('id' => $info['city_id']));
        $data = array(
            'id' => Util::getFromArray('id', $info, 0),
            'title' => Util::getFromArray('title', $info, ''),
            'status_text' => Util::getFromArray($info['status'], EnumCar::$STATUS_TEXT, ''),
            'seller_name' => $info['last_name'] . ' ' . $info['first_name'],
            'seller_phone' => Util::getFromArray('phone1', $info, ''),
            'seller_price' => Util::getFromArray('listing_price', $info, ''),
            'location' => $provinceName . ' - ' . $cityName,
            'color' => $info['exterior_color'],
            'address' => Util::getFromArray('address', $info, ''),
            'miles' => Util::getFromArray('mile', $info, 0),
            'create_time' => !empty($info['create_time']) ? date("Y-m-d H:i", $info['create_time']) : '',
            'create_user' => 'ID: ' . Util::getFromArray('create_user_id', $info, ''),
            'offline_time' => !empty($info['offline_time']) ? date("Y-m-d H:i", $info['offline_time']) : '',
            'offline_reason' => Util::getFromArray('offline_reason', $info, ''),
            'vin_code' => Util::getFromArray('vin_code', $info, ''),
            'body_style' => Util::getFromArray($info['body_style'], EnumCar::$BODY_STYLE, ''),
        );

        $images = CarImagesModel::getInstance()->getAll('*', array(array('car_id', '=', $id)));
        $imgInfo = array();
        foreach ((array)$images as $img) {
            $imgInfo[] = array(
                'small_img' => QiNiu::formatImageUrl($img['url'], array('width' => 100, 'height' => 100)),
                'big_img'   => QiNiu::formatImageUrl($img['url']),
            );
        }

        $this->render('car/detail.php', array(
            'datagrid' => $this->_getDataGrid(),
            'info' => $data,
            'images' => $imgInfo,
        ));
    }

    //跳转回表单
    private function _backForm($form, $msg = '') {
        $provinceList = LocationInterface::getProvince();
        $brandList = BrandInterface::getBrandList();
        $brandData = array();
        foreach((array)$brandList as $row) {
            $brandData[$row['first_char']][] = array(
                'id'   => $row['id'],
                'name' => $row['name'],
            );
        }
        !empty($brandData) && ksort($brandData);

        $data = array(
            'province' => $provinceList,
            'brand'    => $brandData,
            'form'     => $form,
            'showMsg'  => $msg,
        );
        $this->render('car/add.php', $data);
    }

    /**
     * @brief 车源下线理由填写
     */
    public function iframeOffAction() {
        $id = Request::getGET('id', 0);
        $carInfo = !empty($id) ? VehicleModel::getInstance()->getById($id) : array();
        if (empty($id) || empty($carInfo)) {
            return $this->render('include/iframe_error.php', array(
                'errorMsg' => '缺少参数',
            ), 'include/tpl_simple.php');
            return;
        }

        $this->render('car/iframe_off.php', array(
            'id' => $id,
            'carInfo' => $carInfo,
        ), 'include/tpl_simple.php');
    }

    /**
     * @brief 车源下线提交
     */
    public function ajaxSubOfflineAction() {
        $id = Request::getPOST('id', 0);
        $reason = Request::getPOST('reason', '');

        if (empty($id) || empty($reason)) {
            return Response::outputJson(array('errorCode' => 1, 'msg' => '缺少参数'));
        }

        $upData = array(
            'status' => EnumCar::STATUS_OFFLINE,
            'offline_time' => time(),
            'offline_reason' => $reason,
        );
        VehicleModel::getInstance()->updateById($id, $upData);

        Response::outputJson(array('errorCode' => 0, 'msg' => '操作成功'));
    }
}