<?php
/**
 * @brief 公共控制器
 * @author 缪石乾<miaoshiqian@gmail.com>
 * @since 2015-12-20
 */

class CommonController extends BcController{

    //更具state_id获取城市列表
    public function ajaxGetCityAction() {
        $provinceId = Request::getPOST('province_id', 0); //这里的province_id是美国的洲id
        if (empty($provinceId)) {
            return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => array()));
        }

        $cityList = UsLocationInterface::getCityByStateId(array('state_id' => $provinceId));
        $data = array();
        foreach((array)$cityList as $row) {
            $data[] = array('id' => $row['id'], 'name' => $row['city']);
        }

        return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => $data));
    }

    //根据品牌id获取车系列表
    public function ajaxGetClassAction() {
        $brandId = Request::getPOST('brand_id', 0);
        if (empty($brandId)) {
            return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => array()));
        }

        $classList = BrandInterface::getClassList($brandId, 'id,name');
        $data = array();
        foreach((array)$classList as $row) {
            $data[] = array('id' => $row['id'], 'name' => $row['name']);
        }

        return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => $data));
    }

    //根据车系id获取年份列表
    public function ajaxGetYearAction() {
        $classId = Request::getPOST('class_id', 0);
        if (empty($classId)) {
            return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => array()));
        }

        $list = BrandInterface::getVehicleYearList($classId);
        $data = array();
        foreach((array)$list as $row) {
            $data[] = array('id' => $row['year'], 'name' => $row['year']);
        }

        return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => $data));
    }

    //根据车系和年款获取车型
    public function ajaxGetModelAction() {
        $classId = Request::getPOST('class_id', 0);
        $year    = Request::getPOST('year', 0);
        if (empty($classId) || empty($year)) {
            return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => array()));
        }

        $list = BrandInterface::getVehicleList($classId, $year);
        $data = array();
        foreach((array)$list as $row) {
            $data[] = array('id' => $row['id'], 'name' => $row['fullname']);
        }

        return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => $data));
    }

    //根据 year 获取 make列表
    public function ajaxGetMakeAction() {
        $year = Request::getPOST('year', 0);
        if (empty($year)) {
            return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => array()));
        }

        $field = 'distinct model_make_id';
        $classList = CarModelsModel::getInstance()->getAll($field, array(array('model_year', '=', $year)));
        $data = array();
        foreach((array)$classList as $row) {
            $data[] = array('name' => $row['model_make_id']);
        }

        return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => $data));
    }

    //根据 year 和 make 获取 model列表
    public function ajaxGetEnModelAction() {
        $year = Request::getPOST('year', 0);
        $make = Request::getPOST('make', '');
        if (empty($year) || empty($make)) {
            return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => array()));
        }

        $field = 'model_name, model_id';
        $classList = CarModelsModel::getInstance()->getAll($field, array(array('model_year', '=', $year), array('model_make_id', '=', $make), 'group_by' => 'model_name'));
        $data = array();
        foreach((array)$classList as $row) {
            $data[] = array('id' => $row['model_id'], 'name' => $row['model_name']);
        }

        return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => $data));
    }


    //根据year和 model 获取 trim列表
    public function ajaxGetTrimAction() {
        $year  = Request::getPOST('year', 0);
        $model = Request::getPOST('model', '');
        if (empty($year) || empty($model)) {
            return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => array()));
        }

        $field = 'model_trim, model_id';
        $classList = CarModelsModel::getInstance()->getAll($field, array(array('model_year', '=', $year), array('model_name', '=', $model)));
        $data = array();
        foreach((array)$classList as $row) {
            $data[] = array('id' => $row['model_id'], 'name' => $row['model_trim']);
        }

        return Response::outputJson(array('errorCode' => 0, 'msg' => '', 'data' => $data));
    }
}