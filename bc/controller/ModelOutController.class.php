<?php
/**
 * @brief 品牌车系管理控制器
 * @author 缪石乾<miaoshiqian@gmail.com>
 * @since 2015-12-20
 */

class ModelOutController extends BcController{

    public function defaultAction() {
        $this->IndexAction();
    }

    public function IndexAction(){
        $this->view->assign('pageTitle', '国外车型列表');

        $this->render('model_out/list.php', array(
            'datagrid' => $this->_getDataGrid(),
        ));
    }

    /**
     * @brief 设置表格头
     * @fieldx 仅仅是个名称、可以自定义。只需要和 ajaxGetData中的 数据key对上就ok了
     * @text 表头显示的名称内容
     * @style 表头中需要显示 style样式的时候
     * @class 表头中需要添加class的时候
     * @data 表头中需要存储 data-xxx数据的时候 例如: array('a' => 100) dom上面为：data-a="100"
     */
    private function _getDataGrid() {
        $dataGrid = new DataGridWidget();
        $dataGrid->setUrl('/ModelOut/ajaxGetData/?' . $this->getQueryString());
        $field = array(
            'field1' => array('text' => 'ID', 'style' => 'width:10%'),
            'field2' => array('text' => 'year', 'style' => 'width:15%'),
            'field3' => array('text' => 'make', 'style' => 'width:15%'),
            'field4' => array('text' => 'model', 'style' => 'width:10%'),
            'field5' => array('text' => 'operations', 'style' => 'width:20%'),
        );
        $dataGrid->setCol($field);
        return $dataGrid;
    }

    //ajax获取表格的数据
    public function ajaxGetDataAction() {
        $dataGrid = $this->_getDataGrid();
        $this->maxSize = 30000;
        $this->currentPage = (int) Request::getGET('page', 1); //默认第一页
        $offset  = ($this->currentPage - 1) * $this->pageSize;

        //查询条件
        $make = Request::getGET('make', 0); //品牌名称模糊查询
        $year    = Request::getGET('year', 0);
        $modelId = Request::getGET('model_id', 0);
        $name    = Request::getGET('model_name', ''); //车型名称模糊查询

        $filters = array();
        if (!empty($modelId)) {
            $filters[] = array('id', '=', $modelId);
        }
        if (!empty($make)) {
            $filters[] = array('make', 'like', '%' . $make . '%');
        }
        if (!empty($year)) {
            $filters[] = array('year', '=', $year);
        }
        if (!empty($name)) {
            $filters[] = array('model', 'like', '%' . urldecode($name) . '%');
        }

        $allCount = ModelYearModel::getInstance()->getCount($filters);
        $field = 'id, year,make,model';
        $data = ModelYearModel::getInstance()->getAll($field, $filters, array('id' => 'asc'), $this->pageSize, $offset);

        $list = array();
        foreach ((array)$data as $row) {
            $operate = '<a target="_blank" href="/modelOut/add/?id='.$row['id'].'">编辑</a>';
            $l = array(
                'field1' => Util::getFromArray('id', $row, 0),
                'field2' => $row['year'],
                'field3' => $row['make'],
                'field4' => $row['model'],
                'field5' => $operate,
            );
            $list[] = $l;
        }

        $dataGrid->setData($list);
        //设置分页信息
        $maxSize = $allCount > $this->maxSize ? $this->maxSize : $allCount;
        $dataGrid->setPager($maxSize, $this->currentPage, $this->pageSize);

        $data = $dataGrid->getData();
        echo json_encode(array('data' => $data));
    }

    /**
     * @brief 添加或编辑品牌车系
     */
    public function addAction() {
        $id = Request::getGET('id', 0);
        $modelInfo = !empty($id) ? ModelYearModel::getInstance()->getById($id) : array();
        $this->render('model_out/add.php', array(
            'data' => $modelInfo,
        ));
    }

    /**
     * @brief 添加品牌车型 提交处理
     */
    public function subModelAction() {
        $data = $_POST;
        $id = $data['id'];
        if (empty($id)) {
            exit('缺少id，无法更新');
        }

        unset($data['id']);
        ModelYearModel::getInstance()->updateById($id, $data);

        Response::redirect('/modelOut/?model_id=' . $id);
    }


}