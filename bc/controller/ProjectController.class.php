<?php
/**
 * @brief 牛人控制器
 * @author 缪石乾<miaoshiqian@gmail.com>
 * @since 2015-12-20
 */

class ProjectController extends BcController{

    public function defaultAction() {
        $this->IndexAction();
    }

    public function IndexAction(){
        $this->view->assign('pageTitle', '项目列表');

        $currentUrl = $this->getCurrentUrl();


        $this->render('index/index.php', array(
            'test' => array(
                'num' => 10,
            )
        ));
    }

    public function AddAction(){
        $this->view->assign('pageTitle', '牛人列表');

        $currentUrl = $this->getCurrentUrl();


        $this->render('index/index.php', array(
            'test' => array(
                'num' => 10,
            )
        ));
    }
}