<?php
/**
 * @brief 反馈管理控制器
 * @author 缪石乾<miaoshiqian@gmail.com>
 * @since 2015-12-20
 */

class FeedbackController extends BcController{

    public function defaultAction() {
        $this->IndexAction();
    }

    public function IndexAction(){
        $this->view->assign('pageTitle', '反馈列表');

        $this->render('feedback/list.php', array(
            'datagrid' => $this->_getDataGrid(),
        ));
    }

    /**
     * @brief 设置表格头
     * @fieldx 仅仅是个名称、可以自定义。只需要和 ajaxGetData中的 数据key对上就ok了
     * @text 表头显示的名称内容
     * @style 表头中需要显示 style样式的时候
     * @class 表头中需要添加class的时候
     * @data 表头中需要存储 data-xxx数据的时候 例如: array('a' => 100) dom上面为：data-a="100"
     */
    private function _getDataGrid() {
        $dataGrid = new DataGridWidget();
        $dataGrid->setUrl('/Feedback/ajaxGetData/?' . $this->getQueryString());
        $field = array(
            'field1' => array('text' => '头像', 'style' => 'width:10%'),
            'field2' => array('text' => '姓名', 'style' => 'width:10%'),
            'field3' => array('text' => '(反馈/回复)内容', 'style' => 'width:30%'),
            'field4' => array('text' => '(反馈/回复)时间', 'style' => 'width:20%'),
            'field5' => array('text' => '状态', 'style' => 'width:10%'),
            'field6' => array('text' => '操作', 'style' => 'width:20%'),
        );
        $dataGrid->setCol($field);
        return $dataGrid;
    }

    //ajax获取表格的数据
    public function ajaxGetDataAction() {
        $dataGrid = $this->_getDataGrid();
        $this->currentPage = (int) Request::getGET('page', 1); //默认第一页
        $offset  = ($this->currentPage - 1) * $this->pageSize;

        //查询条件
        $status    = Request::getGET('status', 0);

        $filters = array();
        if (!empty($status)) {
            $filters[] = array('status', '=', $status);
        }
        $params = array(
            'filters' => $filters,
            'limit'   => $this->pageSize,
            'offset'  => $offset,
        );
        $allCount = FeedbackInterface::getCount($params);
        $data = FeedbackInterface::getList($params);

        //获取用户信息
        $userIds = !empty($data) ? array_column($data, 'user_id') : array();
        $user    = !empty($userIds) ? UserInterface::getList(array('filters' => array(array('id', 'in', $userIds)))) : array();
        $userList = array();
        foreach (($user) as $row) {
            $userList[$row['id']] = $row;
        }

        $list = array();
        foreach ((array)$data as $row) {
            $userInfo = Util::getFromArray($row['user_id'], $userList, array());
            $photo = QiNiu::formatImageUrl(Util::getFromArray('head_url', $userInfo, ''), array('width' => 80, 'height' => 80, 'mode' => 1));
            $operate = '';
            if ($row['status'] == EnumFeedback::STATUS_WAIT_DEAL) {
                $operate .= '<a href="javascript:;" data-id="'.$row['id'].'" data-action-type="deal">处理</a>';
            }
            $cont = '反馈：' . Util::getFromArray('content', $row, '');
            $time = '反馈时间：' . (!empty($row['create_time']) ? date('Y-m-d H:i', $row['create_time']) : '');
            if (!empty($row['reply_content'])) {
                $cont .= '<br>回复：' . $row['reply_content'];
                $time .= '<br>回复时间：' . (!empty($row['reply_time']) ? date('Y-m-d H:i', $row['reply_time']) : '');
            }
            $l = array(
                'field1' => '<img class="img-circle" width="80" src="'.$photo.'"/>',
                'field2' => '<a href="/customer/detail/?id='.$row['user_id'].'" target="_blank">' . Util::getFromArray('real_name', $userInfo, '') . '</a><br>(' . Util::getFromArray('mobile', $userInfo, '') . ')',
                'field3' => $cont,
                'field4' => $time,
                'field5' => Util::getFromArray($row['status'], EnumFeedback::$STATUS_TEXT, ''),
                'field6' => $operate,
            );
            $list[] = $l;
        }

        $dataGrid->setData($list);
        //设置分页信息
        $maxSize = $allCount > $this->maxSize ? $this->maxSize : $allCount;
        $dataGrid->setPager($maxSize, $this->currentPage, $this->pageSize);

        $data = $dataGrid->getData();
        echo json_encode(array('data' => $data));
    }

    /**
     * @brief 处理弹层
     */
    public function iframeDealAction() {
        $id = Request::getGET('id');
        if (empty($id)) {
            echo '缺少id参数';
            return;
        }

        $this->render('feedback/iframe_deal.php', array(
            'id' => $id,
        ), 'include/tpl_simple.php');
    }

    /**
     * @brief 设为已处理
     */
    public function ajaxDealAction() {
        $id = Request::getPOST('id');
        $cont = Request::getPOST('cont', '');
        if (empty($id)) {
            Response::outputJson(array('errorCode' => 1, 'msg' => '缺少id'));
        }
        try {
            FeedbackInterface::deal(array('id' => $id, 'cont' => $cont, 'user_id' => $this->getUserId()));
        } catch (Exception $e) {
            Response::outputJson(array('errorCode' => 1, 'msg' => $e->getMessage()));
        }

        Response::outputJson(array('errorCode' => 0, 'msg' => '处理成功'));
    }
}