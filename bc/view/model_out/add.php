<div style="width:950px;margin-left:50px;">
    <br/>
    <div class="box box-info" style="margin-bottom: 0px;">

        <div class="box-body">
            <form id="form" method="post" enctype="multipart/form-data" action="/ModelOut/subModel/">
                <table class="table" style="border:none;">
                    <tbody>
                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">make：</span>
                                <input name="make" value="<?php echo Util::getFromArray('make', $this->data, '');?>" placeholder="Enter the make name" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">year：</span>
                                <input name="year" value="<?php echo Util::getFromArray('year', $this->data, '');?>" placeholder="Enter the year" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">model：</span>
                                <input name="model" value="<?php echo Util::getFromArray('model', $this->data, '');?>" placeholder="Enter the model" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">颜色：</span>
                                <input name="color" value="<?php echo Util::getFromArray('color', $this->data, '');?>" placeholder="Enter the color" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">变速箱：</span>
                                <input name="geearbox" value="<?php echo Util::getFromArray('geearbox', $this->data, '');?>" placeholder="Enter the geearbox" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">车门数：</span>
                                <input name="door" value="<?php echo Util::getFromArray('door', $this->data, '');?>" placeholder="Enter the door" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">座位数：</span>
                                <input name="seat" value="<?php echo Util::getFromArray('seat', $this->data, '');?>" placeholder="Enter the seat" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">长度(mm)：</span>
                                <input name="length" value="<?php echo Util::getFromArray('length', $this->data, '');?>" placeholder="Enter the length" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">宽度(mm)：</span>
                                <input name="width" value="<?php echo Util::getFromArray('width', $this->data, '');?>" placeholder="Enter the width" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">高度(mm)：</span>
                                <input name="height" value="<?php echo Util::getFromArray('height', $this->data, '');?>" placeholder="Enter the height" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">行李箱容积(L)：</span>
                                <input name="volume" value="<?php echo Util::getFromArray('volume', $this->data, '');?>" placeholder="Enter the volume" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">轮毂材料：</span>
                                <input name="hub" value="<?php echo Util::getFromArray('hub', $this->data, '');?>" placeholder="Enter the hub" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">轴距(mm)：</span>
                                <input name="wheelbase" value="<?php echo Util::getFromArray('wheelbase', $this->data, '');?>" placeholder="Enter the wheelbase" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">整备质量(kg)：</span>
                                <input name="quality" value="<?php echo Util::getFromArray('quality', $this->data, '');?>" placeholder="Enter the quality" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">百公里油耗(L)：</span>
                                <input name="oil_100" value="<?php echo Util::getFromArray('oil_100', $this->data, '');?>" placeholder="Enter the oil_100" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">排量：</span>
                                <input name="displacement" value="<?php echo Util::getFromArray('displacement', $this->data, '');?>" placeholder="Enter the displacement" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">油箱容积(L)：</span>
                                <input name="oil_volume" value="<?php echo Util::getFromArray('oil_volume', $this->data, '');?>" placeholder="Enter the oil_volume" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">进气形式：</span>
                                <input name="inlet_style" value="<?php echo Util::getFromArray('inlet_style', $this->data, '');?>" placeholder="Enter the inlet_style" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">最大扭矩(n.m)：</span>
                                <input name="torque" value="<?php echo Util::getFromArray('torque', $this->data, '');?>" placeholder="Enter the torque" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">燃油类型：</span>
                                <input name="oil_type" value="<?php echo Util::getFromArray('oil_type', $this->data, '');?>" placeholder="Enter the oil_type" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">燃油标号：</span>
                                <input name="oil_mark" value="<?php echo Util::getFromArray('oil_mark', $this->data, '');?>" placeholder="Enter the oil_mark" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">排放标准：</span>
                                <input name="emission" value="<?php echo Util::getFromArray('emission', $this->data, '');?>" placeholder="Enter the emission" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">功率(kw)：</span>
                                <input name="power" value="<?php echo Util::getFromArray('power', $this->data, '');?>" placeholder="Enter the power" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">最高车速(km/h)：</span>
                                <input name="high_speed" value="<?php echo Util::getFromArray('high_speed', $this->data, '');?>" placeholder="Enter the high_speed" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">百公里加速度(秒)：</span>
                                <input name="acceleration" value="<?php echo Util::getFromArray('acceleration', $this->data, '');?>" placeholder="Enter the acceleration" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">驱动方式：</span>
                                <input name="drive_type" value="<?php echo Util::getFromArray('drive_type', $this->data, '');?>" placeholder="Enter the drive_type" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">转向系统：</span>
                                <input name="steering_system" value="<?php echo Util::getFromArray('steering_system', $this->data, '');?>" placeholder="Enter the steering_system" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">前悬挂：</span>
                                <input name="pre_suspension" value="<?php echo Util::getFromArray('pre_suspension', $this->data, '');?>" placeholder="Enter the pre_suspension" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">后悬挂：</span>
                                <input name="af_suspension" value="<?php echo Util::getFromArray('af_suspension', $this->data, '');?>" placeholder="Enter the af_suspension" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">前制动：</span>
                                <input name="pre_braking" value="<?php echo Util::getFromArray('pre_braking', $this->data, '');?>" placeholder="Enter the pre_braking" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">后制动：</span>
                                <input name="af_braking" value="<?php echo Util::getFromArray('af_braking', $this->data, '');?>" placeholder="Enter the af_braking" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">前轮胎规格(mm)：</span>
                                <input name="pre_tyre" value="<?php echo Util::getFromArray('pre_tyre', $this->data, '');?>" placeholder="例如：5/55 R16" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 100px;text-align: right;">后轮胎规格(mm)：</span>
                                <input name="af_tyre" value="<?php echo Util::getFromArray('af_tyre', $this->data, '');?>" placeholder="例如：5/55 R16" type="text" class="form-control" style="width:300px;">
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding-left: 100px;" style="border: none;">
                            <input type="hidden" name="id" value="<?php echo Util::getFromArray('id', $this->data);?>">
                            <input type="submit" class="btn btn-success btn-flat" value="Submit">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>

<div style="clear:both;"></div>

<script type="text/javascript">
    G.use(['validator', 'jquery'], function (v, $) {
        G.use(['widget/form/form.js', 'widget/form/form.css', 'com/backend/js/backend.js', 'widget/imageUploader/image_uploader.js'], function (Form, Css, Backend, Uploader) {

        });
    });
</script>
