<div class="search-form-style">
    <form method="get" action="#">
        model name：
        <input type="text" value="<?php echo Request::getGET('model_id', ''); ?>" name="model_id" class="form-control" placeholder="search by model id" style="width:150px;display:inline-block">
        model name：
        <input type="text" value="<?php echo Request::getGET('model_name', ''); ?>" name="model_name" class="form-control" placeholder="search by model name" style="width:150px;display:inline-block">
        make：
        <input type="text" value="<?php echo Request::getGET('make', ''); ?>" name="make" class="form-control" placeholder="search by make" style="width:150px;display:inline-block">
        year：
        <input type="text" value="<?php echo Request::getGET('year', ''); ?>" name="year" class="form-control" placeholder="search by year" style="width:150px;display:inline-block">
        <button data-action-type="grid-search" class="btn btn-success btn-flat">Search</button>
        &nbsp;&nbsp;&nbsp;
        <button data-action-type="operate" data-type="iframe" data-width="450" data-height="550" data-url="/model/iframeAdd/" data-title="添加品牌车型" class="btn btn-info btn-flat">Add Make</button>
    </form>
</div>
<?php echo $this->datagrid->toHTML(); //不传id，默认用 datagrid-1 ?>
<?php //echo $datagrid->toHTML(array('id' => 'datagrid-1', 'class' => 'no_pager')); //不显示分页?>
<?php //echo $datagrid->toHTML(array('id' => 'datagrid-1', 'class' => 'delay_load')); //暂时不发起异步查询，后续触发指定事件后再发起异步查询?>
<div style="clear:both;"></div>

<script type="text/javascript">
    G.use(['com/backend/js/backend.js', 'jquery', 'widget/dialog/dialog.js'], function (Backend, $, Dialog) {
        Backend.run();
        Backend.widget('#datagrid-1').ready(function () {
            var grid = this;
            grid.bindSearch('grid-search');
        });

        Backend.on('operate', function (e) {
            e.preventDefault();
            var $this = $(this);
            var url  = $this.data('url');
            var type = $this.data('type');
            if (type == 'ajax') {
                $this.addClass('disabled');
                $.ajax({
                    url : url,
                    type : "GET",
                    dataType : "json",
                    success : function(result) {
                        if (result.errorCode) {
                            //失败
                            Backend.trigger('alert-error', result.msg);
                            $this.removeClass('disabled');
                        } else {
                            //成功
                            Backend.trigger('alert-success', result.msg);
                            window.setTimeout(function() {
                                Backend.widget('#datagrid-' + localStorage.getItem('backStock_tab')).ready(function () {
                                    var grid = this;
                                    grid.load();
                                    grid.bindSearch('grid-search-' + localStorage.getItem('backStock_tab'));
                                });
                            }, 2000);
                        }
                    },
                    error : function(XMLHttpRequest, textStatus, errorThrown) {
                        Backend.trigger('alert-error', "内容提交失败!");
                        $this.removeClass('disabled');
                    }
                });
            } else if (type == 'iframe') {
                var $width  = $this.data('width');
                var $height = $this.data('height');
                var dialog  = new Dialog({
                    title : $this.data('title'),
                    width : $width || 500,
                    height: $height || 400,
                    skin : 'gray',
                    close : function() {
                        var grid = $this.data('grid');
                        grid = grid || '1';
                        Backend.widget('#datagrid-' + grid).ready(function () {
                            var grid = this;
                            grid.load();
                            grid.bindSearch('grid-search-1');
                        });
                    }
                });
                dialog.ready(function () {
                    this.open(url);
                });
                window.dialog = dialog;
            }
        });
    });
</script>
