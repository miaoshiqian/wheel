<div class="search-form-style">
    <form method="get" action="#">
        姓名：
        <input type="text" value="<?php echo Request::getGET('real_name', ''); ?>" name="real_name" class="form-control" placeholder="筛选姓名" style="width:150px;display:inline-block">
        手机号：
        <input type="text" value="<?php echo Request::getGET('mobile', ''); ?>" name="mobile" class="form-control" placeholder="筛选手机号" style="width:150px;display:inline-block">
        <!--<input type="submit" class="btn btn-success" value="查询" />-->
        <button data-action-type="grid-search" class="btn btn-success">查询</button>
    </form>
</div>
<?php echo $this->datagrid->toHTML(); //不传id，默认用 datagrid-1 ?>
<?php //echo $datagrid->toHTML(array('id' => 'datagrid-1', 'class' => 'no_pager')); //不显示分页?>
<?php //echo $datagrid->toHTML(array('id' => 'datagrid-1', 'class' => 'delay_load')); //暂时不发起异步查询，后续触发指定事件后再发起异步查询?>
<div style="clear:both;"></div>

<script type="text/javascript">
    G.use(['com/backend/js/backend.js', 'jquery', 'tableSort'], function (Backend, $, Sort) {
        Backend.run();
        Backend.widget('#datagrid-1').ready(function () {
            var grid = this;
            grid.bindSearch('grid-search');
            //表格排序
            /*grid.$table.tableSort({
                indexes: [4]
            });*/
        });
        Backend.on('frozen_op', function() {
            $.ajax({
                url : '/customer/ajaxFrozenOp/',
                type : "POST",
                dataType : "json",
                data : {
                    "user_id" : $(this).data('id')
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            Backend.widget('#datagrid-1').ready(function () {
                                this.load();
                            });
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });
        //删除用户
        Backend.on('del', function() {
            $.ajax({
                url : '/customer/ajaxDelUser/',
                type : "POST",
                dataType : "json",
                data : {
                    "user_id" : $(this).data('id')
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            Backend.widget('#datagrid-1').ready(function () {
                                this.load();
                            });
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });
    });
</script>
