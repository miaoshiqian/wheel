<div style="padding-bottom: 10px;padding-top: 10px;">
    <button class="btn btn-flat btn-info" data-action-type="frozen_op" data-id="<?=$this->data['id'];?>">
        <?php echo $this->data['status'] == EnumUser::USER_STATUS_FROZEN ? '解冻' : '冻结';?>
    </button>
    <button class="btn btn-flat btn-info" data-action-type="del" data-id="<?=$this->data['id'];?>">删除</button>
</div>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-fw fa-info-circle"></i> 基本信息</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <!-- 基本信息-->
            <h4><i class="fa fa-fw fa-cog"></i> 基础信息</h4>
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td style="text-align:right;width:150px;">头像：</td>
                    <td style="text-align:left;width:200px;font-weight: bold;"><img class="img-circle" width="80" src="<?= $this->data['head_url'];?>"/></td>
                    <td style="text-align:right;width:100px;">状态：</td>
                    <td style="text-align: left;font-weight: bold;"><?= Util::getFromArray($this->data['status'], EnumUser::$USER_STATUS, '');?></td>
                </tr>

                <tr>
                    <td style="text-align:right;width:150px;">姓名：</td>
                    <td style="text-align:left;width:200px;font-weight: bold;"><?= $this->data['real_name'];?></td>
                    <td style="text-align:right;">手机：</td>
                    <td style="text-align: left;font-weight: bold;"><?= $this->data['mobile'];?></td>
                </tr>

                <tr>
                    <td style="text-align:right;width:150px;">昵称：</td>
                    <td style="text-align:left;width:200px;font-weight: bold;"><?= $this->data['nick_name'];?></td>
                    <td style="text-align:right;">性别：</td>
                    <td style="text-align: left;font-weight: bold;"><?=Util::getFromArray($this->data['sex'], EnumUser::$SEX_TEXT, ''); ?></td>
                </tr>

                <tr>
                    <td style="text-align:right;width:150px;">城市：</td>
                    <td style="text-align:left;width:200px;font-weight: bold;"><?= Util::getFromArray('province_city', $this->data, '');?></td>
                    <td style="text-align:right;">生日：</td>
                    <td style="text-align: left;font-weight: bold;"><?php echo !empty($this->data['birthday']) ? date('Y-m-d', $this->data['birthday']) : '';?></td>
                </tr>

                <tr>
                    <td style="text-align:right;width:150px;">职业：</td>
                    <td style="text-align:left;width:200px;font-weight: bold;"><?= Util::getFromArray($this->data['profession'], EnumUser::$PROFESSION_TEXT, '');?></td>
                    <td style="text-align:right;">公司/职位：</td>
                    <td style="text-align: left;font-weight: bold;"><?= Util::getFromArray('company', $this->data, '') . '--' . Util::getFromArray('position', $this->data, '');?></td>
                </tr>

                <tr>
                    <td style="text-align:right;width:150px;">添加时间：</td>
                    <td style="text-align:left;width:200px;font-weight: bold;"><?php echo !empty($this->data['create_time']) ? date('Y-m-d', $this->data['create_time']) : '';?></td>
                    <td style="text-align:right;">更新时间：</td>
                    <td style="text-align: left;font-weight: bold;"><?php echo !empty($this->data['update_time']) ? date('Y-m-d', $this->data['update_time']) : '';?></td>
                </tr>

                <tr>
                    <td style="text-align:right;width:150px;">简介：</td>
                    <td colspan="3" style="text-align:left;width:200px;font-weight: bold;"><?= Util::getFromArray('brief', $this->data, '');?></td>
                </tr>

                </tbody>
            </table>

        </div><!-- /.tab-pane -->

    </div><!-- /.tab-content -->
</div>

<script type="text/javascript">
    G.use(['com/backend/js/backend.js', 'jquery', 'tableSort'], function (Backend, $, Sort) {
        Backend.run();
        Backend.on('frozen_op', function() {
            var $this = $(this);
            $this.addClass('disabled');
            $.ajax({
                url : '/customer/ajaxFrozenOp/',
                type : "POST",
                dataType : "json",
                data : {
                    "user_id" : $(this).data('id')
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                           location.reload();
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });
        //删除用户
        Backend.on('del', function() {
            var $this = $(this);
            $this.addClass('disabled');
            $.ajax({
                url : '/customer/ajaxDelUser/',
                type : "POST",
                dataType : "json",
                data : {
                    "user_id" : $(this).data('id')
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });
    });
</script>