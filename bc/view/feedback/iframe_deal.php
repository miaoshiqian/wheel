<div class="form-group" style="text-align: center;">
    <br>
    <textarea placeholder="在这里输入回复内容......" id="reply" rows="5" cols="50"></textarea>
    <input type="hidden" name="id" value="<?php echo $this->id;?>">
    <br>
    <button data-action-type="sub" style="margin: auto;" class="btn btn-flat btn-success">提交处理</button>
</div>

<script type="text/javascript">
    G.use(['com/backend/js/backend.js', 'jquery'], function (Backend, $) {
        Backend.run();
        Backend.on('sub', function(e) {
            var $this = $(this);
            $this.addClass('disabled');
            $.ajax({
                url : '/feedback/ajaxDeal/',
                type : "POST",
                dataType : "json",
                data : {
                    "id" : $('input[name="id"]').val(),
                    "cont" : $('#reply').val()
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            parent.dialog.close();
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });
    });
</script>