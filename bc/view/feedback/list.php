<div class="search-form-style">
    <form method="get" action="#">
        状态：
        <select name="status" class="form-control" style="width:150px;display:inline-block">
            <option value="0">选择状态</option>
            <?php foreach((array) EnumFeedback::$STATUS_TEXT as $val => $text) {?>
                <option value="<?=$val?>"><?=$text?></option>
            <?php }?>
        </select>
        <!--<input type="submit" class="btn btn-success" value="查询" />-->
        <button data-action-type="grid-search" class="btn btn-success">查询</button>
    </form>
</div>
<?php echo $this->datagrid->toHTML(); //不传id，默认用 datagrid-1 ?>
<div style="clear:both;"></div>

<script type="text/javascript">
    G.use(['com/backend/js/backend.js', 'jquery', 'widget/dialog/dialog.js'], function (Backend, $, Dialog) {
        Backend.run();
        Backend.widget('#datagrid-1').ready(function () {
            var grid = this;
            grid.bindSearch('grid-search');
        });
        //处理反馈
        Backend.on('deal', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var url = "/Feedback/iframeDeal/?id="+id;
            var dialog = new Dialog({
                title : '处理反馈',
                width : 350,
                height: 200,
                skin : 'gray',
                close : function() {
                    Backend.widget('#datagrid-1').ready(function () {
                        this.load();
                    });
                }
            });
            dialog.ready(function () {
                this.open(url);
            });
            window.dialog = dialog;

            /*$.ajax({
                url : '/feedback/ajaxDeal/',
                type : "POST",
                dataType : "json",
                data : {
                    "id" : $(this).data('id')
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            Backend.widget('#datagrid-1').ready(function () {
                                this.load();
                            });
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });*/
        });
    });
</script>
