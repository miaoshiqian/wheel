<!DOCTYPE html>
<html lang="zh-cn">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title><?php echo $this->pageTitle ?: '后台管理系统'; ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <script type="text/javascript" src="http://<?php echo GlobalConfig::JS_DOMAIN;?>/g/g.js"></script>
    <script type="text/javascript" src="http://<?php echo GlobalConfig::JS_DOMAIN;?>/g/config.js"></script>
    <script type="text/javascript">
        G.use('com/lte/css/bc.cmb.css');
        G.use('com/lte/css/font-awesome.min.css');
        G.use('com/backend/js/bootstrap.min.js');
        G.use('com/lte/js/app.js');
    </script>
</head>

<body class="hold-transition skin-blue sidebar-mini <?php echo !empty($mini_menu_class) ? ' sidebar-collapse' : '';?>">
    <div class="wrapper">

        <!-- header开始-->
        <header class="main-header">
            <!-- Logo -->
            <a href="/" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>BC</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>后台管理系统</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation" >
                <!-- Sidebar toggle button-->
                <a href="javascript:;" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <input type='hidden' id="main-content"/><!--就header中有这个 隐藏域，保留-->
                        <!-- Messages: style can be found in dropdown.less-->

                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="http://<?php echo GlobalConfig::JS_DOMAIN;?>/build/com/lte/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?php $this->put($this->globalUserName) ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="http://<?php echo GlobalConfig::JS_DOMAIN;?>/build/com/lte/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                                    <p>
                                        后台管理
                                        <small>再接再励把业务做好！</small>
                                    </p>
                                </li>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="col-xs-5 text-center">
                                        <a href="/customer/myCustomer"></a>
                                    </div>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a id='changePassword()'  href="/editCustomerMessage/changePwd" class="btn btn-default btn-flat">修改信息</a>
                                    </div>
                                    <div class="pull-right">
                                        <a id='loginout()'  href="http://sso.<?php echo GlobalConfig::BC_COOKIE_DOMAIN;?>/user/logout" class="btn btn-default btn-flat">退出</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button 右侧的设置按钮,暂时隐藏没用 -->
                        <!--
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>
                        -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- header结束-->


        <!--菜单开始-->
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar" style="width: <?php echo !empty($this->menuWidth) ? $this->menuWidth.'px' : '';?>">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="http://<?php echo GlobalConfig::JS_DOMAIN;?>/build/com/lte/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p><?php $this->put($this->globalUserName) ?></p>
                        <a href="javascript:;"><i class="fa fa-circle text-success"></i> 工作中</a>
                    </div>
                </div>


                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <?php if (!isset($menu_level1) && !empty($this->globalAllMenu) && is_array($this->globalAllMenu)) {?>
                        <li class="header">系统菜单</li>
                        <?php foreach ($this->globalAllMenu as $menu) {?>
                            <li class="<?php echo !empty($menu['open']) ? 'active' : '';?> treeview">
                                <a href="javascript:;">
                                    <i class="<?php echo !empty($menu['ico']) ? $menu['ico'] : 'fa fa-dashboard';?>"></i>
                                    <span><?=$menu['text'];?></span>
                                    <i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <?php if (!empty($menu['child']) && is_array($menu['child'])) {?>
                                    <ul class="treeview-menu">
                                        <?php foreach ($menu['child'] as $m) {?>
                                            <li <?php echo !empty($m['open']) ? 'class="active"' : '';?>>
                                                <a href="<?=$m['url'];?>">
                                                    <i class="fa <?php echo !empty($m['open']) ? 'fa-check-circle text-green' : 'fa-circle-o';?>"></i>
                                                    <?=$m['text'];?>
                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>
                                <?php }?>
                            </li>
                        <?php }?>
                    <?php }?>

                    <!-- 单级标签链接-->
                    <?php if (!empty($menu_level1) && is_array($menu_level1)) {?>
                        <li class="header">单级菜单</li>
                        <?php foreach ($menu_level1 as $url => $m) {
                            $m['ico_default'] = empty($m['ico']) ? 'fa-circle-o' : $m['ico'];
                            $m['ico_select'] = empty($m['ico']) ? 'fa-check-circle' : $m['ico'];
                            ?>
                            <li>
                                <a href="<?=$url;?>">
                                    <i class="<?php echo trim($uri, '/') == $m['uri'] ? "fa {$m['ico_select']} text-green" : "fa {$m['ico_default']}";?>"></i>
                                    <span><?=$m['title'];?></span>
                                </a>
                            </li>
                        <?php }?>

                    <?php }?>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
        <!-- 菜单结束-->

        <!-- 主体内容渲染-->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" style="padding-left:10px;min-height: 100vh;margin-left: <?php echo !empty($this->menuWidth) ? $this->menuWidth.'px' : '';?>">
        <?php $this->load($this->globalRenderFile); ?>
        </div>
        <!-- 主体内容渲染结束-->

        <footer class="main-footer" style="margin-left: <?php echo !empty($this->menuWidth) ? $this->menuWidth.'px' : '';?>">
            <div class="pull-right hidden-xs">
                <b>版本：</b> 1.0
            </div>
            <strong>Copyright &copy; 2015-2018 <a href="/">bc.qcb.com</a>.</strong> All rights reserved.
        </footer>
    </div><!-- wraper over-->
    <script type="text/javascript">
        G.use(['jquery', 'util/cookie.js'], function ($, Cookie) {
            $('.sidebar-toggle').click(function(){
                var $body = $('body');
                if ($body.hasClass('sidebar-collapse')) {
                    Cookie.set('mini_menu_class', 1, {
                        expires : 100, //100天
                        path : '/',
                        domain : 'qcb.com'
                    });
                } else {
                    Cookie.set('mini_menu_class', 0, {
                        expires : 100, //100天
                        path : '/',
                        domain : 'qcb.com'
                    });
                }
            });
        });
    </script>
</body>
</html>
