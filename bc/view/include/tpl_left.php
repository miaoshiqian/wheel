<!-- 左侧菜单 -->
<div id="sidebar" class="sidebar responsive ace-save-state">

	<ul class="nav nav-list">
		<?php $this->put($this->helper('menu', $this->globalAllMenu), 1); ?>
	</ul><!-- /.nav-list -->

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
</div>

