<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <title><?php $this->put(sprintf("%s - %s", $this->pageTitle, $this->globalCurrentModuleInfo['text'])); ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <script type="text/javascript" src="http://<?php echo GlobalConfig::JS_DOMAIN;?>/g/g.js"></script>
    <script type="text/javascript" src="http://<?php echo GlobalConfig::JS_DOMAIN;?>/g/config.js"></script>
    <script type="text/javascript">
        G.use('com/lte/css/bc.cmb.css');
        G.use('com/lte/css/font-awesome.min.css');
        G.use('com/backend/js/bootstrap.min.js');
        G.use('com/lte/js/app.js');
    </script>
</head>

<body>
<div class="main-container">
	<div class="main-content">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 center">
				<?php $this->load($this->globalRenderFile); ?>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.main-content -->
</div><!-- /.main-container -->

</body>
</html>