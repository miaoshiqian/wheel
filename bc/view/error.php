<br><br><br><br>
<h2 class="font-bold">
	<i class="ace-icon fa fa-close red2"></i>
	出错了
</h2>
<br><br>
<p>
	<?php $this->put($this->message); ?>
</p>
<br><br>
<div class="form-actions center">
	<a href="<?php $this->put($this->jumpUrl); ?>" class="btn btn-sm btn-success">
		点击跳转
		<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
	</a>
</div>

