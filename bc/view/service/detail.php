<div style="padding-bottom: 10px;padding-top: 10px;">
    <button class="btn btn-flat btn-info" data-action-type="frozen_op" data-id="<?=$this->data['id'];?>">
        <?php echo $this->data['status'] == EnumService::SERVICE_STATUS_FROZEN ? '解冻' : '冻结';?>
    </button>
    <button class="btn btn-flat btn-info" data-action-type="del" data-id="<?=$this->data['id'];?>">删除</button>
</div>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-fw fa-info-circle"></i> 基本信息</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <!-- 基本信息-->
            <h4><i class="fa fa-fw fa-cog"></i> 基础信息</h4>
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td style="text-align:right;width:150px;">服务名称：</td>
                    <td style="text-align:left;width:200px;font-weight: bold;"><?= $this->data['name'];?></td>
                    <td style="text-align:right;width:100px;">服务状态：</td>
                    <td style="text-align: left;font-weight: bold;"><?= Util::getFromArray($this->data['status'], EnumService::$SERVICE_STATUS, '');?></td>
                </tr>

                <tr>
                    <td style="text-align:right;width:150px;">服务类型：</td>
                    <td style="text-align:left;width:200px;font-weight: bold;">
                        <?= Util::getFromArray($this->data['type'], EnumService::$SERVICE_TYPE, '');?>
                    </td>
                    <td style="text-align:right;">服务价格：</td>
                    <td style="text-align: left;font-weight: bold;">
                        价格：<font class="text-red"> <?=$this->data['price'] / 100;?></font> 元
                        &nbsp;&nbsp;&nbsp;
                        股权支付：<font class="text-red"> <?=$this->data['stake_price'] / 100;?></font> 元
                    </td>
                </tr>

                <tr>
                    <td style="text-align:right;width:150px;">服务者姓名：</td>
                    <td style="text-align:left;width:200px;font-weight: bold;"><?=Util::getFromArray('real_name', $this->userInfo, ''); ?></td>
                    <td style="text-align:right;">手机：</td>
                    <td style="text-align: left;font-weight: bold;"><?=Util::getFromArray('mobile', $this->userInfo, ''); ?></td>
                </tr>

                <tr>
                    <td style="text-align:right;width:150px;">服务城市：</td>
                    <td style="text-align:left;width:200px;font-weight: bold;"><?=$this->data['service_city'];?></td>
                    <td style="text-align:right;">空闲时间：</td>
                    <td style="text-align: left;font-weight: bold;"><?=$this->data['free_time'];?></td>
                </tr>

                <tr>
                    <td style="text-align:right;width:150px;">服务承诺：</td>
                    <td colspan="3" style="text-align:left;width:200px;font-weight: bold;"><?=$this->data['promise'];?></td>
                </tr>

                <tr>
                    <td style="text-align:right;width:150px;">服务描述：</td>
                    <td colspan="3" style="text-align:left;width:200px;font-weight: bold;"><?=$this->data['desc'];?></td>
                </tr>

                </tbody>
            </table>

        </div><!-- /.tab-pane -->

    </div><!-- /.tab-content -->
</div>

<script type="text/javascript">
    G.use(['com/backend/js/backend.js', 'jquery', 'tableSort'], function (Backend, $, Sort) {
        Backend.run();
        Backend.on('frozen_op', function() {
            var $this = $(this);
            $this.addClass('disabled');
            $.ajax({
                url : '/service/ajaxFrozenOp/',
                type : "POST",
                dataType : "json",
                data : {
                    "service_id" : $(this).data('id')
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });
        //删除用户
        Backend.on('del', function() {
            var $this = $(this);
            $this.addClass('disabled');
            $.ajax({
                url : '/service/ajaxDelService/',
                type : "POST",
                dataType : "json",
                data : {
                    "service_id" : $(this).data('id')
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });
    });
</script>
