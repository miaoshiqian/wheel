<div class="search-form-style">
    <form method="get" action="#">
        服务名称：
        <input type="text" value="<?php echo Request::getGET('name', ''); ?>" name="name" class="form-control" placeholder="按服务名称筛选" style="width:200px;display:inline-block">
        服务类型：
        <select name="type" class="form-control" style="width:150px;display:inline-block">
            <option value="0">选择类型</option>
            <?php foreach((array) EnumService::$SERVICE_TYPE as $val => $text) {?>
                <option value="<?=$val?>"><?=$text?></option>
            <?php }?>
        </select>
        服务状态：
        <select name="status" class="form-control" style="width:150px;display:inline-block">
            <option value="0">选择状态</option>
            <?php foreach((array) EnumService::$SERVICE_STATUS as $val => $text) {?>
                <option value="<?=$val?>"><?=$text?></option>
            <?php }?>
        </select>
        <button data-action-type="grid-search" class="btn btn-success">查询</button>
    </form>
</div>
<?php echo $this->datagrid->toHTML(); //不传id，默认用 datagrid-1 ?>
<div style="clear:both;"></div>

<script type="text/javascript">
    G.use(['com/backend/js/backend.js', 'jquery', 'tableSort'], function (Backend, $, Sort) {
        Backend.run();
        Backend.widget('#datagrid-1').ready(function () {
            var grid = this;
            grid.bindSearch('grid-search');
        });
        Backend.on('frozen_op', function() {
            $.ajax({
                url : '/service/ajaxFrozenOp/',
                type : "POST",
                dataType : "json",
                data : {
                    "service_id" : $(this).data('id')
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            Backend.widget('#datagrid-1').ready(function () {
                                this.load();
                            });
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });
        //删除用户
        Backend.on('del', function() {
            $.ajax({
                url : '/service/ajaxDelService/',
                type : "POST",
                dataType : "json",
                data : {
                    "service_id" : $(this).data('id')
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            Backend.widget('#datagrid-1').ready(function () {
                                this.load();
                            });
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });
    });
</script>
