<style>
    body {background-color: #fff;}
</style>

<div class="box box-info" style="border-top: none;">
    <!-- form start -->
    <form class="form-horizontal">
        <div class="box-body">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">添加类型</label>
                <div class="col-sm-4" style="display: inline;">
                    <select id="type" name="type" class="form-control" style="display: inline;width: 250px;">
                        <option value="1">添加品牌</option>
                        <option value="2">添加车系</option>
                        <option value="3">添加车型</option>
                    </select>
                </div>
            </div>

            <div class="form-group" style="display: none;" id="brand-select-block">
                <label for="inputEmail3" class="col-sm-2 control-label">选择品牌</label>
                <div class="col-sm-4" style="display: inline;">
                    <select id="js-brand" class="form-control" style="width: 250px;display: inline;" name="brand_id">
                        <option value="0">选择品牌</option>
                        <?php foreach((array)$this->brand as $char => $charList) {?>
                            <option value=""><?=$char;?></option>
                            <?php foreach((array)$charList as $row) {?>
                                <option value="<?=$row['id'];?>"><?php echo $row['name'];?></option>
                            <?php }?>
                        <?php }?>
                    </select>
                </div>
            </div>

            <div class="form-group" style="display: none;" id="class-select-block">
                <label for="inputEmail3" class="col-sm-2 control-label">选择车系</label>
                <div class="col-sm-4" style="display: inline;">
                    <select id="js-class" class="form-control" style="width: 250px;display: inline;" name="class_id">
                        <option value="0">选择车系</option>
                    </select>
                </div>
            </div>

            <div id="brand-block">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">中文品牌</label>
                    <div class="col-sm-4" style="display: inline;">
                        <input type="text" value="" name="brand_name" style="display: inline;width: 250px;" class="form-control" placeholder="中文品牌名称">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">英文品牌</label>
                    <div class="col-sm-4" style="display: inline;">
                        <input type="text" value="" name="en_brand_name" style="display: inline;width: 250px;" class="form-control" placeholder="英文品牌名称">
                    </div>
                </div>
                <hr>
            </div>

            <div id="class-block" style="display: none;">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">中文车系</label>
                    <div class="col-sm-4" style="display: inline;">
                        <input type="text" value="" name="class_name" style="display: inline;width: 250px;" class="form-control" placeholder="中文车系名称">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">英文车系</label>
                    <div class="col-sm-4" style="display: inline;">
                        <input type="text" value="" name="en_class_name" style="display: inline;width: 250px;" class="form-control" placeholder="英文车系名称">
                    </div>
                </div>
                <hr>
            </div>

            <div id="model-block" style="display: none;">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">车辆年款</label>
                    <div class="col-sm-4" style="display: inline;">
                        <input type="text" value="" name="year" style="display: inline;width: 250px;" class="form-control" placeholder="车辆年款">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">中文车型</label>
                    <div class="col-sm-4" style="display: inline;">
                        <input type="text" value="" name="model_short_name" style="display: inline;width: 250px;" class="form-control" placeholder="中文车型名称">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">英文车型</label>
                    <div class="col-sm-4" style="display: inline;">
                        <input type="text" value="" name="en_model_short_name" style="display: inline;width: 250px;" class="form-control" placeholder="中文车型名称">
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <button data-action-type="sub" class="btn btn-info">提交保存</button>
        </div><!-- /.box-footer -->
    </form>
</div>

<script type="text/javascript">
    G.use(['com/backend/js/backend.js', 'jquery'], function (Backend, $) {
        Backend.run();
        Backend.on('sub', function(e) {
            e.preventDefault();
            var $this = $(this);
            $this.addClass('disabled');
            var type = $('select[name="type"]').val();
            var $data = {};
            if (type == 1) {
                $data = {
                    "type" : type,
                    "brand_name" : $('input[name="brand_name"]').val(),
                    "en_brand_name" : $('input[name="en_brand_name"]').val()
                };
            } else if (type == 2) {
                $data = {
                    "type" : type,
                    "brand_id" : $('select[name="brand_id"]').val(),
                    "class_name"    : $('input[name="class_name"]').val(),
                    "en_class_name"    : $('input[name="en_class_name"]').val()
                };
            } else if (type == 3) {
                $data = {
                    "type" : type,
                    "brand_id" : $('select[name="brand_id"]').val(),
                    "class_id"    : $('select[name="class_id"]').val(),
                    "year"    : $('input[name="year"]').val(),
                    "model_short_name"    : $('input[name="model_short_name"]').val(),
                    "en_model_short_name"    : $('input[name="en_model_short_name"]').val()
                };
            }
            $.ajax({
                url : "/Model/ajaxSubAdd/",
                type : "POST",
                dataType : "json",
                data : $data,
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            parent.dialog.close();
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });

        $('#js-brand').change(function(){
            $.ajax({
                url : '/common/ajaxGetClass/',
                type : "POST",
                dataType : "json",
                data : {
                    "brand_id" : $(this).val()
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                    } else {
                        if (result.data.length > 0) {
                            var $html = '<option value="0">选择车系</option>';
                            $.each(result.data, function(n, row) {
                                $html += '<option value="'+row.id+'">'+row.name+'</option>';
                            });
                            $('#js-class').html($html);
                        }
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                }
            });
        });

        $('#type').change(function(){
            var $this = $(this);
            var type = $this.val();
            if (type == 1) {
                $('#brand-block').show();
                $('#brand-select-block').hide();
                $('#class-select-block').hide();
                $('#class-block').hide();
                $('#model-block').hide();
            } else if (type == 2) {
                $('#brand-block').hide();
                $('#brand-select-block').show();
                $('#class-select-block').hide();
                $('#class-block').show();
                $('#model-block').hide();
            } else if (type == 3) {
                $('#brand-block').hide();
                $('#brand-select-block').show();
                $('#class-select-block').show();
                $('#class-block').hide();
                $('#model-block').show();
            }
        });
    });
</script>
