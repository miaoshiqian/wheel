<style>
    body {background-color: #fff;}
</style>

<div class="box box-info" style="border-top: none;">
    <!-- form start -->
    <form class="form-horizontal">
        <div class="box-body">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">中文品牌</label>
                <div class="col-sm-4" style="display: inline;">
                    <input type="text" value="<?php echo $this->brand_name;?>" name="brand_name" style="display: inline;width: 250px;" class="form-control" placeholder="中文品牌名称">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">英文品牌</label>
                <div class="col-sm-4" style="display: inline;">
                    <input type="text" value="<?php echo $this->en_brand_name;?>" name="en_brand_name" style="display: inline;width: 250px;" class="form-control" placeholder="英文品牌名称">
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">中文车系</label>
                <div class="col-sm-4" style="display: inline;">
                    <input type="text" value="<?php echo $this->class_name;?>" name="class_name" style="display: inline;width: 250px;" class="form-control" placeholder="中文车系名称">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">英文车系</label>
                <div class="col-sm-4" style="display: inline;">
                    <input type="text" value="<?php echo $this->en_class_name;?>" name="en_class_name" style="display: inline;width: 250px;" class="form-control" placeholder="英文车系名称">
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">车辆年款</label>
                <div class="col-sm-4" style="display: inline;">
                    <input type="text" value="<?php echo $this->year;?>" name="year" style="display: inline;width: 250px;" class="form-control" placeholder="车辆年款">
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">中文车型</label>
                <div class="col-sm-4" style="display: inline;">
                    <input type="text" value="<?php echo $this->model_short_name;?>" name="model_short_name" style="display: inline;width: 250px;" class="form-control" placeholder="中文车型名称">
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">英文车型</label>
                <div class="col-sm-4" style="display: inline;">
                    <input type="text" value="<?php echo $this->en_model_short_name;?>" name="en_model_short_name" style="display: inline;width: 250px;" class="form-control" placeholder="中文车型名称">
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <input type="hidden" name="id" value="<?php echo $this->id;?>"/>
            <button data-action-type="sub" class="btn btn-info">提交保存</button>
        </div><!-- /.box-footer -->
    </form>
</div>

<script type="text/javascript">
    G.use(['com/backend/js/backend.js', 'jquery'], function (Backend, $) {
        Backend.run();
        Backend.on('sub', function(e) {
            e.preventDefault();
            var $this = $(this);
            $this.addClass('disabled');
            $.ajax({
                url : "/Model/ajaxSubEdit/",
                type : "POST",
                dataType : "json",
                data : {
                    "id"    : $('input[name="id"]').val(),
                    "brand_name"   : $('input[name="brand_name"]').val(),
                    "en_brand_name"    : $('input[name="en_brand_name"]').val(),
                    "class_name"    : $('input[name="class_name"]').val(),
                    "en_class_name"    : $('input[name="en_class_name"]').val(),
                    "year"    : $('input[name="year"]').val(),
                    "model_short_name"    : $('input[name="model_short_name"]').val(),
                    "en_model_short_name"    : $('input[name="en_model_short_name"]').val()
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            parent.dialog.close();
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });
    });
</script>
