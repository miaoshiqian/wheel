<style>
    body {background-color: #fff;}
</style>

<div class="box box-info" style="border-top: none;">
    <!-- form start -->
    <form class="form-horizontal">
        <div class="box-body">
            <p>下线理由：</p>
            <div id="model-block">
                <div class="form-group">
                    <div class="col-sm-4" style="display: inline;">
                        <textarea name="reason" rows="3" cols="30" placeholder="输入下线理由"></textarea>
                    </div>
                </div>
            </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <input type="hidden" name="id" value="<?php echo $this->id;?>"/>
            <button data-action-type="sub" class="btn btn-info">确定下线</button>
        </div><!-- /.box-footer -->
    </form>
</div>

<script type="text/javascript">
    G.use(['com/backend/js/backend.js', 'jquery'], function (Backend, $) {
        Backend.run();
        Backend.on('sub', function(e) {
            e.preventDefault();
            var $this = $(this);
            $this.addClass('disabled');
            $.ajax({
                url : "/Car/ajaxSubOffline/",
                type : "POST",
                dataType : "json",
                data : {
                    "reason" : $('textarea[name="reason"]').val(),
                    "id" : $('input[name="id"]').val()
                },
                success : function(result) {
                    if (result.errorCode) {
                        //失败
                        Backend.trigger('alert-error', result.msg);
                        $this.removeClass('disabled');
                    } else {
                        //成功
                        Backend.trigger('alert-success', result.msg);
                        setTimeout(function() {
                            parent.dialog.close();
                        }, 2000);
                    }
                },
                error : function() {
                    Backend.trigger('alert-error', "提交失败");
                    $this.removeClass('disabled');
                }
            });
        });
    });
</script>
