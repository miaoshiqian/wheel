<div style="width:950px;margin-left:50px;">
    <br/>
    <div class="box box-info" style="margin-bottom: 0px;">

        <div class="box-body">
            <form id="form" method="post" enctype="multipart/form-data" action="/car/submit/">
                <table class="table" style="border:none;">
                    <tbody>
                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon"><font style="color:red;"> * </font>车主姓名：</span>
                                <input type="text" class="form-control" placeholder="车主姓名必须填写" style="width:300px;" <?php echo $this->form->getField('seller_name');?>>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="seller_name_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon"><font style="color:red;"> * </font>车主电话：</span>
                                <input type="text" class="form-control" placeholder="车主电话必须填写" style="width:300px;" <?php echo $this->form->getField('seller_phone');?>>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="seller_phone_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon"><font style="color:red;"> * </font>车辆颜色：</span>
                                <select class="form-control" style="width: 300px" <?php echo $this->form->getField('color');?>>
                                    <option value="">选择车源颜色</option>
                                    <?php foreach((array)EnumCar::$COLOR_TEXT as $val => $text) {?>
                                        <option value="<?=$val;?>"><?=$text;?></option>
                                    <?php }?>
                                </select>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="color_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font style="color:red;"> * </font>洲/市：</span>
                                <select id="js-province" class="form-control" style="width: 150px" <?php echo $this->form->getField('province_id');?>>
                                    <option value="">选择洲</option>
                                    <?php foreach((array)$this->states as $row) {?>
                                        <option value="<?=$row['id'];?>"><?=$row['state'];?></option>
                                    <?php }?>
                                </select>
                                <select id="js-city" class="form-control" style="width: 150px" <?php echo $this->form->getField('city_id');?>>
                                    <option value="0">城市</option>
                                    <?php if (!empty($this->cityList)) {?>
                                        <?php foreach($this->cityList as $city) {?>
                                            <option value="<?=$city['id'];?>" ><?= $city['city'];?></option>
                                        <?php }?>
                                    <?php }?>
                                </select>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="province_id_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon">&nbsp;&nbsp;邮政编码：</span>
                                <input type="text" class="form-control" placeholder="输入邮编" style="width:300px;" <?php echo $this->form->getField('email_num');?>>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="email_num_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon"><font style="color:red;"> * </font>车型品牌：</span>
                                <select id="js-year" class="form-control" style="width: 150px" <?php echo $this->form->getField('vehicle_year');?>>
                                    <option value="">Select Year</option>
                                    <?php for($i = 1; $i <= 20; $i++ ) {?>
                                        <option  value="<?= date('Y') - $i;?>"><?= date('Y') - $i;?></option>
                                    <?php }?>
                                </select>
                                <select id="js-make" class="form-control" style="width: 150px" <?php echo $this->form->getField('make');?>>
                                    <option value="">Select Make</option>
                                    <?php if (!empty($this->makeList)) {?>
                                        <?php foreach($this->makeList as $row){?>
                                            <option value="<?= $row['make'];?>"><?= $row['make'];?></option>
                                        <?php }?>
                                    <?php }?>
                                </select>
                                <select id="js-model" class="form-control" style="width: 200px" <?php echo $this->form->getField('model_id');?>>
                                    <option value="">Select Model</option>
                                    <?php if (!empty($this->modelList)) {?>
                                        <?php foreach($this->modelList as $row){?>
                                            <option value="<?= $row['id'];?>"><?= $row['model'];?></option>
                                        <?php }?>
                                    <?php }?>
                                </select>

                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="make_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border:none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon"><font style="color:red;"> * </font>车源图片：</span>
                                <span style="color: orangered;">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-fw fa-warning"></i>至少上传1张、最多上传20张图片</span>
                                <div id="images" style="margin:5px 0px 0px 5px;"></div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon"><font style="color:red;"> * </font>表显里程：</span>
                                <input type="number" class="form-control" style="width:200px;" <?php echo $this->form->getField('miles');?>> <font style="line-height: 35px;padding-left: 10px;">万公里</font>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="miles_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon"><font style="color:red;"> * </font>车辆定价：</span>
                                <input type="number" class="form-control" style="width:200px;" <?php echo $this->form->getField('seller_price');?>> <font style="line-height: 35px;padding-left: 10px;">万元</font>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="seller_price_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon"><font style="color:red;"> * </font>车辆地址：</span>
                                <input type="text" class="form-control" placeholder="务必填写正确的车辆地址" style="width:300px;" <?php echo $this->form->getField('address');?>>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="address_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon"><font style="color:red;"> * </font>排&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 量：</span>
                                <input type="text" class="form-control" placeholder="排量必须填写" style="width:300px;" <?php echo $this->form->getField('emission');?>>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="emission_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon"><font style="color:red;"> * </font>变 &nbsp;速&nbsp; 箱：</span>
                                <select class="form-control" style="width: 300px" <?php echo $this->form->getField('geerbox');?>>
                                    <option value="">选择变速箱类型</option>
                                    <?php foreach((array)EnumCar::$GEARBOX_TEXT as $val => $text) {?>
                                        <option value="<?=$val;?>"><?=$text;?></option>
                                    <?php }?>
                                </select>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="geerbox_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon"><font style="color:red;"> * </font>车身结构：</span>
                                <select class="form-control" style="width: 300px" <?php echo $this->form->getField('structure');?>>
                                    <option value="">选择车身结构</option>
                                    <?php foreach((array)EnumCar::$STRUCTURE_TEXT as $val => $text) {?>
                                        <option value="<?=$val;?>"><?=$text;?></option>
                                    <?php }?>
                                </select>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="structure_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon">&nbsp;&nbsp;&nbsp;VIN 号码：</span>
                                <input type="text" class="form-control" style="width:300px;" <?php echo $this->form->getField('vin_code');?>>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="vin_code_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon">&nbsp;&nbsp;车牌号码：</span>
                                <input type="text" class="form-control" style="width:300px;" <?php echo $this->form->getField('plate_number');?>>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="plate_number_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon">&nbsp;&nbsp;上牌时间：</span>
                                <input type="text" class="form-control" placeholder="选择车辆上牌时间" style="width:300px;" <?php echo $this->form->getField('plate_time');?> data-min-view-mode="1" data-format="yyyy-mm"  data-widget="datepicker">
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="plate_time_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon">&nbsp;&nbsp;年检到期：</span>
                                <input type="text" class="form-control" placeholder="选择年检到期时间" style="width:300px;" <?php echo $this->form->getField('year_check_time');?> data-min-view-mode="1" data-format="yyyy-mm"  data-widget="datepicker">
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="year_check_time_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon">&nbsp;&nbsp;交险到期：</span>
                                <input type="text" class="form-control" placeholder="选择交强险到期时间" style="width:300px;" <?php echo $this->form->getField('insurance_time');?>  data-widget="datepicker">
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="insurance_time_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon">&nbsp;&nbsp;亮点配置：</span>
                                <?php foreach(EnumCar::$HIGH_LIGHT as $val => $text) {?>
                                    <input name="high_lights[]" type="checkbox" value="<?php echo $val;?>" <?php if(in_array($val, $this->editInfo['high_lights'])){echo 'checked';}?>> <?php echo $text;?>&nbsp;&nbsp;
                                <?php }?>
                            </div>
                        </td>
                    </tr>


                    <tr>
                        <td style="padding-left: 100px;" style="border: none;">
                            <input type="hidden" <?php echo $this->form->getField('year_name');?>/>
                            <input type="hidden" <?php echo $this->form->getField('make_name');?>/>
                            <input type="hidden" <?php echo $this->form->getField('model_name');?>/>
                            <input type="hidden" <?php echo $this->form->getField('id');?>/>
                            <a id="submit" data-submit="1" href="javascript:;" class="btn btn-success btn-flat">发布车源</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>

<div style="clear:both;"></div>

<script type="text/javascript">
    G.use(['validator', 'jquery'], function (v, $) {
        G.use(['widget/form/form.js', 'widget/form/form.css', 'com/backend/js/backend.js', 'widget/imageUploader/image_uploader.js'], function (Form, Css, Backend, Uploader) {
            Backend.run();

            <?php if (!empty($this->showMsg)) { ?>
                Backend.trigger('alert-error', '<?php echo $this->showMsg;?>');
            <?php } ?>

            var form = new Form({
                el: '#form'
            });

            window.form = form;

            //提交发布
            $('#submit').click(function(){
                var $this = $(this);
                var isSubmit = $this.data('submit');
                var $form = $('#form');
                if (isSubmit == 0) {
                    Backend.trigger('alert-error', '已经提交，请耐心等待');
                    return;
                }

                form.validate().done(function(){
                    //验证车源图片是否上传了
                    var carImages = $('input[name="images"]').val();
                    if ($.parseJSON(carImages).length <= 0) {
                        var offset = $('body').find('#images').offset();
                        if (offset) {
                            $('html, body').scrollTop(offset.top);
                            Backend.trigger('alert-error', '车源图片必须上传至少一张');
                            return;
                        }
                    }
                    $form.submit();
                }).fail(function(){
                    var offset = $form.find('.ui-field-fail').offset();
                    if (offset) {
                        $('html, body').scrollTop(offset.top);
                    }
                    return;
                });
            });

            //上传照片
            var uploader = new Uploader({
                el: '#images',
                name: 'images',
                width: 106,
                height: 36,
                maxNum: 20,
                'is_cover' : true,
                type : 'jpg,jpeg,png,gif',
                uploadedFiles : <?php echo !empty($this->images) ? $this->images : '[]';?>,
                button_image_url : "/build/com/backend/img/up_btn/default.png",
                editAble : true,
                coverStyle : 'v2',
                postParams : {
                    thumbWidth:120,
                    thumbHeight:90
                },
                url: '/upload_img/uploader.php?pre=car'
            });

            $('#js-province').change(function(){
                $.ajax({
                    url : '/common/ajaxGetCity/',
                    type : "POST",
                    dataType : "json",
                    data : {
                        "province_id" : $(this).val()
                    },
                    success : function(result) {
                        if (result.errorCode) {
                            //失败
                            Backend.trigger('alert-error', result.msg);
                        } else {
                            if (result.data.length > 0) {
                                var $html = '<option value="">城市</option>';
                                $.each(result.data, function(n, row) {
                                    $html += '<option value="'+row.id+'">'+row.name+'</option>';
                                });
                                $('#js-city').html($html);
                                //form.getField('city_id').validate();
                            }
                        }
                    },
                    error : function() {
                        Backend.trigger('alert-error', "提交失败");
                    }
                });
            });

            $('#js-year').change(function(){
                $.ajax({
                    url : '/common/ajaxGetMake/',
                    type : "POST",
                    dataType : "json",
                    data : {
                        "year" : $(this).val()
                    },
                    success : function(result) {
                        if (result.errorCode) {
                            //失败
                            Backend.trigger('alert-error', result.msg);
                        } else {
                            if (result.data.length > 0) {
                                var $html = '<option value="">Select Make</option>';
                                $.each(result.data, function(n, row) {
                                    $html += '<option value="'+row.name+'">'+row.name+'</option>';
                                });
                                $('#js-make').html($html);
                            }

                            $('input[name="year_name"]').val($('#js-year').find("option:selected").text());
                        }
                    },
                    error : function() {
                        Backend.trigger('alert-error', "提交失败");
                    }
                });
            });

            $('#js-make').change(function(){
                $.ajax({
                    url : '/common/ajaxGetEnModel/',
                    type : "POST",
                    dataType : "json",
                    data : {
                        "make" : $(this).val(),
                        "year" : $('input[name="year_name"]').val()
                    },
                    success : function(result) {
                        if (result.errorCode) {
                            //失败
                            Backend.trigger('alert-error', result.msg);
                        } else {
                            if (result.data.length > 0) {
                                var $html = '<option value="">Select Model</option>';
                                $.each(result.data, function(n, row) {
                                    $html += '<option value="'+row.id+'">'+row.name+'</option>';
                                });
                                $('#js-model').html($html);
                            }

                            $('input[name="make_name"]').val($('#js-make').find("option:selected").text());
                        }
                    },
                    error : function() {
                        Backend.trigger('alert-error', "提交失败");
                    }
                });
            });

            $('#js-model').change(function () {
                $('input[name="model_name"]').val($('#js-model').find("option:selected").text());
            });
        });
    });
</script>
