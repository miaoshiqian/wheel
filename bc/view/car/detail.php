<br>
<table class="table table-bordered" style="margin-bottom: 0px;">
    <tbody>
    <tr>
        <td style="text-align:right;width:150px;">车源状态：</td>
        <td colspan="3" style="text-align:left;width:200px;font-weight: bold;"><?php echo $this->info['status_text'];?></td>
    </tr>

    <tr>
        <td style="text-align:right;width:150px;">车源信息：</td>
        <td colspan="3" style="text-align:left;width:200px;font-weight: bold;">【<?= $this->info['id'];?>】<?php echo $this->info['title'];?></td>
    </tr>

    <tr>
        <td style="text-align:right;width:150px;">车主信息：</td>
        <td colspan="3" style="text-align:left;width:200px;font-weight: bold;"><?= $this->info['seller_name'];?>(<?= $this->info['seller_phone'];?>)</td>
    </tr>

    <tr>
        <td style="text-align:right;width:150px;">发布人ID：</td>
        <td style="text-align:left;width:200px;font-weight: bold;"><?= $this->info['create_user'];?></td>
        <td style="text-align:right;">发布时间：</td>
        <td style="text-align: left;font-weight: bold;"><?php echo $this->info['create_time'];?></td>
    </tr>

    <tr>
        <td style="text-align:right;width:150px;">车辆报价：</td>
        <td style="text-align:left;width:200px;font-weight: bold;"><font style="color: orangered;"> <?= $this->info['seller_price'];?></font> $</td>
        <td style="text-align:right;">车辆地区：</td>
        <td style="text-align: left;font-weight: bold;"><?php echo $this->info['location'];?></td>
    </tr>


    <tr>
        <td style="text-align:right;width:150px;">车身颜色：</td>
        <td style="text-align:left;width:200px;font-weight: bold;"><?php echo $this->info['color'];?></td>
        <td style="text-align:right;width:150px;">车辆地址：</td>
        <td style="text-align: left;font-weight: bold;"><?php echo $this->info['address'];?></td>
    </tr>


    <tr>
        <td style="text-align:right;width:150px;">车身结构：</td>
        <td style="text-align:left;width:200px;font-weight: bold;"><?php echo $this->info['body_style'];?></td>
        <td style="text-align:right;width:150px;">行驶里程：</td>
        <td style="text-align: left;font-weight: bold;"><?php echo $this->info['miles'];?> 万公里</td>
    </tr>

    <tr>
        <td style="text-align:right;width:150px;">VIN 码：</td>
        <td colspan="3" style="text-align:left;width:200px;font-weight: bold;"><?php echo $this->info['vin_code'];?></td>
    </tr>

    <?php if (!empty($info['offline_time'])) {?>
    <tr>
        <td style="text-align:right;width:150px;">下线时间：</td>
        <td style="text-align:left;width:200px;font-weight: bold;"><?php echo $this->info['offline_time'];?></td>
        <td style="text-align:right;width:150px;">下线理由：</td>
        <td style="text-align: left;font-weight: bold;"><?php echo $this->info['offline_reason'];?></td>
    </tr>
    <?php }?>

    <?php if (!empty($info['sold_time'])) {?>
    <tr>
        <td style="text-align:right;width:150px;">售出时间：</td>
        <td style="text-align:left;width:200px;font-weight: bold;"><?php echo $this->info['sold_time'];?></td>
        <td style="text-align:right;width:150px;"></td>
        <td style="text-align: left;font-weight: bold;"></td>
    </tr>
    <?php }?>

    <tr>
        <td style="text-align:right;width:150px;">车源图片：</td>
        <td colspan="3" style="text-align:left;width:200px;font-weight: bold;">
            <?php foreach((array)$this->images as $img) {?>
                <a href="<?=$img['big_img'];?>" target="_blank"> <img src="<?=$img['small_img'];?>"/></a>
            <?php }?>
        </td>
    </tr>

    </tbody>
</table>