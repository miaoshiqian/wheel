<div style="width:950px;margin-left:50px;">
    <br/>
    <div class="box box-info" style="margin-bottom: 0px;">

        <div class="box-body">
            <form id="form" method="post" enctype="multipart/form-data" action="/car/submit/">
                <table class="table" style="border:none;">
                    <tbody>
                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">First Name：</span>
                                <input <?php echo $this->form->getField('first_name');?> type="text" class="form-control" style="width:300px;" >
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="first_name_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Last Name：</span>
                                <input <?php echo $this->form->getField('last_name');?> type="text" class="form-control" style="width:300px;" >
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="last_name_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">State / City：</span>
                                <select id="js-province" class="form-control" style="width: 150px" <?php echo $this->form->getField('province_id');?>>
                                    <option value="">Select State</option>
                                    <?php foreach((array)$this->states as $row) {?>
                                        <option value="<?=$row['id'];?>"><?=$row['state'];?></option>
                                    <?php }?>
                                </select>
                                <select id="js-city" class="form-control" style="width: 150px" <?php echo $this->form->getField('city_id');?>>
                                    <option value="0">Select City</option>
                                    <?php if (!empty($this->cityList)) {?>
                                        <?php foreach($this->cityList as $city) {?>
                                            <option value="<?=$city['id'];?>" ><?= $city['city'];?></option>
                                        <?php }?>
                                    <?php }?>
                                </select>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="province_id_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Address：</span>
                                <input <?php echo $this->form->getField('address');?> type="text" class="form-control" placeholder="number and street" style="width:300px;" >
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="address_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">More Address：</span>
                                <input <?php echo $this->form->getField('more_address');?> type="text" class="form-control" placeholder="More address information(optional)" style="width:300px;" >
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="more_address_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Zip Code：</span>
                                <input <?php echo $this->form->getField('zip_code');?> type="text" class="form-control" style="width:300px;">
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="zip_code_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Email Address：</span>
                                <input <?php echo $this->form->getField('email_address');?> type="text" class="form-control" style="width:300px;">
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="email_address_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Phone1：</span>
                                <input <?php echo $this->form->getField('phone1');?> type="text" class="form-control" style="width:300px;">
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="phone1_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Phone2：</span>
                                <input <?php echo $this->form->getField('phone2');?> type="text" class="form-control" style="width:300px;" placeholder="(optional)">
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="phone2_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Car Brand：</span>
                                <select id="js-year" class="form-control" style="width: 100px" <?php echo $this->form->getField('vehicle_year');?>>
                                    <option value="">Select Year</option>
                                    <?php for($i = 0; $i < 20; $i++ ) {?>
                                        <option  value="<?= date('Y') - $i;?>"><?= date('Y') - $i;?></option>
                                    <?php }?>
                                </select>
                                <select id="js-make" class="form-control" style="width: 150px" <?php echo $this->form->getField('make');?>>
                                    <option value="">Select Make</option>
                                    <?php if (!empty($this->makeList)) {?>
                                        <?php foreach($this->makeList as $row){?>
                                            <option value="<?= $row['make'];?>"><?= $row['make'];?></option>
                                        <?php }?>
                                    <?php }?>
                                </select>
                                <select id="js-model" class="form-control" style="width: 200px" <?php echo $this->form->getField('model_name');?>>
                                    <option value="">Select Model</option>
                                    <?php if (!empty($this->modelList)) {?>
                                        <?php foreach($this->modelList as $row){?>
                                            <option value="<?= $row['model_name'];?>"><?= $row['model_name'];?></option>
                                        <?php }?>
                                    <?php }?>
                                </select>

                                <select id="js-trim" class="form-control" style="width: 200px" <?php echo $this->form->getField('model_trim');?>>
                                    <option value="">Select Trim</option>
                                    <?php if (!empty($this->trimList)) {?>
                                        <?php foreach($this->trimList as $row){?>
                                            <option value="<?= $row['model_id'];?>"><?= $row['model_trim'];?></option>
                                        <?php }?>
                                    <?php }?>
                                </select>

                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="make_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border:none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Images：</span>
                                <span style="color: orangered;">&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-fw fa-warning"></i>至少上传1张、最多上传20张图片</span>
                                <div id="images" style="margin:5px 0px 0px 5px;"></div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Listing Price：</span>
                                <input <?php echo $this->form->getField('listing_price');?> type="number" class="form-control" style="width:300px;"> $
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="listing_price_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Mileage：</span>
                                <input <?php echo $this->form->getField('mile');?> type="number" class="form-control" style="width:300px;">
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="mile_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Exterior Color：</span>
                                <input <?php echo $this->form->getField('exterior_color');?> type="text" class="form-control" style="width:300px;">
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="exterior_color_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Interior Color：</span>
                                <input <?php echo $this->form->getField('interior_color');?> type="text" class="form-control" style="width:300px;">
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="interior_color_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Transmission：</span>
                                <select <?php echo $this->form->getField('transmission');?> class="form-control" style="width:300px;">
                                    <option value="0"> Select transmission</option>
                                    <?php foreach(EnumCar::$TRANSMISSION as $val => $text) {?>
                                        <option value="<?= $val;?>"><?=$text;?></option>
                                    <?php }?>
                                </select>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="transmission_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Title Status：</span>
                                <select <?php echo $this->form->getField('title_status');?> class="form-control" style="width:300px;">
                                    <option value="0"> Select TitleStatus</option>
                                    <?php foreach(EnumCar::$TITLE_STATUS as $val => $text) {?>
                                        <option value="<?= $val;?>"><?=$text;?></option>
                                    <?php }?>
                                </select>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="title_status_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Body Style：</span>
                                <select <?php echo $this->form->getField('body_style');?> class="form-control" style="width:300px;">
                                    <option value="0"> Select BodyStyle</option>
                                    <?php foreach(EnumCar::$BODY_STYLE as $val => $text) {?>
                                        <option value="<?= $val;?>"><?=$text;?></option>
                                    <?php }?>
                                </select>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="body_style_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">&nbsp;&nbsp;&nbsp;VIN Number：</span>
                                <input type="text" class="form-control" style="width:300px;" <?php echo $this->form->getField('vin_code');?>>
                                <span class="checkinfo">
                                    <i class="i-infom"></i>
                                    <span id="vin_code_tip"></span>
                                </span>
                            </div>
                        </td>
                    </tr>


                    <tr>
                        <td style="border: none;">
                            <div class="input-group ui-field">
                                <span class="input-group-addon" style="width: 120px;">Highlights：</span>
                                <?php foreach(EnumCar::$HIGH_LIGHT as $val => $text) {?>
                                    <input name="high_lights[]" type="checkbox" value="<?php echo $val;?>" <?php if(in_array($val, $this->editInfo['high_lights'])){echo 'checked';}?>> <?php echo $text;?><br>
                                <?php }?>
                            </div>
                        </td>
                    </tr>


                    <tr>
                        <td style="padding-left: 100px;" style="border: none;">
                            <input type="hidden" <?php echo $this->form->getField('year_name');?>/>
                            <input type="hidden" <?php echo $this->form->getField('make_name');?>/>
                            <input type="hidden" <?php echo $this->form->getField('model_name');?>/>
                            <input type="hidden" <?php echo $this->form->getField('model_trim_name');?>/>
                            <input type="hidden" name="model_trim"/>
                            <input type="hidden" <?php echo $this->form->getField('id');?>/>
                            <a id="submit" data-submit="1" href="javascript:;" class="btn btn-success btn-flat">发布车源</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>

<div style="clear:both;"></div>

<script type="text/javascript">
    G.use(['validator', 'jquery'], function (v, $) {
        G.use(['widget/form/form.js', 'widget/form/form.css', 'com/backend/js/backend.js', 'widget/imageUploader/image_uploader.js'], function (Form, Css, Backend, Uploader) {
            Backend.run();

            <?php if (!empty($this->showMsg)) { ?>
                Backend.trigger('alert-error', '<?php echo $this->showMsg;?>');
            <?php } ?>

            var form = new Form({
                el: '#form'
            });

            window.form = form;

            //提交发布
            $('#submit').click(function(){
                var $this = $(this);
                var isSubmit = $this.data('submit');
                var $form = $('#form');
                if (isSubmit == 0) {
                    Backend.trigger('alert-error', '已经提交，请耐心等待');
                    return;
                }

                form.validate().done(function(){
                    //验证车源图片是否上传了
                    var carImages = $('input[name="images"]').val();
                    if ($.parseJSON(carImages).length <= 0) {
                        var offset = $('body').find('#images').offset();
                        if (offset) {
                            $('html, body').scrollTop(offset.top);
                            Backend.trigger('alert-error', '车源图片必须上传至少一张');
                            return;
                        }
                    }
                    $form.submit();
                }).fail(function(){
                    var offset = $form.find('.ui-field-fail').offset();
                    if (offset) {
                        $('html, body').scrollTop(offset.top);
                    }
                    return;
                });
            });

            //上传照片
            var uploader = new Uploader({
                el: '#images',
                name: 'images',
                width: 106,
                height: 36,
                maxNum: 20,
                'is_cover' : true,
                type : 'jpg,jpeg,png,gif',
                uploadedFiles : <?php echo !empty($this->images) ? $this->images : '[]';?>,
                button_image_url : "/build/com/backend/img/up_btn/default.png",
                editAble : true,
                coverStyle : 'v2',
                postParams : {
                    thumbWidth:120,
                    thumbHeight:90
                },
                url: '/upload_img/uploader.php?pre=car'
            });

            $('#js-province').change(function(){
                $.ajax({
                    url : '/common/ajaxGetCity/',
                    type : "POST",
                    dataType : "json",
                    data : {
                        "province_id" : $(this).val()
                    },
                    success : function(result) {
                        if (result.errorCode) {
                            //失败
                            Backend.trigger('alert-error', result.msg);
                        } else {
                            if (result.data.length > 0) {
                                var $html = '<option value="">城市</option>';
                                $.each(result.data, function(n, row) {
                                    $html += '<option value="'+row.id+'">'+row.name+'</option>';
                                });
                                $('#js-city').html($html);
                                //form.getField('city_id').validate();
                            }
                        }
                    },
                    error : function() {
                        Backend.trigger('alert-error', "提交失败");
                    }
                });
            });

            $('#js-year').change(function(){
                $.ajax({
                    url : '/common/ajaxGetMake/',
                    type : "POST",
                    dataType : "json",
                    data : {
                        "year" : $(this).val()
                    },
                    success : function(result) {
                        if (result.errorCode) {
                            //失败
                            Backend.trigger('alert-error', result.msg);
                        } else {
                            if (result.data.length > 0) {
                                var $html = '<option value="">Select Make</option>';
                                $.each(result.data, function(n, row) {
                                    $html += '<option value="'+row.name+'">'+row.name+'</option>';
                                });
                                $('#js-make').html($html);
                            }

                            $('input[name="year_name"]').val($('#js-year').find("option:selected").text());
                        }
                    },
                    error : function() {
                        Backend.trigger('alert-error', "提交失败");
                    }
                });
            });

            $('#js-make').change(function(){
                $.ajax({
                    url : '/common/ajaxGetEnModel/',
                    type : "POST",
                    dataType : "json",
                    data : {
                        "make" : $(this).val(),
                        "year" : $('input[name="year_name"]').val()
                    },
                    success : function(result) {
                        if (result.errorCode) {
                            //失败
                            Backend.trigger('alert-error', result.msg);
                        } else {
                            if (result.data.length > 0) {
                                var $html = '<option value="">Select Model</option>';
                                $.each(result.data, function(n, row) {
                                    $html += '<option value="'+row.name+'">'+row.name+'</option>';
                                });
                                $('#js-model').html($html);
                            }

                            $('input[name="make_name"]').val($('#js-make').find("option:selected").text());
                        }
                    },
                    error : function() {
                        Backend.trigger('alert-error', "提交失败");
                    }
                });
            });

            $('#js-model').change(function(){
                $.ajax({
                    url : '/common/ajaxGetTrim/',
                    type : "POST",
                    dataType : "json",
                    data : {
                        "model": $(this).val(),
                        "year" : $('input[name="year_name"]').val()
                    },
                    success : function(result) {
                        if (result.errorCode) {
                            //失败
                            Backend.trigger('alert-error', result.msg);
                        } else {
                            if (result.data.length > 0) {
                                var $html = '<option value="">Select Trim</option>';
                                $.each(result.data, function(n, row) {
                                    $html += '<option value="'+row.id+'">'+row.name+'</option>';
                                });
                                $('#js-trim').html($html);
                            }

                            $('input[name="model_name"]').val($('#js-model').find("option:selected").text());
                        }
                    },
                    error : function() {
                        Backend.trigger('alert-error', "提交失败");
                    }
                });
            });

            $('#js-trim').change(function () {
                $('input[name="model_trim_name"]').val($('#js-trim').find("option:selected").text());
                $('input[name="model_trim"]').val($('#js-trim').val());
            });
        });
    });
</script>
