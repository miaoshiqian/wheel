<?php
/**
 *  生成菜单
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/19
 * Time: 21:56
 */

function menu_helper($menuData, $isOpen = false, $top = 1){
	$html = '';
	$tpl = '
        <li class="%s">
			<a href="%s" class="%s">
				<i class="menu-icon fa %s"></i>
				%s
				%s
			</a>
			<b class="arrow"></b>
			%s
		</li>';

	foreach ($menuData as $menu) {
		if (!$menu['show']) continue;

		$activeClass = $angleDownClass = $childHtml = '';
		if (!empty($menu['open'])) {
			$activeClass = 'active';
			if (!empty($menu['child'])) {
				$activeClass .= ' open';
			}
		}
		if (!empty($menu['child'])) {
			$angleDownClass = '<b class="arrow fa fa-angle-down"></b>';
			$childHtml = menu_helper($menu['child'], !empty($menu['open']), $top+1);
			$childHtml = '<ul class="submenu">' . $childHtml . '</ul>';
		}

		$url = empty($menu['url']) ? '#' : $menu['url'];

		$urlClass = !empty($menu['child']) ? 'dropdown-toggle' : '';


		if (empty($menu['icon'])) {
			if ($top == 1) {
				die('请指写菜单' . $menu['text'] . '的icon');
			}
			$iconClass = 'fa-caret-right';
		} else {
			$iconClass = $menu['icon'];
		}

		$text = $top == 1 ? '<span class="menu-text"> ' . $menu['text'] . ' </span>' : $menu['text'];

		$html .= sprintf($tpl, $activeClass, $url, $urlClass, $iconClass, $text, $angleDownClass, $childHtml);

	}

	return $html;
}