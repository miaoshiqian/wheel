<?php
/**
 * 简介: 生成模块菜单
 * User: 龙卫国<longweiguo@xiongying.com>
 * Date: 2015/11/19
 * Time: 21:56
 * Copyright (c) 2015, mingtian.com
 */

function menu_module_helper($params) {
	$html = '';
	$tpl  = '
 					<li>
						<a href="%s">
							<i class="ace-icon fa %s bigger-110 blue"></i>
							%s
						</a>
					</li>';

	foreach ($params[0] as $menu) {
		$html .= sprintf(
			$tpl,
			"http://{$menu['module']}.bc.mingtian.com{$menu['url']}",
			$menu['icon'],
			$menu['text']
		);
	}

	$tpl = '
        <ul class="nav navbar-nav">
			<li>
				<a href="%s" class="dropdown-toggle" data-toggle="dropdown">
					%s
					&nbsp;
					<i class="ace-icon fa fa-angle-down bigger-110"></i>
				</a>
				<ul class="dropdown-menu dropdown-light-blue dropdown-caret">
					%s
				</ul>
			</li>
		</ul>';

	$html = sprintf(
		$tpl,
		"http://{$params[1]['module']}.bc.mingtian.com{$params[1]['url']}",
		$params[1]['text'],
		$html
	);

	return $html;
}