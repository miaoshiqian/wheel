<?php
/**
 *
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/23
 * Time: 11:49
 */

class CommonController extends BcController{

	public function errorAction(){
		$message = empty($_GET['message']) ? '对不起！操作失败，请稍后再试。' : $_GET['message'];
		$code = empty($_GET['code']) ? 1 : $_GET['code'];
		$jumpUrl = empty($_GET['url']) ? '/' : $_GET['url'];
		if (Request::isAjax()) {
			Response::outputJson(array(
				'message' => $message,
				'code'    => $code
			));
		} else if (in_array('?', $_GET)) {
			$key = array_search('?', $_GET);
			Response::outputJsonp($message, $key);
		} else {
			$this->assign(array(

			));
			$this->render('error.php', array(
				'jumpUrl' => $jumpUrl,
				'message' => $message,
			));
		}
		exit;
	}

	public function successAction() {
		$message = empty($_GET['message']) ? '操作成功' : $_GET['message'];
		$code = 0;
		$jumpUrl = empty($_GET['url']) ? '/' : $_GET['url'];
		if (Request::isAjax()) {
			Response::outputJson(array(
				'message' => $message,
				'code'    => $code
			));
		} else if (in_array('?', $_GET)) {
			$key = array_search('?', $_GET);
			Response::outputJsonp($message, $key);
		} else {
			$this->assign(array(

			));
			$this->render('success.php', array(
				'jumpUrl' => $jumpUrl,
				'message' => $message,
			));
		}
		exit;
	}
}