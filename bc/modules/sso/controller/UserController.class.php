<?php
/**
 *
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/23
 * Time: 9:45
 */


class UserController extends BcController{

    public function defaultAction() {

    }

    public function loginAction(){
        $this->setPageTitle('后台登录');
        $userName = Request::getPOST('username', '');
        $password = Request::getPOST('passwd', '');
        $backUrl = Request::getPOST('refer', '');
        if(!$backUrl){
            $refer = Request::getRefer();
            if(false !== strpos($refer, '/index/error')){
                $backUrl = substr($refer, strrpos($refer, 'backUrl')+10);
            }
        }
        if(Request::isGet()){
            $this->render('/user/login.php', array(
                'backUrl' => $backUrl
            ), 'include/tpl_simple.php');
            exit;
        }
        $userInfo = AuthInterface::login(array(
            'username' => $userName, 'password' => $password
        ));
        if(!$userInfo){
            $this->jumpToFail('用户名或密码错误', '/user/login/?backUrl='.urlencode($backUrl));
        }

        // 保存用户信息于cookie
        $cookieUserInfo = array(
            'id' => $userInfo['id'], 'username' => $userInfo['username'], 'token' => $userInfo['token']
        );
        Cookie::set('bcUserInfo', json_encode($cookieUserInfo), GlobalConfig::BC_COOKIE_EXPIRE(), '/', GlobalConfig::BC_COOKIE_DOMAIN);

        $redirectUrl = empty($backUrl) ? 'http://'.GlobalConfig::BC_COOKIE_DOMAIN.'/' : $backUrl;
        Response::redirect($redirectUrl);
    }

    public function logoutAction(){
        $expire = time() - 1;
        Cookie::set('bcUserInfo', '', $expire, '/', GlobalConfig::BC_COOKIE_DOMAIN);
        Response::redirect('/user/login/?backUrl='.Request::getRefer());
    }
}