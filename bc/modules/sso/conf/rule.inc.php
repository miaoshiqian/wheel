<?php
/**
 *  权限表
 * log: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 11:22
 */

/*
 *  url => array(),
 *  admin_group为*的所有人都可访问
 *
 *  1. 查看
 *  2. 添加 修改 审核 禁用 删除
 */

return array(
	'/index/error/' => array(
		'text' => '公共错误跳转页',
		'admin_group' => '*',
	),
	'/index/success/' => array(
		'text' => '公共成功跳转页',
		'admin_group' => '*',
	),
	'/index/default/' => array(
		'text' => '首页',
		'admin_group' => 'admin-1',
	),
	'/error/default/' => array(
		'text' => '错误页',
		'admin_group' => '*',
	),

	'/user/login/' => array(
		'text' => '用户登录',
		'admin_group' => '*',
	),
	'/user/logout/' => array(
		'text' => '用户退出',
		'admin_group' => '*',
	),

);