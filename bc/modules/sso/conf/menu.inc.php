<?php
/**
 *  user菜单配置
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 11:21
 */

return array(
	0 => array(
		'url'   => '', // 访问url
		'text'  => '用户管理',       // 显示
		'icon'  => '',     // 菜单图标
		'class' => '', // 样式class
		'child' => array(
			array(
				'url'   => '/user/list/', // 访问url
				'text'  => '用户列表',       // 显示
				'icon'  => '',     // 菜单图标
				'class' => '', // 样式class
				'child' => array(),
			),
			array(
				'url'   => '', // 访问url
				'text'  => '编辑用户',       // 显示
				'icon'  => '',     // 菜单图标
				'class' => '', // 样式class
				'child' => array(
					array(
						'url'   => '/user/delete/', // 访问url
						'text'  => '删除用户',       // 显示
						'icon'  => '',     // 菜单图标
						'class' => '', // 样式class
						'child' => array(),
					)
				),
			),

		),
	),
);