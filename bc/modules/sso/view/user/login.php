<!DOCTYPE html>
<html>
<body style="background-color: #d2d6de;">
<div class="login-box-body" style="width: 350px;position:fixed; top: 50%;left: 50%;right: 0;bottom: 0;height: 200px;margin: -100px 0 0 -175px; ">
    <div class="header">登录</div>
    <form action="/user/login" method="post">
        <div class="form-group has-feedback">
            <input type="text" class="form-control" name="username" placeholder="用户名">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="passwd" class="form-control" placeholder="密码">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label class="">
                        <div class="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style="position: relative;">
                        </div>
                    </label>
                </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
                <input type="hidden" name="refer" value="<?php echo $this->backUrl;?>">
                <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">登 陆</button>
            </div><!-- /.col -->
        </div>
    </form>

</div>

<script>
    G.use(['jquery'], function ($) {
        var input_username = $("input[name=username]");
        var input_passwd = $("input[name=passwd]");
        var div_result = $("span[name=result]");
        var btn_submit = $("input[name=submit]");

        btn_submit.click(function () {
            var checkSubmit = true;

            div_result.html("");

            if ($.trim(input_username.val()) == "") {
                div_result.html("用戶名不能空");
                checkSubmit = false;
            } else if ($.trim(input_passwd.val()) == "") {
                div_result.html("密码不能为空");
                checkSubmit = false;
            }

            return checkSubmit;
        });
    });


</script>


</body>
</html>