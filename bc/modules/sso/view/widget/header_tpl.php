<header class="header">
    <a href="/" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        <i class="fa fa-fw fa-home"></i>
        安心客后台系统
    </a>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> 
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user"></i> <span><?php echo $this->userInfo['user']['real_name'];?> <i class="caret"></i></span> </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                            <img src="<?php $photo = QiNiu::formatImageUrl(!empty($this->userInfo['user']['face_url']) ? $this->userInfo['user']['face_url'] : SsoUserConfig::$WORKER_PHOTO['1'] , array('width' => 80, 'height' => 80, 'mode' => 1)); echo $photo; ?>" class="img-circle" alt="User Image" />
                            <p> <?php $roleId = $this->userInfo['user']['role_id']; echo !empty($roleId) ? (!empty($this->role['sso_major_role']) ? $this->role['sso_major_role'][$roleId] : '') : '';?><small>努力工作、再接再厉！</small> </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-4 text-center" style="padding:0"> <a href="/sso/userManagement/detail/?id=<?php echo $this->userInfo['user']['id'];?>">个人信息</a> </div>
                            <div class="col-xs-4 text-center" style="padding:0"> <a href="javascript:;" data-id="<?php echo $this->userInfo['user']['id'];?>" data-action-type="updatePw">修改密码</a> </div>
                            <div class="col-xs-4 text-center"  style="padding:0"> <a href="javascript:;">修改头像</a> </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right"> <a href="http://<?php echo UrlConfig::TEST_BC_URL;?>/sso/user/logout" class="btn btn-default btn-flat">退出</a> </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>