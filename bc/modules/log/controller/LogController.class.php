<?php
/**
 *
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/23
 * Time: 9:45
 */


class LogController extends BcController{

	public function defaultAction() {
		$this->listAction();
	}

	public function listAction($params){
		$this->render('/list.php');
	}
}