<?php
/**
 *  log 后台首页
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/23
 * Time: 9:28
 */

class IndexController extends BcController{

	public function defaultAction() {
		Response::redirect($this->createUrl('log', 'list'));
	}
}