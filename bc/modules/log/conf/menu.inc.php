<?php
/**
 *  log菜单配置
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 11:21
 */

return array(
	array(
		'url'   => '/log/list/',
		'text'  => '日志列表',
		'icon'  => 'fa-info-circle',
		'class' => '',
		'child' => array(),
	),
	array(
		'url'   => '',
		'text'  => '数据库日志',
		'icon'  => 'fa-database',
		'class' => '',
		'child' => array(
			array(
				'url'   => '/log/list/mysql.error/',
				'text'  => '错误日志',
				'icon'  => 'fa-times-circle-o',
				'class' => '',
				'child' => array(),
			),
			array(
				'url'   => '/log/list/mysql.slowest/',
				'text'  => '特慢查询',
				'icon'  => 'fa-exclamation-triangle',
				'class' => '',
				'child' => array(),
			),
			array(
				'url'   => '/log/list/mysql.slow/',
				'text'  => '慢查询',
				'icon'  => 'fa-exclamation-circle',
				'class' => '',
				'child' => array(),
			),
		),
	),
);