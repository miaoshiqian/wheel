<?php
/**
 *  权限表
 * log: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 11:22
 */

/*
 *  url => array(),
 *  admin_group为*的所有人都可访问
 *
 *  1. 查看
 *  2. 添加 修改 审核 禁用 删除
 */

return array(
	'/index/error/' => array(
		'text' => '公共错误跳转页',
		'admin_group' => '*',
	),
	'/index/success/' => array(
		'text' => '公共成功跳转页',
		'admin_group' => '*',
	),

	'/index/default/' => array(
		'text' => '日志首页',
		'admin_group' => 'admin-log-1',
	),
	'/log/list/' => array(
		'text' => '日志列表',
		'admin_group' => 'admin-log-1',
	),

);