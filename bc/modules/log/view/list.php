<!--css:start-->
<link href="/sta/css/plugins/footable/footable.core.css" rel="stylesheet">
<!--css:end-->

<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>FooTable with row toggler, sorting and pagination</h5>
			</div>
			<div class="ibox-content">
				<div class="row">
					<div class="col-sm-4 m-b-xs">
						<div data-toggle="buttons" class="btn-group">
							<label class="btn btn-sm btn-white active"> <input type="radio" id="option1" name="options">  All  </label>
							<label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="options"> Fatal </label>
							<label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="options"> Warn </label>
							<label class="btn btn-sm btn-white"> <input type="radio" id="option2" name="options"> Debug </label>
							<label class="btn btn-sm btn-white"> <input type="radio" id="option3" name="options">  Info  </label>
						</div>
					</div>
					<div class="col-sm-2 m-b-xs">
						<select class="input-sm form-control input-s-sm inline">
							<option value="0">Option 1</option>
							<option value="1">Option 2</option>
							<option value="2">Option 3</option>
							<option value="3">Option 4</option>
						</select>
					</div>
					<div class="col-sm-3">
						<div class="input-group"><input type="text" placeholder="Search" class="input-sm form-control"> <span class="input-group-btn">
                                        <button type="button" class="btn btn-sm btn-primary"> Go!</button> </span></div>
					</div>
				</div>
				<table class="footable table table-stripped toggle-arrow-tiny" data-page-size="8">
					<thead>
					<tr>

						<th data-toggle="true">时间</th>
						<th>类型</th>
						<th>Url</th>
						<th data-hide="all">标签</th>
						<th data-hide="all">信息</th>
						<th data-hide="all">跟踪</th>
						<th>展开</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>Project - This is example of project</td>
						<td>Patrick Smith</td>
						<td>0800 051213</td>
						<td>Inceptos Hymenaeos Ltd</td>
						<td><span class="pie">0.52/1.561</span></td>
						<td>20%</td>
						<td><i class="fa fa-chevron-down"></i></td>
					</tr>
					<tr>
						<td>Alpha project</td>
						<td>Alice Jackson</td>
						<td>0500 780909</td>
						<td>Nec Euismod In Company</td>
						<td><span class="pie">6,9</span></td>
						<td>40%</td>
						<td><i class="fa fa-chevron-down"></i></td>
					</tr>
					</tbody>
					<tfoot>
					<tr>
						<td colspan="5">
							<ul class="pagination pull-right"></ul>
						</td>
					</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>

<!--js:start-->
<script src="/sta/js/plugins/footable/footable.all.min.js"></script>
<script>
	$(document).ready(function() {

		$('.footable').footable();
		$('.footable2').footable();

	});

</script>
<!--js:end-->
