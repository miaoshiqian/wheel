<?php
/**
 * 简介: 权限表
 * log: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 11:22
 * Copyright (c) 2015, mingtian.com
 */

/*
 *  url => array(),
 *  admin_group为*的所有人都可访问
 *
 *  1. 查看
 *  2. 添加 修改 审核 禁用 删除
 */

return array(
	'/index/error/' => array(
		'text' => '公共错误跳转页',
		'admin_group' => '*',
	),
	'/index/success/' => array(
		'text' => '公共成功跳转页',
		'admin_group' => '*',
	),

	'/index/default/' => array(
		'text' => '文档首页',
		'admin_group' => 'admin-1',
	),

	'/api/default/' => array(
		'text' => '更新日志',
		'admin_group' => 'admin-1',
	),
	'/api/create/' => array(
		'text' => '创建文档',
		'admin_group' => 'admin-doc-2',
	),
	'/api/updateLog/' => array(
		'text' => '更新日志',
		'admin_group' => 'admin-1',
	),
	'/api/ajaxGetUpdateLog/' => array(
		'text' => '更新日志',
		'admin_group' => 'admin-1',
	),
	'/api/detail/' => array(
		'text' => '文档详情',
		'admin_group' => 'admin-1',
	),


);