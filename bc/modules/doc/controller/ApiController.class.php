<?php
/**
 * @brief 简介：
 * @author 龙卫国<longweiguo@xiongying.com>
 * @date 15/9/20
 */

require_once __DIR__ . '/../common/DocCreater.class.php';

class ApiController extends BcController{

	private $DocCreater;

	public function init() {
		parent::init();

		$outputDir = __DIR__ . '/../data';
		$this->DocCreater = new DocCreater('Interface.class.php', API_PATH, $outputDir);
	}

    public function defaultAction() {
	    $this->updateLogAction();
    }

	public function updateLogAction() {
		$data = $this->DocCreater->getUpdateLog(1, 20);
		$this->render('api_update_log.php', array(
			'updateLogData' => json_encode($data),
		));
	}

	public function ajaxGetUpdateLogAction() {
		$startLine = Request::getGET('startLine', 1);
		$data = $this->DocCreater->getUpdateLog($startLine, 20);
		$this->outputJson(0, '', $data);
	}

	public function detailAction($params) {
		if (empty($params[0])) {
			$this->jumpToFail('您要查看的文档不存在');
		}
		$docName = $params[0];
		try {
			$data = $this->DocCreater->getDetail($docName);
		} catch (UserException $e) {
			$this->jumpToFail($e->getMessage());
		}

		$this->render('api_detail.php', array(
			'docName' => $docName,
			'docDetail' => $data,
		));
	}

    public function createAction(){
	    $this->DocCreater->create();

	    $url = $this->createUrl('api', 'updateLog');
	    $this->jumpToSuccess('创建完毕', $url);
    }
}