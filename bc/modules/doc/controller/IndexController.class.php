<?php
/**
 * @brief 简介：
 * @author 陈朝阳<chenchaoyang@XXX.com>
 * @date 15/9/20
 */

class IndexController extends BcController{

    public function defaultAction() {
	    $this->render('index/index.php');
    }
}