<div class="row">
	<div class="col-xs-12">
		<?php
		$html = '';
		if (!empty($this->docDetail['doc']) && !empty($this->docDetail['doc']['brief'])) {
			$html .= "<pre>{$this->docDetail['doc']['brief']}</pre>";
		}
		if (!empty($this->docDetail['func'])) {
			$html .= '<h3>接口：</h3>';
			foreach ($this->docDetail['func'] as $func => $doc) {
				$html .= '<hr>';
				$title = !empty($doc['title']) ? "<small>{$doc['title']}</small>" : '';
				$html .= "<h3 id=\"{$func}\">{$this->docName}::$func {$title}</h3>";
				if (!empty($doc['brief'])) {
					$html .= "<pre>{$doc['brief']}</pre>";
				}
				if (!empty($doc['param'])) {
					$html .= '<h5>参数：</h5>';
					$html .= '<table class="table table-striped table-bordered table-hover"><tbody>';
					foreach ($doc['param'] as $param) {
						$paramInfo = preg_split("/\s+/", trim($param));
						if (preg_match("/^(bool|string|array|int|\|)+$/", $paramInfo[0])) {
							$paramType = array_shift($paramInfo);
						} else {
							$paramType = 'unknown';
						}
						$paramName = array_shift($paramInfo);
						if (!empty($paramInfo[0]) && preg_match("/^[\w]+$/", $paramInfo[0])) {
							$paramName .= array_shift($paramInfo);
						}
						$html .= '<tr>';
						$html .= '<td style="width:200px;">' . $paramName . '</td>';
						$html .= '<td style="width:160px;">' . $paramType . '</td>';
						$html .= '<td>' . implode(' ', $paramInfo). '</td>';
						$html .= '</tr>';
					}
					$html .= '</tbody></table>';
				}
				if (!empty($doc['return'])) {
					$return = explode("\n", $doc['return']);
					$retType = array_shift($return);
					$html .= '<h5>返回：</h5>';
					$html .= "<p>{$retType}</p>";
					if (count($return) > 0) {
						$retDetail = implode("\n", $return);
						$html .= "<pre>{$retDetail}</pre>";
					}
				}
				if (!empty($doc['throws'])) {
					$html .= '<h5>异常：</h5>';
					$html .= "<p>{$doc['throws']}</p>";
				}
			}
		}
		$this->put($html, true);
		?>
	</div>
</div>
<!--js:start-->
<script type="text/javascript">
	jQuery(function ($) {
	});
</script>
<!--js:end-->