<?php 
return array (
  'doc' => 
  array (
    'title' => '章节课相关接口',
    'brief' => '',
  ),
  'func' => 
  array (
    'priceInfo' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/e9b07010181542bca819db47236ddb31/',
      'return' => 'array',
      'title' => '章节课购买信息',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params[chapterCourseSiteId]',
        1 => '$params[buyType]',
      ),
      'throws' => 'SysException',
    ),
    'orderConfirm' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/e9b07010181542bca819db47236ddb31/',
      'return' => 'array',
      'title' => '和教材同步一致',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params[examSiteId]  章节ID，必填',
        1 => '$params[discountType]  折扣类型，必填//上个接口的DiscountType字段',
        2 => '$params[quantityType]  数量类型，必填//上个接口的Quantity字段',
        3 => '$params[deduct]  用户抵扣金额，浮点型',
        4 => '$params[couponId]  代金卷ID，int类型',
        5 => '$params[buyType]   必填 课程：50，讲义：60',
      ),
      'throws' => 'SysException',
    ),
    'getEvaluationList' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/d018c2bdcd3d4ca990199e05bcda5321/',
      'return' => 'array',
      'title' => '课程评价列表',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params[pageIndex]        当前页码',
        1 => '$params[chapterCourseId]  课程ID',
      ),
      'throws' => 'SysException',
    ),
    'saveEvaluation' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/8e3dd7f1fddd4581b83b6f98bb328416/',
      'return' => 'array',
      'title' => '保存用户课程评价',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params[chapterCourseId]  课程ID',
        1 => '$params[evaluationContent]  评价内容',
        2 => '$params[professionalStarLevel]  专业获取的星级数量',
        3 => '$params[styleStarLevel]',
        4 => '$params[informationStarLevel]  资料信息获取的星级数量',
      ),
      'throws' => 'SysException',
    ),
    'saveUserCourseHistory' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/87300277f9cf48798f1d926194e41df8/',
      'return' => 'array',
      'title' => '保存用户学习进度',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params[chapterCourseSiteId]  考点ID',
        1 => '$params[chapterCourseId]  课程ID',
        2 => '$params[learnDurationLen]  学习时长',
        3 => '$params[chapterLectureId]  讲义ID',
        4 => '$params[historyType]   1：为课程 ，2：讲义',
      ),
      'throws' => 'SysException',
    ),
    'courseSiteLectureTree' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/30a4581d27cb456cbbfc1ff8159a4c0e/',
      'return' => 'array',
      'title' => '章节课讲义考点树',
      'author' => '李登科<lidengke@xiongying.com>',
      'throws' => 'SysException',
    ),
    'courseSiteTree' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/d50ea4822b6e4c449220b5f46562515d/',
      'return' => 'array',
      'title' => '章节课课程考点树',
      'author' => '李登科<lidengke@xiongying.com>',
      'throws' => 'SysException',
    ),
  ),
);
