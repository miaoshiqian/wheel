<?php 
return array (
  'doc' => 
  array (
    'title' => '验证码相关、短信、图形',
    'brief' => '',
  ),
  'func' => 
  array (
    'verifySmsCode' => 
    array (
      'brief' => '',
      'return' => 'array',
      'title' => '验证短信验证码',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params [mobile]',
        1 => '$params [verCode]',
      ),
      'throws' => 'SysException',
    ),
    'verifyCaptchaCode' => 
    array (
      'brief' => '',
      'return' => 'bool',
      'title' => '验证图形',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params [captchaCode]',
      ),
    ),
    'getCaptcha' => 
    array (
      'brief' => '',
      'return' => 
      array (
      ),
      'title' => '生成图形验证码',
      'author' => '李登科<lidengke@xiongying.com>',
    ),
    'getBindMobileVerCode' => 
    array (
      'brief' => '',
      'return' => 'array',
      'title' => '注册或者登录后老用户绑定时获取验证码',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params [mobile]',
      ),
      'throws' => 'SysException',
    ),
    'getPasswordVerCode' => 
    array (
      'brief' => '',
      'return' => 'array',
      'title' => '重置密码时获取验证码',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params[mobile]',
      ),
      'throws' => 'SysException',
    ),
  ),
);
