<?php 
return array (
  'doc' => 
  array (
    'title' => '直播课的接口',
    'brief' => '',
  ),
  'func' => 
  array (
    'getCourseEntity' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/dee23cb71c6a439e9b92ee39367586ab/',
      'return' => 'bool|mixed',
      'title' => '直播详情',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params [courseID]',
      ),
      'throws' => 'SysException',
    ),
    'getCourseList' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/4b3d66bdf1c34a428b5cf78501edab7a/',
      'return' => 'bool|mixed',
      'title' => '直播课程列表',
      'author' => '李登科<lidengke@xiongying.com>',
    ),
    'saveUserCourseHistory' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/11bcf1958c3143bd81e7c10541f6484a/',
      'return' => 'array',
      'title' => '直播课学习记录 post',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params [courseID]',
        1 => '$params [learnDurationLen]',
      ),
    ),
    'getYuGaoList' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/00500c750f51402589adb7979d6beb3e/',
      'return' => 'bool|mixed',
      'title' => '近期直播',
      'author' => '李登科<lidengke@xiongying.com>',
    ),
    'booking' => 
    array (
      'brief' => 'https://tower.im/projects/ffc160e2a6a24aceb294a699d3172564/docs/87d9f1db8bf64695858a67b6af3642e6/',
      'return' => 'bool|mixed',
      'title' => '直播课预约 预约、取消预约',
      'param' => 
      array (
        0 => 'string $params[courseID] 课程ID',
        1 => 'int    $params[isCancel] 预约还是取消预约，0-预约；1-取消预约',
      ),
      'author' => '李登科<lidengke@xiongying.com>',
    ),
  ),
);
