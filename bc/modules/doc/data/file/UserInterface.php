<?php 
return array (
  'doc' => 
  array (
    'title' => '用户接口',
    'brief' => '',
  ),
  'func' => 
  array (
    'login' => 
    array (
      'brief' => '',
      'return' => 'array
 {
     username: 用户名
     token:    token验证字符串
 };',
      'title' => '用户登录',
      'author' => '龙卫国<longweiguo@xiongying.com>',
      'update' => 'fd8fdfdsfdsfdfdsfdfdsfdsfd55',
      'param' => 
      array (
        0 => '$params[username] 用户名',
        1 => '$params[password] 密码',
      ),
      'throws' => 'SysException',
    ),
    'thirdLogin' => 
    array (
      'brief' => '',
      'return' => 'array',
      'title' => '第三方登录',
      'author' => '龙卫国<longweiguo@xiongying.com>',
      'param' => 
      array (
        0 => '$params[unionId]    第三方 unionId',
        1 => '$params[infoType]  第三方类型 weixin,qq,sina,baidu,renren',
      ),
      'throws' => 'SysException',
    ),
    'mobileRegister' => 
    array (
      'brief' => '',
      'return' => 'bool|mixed',
      'title' => '手机注册',
      'author' => '龙卫国<longweiguo@xiongying.com>',
      'param' => 
      array (
        0 => '$params[mobile]  手机号',
        1 => '$params[password]  密码',
        2 => '$params[verCode]  短信验证码',
      ),
      'throws' => 'SysException',
    ),
    'resetUserPassword' => 
    array (
      'brief' => '',
      'return' => 'array',
      'title' => '手机号重置登录密码',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params[mobile] 手机号',
        1 => '$params[mobileContext] 调完手机号验证码验证完后的值',
        2 => '$params[password] 明文密码',
      ),
      'throws' => 'SysException',
    ),
  ),
);
