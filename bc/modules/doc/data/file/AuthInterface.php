<?php 
return array (
  'doc' => 
  array (
    'title' => '后台权限接口',
    'brief' => '',
  ),
  'func' => 
  array (
    'getGroupCodesByUserId' => 
    array (
      'brief' => '',
      'return' => 'array|bool 组编号集合 没有返回false',
      'title' => '根据用户user_id获取组编号',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params[\'user_id\']',
      ),
      'throws' => 'Exception',
    ),
    'checkAuth' => 
    array (
      'brief' => '',
      'return' => 'true 权限通过, -1 token 无效, false 权限不通过',
      'title' => '检查用户权限',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => '$params[\'token\']',
        1 => '$params[\'user_id\']',
        2 => '$params[\'rule_group\']',
      ),
      'throws' => 'Exception',
    ),
    'login' => 
    array (
      'brief' => '',
      'return' => 'bool|int 失败|用户id',
      'title' => '登录接口',
      'author' => '李登科<lidengke@xiongying.com>',
      'param' => 
      array (
        0 => 'string $params[\'username\']',
        1 => 'string $params[\'password\']',
      ),
      'throws' => 'Exception',
    ),
  ),
);
