<?php 
return array (
  0 => 
  array (
    'url' => '/api/detail/ChapterInterface/',
    'text' => '章节课相关接口',
  ),
  1 => 
  array (
    'url' => '/api/detail/LiveInterface/',
    'text' => '直播课的接口',
  ),
  2 => 
  array (
    'url' => '/api/detail/LogInterface/',
    'text' => 'LogInterface',
  ),
  3 => 
  array (
    'url' => '/api/detail/SmsInterface/',
    'text' => 'SmsInterface',
  ),
  4 => 
  array (
    'url' => '/api/detail/AuthInterface/',
    'text' => '后台权限接口',
  ),
  5 => 
  array (
    'url' => '/api/detail/UserInterface/',
    'text' => '用户接口',
  ),
  6 => 
  array (
    'url' => '/api/detail/VerCodeInterface/',
    'text' => '验证码相关、短信、图形',
  ),
  7 => 
  array (
    'url' => '/api/detail/VipInterface/',
    'text' => 'VIP 课相关接口',
  ),
);
