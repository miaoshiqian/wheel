<?php

class DocCreater {
	public $sourceDir = '';
	public $outputDir = '';
	public $fileRule = 'Controller.class.php';
	private $currData = array();
	private $currName = '';
	private $docData = array();

	public function __construct($fileRule, $sourceDir = '', $outputDir = '') {
		$this->fileRule = $fileRule;
		if (!empty($sourceDir)) {
			$this->sourceDir = $sourceDir;
		}
		if (!empty($outputDir)) {
			$this->outputDir = $outputDir;
		}
	}

	public function getUpdateLog($startLine = 1, $limitLine = 20) {
		$filePath = $this->outputDir . '/update_log.txt';
		if (!is_file($filePath)) {
			return array(array(), 1, 1);
		}

		$fp = new SplFileObject($filePath, 'a+b');
		$index = 0;
		while (!$fp->eof()) {
			$index += 10000;
			$fp->seek($index);
		}
		$currIndex = $fp->key() - $startLine;
		$fp->seek($currIndex);

		$i = 0;
		$logs = array();
		$currDate = 0;
		while ($currIndex >= 0) {
			$str = $fp->current();// current()获取当前行内容
			if (empty($str)) {
				continue;
			}
			$row = json_decode($str, true);

			$date = $row['date'];
			if (!is_numeric($date)) {
				throw new SysException('更新日志内容异常');
			}
			if ($date != $currDate && $i >= $limitLine) { // 把同一个日期的日志取完
				break;
			}
			$dateStr = date('Y年m月d日', strtotime($date));
			if (!isset($logs[$dateStr])) {
				$logs[$dateStr] = array();
			}
			$logs[$dateStr][] = $row;

			$currIndex--;
			if ($currIndex >= 0) {
				$fp->seek($currIndex);
			}

			$currDate = $date;
			$i++;
		}

		$end = $currIndex <= 0 ? 1 : 0;  //是否到达末尾

		return array($logs, $i+1, $end);
	}

	public function getDetail($name) {
		$dataFile = $this->outputDir . '/file/' . $name . '.php';
		if (!is_file($dataFile)) {
			throw new UserException('您要查看的文档不存在');
		}
		$data = include $dataFile;
		return $data;
	}

	public function create() {
		if (empty($this->sourceDir) || !is_dir($this->sourceDir)) {
			throw new SysException('sourceDir不能为空');
		}
		if (empty($this->outputDir) || !is_dir($this->outputDir)) {
			throw new SysException('outputDir不能为空');
		}

		$this->parseDir($this->sourceDir);

		//记录文档菜单数据
		$this->writeMenuData();
	}

	private function parseDir($path) {
		foreach (scandir($path) as $afile) {
			if ($afile == '.' || $afile == '..') {
				continue;
			}
			$filePath = $path . '/' . $afile;
			if (is_dir($filePath)) {
				$this->parseDir($path . '/' . $afile);
			} else if (strpos($afile, $this->fileRule) !== false) {
				$this->parseFile($filePath);
			}
		}
	}

	private function parseFile($file) {
		$this->currName = $this->getName($file);
		$this->currData = array();
		$this->docData[$this->currName] = array('title' => $this->currName, 'brief' => '');
		$this->parseComment(file($file));
		$fileDir = $this->outputDir . '/file';
		if (!is_dir($fileDir)) {
			mkdir($fileDir, 0755, true);
		}
		$dataFile = $fileDir . '/' . $this->currName . '.php';
		$this->editUpdateLog($dataFile);
		file_put_contents($dataFile, "<?php \nreturn " . var_export($this->currData, true) . ";\n");
	}

	private function writeMenuData() {
		$menuData = array();
		foreach ($this->docData as $name => $data) {
			$menuData[] = array(
				'url' => "/api/detail/{$name}/",
				'text' => $data['title'],
			);
		}
		$dataFile = $this->outputDir . '/menu_doc.php';
		file_put_contents($dataFile, "<?php \nreturn " . var_export($menuData, true) . ";\n");
	}

	private function getName($file) {
		$name = basename($file);
		return str_replace(".class.php", "", $name);
	}

	private function editUpdateLog($dataFile) {
		$oldFuncs = array();
		if (is_file($dataFile)) {
			$oldData = include $dataFile;
			if (!empty($oldData['func'])) {
				$oldFuncs = $oldData['func'];
			}
		}
		if (!empty($this->currData['func'])) {
			$newFuncs = $this->currData['func'];
		} else {
			$newFuncs = array();
		}

		foreach ($newFuncs as $func => $val) {
			if (!isset($oldFuncs[$func])) {
				$this->writeUpdateLog('新增', $func, $val['title']);
			} else if (!empty($val['update']) && (empty($oldFuncs[$func]['update']) || $val['update'] != $oldFuncs[$func]['update'])) {
				$this->writeUpdateLog('修改', $func, $val['update']);
			}
		}
		foreach ($oldFuncs as $func => $val) {
			if (!isset($newFuncs[$func])) {
				$this->writeUpdateLog('删除', $func);
			}
		}
	}

	private function writeUpdateLog($type, $func, $title = '', $brief = '') {
		$data = array(
		    'type' => $type,
			'date' => date('Ymd'),
			'name' => $this->currName,
			'func' => $func,
			'title' => $title,
			'brief' => $brief,
		);
		$str = json_encode($data) . "\n";
		$filePath = $this->outputDir . '/update_log.txt';
		file_put_contents($filePath, $str, FILE_APPEND | LOCK_EX);
	}

	private function parseComment($arr) {
		$isFileDoc = false;
		$start = false;
		$end = false;
		$content = array();
		$index = -1;
		foreach ($arr as $line) {
			$line = trim($line);
			if ($line == '') continue;
			$index++;
			if ($index == 0 && ($line == '<?php' || $line == '<?PHP' || $line == '<?')) {
				$isFileDoc = true;
				continue;
			}
			if ($line == '/**') {
				$start = true;
				$content = array();
				continue;
			}
			if ($line == '*/') {
				$start = false;
				$end = true;
				continue;
			}
			if (strpos($line, '*') === 0) {
				if ($start) {
					$content[] = trim($line, '*');
				}
				continue;
			}
			if ($end) {
				$end = false;
				if (strpos($line, 'function') === false) {
					if ($isFileDoc) {
						$this->parseDocComment($content);
					}
				} else {
					$isFileDoc = false;
					if (strpos($line, 'public') !== false && strpos($line, '//') === false) {
						preg_match("/function +(\w+) *\(/", $line, $reg);
						$this->parseFuncComment($content, $reg[1]);
					}
				}
			}
		}
	}

	private function parseDocComment($content) {
		$start = true;
		$brief = array();
		foreach ($content as $line) {
			if ($start) {
				if (trim($line) == '') {
					continue;
				}
				$this->docData[$this->currName]['title'] = trim($line);
				$start = false;
				continue;
			}

			if (preg_match("/^(User|Date|Time|Copyright|@\w+)(.*)$/", trim($line), $reg)) {
				break;
			}

			$brief[] = $line;
		}

		$this->docData[$this->currName]['brief'] = implode("\n", $brief);
		$this->currData['doc'] = $this->docData[$this->currName];
	}

	private function parseFuncComment($content, $func) {
		$data = array('brief' => array());
		$lastTag = 'brief';
		$start = true;
		foreach ($content as $line) {
			if ($start) {
				if (trim($line) == '') {
					continue;
				}
				$data['title'] = trim($line);
				$start = false;
				continue;
			}

			if (preg_match("/^@([a-z]+) +(.*)$/", trim($line), $regs)) {
				$lastTag = $regs[1];
				if ($regs[1] == 'param') {
					if (empty($data['param'])) {
						$data['param'] = array();
					}
					$data['param'][] = trim($regs[2]);
				} else {
					$data[$lastTag] = trim($regs[2]);
				}
			} else {
				if ($lastTag == 'param') {
					$data[$lastTag][count($data[$lastTag]) - 1] .= "\n" . $line;
				} else if ($lastTag == 'brief') {
					$data[$lastTag][] = $line;
				} else {
					$data[$lastTag] .= "\n" . $line;
				}
			}
		}
		$data['brief'] = implode("\n", $data['brief']);
		$this->currData['func'][$func] = $data;
	}
}

