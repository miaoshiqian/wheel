<?php 
return array (
  'doc' => 
  array (
    'title' => '直播',
    'brief' => '',
    'User' => '李登科<lidengke@xiongying.com>',
    'Date' => '2015/12/10',
    'Time' => '20:09',
  ),
  'func' => 
  array (
    'getYuGaoList' => 
    array (
      'brief' => '',
      'return' => 'array',
      'title' => '第三方登录',
      'author' => '龙卫国<longweiguo@xiongying.com>',
      'param' => 
      array (
        0 => '$params[unionId]    第三方 unionId',
        1 => '$params[infoType]  第三方类型 weixin,qq,sina,baidu,renren',
      ),
      'throws' => 'SysException',
    ),
  ),
);
