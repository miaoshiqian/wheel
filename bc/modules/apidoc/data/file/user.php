<?php 
return array (
  'doc' => 
  array (
    'title' => 'Class UserController',
    'brief' => '',
  ),
  'func' => 
  array (
    'loginAction' => 
    array (
      'brief' => '',
      'title' => '登录接口',
      'param' => 
      array (
        0 => 'string $UserName 用户名或手机号',
        1 => 'string $Password 密码，格式为MD5(明文)
',
      ),
      'author' => '李登科<lidengke@xiongying.com>',
      'return' => 'array
 array (
  \'code\' => 0,
  \'message\' => \'成功\',
  \'data\' =>
  array (
    \'UserID\' => 7516850,
    \'UserName\' => \'18210948904\',
    \'RegMobile\' => \'18210948904\',
    \'ExistRegMobile\' => 1,
    \'GroupID\' => 1,
    \'Password\' => \'a31611d8f31c42c4\',
    \'Token\' => \'jFlMa5t%2BB1f84I8oxUAt4xXbfD%2FcJkS4vZTahFjjlSCJLxrG%2BgnAGQguKbLgNcug6KMdl1h7ZR%2F7JlejKqzcqF7J0LycEqhC\',
    \'MobileContext\' => \'77E1A0D52E557EDFF4FCD0277C2C86FA9B1CC0FDC4A0DDD899C704C9D53043F7\',
    \'Portrait\' => \'http://img02.exam8.com/img2013/wantiku/paihang/default_rank_head.png\',
    \'SubjectParentId\' => 0,
  ),
)',
    ),
    'registerAction' => 
    array (
      'brief' => '',
      'title' => '注册接口',
      'param' => 
      array (
        0 => 'string $UserName 手机号',
        1 => 'string $Password 密码明文',
        2 => 'int $VerCode 手机下发的验证码
',
      ),
      'author' => '李登科<lidengke@xiongying.com>',
      'return' => 'array
 array (
  \'code\' => 0,
  \'message\' => \'成功\',
  \'data\' =>
  array (
    \'UserID\' => 7516850,
    \'UserName\' => \'18210948904\',
    \'RegMobile\' => \'18210948904\',
    \'ExistRegMobile\' => 1,
    \'GroupID\' => 1,
    \'Password\' => \'a31611d8f31c42c4\',
    \'Token\' => \'jFlMa5t%2BB1f84I8oxUAt4xXbfD%2FcJkS4vZTahFjjlSCJLxrG%2BgnAGQguKbLgNcug6KMdl1h7ZR%2F7JlejKqzcqF7J0LycEqhC\',
    \'MobileContext\' => \'77E1A0D52E557EDFF4FCD0277C2C86FA9B1CC0FDC4A0DDD899C704C9D53043F7\',
    \'Portrait\' => \'http://img02.exam8.com/img2013/wantiku/paihang/default_rank_head.png\',
  ),
)',
    ),
  ),
);
