<?php
/**
 * @brief 简介：
 * @author 龙卫国<longweiguo@xiongying.com>
 * @date 15/9/20
 */

require_once __DIR__ . '/../common/DocCreater.class.php';

class IndexController extends BcController{

	protected $docCreater = null;

	public function init() {
		parent::init();

		$sourceDir = ROOT_PATH . '/appserv/controller';
		$outputDir = __DIR__ . '/../data';
		$this->docCreater = new DocCreater('Controller.class.php', $sourceDir, $outputDir);
	}

	public function defaultAction() {
		$url = $this->createUrl('index', 'updateLog', array(), 'apidoc');
		$this->turnTo($url);
	}

	public function updateLogAction() {
		$data = $this->docCreater->getUpdateLog(1, 20);
		$this->render('update_log.php', array(
			'updateLogData' => json_encode($data),
		));
	}

	public function ajaxGetUpdateLogAction() {
		$startLine = Request::getGET('startLine', 1);
		$data = $this->docCreater->getUpdateLog($startLine, 20);
		$this->outputJson(0, '', $data);
	}

	public function detailAction($params) {
		if (empty($params[0])) {
			$this->jumpToFail('您要查看的文档不存在');
		}
		$docName = $params[0];
		try {
			$data = $this->docCreater->getDetail($docName);
		} catch (UserException $e) {
			$this->jumpToFail($e->getMessage());
		}

		$this->render('detail.php', array(
			'docName' => $docName,
			'docDetail' => $data,
		));
	}

	public function createAction(){
		$this->docCreater->create();

		$url = $this->createUrl('index', 'updateLog');
		$this->jumpToSuccess('创建完毕', $url);
	}
}