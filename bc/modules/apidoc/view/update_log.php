<div class="row">
	<div class="col-xs-12">
		<div id="container_update_log"></div>

		<div class="form-actions center" id="load_more_log">
			<button type="button" class="btn btn-sm btn-info">
				查看更多
				<i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
			</button>
		</div>
	</div>
</div>
<!--js:start-->
<script type="text/javascript">
	jQuery(function ($) {
		var $container = $('#container_update_log');
		var $btnLoad = $('#load_more_log').find('button');
		var data = <?php $this->put($this->updateLogData, true); ?>;
		var startLine = 1;

		function createHtml() {
			if (!data || !data[0]) {
				return;
			}

			startLine = data[1];

			$.each(data[0], function (date, logs) {
				$container.append('<h3>' + date + '</h3>');
				$.each(logs, function (key, log) {
					$container.append('<p>' + log.type + ' <a href="/index/detail/' + log.name + '/#' + log.fnnc + '">' + log.name + '.' + log.func + '</a> ' + (log.title ? log.title : '') + '</p>');
					if (log.brief) {
						$container.append('<pre>' + log.brief + '</pre>');
					}
				});
			});

			if (data[2]) {
				$btnLoad.removeClass('btn-info').addClass('disabled').unbind('click');
			}
		}

		createHtml();

		var $icon = $(this).find('i');

		function setLoadiing() {
			$icon.removeClass('fa-refresh').addClass('fa-spinner').addClass('fa-spin');
		}

		function endLoading() {
			$icon.removeClass('fa-spinner').removeClass('fa-spin').addClass('fa-refresh');
		}

		function getUpdateLogData() {
			$.ajax({
				url: "/index/ajaxGetUpdateLog/",
				dataType: "json",
				async: true,
				data: {"startLine": data[1]},
				type: "GET",
				beforeSend: function () {
					setLoadiing();
				},
				success: function (req) {
					console.log(req);
					endLoading();
					if (req.code) {
						$.gritter.add({
							title: '错误提示',
							text: req.message,
							class_name: 'gritter-error gritter-center gritter-light'
						});
					} else {
						data = req.data;
						createHtml();
					}
				},
				error: function (e) {
					console.log(e);
					endLoading();
					$.gritter.add({
						title: '请求错误',
						text: 'error',
						class_name: 'gritter-error gritter-center gritter-light'
					});
				}
			});
		}

		if (!data[2]) {
			$btnLoad.click(function () {
				getUpdateLogData();
			});
		}
	});
</script>
<!--js:end-->