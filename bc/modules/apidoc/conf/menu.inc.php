<?php
/**
 * 简介: log菜单配置
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/20
 * Time: 11:21
 * Copyright (c) 2015, mingtian.com
 */

return array(
	array(
		'url'   => '/index/create/',
		'text'  => '执行更新',
		'icon'  => 'fa-pencil-square-o',
		'class' => '',
		'child' => array(),
	),
	array(
		'url'   => '/index/updateLog/',
		'text'  => '更新日志',
		'icon'  => 'fa-asterisk',
		'class' => '',
		'child' => array(),
	),
	array(
		'url'   => '',
		'text'  => '文档列表',
		'icon'  => 'fa-list',
		'class' => '',
		'child' => is_file(__DIR__ . '/../data/menu_doc.php') ? include __DIR__ . '/../data/menu_doc.php' : array(),
	),
);