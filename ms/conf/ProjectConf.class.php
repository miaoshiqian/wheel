<?php
/**
 * @brief 简介：
 * @author 陈朝阳<chenchaoyang@qingchuangba.com>
 * @date 16/1/20
 * @copyright Copyright (c) 2016 轻创吧 Inc. (http://www.qingchuangba.com)
 */

class ProjectConf {
    //登陆session
    const IMG_CODE_SESSION = 'session_wheel_img_code';
    //登陆session
    const SMS_CODE_SESSION = 'session_wheel_sms_code';
    //登陆session
    const LOGIN_SESSION = 'session_wheel_login_auth';
    //登陆授权cookie
    const AUTH_COOKIE_KEY = 'cookie_wheel_login_auth';

    const SITE_DOMAIN = 'wheelsrnr.com';

    public static $AGE_CONF = array(
        1 => '<1',
        2 => '1-3',
        3 => '3-5',
        4 => '5-8',
        5 => '>8',
    );
    public static $AGE_RANGE = array(
        1 => array(0,1),
        2 => array(1,3),
        3 => array(3,5),
        4 => array(5,8),
        5 => array(8,99),
    );

    public static $SORT_CONF = array(
        1 => '按价格从低到高',
        2 => '按价格从高到低',
        3 => '按里程从低到高',
        4 => '按里程从高到低'
    );
}