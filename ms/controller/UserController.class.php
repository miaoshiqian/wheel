<?php
/**
 *  用户登录注册相关
 * User: 龙卫国<longweiguo@xiongying.com>
 * Date: 2015/12/2
 */

class UserController extends MsController {
    public function loginAction() {
        $this->render('/' . self::$LANGUAGE . '/user/login.php',array(

        ));
    }
    public function registerAction() {
        $provinceList = LocationInterface::getProvince();
        $this->render('/' . self::$LANGUAGE . '/user/register.php',array(
            'provinceList' => $provinceList,
        ));
    }

    public function garageAction() {
        $this->render('/' . self::$LANGUAGE . '/user/garage.php',array(

        ));
    }

    public function compareAction() {
        $this->render('/' . self::$LANGUAGE . '/user/compare.php',array(

        ));
    }

    public function payAction() {
        if (!$this->checkUser()) { //校验登陆
            Response::redirect('/user/login/');
        }
        $id = Request::getGET('car_id', ''); //车源id
        $carInfo = CarModel::getInstance()->getRow('id,title,seller_price,miles,vin_code,status', array(array('id', '=', $id)));
        if (empty($carInfo) || $carInfo['status'] != EnumCar::STATUS_ONLINE) {
            $this->errorAction();
        }

        $coverRow = CarImagesModel::getInstance()->getRow('*', array(array('car_id', '=', $id), array('type', '=', 1)));
        $cover = !empty($coverRow) ? QiNiu::formatImageUrl($coverRow['url'], array('width' => 200, 'height' => 200)) : '';
        $bigCover = !empty($coverRow) ? QiNiu::formatImageUrl($coverRow['url'], array('width' => 650, 'height' => 390)) : '';

        $this->render('/' . self::$LANGUAGE . '/user/pay.php',array(
            'carInfo' => $carInfo,
            'cover' => $cover,
            'bigCover' => $bigCover,
            'userInfo' => $this->userInfo,
        ));
    }

    public function orderAction() {
        if (!$this->checkUser()) {
            Response::redirect('/user/login/');
        }

        //$order_list =
        $this->render('/' . self::$LANGUAGE . '/user/order.php',array(

        ));
    }

    public function ajaxRegisterAction() {
        $name = Request::getPOST('name', '');
        $province = Request::getPOST('province', '');
        $city = Request::getPOST('city', '');
        $district = Request::getPOST('district', '');
        $phone = Request::getPOST('phone', '');
        $username = Request::getPOST('username', '');
        $mail = Request::getPOST('mail', '');
        $password = Request::getPOST('password', '');
        $code = Request::getPOST('code', '');
        if (empty($name) || !ProjectHelper::isMobile($phone) || empty($username) ||
            empty($mail) || !filter_var($mail, FILTER_VALIDATE_EMAIL) || empty($password) || empty($code)) {
            $this->outputJson(1, '参数错误', '');
        }
        if (md5(strtolower($code)) != Session::getValue(ProjectConf::IMG_CODE_SESSION)) {
            $this->outputJson(1, '验证码错误', '');
        }
        if (UserInterface::getCount(array('filters' => array(array('username', '=' , $username))))) {
            $this->outputJson(1, '用户名已经存在', '');
        }
        if (UserInterface::getCount(array('filters' => array(array('mobile', '=' , $phone))))) {
            $this->outputJson(1, '手机号已经存在', '');
        }
        if (UserInterface::getCount(array('filters' => array(array('mail', '=' , $mail))))) {
            $this->outputJson(1, '邮箱已经存在', '');
        }
        $userId = UserInterface::add(array(
            'real_name'   => $name,
            'mobile'      => $phone,
            'username'    => $username,
            'mail'        => $mail,
            'passwd'      => md5($password),
        ));
        if ($userId) {
            $this->outputJson(0, '注册成功，马上去登录吧！', '');
        } else {
            $this->outputJson(0, '未知错误', '');
        }
    }

    public function ajaxLoginAction() {
        $username = Request::getPOST('username', '');
        $password = Request::getPOST('password', '');
        if (!$username || !$password) {
            $this->outputJson(1, '参数错误', '');
        }
        $user = array();
        if (ProjectHelper::isMobile($username)) {
            $count = UserInterface::getCount(array(
                'filters' => array(
                    array('mobile', '=', $username),
                    array('passwd', '=', md5($password))
                )
            ));
            if ($count) {
                $user = UserInterface::getByMobile(array('mobile' => $username));
            }
        } else if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $count = UserInterface::getCount(array(
                'filters' => array(
                    array('mail', '=', $username),
                    array('passwd', '=', md5($password))
                )
            ));
            if ($count) {
                $user = UserInterface::getByMail(array('mail' => $username));
            }
        } else {
            $count = UserInterface::getCount(array(
                'filters' => array(
                    array('username', '=', $username),
                    array('passwd', '=', md5($password))
                )
            ));
            if ($count) {
                $user = UserInterface::getByUsername(array('username' => $username));
            }
        }
        if ($user) {
            $autStr = UserInterface::encodeSsoTicket(array('username' => $user['username'], 'password' => $user['passwd']));
            setcookie(ProjectConf::AUTH_COOKIE_KEY, $autStr, time() + 3600 * 24, '/', '.wheelsrnr.com');
            $userInfo = Session::setValue(ProjectConf::LOGIN_SESSION, array(
                'auth_str' => $autStr,
                'user'     => $user
            ));
            $this->outputJson(0, '登陆成功', '');
        } else {
            $this->outputJson(1, '用户名或密码错误', '');
        }
    }
}