<?php
/**
 *
 * @author 陈朝阳<chenchaoyang@XXX.com>
 * @date 15/9/20
 */

class DetailController extends MsController{
    public function init() {
        parent::init();
        if (self::$LANGUAGE == 'en') {
//            EnumCar::$GEARBOX_TEXT = array(
//                EnumCar::Unknown => 'Unknown',
//                EnumCar::Manual => 'Manual',
//                EnumCar::Auto => 'Auto',
//                EnumCar::DSG => 'DSG',
//                EnumCar::AutoManual => 'AutoManual',
//                EnumCar::CVT => 'CVT',
//            );
//            EnumCar::$STRUCTURE_TEXT = array(
//                EnumCar::Hatchback => 'Hatchback',
//                EnumCar::Sedan => 'Sedan',
//                EnumCar::SUV => 'SUV',
//                EnumCar::MPV => 'MPV',
//                EnumCar::Wagon => 'Sedan Wagon',
//                EnumCar::Sports => 'Sports Car',
//                EnumCar::PickUp => 'Pickup Trucks',
//                EnumCar::Minivan => 'Minibus',
//            );
        }
    }

    public function defaultAction($params) {
        $id = Request::getGET('id', '');
        if (!$id) {
            $id = $params[0];
        }
        if (!$id) {
            $this->errorAction();
        }
        $carInfo = VehicleModel::getInstance()->getRow('*', array(array('id', '=', $id)));
        if ($carInfo['city_id']) {
            $cityName = UsLocationInterface::getCityNameById(array('id' => $carInfo['city_id']));
            $carInfo['city_name'] = $cityName;
        }
        if ($carInfo['province_id']) {
            $stateName = UsLocationInterface::getStateNameById(array('id' => $carInfo['province_id']));
            $carInfo['state_name'] = $stateName;
        }
        if ($carInfo['status'] != EnumCar::STATUS_ONLINE) {
            $this->errorAction();
        }
        $carInfo = $this->getExtendInfo($carInfo);
        $this->render('/' . self::$LANGUAGE . '/car/detail.php', array(
            'carInfo' => $carInfo,
        ));
    }

    private function getExtendInfo($carInfo) {
        $carInfo['structure_text'] = isset(EnumCar::$BODY_STYLE[$carInfo['body_style']]) ? EnumCar::$BODY_STYLE[$carInfo['body_style']] : 'unknown';
        $carInfo['gearbox_text'] = isset(EnumCar::$TRANSMISSION[$carInfo['transmission']]) ? EnumCar::$TRANSMISSION[$carInfo['transmission']] : 'unknown';
        $imageList = CarImagesModel::getInstance()->getAll('*', array(
            array('car_id', '=', $carInfo['id']),
            array('status', '=', 1),
        ));
        if (!empty($imageList)) {
            foreach ($imageList as &$image) {
                $image['url'] = QiNiu::formatImageUrl($image['url']);
            }
        }
        $imageList = is_array($imageList) ? $imageList : array();
        $carInfo['image_list'] = $imageList;

        $carModelInfo = VehicleModelInterface::getModelById($carInfo['model_trim']);
        $carInfo['model_info'] = $carModelInfo;
        if ($carInfo['highlights']) {
            $carInfo['highlights'] = json_decode($carInfo['highlights'], true);
            foreach ($carInfo['highlights'] as $highLightId) {
                $highLight = EnumCar::$HIGH_LIGHT[$highLightId];
                if ($highLight) {
                    $carInfo['high_lights_text'][$highLightId] = $highLight;
                }
            }
        }
        $carInfo = array_merge($carInfo, $carModelInfo);
        return $carInfo;
    }
}