<?php
/**
 *
 * @author 陈朝阳<chenchaoyang@XXX.com>
 * @date 15/9/20
 */

class ListController extends MsController{
    private $_pageSize = 10;

    private $_brandInfo = array();

    private $_seriesInfo = array();

    private $_searchParams = array();

    private $_requestParams = array();

    private $_requestNames = array();

    private $_urlUtil = array();

    public function defaultAction() {
        if (self::$LANGUAGE == 'en') {
            ProjectConf::$SORT_CONF = array(
                1 => 'Price:Low to High',
                2 => 'Price:High to Low',
                3 => 'Mileage:Low to High',
                4 => 'Mileage:High to Low',
            );
//            EnumCar::$GEARBOX_TEXT = array(
//                EnumCar::Unknown => 'Unknown',
//                EnumCar::Manual => 'Manual',
//                EnumCar::Auto => 'Auto',
//                EnumCar::DSG => 'DSG',
//                EnumCar::AutoManual => 'AutoManual',
//                EnumCar::CVT => 'CVT',
//            );
//            EnumCar::$STRUCTURE_TEXT = array(
//                EnumCar::Hatchback => 'Hatchback',
//                EnumCar::Sedan => 'Sedan',
//                EnumCar::SUV => 'SUV',
//                EnumCar::MPV => 'MPV',
//                EnumCar::Wagon => 'Sedan Wagon',
//                EnumCar::Sports => 'Sports Car',
//                EnumCar::PickUp => 'Pickup Trucks',
//                EnumCar::Minivan => 'Minibus',
//            );
        }
        $this->_urlUtil = new UrlUtil();
        $this->_urlUtil->parseUrl();
        $this->_requestParams = $this->_urlUtil->requestParams;

        $page = $this->_requestParams['page'];
        $page = $page > 0 ? $page : 1;
        $offset = ($page - 1) * $this->_pageSize;
        $limit = $this->_pageSize;
        $this->_buildSearch();
        $carList = VehicleModel::getInstance()->getAll('*',$this->_searchParams['filter'],$this->_searchParams['order'], $limit, $offset);
        $total = VehicleModel::getInstance()->getCount($this->_searchParams['filter']);
        if (!empty($carList)) {
            foreach ($carList as &$car) {
                $imgList = CarImagesModel::getInstance()->getAll('type,url', array(
                    array('car_id', '=', $car['id']),
                    array('status', '=', EnumCarImage::STATUS_NORMAL)
                ));
                if ($imgList) {
                    foreach($imgList as $img) {
                        $img['big_url'] = QiNiu::formatImageUrl($img['url'] . '?imageMogr2/thumbnail/787x', array('width'=>787,'height'=>473,'mode'=>1));
                        $img['small_url'] = QiNiu::formatImageUrl($img['url'], array('width'=>403,'height'=>231,'mode'=>1));
                        $car['image_list'][] = $img;
                    }
                }
            }
        }
        $carList = ProjectHelper::formatCarList($carList);

        $pageData = Pager2::getPager($total, $page, $this->_pageSize);

        $queryOptions = $this->_buildQueryOptions();
        
        $sortTitle = '';
        if (isset(ProjectConf::$SORT_CONF[$this->_requestParams['sort']])) {
            $sortTitle = ProjectConf::$SORT_CONF[$this->_requestParams['sort']];
        }

        $this->render('/' . self::$LANGUAGE . '/car/list.php',array(
            'carList' => $carList,
            'pager' => $pageData,
            'queryOptions' => $queryOptions,
            'brandInfo' => $this->_brandInfo,
            'seriesInfo' => $this->_seriesInfo,
            'requestNames' => $this->_requestNames,
            'requestParams' => $this->_requestParams,
            'sortTitle' => $sortTitle,
            'urlUtil' => $this->_urlUtil
        ));
    }

    private function _buildSearch() {
        $searchParams = array(array('status', '=', EnumCar::STATUS_ONLINE));
        if (isset($this->_requestParams['brand'])) {
            $models = VehicleModelInterface::getModelsByFilter(array('brand' => $this->_requestParams['brand']));
            $models = array_column($models, 'model_id');
            if ($models) {
                $searchParams[] = array('model_trim', 'in', $models);
                $this->_brandInfo['name'] = $this->_requestParams['brand'];
            }
        }
        if (isset($this->_requestParams['series'])) {
            $models = VehicleModelInterface::getModelsByFilter(array('series' => $this->_requestParams['series']));
            $models = array_column($models, 'model_id');
            if ($models) {
                $searchParams[] = array('model_trim', 'in', $models);
                $this->_seriesInfo['name'] = $this->_requestParams['series'];
            }
        }
        if (isset($this->_requestParams['gearbox_id'])) {
            $this->_requestNames['gearbox'] = EnumCar::$TRANSMISSION[$this->_requestParams['gearbox_id']];
            $searchParams[] = array('transmission', '=', $this->_requestParams['gearbox_id']);
        }
        if (isset($this->_requestParams['age']) && isset(ProjectConf::$AGE_CONF[$this->_requestParams['age']])) {
            $this->_requestNames['age'] = ProjectConf::$AGE_CONF[$this->_requestParams['age']];
            $ageRange = ProjectConf::$AGE_RANGE[$this->_requestParams['age']];
            $minTime = date('Y', time()) - $ageRange[1];
            $maxTime = date('Y', time()) - $ageRange[0];
            $searchParams[] = array('vehicle_year', '>=', $minTime);
            $searchParams[] = array('vehicle_year', '<=', $maxTime);
        }
        if (isset($this->_requestParams['min_price']) && $this->_requestParams['min_price'] > 0) {
            $searchParams[] = array('listing_price', '>=', $this->_requestParams['min_price']);
        }
        if (isset($this->_requestParams['max_price']) && $this->_requestParams['max_price'] > 0) {
            $searchParams[] = array('listing_price', '<=', $this->_requestParams['max_price']);
        }
        if (isset($this->_requestParams['min_mile']) && $this->_requestParams['min_mile'] > 0) {
            $searchParams[] = array('mile', '>=', $this->_requestParams['min_mile'] / 10000);
        }
        if (isset($this->_requestParams['max_mile']) && $this->_requestParams['max_mile'] > 0) {
            $searchParams[] = array('mile', '<=', $this->_requestParams['max_mile'] / 10000);
        }
        if (isset($this->_requestParams['keyword'])) {
            $searchParams[] = array('title', 'like', "%{$this->_requestParams['keyword']}%");
        }

        $sortParams = array();
        if ($this->_requestParams['sort']) {
            switch($this->_requestParams['sort']) {
                case 1:
                    $sortParams['listing_price'] = 'asc';
                    break;
                case 2:
                    $sortParams['listing_price'] ='desc';
                    break;
                case 3:
                    $sortParams['mile'] = 'asc';
                    break;
                case 4:
                    $sortParams['mile'] = 'desc';
                    break;
            }
        }
        $this->_searchParams['filter'] = $searchParams;
        $this->_searchParams['order'] = $sortParams;
    }

    private function _buildQueryOptions() {
        $brandOptions = array();
        $brandList = VehicleModelInterface::getBrandList();
        if ($brandList) {
            $urlInfo = $this->_urlUtil->createListUrl('brand', '', array('series' => ''));
            $urlInfo['name'] = self::$LANGUAGE == 'en' ? 'default' : '不限';
            $brandOptions[] = $urlInfo;
            foreach($brandList as $brand) {
                $urlInfo = $this->_urlUtil->createListUrl('brand', $brand['make'], array('series' => 0));
                $urlInfo['name'] = $brand['make'];
                $brandOptions[] = $urlInfo;
            }
        }

        $seriesOptions = array();
        $urlInfo = $this->_urlUtil->createListUrl('series', '');
        $urlInfo['name'] = self::$LANGUAGE == 'en' ? 'default' : '不限';
        $seriesOptions[] = $urlInfo;
        if (isset($this->_requestParams['brand']) && $this->_requestParams['brand']) {
            $seriesList = VehicleModelInterface::getSeriesList($this->_requestParams['brand']);
            foreach($seriesList as $series) {
                $urlInfo = $this->_urlUtil->createListUrl('series', $series['name']);
                $urlInfo['name'] = $series['name'];
                $seriesOptions[] = $urlInfo;
            }
        }

        $ageOptions = array();
        $urlInfo = $this->_urlUtil->createListUrl('age', 0);
        $urlInfo['name'] = self::$LANGUAGE == 'en' ? 'default' : '不限';
        $ageOptions[] = $urlInfo;
        foreach (ProjectConf::$AGE_CONF as $value => $name) {
            $urlInfo = $this->_urlUtil->createListUrl('age', $value);
            $urlInfo['name'] = $name;
            $ageOptions[] = $urlInfo;
        }

        $gearboxOptions = array();
        $urlInfo = $this->_urlUtil->createListUrl('gearbox_id', 0);
        $urlInfo['name'] = self::$LANGUAGE == 'en' ? 'default' : '不限';
        $gearboxOptions[] = $urlInfo;
        foreach (EnumCar::$TRANSMISSION as $value => $name) {
            $urlInfo = $this->_urlUtil->createListUrl('gearbox_id', $value);
            $urlInfo['name'] = $name;
            $gearboxOptions[] = $urlInfo;
        }

        $sortOptions = array();
        $urlInfo = $this->_urlUtil->createListUrl('sort', 0);
        $urlInfo['name'] = self::$LANGUAGE == 'en' ? 'default option' : '默认排序';
        $sortOptions[] = $urlInfo;
        foreach (ProjectConf::$SORT_CONF as $value => $name) {
            $urlInfo = $this->_urlUtil->createListUrl('sort', $value);
            $urlInfo['name'] = $name;
            $sortOptions[] = $urlInfo;
        }

        return array(
            'brand_options' => $brandOptions,
            'series_options' => $seriesOptions,
            'age_options' => $ageOptions,
            'gearbox_options' => $gearboxOptions,
            'sort_options' => $sortOptions
        );
    }
}
