<?php

/**
 *
 * User: 陈朝阳<chenchaoyang@wheel.com>
 * Date: 2015/12/8
 * Time: 15:12
 */
define('VALID_WIDTH',60);
define('VALID_HEIGHT',20);
define('VALID_CODE_TYPE', 3);
define('VALID_CODE_LENGTH', 4);
class AjaxController extends MsController {

    public function cityAction() {
        $province = Request::getPOST('province_id', '');
        if (!$province) {
            $this->outputJson(1, '参数错误');
        }
        $cityList = LocationInterface::getCityByProvinceId(array(
            'province_id' => $province
        ));
        if (!$cityList) {
            $cityList = array();
        }
        $this->outputJson(0, '', $cityList);
    }

    public function districtAction() {
        $city = Request::getPOST('city_id', '');
        if (!$city) {
            $this->outputJson(1, '参数错误');
        }
        $districtList = LocationInterface::getDistrictByCityId(array(
            'city_id' => $city
        ));
        if (!$districtList) {
            $districtList = array();
        }
        $this->outputJson(0, '', $districtList);
    }

    public function showCodeAction() {
        header("Pragma: no-cache");
        header("Cache-control: no-cache");

        $image = new Image();
        $value = $image->imageValidate(106, 38, 4, 3,'#3e3e3e','#B6B6B6');
        Session::setValue(ProjectConf::IMG_CODE_SESSION, md5(strtolower($value)));
        $image->display();
    }

    public function sendMsgAction() {
        $mobile = Request::getPOST('mobile');
        if (empty($mobile) || !ProjectHelper::isMobile($mobile)) {
            $this->outputJson('', 1, '请输入正确的手机号！');
        }
        $code = mt_rand(100000, 999999);
        Session::setValue(ProjectConf::SMS_CODE_SESSION, $code);

        $ret = SmsInterface::send(array(
            'conf'   => SMSConfig::$LOGIN_CODE,
            'code'   => $code,
            'mobile' => $mobile
        ));
        if (!$ret) {
            $this->outputJson(1, '系统繁忙，请稍后重试！', '');
        } else {
            $this->outputJson(0, '发送成功！', '');
        }
    }

    public function checkCodeAction() {
        $jsonp      = RequestUtil::getGET('callback', '');
        $name       = RequestUtil::getGET('code_name', 'code');
        $cookie     = RequestUtil::getCOOKIE($name);
        $val        = RequestUtil::getGET($name, '');
        if (RequestUtil::getGET('form_check', false)) {
            //是否使用了表单控件
            if ($cookie == md5(strtolower($val))) {
                $flag['error'] = 0;
            } else {
                $flag['error'] = 1;
            }
            $flag = json_encode($flag);
        } else {
            if ($cookie == md5(strtolower($val))) {
                $flag = 1;
            } else {
                $flag = 0;
            }
        }
        echo $jsonp . '(' . $flag . ')';
    }

    public function genListUrlAction() {
        $originUrl = Request::getPOST('origin_url');
        if (!$originUrl) {
            $this->outputJson(1, '参数错误', '');
        }
        $urlUtil = new UrlUtil();
        $urlUtil->parseUrl($originUrl);
        $minMile = Request::getPOST('min_mile', 0);
        $maxMile = Request::getPOST('max_mile', 0);
        if ($minMile || $maxMile) {
            $urlInfo = $urlUtil->createListUrl('min_mile', $minMile, array('max_mile' => $maxMile));
            $this->outputJson(0, '', $urlInfo);
        }

        $minPrice = Request::getPOST('min_price', 0);
        $maxPrice = Request::getPOST('max_price', 0);
        if ($minPrice || $maxPrice) {
            $urlInfo = $urlUtil->createListUrl('min_price', intval($minPrice), array('max_price' => $maxPrice));
            $this->outputJson(0, '', $urlInfo);
        }
    }
}