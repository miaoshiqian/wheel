<?php

class MsController extends BaseController {
    public static $LANGUAGE = 'en';
    

    public function __construct() {
        parent::__construct();
        // .....
    }

    public function init() {
        self::$LANGUAGE = Request::getGET('lan', 'en');
        if (self::$LANGUAGE != 'cn') {
            self::$LANGUAGE = 'en';
        }
        $pathInfo = parse_url(Request::getCurrentUrl());
        $this->assign('pathInfo', $pathInfo);

        $lanSwitchUrl = Request::getCurrentUrl();
        if (isset($_GET['lan'])) {
            $lanSwitchUrl = self::$LANGUAGE == 'cn' ?
                str_replace('lan=cn', 'lan=en', $lanSwitchUrl) : str_replace('lan=en', 'lan=cn', $lanSwitchUrl);
        } else {
            if (isset($pathInfo['query'])) {
                $lanSwitchUrl = self::$LANGUAGE == 'cn' ?
                    $lanSwitchUrl . '&lan=en' : $lanSwitchUrl . '&lan=cn';
            } else {
                $lanSwitchUrl = self::$LANGUAGE == 'cn' ?
                    $lanSwitchUrl . '?lan=en' : $lanSwitchUrl . '?lan=cn';
            }
        }
        $this->assign('lanSwitchUrl', $lanSwitchUrl);
    }

    protected function checkUser($update = false) {
        $authCookie = Util::getFromArray(ProjectConf::AUTH_COOKIE_KEY, $_COOKIE, null);
        if (!empty($authCookie)) {
            $sessionInfo = Session::getValue(ProjectConf::LOGIN_SESSION);
            if ($authCookie == $sessionInfo['auth_str']) {
                $ticketInfo = UserInterface::decodeSsoTicket(array('passport' => $authCookie));
                if (count($ticketInfo) < 4) {
                    //无效
                    setcookie(ProjectConf::AUTH_COOKIE_KEY, '', time() - 1, '/', ProjectConf::SITE_DOMAIN);
                } elseif (time() - $ticketInfo[3] > 3600) {
                    //过期
                    setcookie(ProjectConf::AUTH_COOKIE_KEY, '', time() - 1, '/', ProjectConf::SITE_DOMAIN);
                } else {
                    if ($update) {
                        $this->userInfo = UserInterface::getById(array('id' => $sessionInfo['user']['id']));
                        if (!empty($this->userInfo)) {
                            Session::setValue(ProjectConf::LOGIN_SESSION, array(
                                'auth_str' => $sessionInfo['cookie'],
                                'user'     => $this->userInfo
                            ));
                        }
                    } else {
                        $this->userInfo = $sessionInfo['user'];
                    }
                    return true;
                }
            }
        }
        return false;
    }

	/**
	 * 给模板赋值
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string|array $key
	 * @param null         $value
	 */
	public function assign($key, $value = null) {
		$this->view->assign($key, $value);
	}

	/**
	 * 渲染模板
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string $tplFile
	 * @param array  $params
	 */
	public function render($tplFile, $params = array()) {
		$content = $this->view->fetch($tplFile, $params);
		Response::output($content);
	}

	public function createUrl($controller = 'index', $action = 'default', $params = array(), $module = '', $get = array()) {
		return Router::createUrl($controller, $action, $params, $module, $get);
	}

	public function redirectSafe($url) {
		$parseUrl = parse_url($url);
		$domain = isset($parseUrl['hostname']) ? $parseUrl['hostname'] : '';
		if($domain && false === strpos($domain, 'qingchuangba.com')){ // 非本站跳转
			Log::warn('safe.jumpurl', $url);
			$url = '/';
		}
		Response::redirect($url);
	}

	public function jumpToFail($message, $url = '') {
		if(Request::isAjax()){
			$this->outputJson(1, $message);
		}
		Response::redirect($this->createUrl('index', 'error', array(), '', array(
			'message' => $message,
			'code'    => 1,
			'url'     => $url,
		)));
	}

	public function jumpToSuccess($message, $url = '') {
		if(Request::isAjax()){
			$this->outputJson(0, $message);
		}
		Response::redirect($this->createUrl('index', 'success', array(), '', array(
			'message' => $message,
			'code'    => 0,
			'url'     => $url,
		)));
	}

	public function outputJson($code = 0, $message = '', $extraData = array()) {
		$params = array(
			'code'    => $code,
			'message' => $message,
			'data'    => $extraData,
		);
		Response::outputJson($params);
		exit;
	}

	public function errorAction() {
		$message = empty($_GET['message']) ? '对不起！操作失败，请稍后再试。' : $_GET['message'];
		$code = empty($_GET['code']) ? 1 : $_GET['code'];
		$jumpUrl = empty($_GET['url']) ? '/' : $_GET['url'];
		if (Request::isAjax()) {
			Response::outputJson(array(
				'message' => $message,
				'code'    => $code
			));
		} else if (in_array('?', $_GET)) {
			$key = array_search('?', $_GET);
			Response::outputJsonp($message, $key);
		} else {
			$this->assign(array(

			));
			$this->render('error.php', array(
				'jumpUrl' => $jumpUrl,
				'message' => $message,
			));
		}
		exit;
	}

	public function successAction() {
		$message = empty($_GET['message']) ? '操作成功' : $_GET['message'];
		$code = 0;
		$jumpUrl = empty($_GET['url']) ? '/' : $_GET['url'];
		if (Request::isAjax()) {
			Response::outputJson(array(
				'message' => $message,
				'code'    => $code
			));
		} else if (in_array('?', $_GET)) {
			$key = array_search('?', $_GET);
			Response::outputJsonp($message, $key);
		} else {
			$this->assign(array(

			));
			$this->render('success.php', array(
				'jumpUrl' => $jumpUrl,
				'message' => $message,
			));
		}
		exit;
	}
}