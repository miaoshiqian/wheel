<?php

/**
 *  用户登录有关的session cookie等存储与检测
 * User: 龙卫国<longweiguo@xiongying.com>
 * Date: 2015/12/8
 */
class UserAuth {
	private static $_userId    = null;      // 用户id
	private static $_username  = null;      // 用户名
	private static $_loginTime = null;      // 登录时间
	private static $_hashCode  = null;      // 校验码
	private static $_isLogin   = null;      // 是否已登录

	/**
	 * 初始化，从cookie中把user_id取出来，赋予成员变量
	 * @author 龙卫国<longweiguo@xiongying.com>
	 */
	private static function _init() {
		static $init = false;
		if ($init) return;
		$init = true;

		$userInfo = Cookie::get('loginInfo');
		$userInfo = $userInfo ? json_decode($userInfo, 1) : false;

		if ($userInfo) {
			self::$_userId    = isset($userInfo['user_id']) ? $userInfo['user_id'] : null;
			self::$_username  = isset($userInfo['username']) ? $userInfo['username'] : null;
			self::$_hashCode  = isset($userInfo['code']) ? $userInfo['code'] : null;
		}
	}

	/**
	 * 设置用户登录后保存信息，包括session和cookie
	 * @author 龙卫国<longweiguo@xiongying.com>
	 *
	 * @param      $userInfo
	 * @param null $thirdToken
	 *
	 * @return bool
	 */
	public static function setLogin($userInfo, $thirdToken = null) {
		if (empty($userInfo) || empty($userInfo['user_id'])) {
			return false;
		}

		$time = time();
		$code = substr(md5(uniqid(mt_rand(1, 10000))), 0, 16); // 给每个用户生成一个唯一校验码

		self::$_userId    = $userInfo['user_id'];
		self::$_username  = $userInfo['username'];
		self::$_loginTime = $time;

		$cookieData = array(
			'user_id'  => self::$_userId,
			'username' => self::$_username,
			'code'     => $code,
		);
		Cookie::set(
			'loginInfo',
			json_encode($cookieData),
			GlobalConfig::MS_COOKIE_EXPIRE(),
			'/',
			GlobalConfig::MS_COOKIE_DOMAIN
		);

		self::_sessionInit(self::$_userId);
		Session::setValue('userInfo', $userInfo);
		Session::setValue('loginTime', $time);
		Session::setValue('hashCode', $code);
		if (!empty($thirdToken)) {
			Session::setValue('thirdToken', $thirdToken);
		}
	}

	/**
	 * 用户登出后，清除session数据
	 * @author 龙卫国<longweiguo@xiongying.com>
	 */
	public static function setLogout() {
		self::$_isLogin = false;
		self::_init();
		if (self::$_userId) {
			self::_sessionInit(self::$_userId);
			Session::delete('userInfo');
			Session::delete('loginTime');
			Session::delete('thirdToken');
			Session::delete('hashCode');
		}
	}

	/**
	 * 检测用户是否已登录
	 * @author 龙卫国<longweiguo@xiongying.com>
	 * @return bool|null
	 */
	public static function checkLogin() {
		if (self::$_isLogin !== null) {
			return self::$_isLogin;
		}
		self::$_isLogin = false;
		self::_init();
		if (!self::$_userId) {
			return false;
		}
		self::_sessionInit(self::$_userId);
		$userInfo = Session::getValue('userInfo');
		if (empty($userInfo) || empty($userInfo['user_id'])) {
			return false;
		}
		$hashCode = Session::getValue('hashCode');
		if (!self::$_hashCode || self::$_hashCode != $hashCode) {
			return false;
		}
		self::$_isLogin = true;
		return true;
	}

	public static function getUserInfo() {
		return self::getFromSession('userInfo');
	}

	public static function getThirdToken() {
		return self::getFromSession('thirdToken');
	}

	public static function getFromSession($key) {
		if (!self::checkLogin()) {
			return false;
		}
		return Session::getValue($key);
	}

	public static function saveToSession($key, $val) {
		self::_init();
		if (!self::$_userId) {
			return false;
		}
		Session::getValue($key, $val);
		return true;
	}

	public static function getUserId() {
		self::_init();
		return self::$_userId;
	}

	public static function getUsername() {
		self::_init();
		return self::$_username;
	}

	private static function _sessionInit($userId) {
		Session::init(array(
			'cookie_domain' => GlobalConfig::MS_COOKIE_DOMAIN,
			'expire'        => GlobalConfig::MS_COOKIE_EXPIRE(),
			'session_id'    => md5($userId),
		));
	}
}