<?php
/**
 * @brief 简介：
 * @author 陈朝阳<chenchaoyang@haoche51.com>
 * @date 16/5/18
 * @copyright Copyright (c) 2016 好车无忧 Inc. (http://www.haoche51.com)
 */


class UrlUtil {
    private $urlMap = array(

    );
    public $requestParams;
    public function createListUrl($type, $value, $params = array(), $requestParams = array()) {
        if (empty($requestParams)) {
            $requestParams = $this->requestParams;
        }
        $active = ((empty($requestParams[$type]) && $value == 0) || (!empty($requestParams[$type]) && $requestParams[$type] == $value)) ? true : false;
        if ($value || $value == 0) {
            $params[$type] = $value;
        }
        $params = array_merge($requestParams, $params);
        $url = http_build_query($params);
        return array(
            'url'    => $url,
            'active' => $active
        );
    }

    //url取查询参数
    public function parseUrl($url = '') {
        $params = array();
        if ($url) {
            $pathInfo = parse_url($url);
            if (isset($pathInfo['query'])) {
                parse_str($pathInfo['query'], $params);
            }
        }
        if ($url) {
            $this->requestParams['lan'] = Util::getFromArray('lan', $params, 0);
        } else {
            $this->requestParams['lan'] = MsController::$LANGUAGE;
        }
        //品牌
        if ($url) {
            $brand = Util::getFromArray('brand', $params, '');
        } else {
            $brand = Request::getGET('brand', 0);
        }
        if ($brand) {
            $this->requestParams['brand'] = $brand;
        }

        //车系
        if ($url) {
            $series = Util::getFromArray('series', $params, 0);
        } else {
            $series = Request::getGET('series', 0);
        }
        if ($series) {
            $this->requestParams['series'] = $series;
        }

        //变速箱
        if ($url) {
            $gearboxId = Util::getFromArray('gearbox_id', $params, 0);
        } else {
            $gearboxId = Request::getGET('gearbox_id', 0);
        }
        if (is_numeric($gearboxId) && $gearboxId > 0) {
            $this->requestParams['gearbox_id'] = $gearboxId;
        }

        //车龄
        if ($url) {
            $age = Util::getFromArray('age', $params, 0);
        } else {
            $age = Request::getGET('age', 0);
        }
        if (is_numeric($age) && $age > 0) {
            $this->requestParams['age'] = $age;
        }

        //最小里程
        if ($url) {
            $minMile = Util::getFromArray('min_mile', $params, 0);
        } else {
            $minMile = Request::getGET('min_mile', 0);
        }
        if (is_numeric($minMile) && $minMile >= 0) {
            $this->requestParams['min_mile'] = $minMile;
        }

        //最大里程
        if ($url) {
            $maxMile = Util::getFromArray('max_mile', $params, 0);
        } else {
            $maxMile = Request::getGET('max_mile', 0);
        }
        if (is_numeric($maxMile) && $maxMile >= 0) {
            $this->requestParams['max_mile'] = $maxMile;
        }

        //最低价格
        if ($url) {
            $minPrice = Util::getFromArray('min_price', $params, 0);
        } else {
            $minPrice = Request::getGET('min_price', 0);
        }
        if (is_numeric($minPrice) && $minPrice >= 0) {
            $this->requestParams['min_price'] = $minPrice;
        }

        //最高价格
        if ($url) {
            $maxPrice = Util::getFromArray('max_price', $params, 0);
        } else {
            $maxPrice = Request::getGET('max_price', 0);
        }
        if (is_numeric($maxPrice) && $maxPrice >= 0) {
            $this->requestParams['max_price'] = $maxPrice;
        }

        //页码
        if ($url) {
            $page = Util::getFromArray('page', $params, 0);
        } else {
            $page = Request::getGET('page', 1);
        }
        if (is_numeric($page) && $page > 0) {
            $this->requestParams['page'] = $page;
        }

        //排序
        if ($url) {
            $sort = Util::getFromArray('sort', $params, 0);
        } else {
            $sort = Request::getGET('sort', 0);
        }
        if (is_numeric($sort) && $sort > 0) {
            $this->requestParams['sort'] = $sort;
        }

        //关键字
        if ($url) {
            $keyword = Util::getFromArray('keyword', $params, '');
        } else {
            $keyword = Request::getGET('keyword', 0);
        }
        $keyword = addslashes($keyword);
        $keyword = htmlentities($keyword);
        if ($keyword) {
            $this->requestParams['keyword'] = $keyword;
        }
    }
} 
