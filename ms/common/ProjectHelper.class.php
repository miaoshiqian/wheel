<?php
/**
 * @brief 简介：
 * @author 陈朝阳<chenchaoyang@qingchuangba.com>
 * @date 16/1/20
 * @copyright Copyright (c) 2016 轻创吧 Inc. (http://www.qingchuangba.com)
 */



class ProjectHelper {
    public static function isMobile($mobile) {
        if (preg_match('/^[0-9]{3}[-]?[0-9]{3}[-]?[0-9]{4}$/', $mobile)) {
            return true;
        } else {
            return preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $mobile) ? true : false;
        }
    }

    public static function formatCarList($carList) {
        if (empty($carList)) {
            return array();
        }
        foreach ($carList as &$car) {
            $car['listing_price'] = number_format($car['listing_price'], 2);
            $car['transmission_text'] = isset(EnumCar::$TRANSMISSION[$car['transmission']]) ? EnumCar::$TRANSMISSION[$car['transmission']] : 'unknown';
            $car['body_style_text'] = isset(EnumCar::$BODY_STYLE[$car['body_style']]) ? EnumCar::$BODY_STYLE[$car['body_style']] : 'unknown';
            $car['detail_url'] = '/car/' . $car['id'] . '.html?lan=' . MsController::$LANGUAGE;
        }
        return $carList;
    }
}