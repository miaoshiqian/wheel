<?php

//翻页条

function pager_helper($params) {
	$pager = $params['pager'];
    $urlUtil = $params['urlUtil'];
	$requestParams = $params['requestParams'];
	if ($pager['last'] <= 1) {
		return '';
	}
	if (isset($requestParams['page'])) {
		unset($requestParams['page']);
	}
    $tpl = '<li class="radius-5 %s"><a href="/car/?%s">%s</a></li>';
	$activeClass = 'active';
	$ret = '<div class="pagination"><ul>';
	if ($pager['current'] > $pager['first']) {
		$urlData = $urlUtil->createListUrl('page', $pager['current'] - 1, $requestParams);
		$ret .= sprintf($tpl, '', $urlData['url'], '<span class="back-p">&lt;</span>');
	}
	if ($pager['current'] <= 7 || $pager['last'] <= 12) {
		for ($i = 1, $n = min($pager['last'], 12); $i <= $n; $i++) {
			$urlData = $urlUtil->createListUrl('page', $i, $requestParams);
			$ret .= sprintf($tpl, $pager['current'] == $i ? $activeClass : '', $urlData['url'], $i);
		}
	} else {
		for ($i = 1; $i <= 3; $i++) {
			$urlData = $urlUtil->createListUrl('page', $i, $requestParams);
			$ret .= sprintf($tpl, '', $urlData['url'], $i);
		}
		$ret .= '<li class="radius-5">...</li>';
		for ($i = min($pager['current'] - 4, $pager['last'] - 9), $n = min($pager['last'], $pager['current'] + 4); $i <= $n; $i++) {
			$urlData = $urlUtil->createListUrl('page', $i, $requestParams);
			$ret .= sprintf($tpl, $pager['current'] == $i ? $activeClass : '', $urlData['url'], $i);
		}
	}
	if ($i <= $pager['last']) {
		$ret .= '<li class="radius-5">...</li>';
	}
	if ($pager['current'] < $pager['last']) {
		$urlData = $urlUtil->createListUrl('page', $pager['current'] + 1, $requestParams);
		$ret .= sprintf($tpl, '', $urlData['url'], '<span class="next-p">&gt;</span>');
	}
	$ret .= '</ul></div>';
	return $ret;
}

