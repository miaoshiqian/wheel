<?php
/**
 * 自定义模块测试
 * Created by PhpStorm.
 * User: lidengke
 * Date: 2015/11/11
 * Time: 15:18
 */


class IndexController extends BaseController{
	public function printAction(){
		echo '<pre>';
		echo 'http://www.ke.com/testmodule/index/print?name=lidengke&int=123dd&float=1.22333333&array[n1]=v1&array[n2]=v2';
		$var = array(
			'getCurrentUrl' => Request::getCurrentUrl(),
			'getCOOKIE' => Request::getCOOKIE('name'),
			'getCurrentQuery' => Request::getCurrentQuery(),
			'getGET' => Request::getGET('get'),
			'getInt' => Request::getInt('int'),
			'getFloat' => Request::getFloat('float'),
			'getArray' => Request::getArray('array', array()),
		);


		var_dump($var);

		$content = $this->view->fetch('index/print.php', array(
			'test' => array(
				'num' => 10,
			)
		));
		Response::output($content);
	}

	public function redirectAction(){
		Response::redirect('/');
	}


	public function modelAction(){
		$model = new Model();

		var_dump($model);
	}
}