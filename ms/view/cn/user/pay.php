<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>付款（1确定车辆信息）新银行支付</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css"/>
    <!--<link rel="stylesheet" type="text/css" href="css/focus-slide.css"/>-->
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/input_hide.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/pay-fun.js"></script>
    <!--<script type="text/javascript" src="js/jquery.SuperSlide.2.1.2.js"></script>-->
    <script type="text/javascript">
        $(function () {
            $(".pay-s-tpls .pt-rd").click(function () {
                var index=$(".pay-s-tpls .pt-rd").index(this);
                $(".pay-s-tpls .pt-rd").removeClass("active");
                $(this).toggleClass("active");
                $(".pay-s-tpm").eq(index).show().siblings(".pay-s-tpm").hide();
            })
            $(".pay-sbk-ls li .pay-bk-rd").click(function () {
                $(".pay-sbk-ls li .pay-bk-rd").removeClass("active");
                $(this).toggleClass("active");
            })
        })
    </script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo.png" class="my_logo" /></div>
<div class="process-T">
    <div class="mid-block">
        <div class="tBox">
            <div class="daohang">
                <li><span>您好，</li><li>thinkteam</span></li>
                <li><a href="#">主页</a></li>
                <li><a href="#" class="yj">买车</a></li>
                <li><a href="#" class="yj">卖车</a></li>
                <div class="my_nav"><div>
                        <a href="#">其他服务</a>
                        <ul><li><a href="#">周边服务</a></li><li><a href="#">汽车检测</a></li><li><a href="#">汽车团购</a></li></ul>
                    </div></div>
                <li><a href="#">关于我们</a></li>
                <li><a href="javascript:void(0);" target="_blank">我的订单</a></li>
                <li><a href="javascript:void(0);" target="_blank">帮助中心</a></li>
                <li><a href="#">退出</a></li>
            </div>
            <div class="clear"></div>
        </div>

    </div>
</div>

<div class="mid-block">
<div class="pay-s-m1 clearfix">
    <div class="pay-s-m1i clearfix">
        <div class="Lb">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/pay-pic-1.jpg">
            </div>
            <div class="pdes">
                <p>2011 Lexus RX 350 AWD</p>

                <p>价格：$26,900</p>

                <p>VIN:2T2BK1BA9BC099629</p>

                <p>里程数：49,151</p>
            </div>
        </div>
        <div class="osp"></div>
    </div>
    <div class="pay-s-m1i clearfix">
        <div class="Rb">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/pay-upic.jpg">
            </div>
            <div class="pdes">
                <p>姓名：mii</p>

                <p>地址：apt 1212 401E 32nd St</p>

                <p>手机号：3125963596</p>

                <p>邮箱：milljif@gmail.com</p>
            </div>
        </div>
    </div>
</div>
<div class="pay-s-info">

<div class="priB">
    <span class="tip">支付定金：</span>
    <span class="num">500</span>
    <span class="unit">元</span>
</div>

<div class="pay-s-typeB">
    <ul class="pay-s-tpls clearfix">
        <li class="clearfix">
                    <span class="l pt-rd">
                        <input type="radio" name="pay_type"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/pay-type-1.png">
            </div>
            <div class="spl"></div>
        </li>
        <li class="clearfix">
                    <span class="l pt-rd">
                        <input type="radio" name="pay_type"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/pay-type-2.png">
            </div>
            <div class="spl"></div>
        </li>
        <li class="clearfix">
                    <span class="l pt-rd active">
                        <input type="radio" name="pay_type" checked/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/pay-type-3.png">
            </div>
        </li>
    </ul>
</div>
<!--支付宝-->
<div class="pay-s-tpm">
    <div class="Top clearfix">
        <a href="javascript:void(0);"><span>银行卡</span></a>
        <a href="javascript:void(0);"><span>信用卡</span></a>
    </div>
    <div class="zfb-pay">
        <form action="" method="post">
            <p class="warm">你在安全的环境中，请放心使用！</p>
            <div class="pwd-box">
                <p class="tip-1">支付宝支付密码：</p>
                <div class="mItem clearfix">
                    <input type="password" name="zfb_pwd" class="l zfb-pwd-inp"/>
                    <a href="javascript:void(0);" class="l fpwd">忘记密码？</a>
                </div>
                <p class="tip-2">请输入6位数字支付密码</p>
            </div>
            <div class="yes-box">
                <input type="submit" value="确认付款" class="zfb-yes"/>
            </div>
        </form>
    </div>
</div>
<!--微信支付-->
<div class="pay-s-tpm">
</div>
<!--银行卡-->
<div class="pay-s-tpm" style="display: block;">
    <div class="Top clearfix">
        <a href="javascript:void(0);"><span>银行卡</span></a>
        <a href="javascript:void(0);"><span>信用卡</span></a>
    </div>
    <ul class="pay-sbk-ls clearfix">
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-1.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-2.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-3.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-4.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-5.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-6.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-7.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-8.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-9.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-10.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-11.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-12.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-13.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-14.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-15.png">
            </div>
        </li>
    </ul>
</div>

</div>
<div class="pay-s-checkout">
    <div class="title">
        <a href="javascript:void(0);" class="back">返回</a>

        <div class="pay-scar-step clearfix">
            <div class="step-itm active">
                <div class="t-num">1</div>
                <p class="txt">确认车辆信息</p>

                <div class="spl-b"></div>
            </div>
            <div class="step-itm">
                <div class="t-num">2</div>
                <p class="txt">填写配件信息</p>

                <div class="spl-b"></div>
            </div>
            <div class="step-itm">
                <div class="t-num">3</div>
                <p class="txt">支付订金</p>
            </div>
        </div>
    </div>
    <!--确认车辆信息-->
    <div class="ps-cko-m s-1 clearfix" style="display: block;">
        <div class="Lb">
            <a href="javascript:void(0);" class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/pay-pic-2.jpg">
            </a>

            <div class="btm-txt">
                <p>2011 Lexus RX 350 AWD</p>

                <p>里程数：49,151</p>
            </div>
        </div>
        <div class="Rb">
            <div class="T-total">
                <span class="tip">总价（税前）：</span>
                <span class="t-c">$26,900</span>
            </div>
            <div class="fee clearfix">
                <div class="item">
                    <span class="tip">文件费：</span>
                    <span class="t-c">$20</span>
                    <span class="spl"></span>
                </div>
                <div class="item">
                    <span class="tip">过户费：</span>
                    <span class="t-c">$160</span>
                </div>
            </div>
            <a href="javascript:void(0);" class="info-yes">确定信息</a>
            <ul class="tip-ls clearfix">
                <li><span>203项车辆质检</span></li>
                <li><span>十天试驾</span></li>
                <li><span>100天质保</span></li>
                <li><span>代办DVM过户</span></li>
                <li><span>免费一次洲年检</span></li>
                <li><span>免费一次加满油</span></li>
                <li><span>免费一次洗车打蜡</span></li>
                <li><span>送车上门</span></li>
            </ul>
        </div>
    </div>
    <!--填写配件信息-->
    <div class="ps-cko-m s-2 clearfix">
        <div class="ps-cko-m2L clearfix">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/pay-pic-1.jpg">
            </div>
            <div class="pdes">
                <p>2011 Lexus RX 350 AWD</p>

                <p>价格：$26,900</p>

                <p>VIN:2T2BK1BA9BC099629</p>

                <p>里程数：49,151</p>
            </div>
        </div>
        <div class="ps-cko-m2R clearfix">
            <!--<form action="" method="post">-->
            <div class="o-item name clearfix">
                <span class="l tip">姓名：</span>
                <input type="text" class="ps-inp-1 radius-5"/>
            </div>
            <div class="o-item addr clearfix">
                <span class="l tip">地址：</span>
                <input type="text" class="ps-inp-1 radius-5"/>
            </div>
            <div class="o-item email clearfix">
                <span class="l tip">邮箱：</span>
                <input type="text" class="ps-inp-1 radius-5"/>
            </div>
            <div class="o-item phone clearfix">
                <span class="l tip">电话：</span>
                <input type="text" class="ps-inp-1 radius-5"/>
            </div>
            <div class="o-item city clearfix">
                <span class="l tip">所在城市：</span>
                <input type="text" class="ps-inp-1 radius-5" value="Chicago"/>
            </div>
            <div class="o-item clearfix">
                <dl class="l select radius-5">
                    <!--title用于存储选值-->
                    <dt title="value">1L</dt>
                    <div class="option">
                        <dd value="1">1L</dd>
                        <dd value="2">2L</dd>
                        <dd value="3">3L</dd>
                    </div>
                </dl>
            </div>
            <div class="o-item cd clearfix">
                <input type="text" class="ps-inp-1 radius-5" value="60616"/>
            </div>
            <div class="clear"></div>
            <div class="sub-box">
                <input type="button" value="Next" class="radius-5 submit-btn"/>
            </div>
            <!--</form>-->
        </div>
    </div>
    <!--支付完成-->
    <div class="ps-cko-m s-3 clearfix">
    </div>

</div>
</div>

<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
