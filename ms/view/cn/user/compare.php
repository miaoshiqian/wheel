<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>车辆比较页</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/focus-slide.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.SuperSlide.2.1.2.js"></script>

    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo.png" class="my_logo" /></div>
<div class="process-T">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>

    </div>
</div>

<div class="bgc-f9f9f9">

<div class="mid-block">
<div class="car-compar">
<ul class="car-compar-ls clearfix">
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-1.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;1,2655</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
            <li class="clearfix">
                <span class="l tip">国内价格：</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">dealer价格: </span>
                <span class="l tc">15000 节约3000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">年份：</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">里程：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">排量：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">前任用车：</span>
                <span class="l tc">私家车</span>
            </li>
            <li class="clearfix">
                <span class="l tip">蓝牙：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">汽车座椅加热：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">真皮座椅：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">远程启动：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">自动空调：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">巡航定速：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">长宽高：</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">车重：</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
                <span class="l">质检师意见：</span>&nbsp;
                <span class="l">非常值得购买，性价比超过。</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">购买</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-2.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;3,1956</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
            <li class="clearfix">
                <span class="l tip">国内价格：</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">dealer价格: </span>
                <span class="l tc">15000 节约3000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">年份：</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">里程：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">排量：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">前任用车：</span>
                <span class="l tc">私家车</span>
            </li>
            <li class="clearfix">
                <span class="l tip">蓝牙：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">汽车座椅加热：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">真皮座椅：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">远程启动：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">自动空调：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">巡航定速：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">长宽高：</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">车重：</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
                <span class="l">质检师意见：</span>&nbsp;
                <span class="l">非常值得购买，性价比超过。</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">购买</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-3.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;2,3645</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
            <li class="clearfix">
                <span class="l tip">国内价格：</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">dealer价格: </span>
                <span class="l tc">15000 节约3000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">年份：</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">里程：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">排量：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">前任用车：</span>
                <span class="l tc">私家车</span>
            </li>
            <li class="clearfix">
                <span class="l tip">蓝牙：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">汽车座椅加热：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">真皮座椅：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">远程启动：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">自动空调：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">巡航定速：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">长宽高：</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">车重：</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
                <span class="l">质检师意见：</span>&nbsp;
                <span class="l">非常值得购买，性价比超过。</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">购买</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-1.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;1,2655</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
            <li class="clearfix">
                <span class="l tip">国内价格：</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">dealer价格: </span>
                <span class="l tc">15000 节约3000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">年份：</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">里程：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">排量：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">前任用车：</span>
                <span class="l tc">私家车</span>
            </li>
            <li class="clearfix">
                <span class="l tip">蓝牙：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">汽车座椅加热：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">真皮座椅：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">远程启动：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">自动空调：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">巡航定速：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">长宽高：</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">车重：</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
                <span class="l">质检师意见：</span>&nbsp;
                <span class="l">非常值得购买，性价比超过。</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">购买</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-2.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;3,1956</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
            <li class="clearfix">
                <span class="l tip">国内价格：</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">dealer价格: </span>
                <span class="l tc">15000 节约3000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">年份：</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">里程：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">排量：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">前任用车：</span>
                <span class="l tc">私家车</span>
            </li>
            <li class="clearfix">
                <span class="l tip">蓝牙：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">汽车座椅加热：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">真皮座椅：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">远程启动：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">自动空调：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">巡航定速：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">长宽高：</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">车重：</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
                <span class="l">质检师意见：</span>&nbsp;
                <span class="l">非常值得购买，性价比超过。</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">购买</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-3.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;2,3645</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
            <li class="clearfix">
                <span class="l tip">国内价格：</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">dealer价格: </span>
                <span class="l tc">15000 节约3000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">年份：</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">里程：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">排量：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">前任用车：</span>
                <span class="l tc">私家车</span>
            </li>
            <li class="clearfix">
                <span class="l tip">蓝牙：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">汽车座椅加热：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">真皮座椅：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">远程启动：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">自动空调：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">巡航定速：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">长宽高：</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">车重：</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
                <span class="l">质检师意见：</span>&nbsp;
                <span class="l">非常值得购买，性价比超过。</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">购买</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
</ul>
</div>
</div>
</div>

<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.masonry.min.js"></script>
<script type="text/javascript">
    $(function(){
        var $container = $(".car-compar-ls");
        $container.imagesLoaded(function(){
            $container.masonry({
                itemSelector:".car-compar-lsi",
                columnWidth:0 //每两列之间的间隙(像素)
            });
        });
    })
</script>

<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
