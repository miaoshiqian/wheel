<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>登录页</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css" />

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/fun.js"></script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="login-mbox">
    <div class="mid-block">
        <div class="l login-picL">
            <img src="http://sta.wheelsrnr.com/ms/images/login-pic-1.png"/>
        </div>
        <div class="r login-frb">
            <div class="logo"><img src="http://sta.wheelsrnr.com/ms/images/logo2.png"/></div>
            <div class="login-fr-inB radius-5">
                <div class="tpB">
                    <div class="login-fr-in">
                        <form action="#" method="post">
                            <div class="top">
                                <b class="t">登录</b>
                                <span class="txt">login</span>
                            </div>
                            <div class="item">
                                <input id="js_username" type="text" placeholder="用户名/手机" class="radius-5 login-inp"/>
                            </div>
                            <div class="item">
                                <input id="js_password" type="password" placeholder="密码" class="radius-5 login-inp"/>
                            </div>
                            <div class="item">
                                <input id="js_submit" type="button" value="登录" class="radius-5 login-sub"/>
                            </div>
                            <div class="item clearfix">
                                <a href="javascript:void(0);" class="fw-s-1">忘记密码？</a>
                        <span class="r login-qes">
                        有问题联系<a href="javascript:void(0);" class="online-kf">在线客服</a>
                        </span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="btm-B">
                    <div class="login-spl"></div>
                    <a href="/user/register/" class="radius-5 lreg-btn">注册账号</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#js_submit').click(function(){
            var username = $('#js_username').val();
            var password = $('#js_password').val();
            if (!username || !password) {
                alert('请输入用户名或密码！');
                return false;
            }
            var $this = $(this);
            $this.val('登录中...');
            $this.attr('disabled', true);
            $.ajax({
                url : '/user/ajaxLogin/',
                type : 'post',
                data : {username : username, password : password},
                dataType : 'json',
                success:function(ret){
                    if (ret.code == 0) {
                        location.href = '/user/order/';
                    } else if (ret.message) {
                        alert(ret.message);
                    }
                    $this.removeAttr('disabled');
                    $this.val('登录');
                },
                error:function(ret){
                    $this.removeAttr('disabled');
                    $this.val('登录');
                }
            });
        });
    });
</script>
</body>
</html>
