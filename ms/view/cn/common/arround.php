<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>周边服务</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <!--<link rel="stylesheet" type="text/css" href="css/dlselect.css" />-->
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <!--<script type="text/javascript" src="js/dlselect.js"></script>-->
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo2.png" class="my_logo" /></div>
<div class="related-serv-T">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>

        <div class="part">
            <p class="t">提供汽车售后服务</p>
            <p class="des">不隐藏、没虚假 我们只提供最好的产品</p>
        </div>

    </div>
</div>

<div class="related-serv-two">
    <div class="mid-block">
        <div class="related-serv-list">
            <ul class="clearfix">
                <li class="item-0">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">贷款业务</p>
                        <div class="des">我们提供汽车售后使用一系列相<br/>关服务。没有隐藏信息，没</div>
                    </div>
                </li>
                <li class="item-1">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">保险业务</p>
                        <div class="des">做到每一辆售出的车都<br/>是良好优质车辆，但</div>
                    </div>
                </li>
                <li class="item-2">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">事故理赔咨询</p>
                        <div class="des">我们提供汽车售后使用一系列相<br/>关服务。没有隐藏信息，没</div>
                    </div>
                </li>
                <li class="item-3">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">地址更改</p>
                        <div class="des">做到每一辆售出的车都<br/>是良好优质车辆，但</div>
                    </div>
                </li>
                <div style="clear:both"></div>
                <li class="item-4">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">驾驶学校</p>
                        <div class="des">我们提供汽车售后使用一系列相<br/>关服务。没有隐藏信息，没</div>
                    </div>
                </li>
                <li class="item-5">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">汽车保养</p>
                        <div class="des">我们提供汽车售后使用一系列相<br/>关服务。没有隐藏信息，没</div>
                    </div>
                </li>
                <li class="item-6">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">汽车美容、升级</p>
                        <div class="des">我们提供汽车售后使用一系列相<br/>关服务。没有隐藏信息，没</div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="AboutContact">
    <div class="mid-block">
        <div class="title">联系我们</div>
        <div class="info clearfix">
            <div class="M">
                <span class="li loc">47 W. Polk St. Ste. 208, Chicago, IL 60605</span>
                <span class="li phone">312-929-8499</span>
                <span class="li email">help@wheelsrnr.com</span>
            </div>
        </div>
        <p class="start-time">上班时间：Monday - Thursday: 10am - 6pm</p>
        <p class="ck-map-b"><a href="javascript:void(0);">查看地图位置</a></p>
    </div>
</div>


<!--底部-->
<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
