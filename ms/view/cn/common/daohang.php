<style>
    .daohang li{
        width: auto !important;
        margin-left: 40px !important;
    }
    .daohang .my_nav {
        margin-left: 40px !important;
    }
    .daohang .my_nav li{
        margin-left: 0px !important;
    }
    .daohang li a.yj{
        width: auto !important;
    }

</style>
<div class="daohang">
    <li><a href="/" <?php echo $this->pathInfo['path'] == '/' ? 'class="yj"' : '' ?>>主页</a></li>
    <li ><a href="/car/" <?php echo $this->pathInfo['path'] == '/car/' ? 'class="yj"' : '' ?>>买车</a></li>
    <li><a href="/sell/" <?php echo $this->pathInfo['path'] == '/sell/' ? 'class="yj"' : '' ?>>卖车</a></li>
    <div class="my_nav">
        <div>
            <a href="#">其他服务</a>
            <ul><li><a href="/common/arround/">周边服务</a></li><li><a href="/common/check/">汽车检测</a></li><li><a href="/common/group/">汽车团购</a></li></ul>
        </div>
    </div>
    <li><a href="/about/" <?php echo $this->pathInfo['path'] == '/about/' ? 'class="yj"' : '' ?>>关于我们</a></li>
    <li class="wd"><a href="/user/login/" >登录</a></li><font>|</font><li class="wd" style="margin-left: 5px"><a href="/user/register/" >注册</a></li>
    <li><a href="<?php echo $this->lanSwitchUrl?>">English</a></li>
</div>