<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>汽车团购</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <!--<link rel="stylesheet" type="text/css" href="css/dlselect.css" />-->
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <!--<script type="text/javascript" src="js/dlselect.js"></script>-->
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
    <style>
        .daohang li a,.my_nav div a,.my_nav div li a:visited{
            color:#3b3a3a; border-color:#3b3a3a;
        }
    </style>
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo.png" class="my_logo" /></div>
<div class="carTuan-T">
    <div class="mid-block" style="position:absolute; left:0; right:0">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>
    </div>
    <img src="http://sta.wheelsrnr.com/ms/images/tuan-top-bg.jpg" width="100%" />
</div>

<div class="carTuan-two">
    <div class="mid-block">
        <div class="carT-tip"></div>
    </div>
</div>

<div class="carTuan-is">
    <div class="mid-block">
        <div class="tuan-is-tbox">
            <div class="btm">
                <p class="txt">汽车团购是什么？</p>
                <a class="dp-ico"></a>
            </div>
        </div>
        <div class="txt-box">
            <p class="item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;团购（Group purchase）就是团体购物，指认识或不认识的消费者联合起来，加大与商家的谈判能力，以求得最优价格的一种购物方式。根据薄利多销的原理，商家可以给出低于零售价格的团购折扣和单独购买得不到的优质服务。团购作为一种新兴的电子商务模式，通过消费者自行组团、专业团购网站、商家组织团购等形式，提升用户与商家的议价能力，并极大程度地获得商品让利，引起消费者及业内厂商、甚至是资本市场关注。</p>
            <p class="item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;对于没有多少时间的顾客或者一些要求比较高的顾客是有价格上的好处的（这类顾客在论坛上比较少）。不是每个顾客都会花大量的时间去学习知识，也不是每个顾客都知道那里买东西便宜，很多顾客甚至只逛商场，那么团购的价格的确是比商场便宜了。</p>
            <p class="item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;每天面对形形色色的美食，很多时候会被其价格阻挡在门外，对于一家环境优美、品味时尚的餐厅，没有充足的消费者会很可惜，这时候，团购平台创建的不仅是可以满足消费者的口味，也能给团购商家带去充足的客源和人气。</p>
        </div>
    </div>
</div>

<div class="carTuan-list-b">
    <div class="mid-block">
        <div class="T-t"></div>
        <div class="carTuan-ls">
            <ul class="clearfix">
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-1.jpg"/></a>
                        <div class="pic-txt">康迪K10</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">最多直降1.2万</div>
                            <a href="javascript:void(0);" class="r join-now">立即<br/>参团</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-2.jpg"/></a>
                        <div class="pic-txt">欧蓝德（进口）2016款 2.4L 四驱 精英版</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">最多直降1.2万</div>
                            <a href="javascript:void(0);" class="r join-now">立即<br/>参团</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-3.jpg"/></a>
                        <div class="pic-txt">马自达 阿特兹 2015款 2.0L 蓝天 豪华版</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">最多直降2.2万</div>
                            <a href="javascript:void(0);" class="r join-now">立即<br/>参团</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-4.jpg"/></a>
                        <div class="pic-txt">上海大众 途观 2016款 300TSI</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">最多直降3.6万</div>
                            <a href="javascript:void(0);" class="r join-now">立即<br/>参团</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-1.jpg"/></a>
                        <div class="pic-txt">康迪K10</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">最多直降1.2万</div>
                            <a href="javascript:void(0);" class="r join-now">立即<br/>参团</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-2.jpg"/></a>
                        <div class="pic-txt">欧蓝德（进口）2016款 2.4L 四驱 精英版</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">最多直降1.2万</div>
                            <a href="javascript:void(0);" class="r join-now">立即<br/>参团</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-3.jpg"/></a>
                        <div class="pic-txt">马自达 阿特兹 2015款 2.0L 蓝天 豪华版</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">最多直降2.2万</div>
                            <a href="javascript:void(0);" class="r join-now">立即<br/>参团</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-4.jpg"/></a>
                        <div class="pic-txt">上海大众 途观 2016款 300TSI</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">最多直降3.6万</div>
                            <a href="javascript:void(0);" class="r join-now">立即<br/>参团</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="AboutContact">
    <div class="mid-block">
        <div class="title">联系我们</div>
        <div class="info clearfix">
            <div class="M">
                <span class="li loc">47 W. Polk St. Ste. 208, Chicago, IL 60605</span>
                <span class="li phone">312-929-8499</span>
                <span class="li email">help@wheelsrnr.com</span>
            </div>
        </div>
        <p class="start-time">上班时间：Monday - Thursday: 10am - 6pm</p>
        <p class="ck-map-b"><a href="javascript:void(0);">查看地图位置</a></p>
    </div>
</div>


<!--底部-->
<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
