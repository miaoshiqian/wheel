<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>关于我们</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <!--<link rel="stylesheet" type="text/css" href="css/dlselect.css" />-->
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <!--<script type="text/javascript" src="js/dlselect.js"></script>-->
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo2.png" class="my_logo" /></div>
<div class="AboutTop">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>
        <div class="about-serv">
            <div class="item iR itm-0">
                <p class="t"><span class="txt">一击买车</span></p>

                <div class="ct">
                    买家可以直接在平台看到最真实<br/>的车辆信息，指尖一点，爱车到家门口
                </div>
            </div>
            <div class="item iL itm-1">
                <p class="t"><span class="txt">汽车质检 & 精确定价</span></p>

                <div class="ct">
                    专业的汽车质检师上门服务，对汽车进<br/>行整体检测，准确定价
                </div>
                <div class="f-ico"></div>
            </div>
            <div class="item iR itm-2">
                <p class="t"><span class="txt">门前试驾，无忧购车</span></p>

                <div class="ct">
                    在签收新车前，30分钟试驾；购<br/>买车辆后，7天/100公里无条件退货
                </div>
            </div>
            <div class="item iL itm-3">
                <p class="t"><span class="txt">量子化卖车</span></p>

                <div class="ct">
                    在我们的线上平台展示的同时，您可以<br/>继续开着她。薛定谔车，边卖边开
                </div>
                <div class="f-ico"></div>
            </div>
        </div>
        <span class="dp-ico"></span>
    </div>
    <div class="o-opa"></div>
</div>

<div class="AboutModel">
    <div class="mid-block">
        <div class="T">
            <p class="t-1"><img src="http://sta.wheelsrnr.com/ms/images/aboutUs-mdl-txt.png"></p>

            <p class="t-2">简化一切二手车交易手续，透明化二手车价格，让用户可以安心的买二手车，<br/>快速的找到自己喜欢的二手车</p>
        </div>
        <div class="C clearfix abbg">
            <div class="Citem">
                <div class="in-b n_ab01">
                    <!--<div class="pic"><i class="i-0"></i></div>-->
                    <p class="p-t">价格透明，公平交易</p>

                    <div class="btm-t">
                        无论对已卖车方亦或是买车方，二手车的<br/>价格都将公开透明，做到真正公平交易
                    </div>
                </div>
            </div>
            <div class="Citem">
                <div class="in-b n_ab02">
                    <!--<div class="pic"><i class="i-1"></i></div>-->
                    <p class="p-t">简化购车流程</p>

                    <div class="btm-t">
                        用户不再需要亲自到店里选车，节省更多购车<br/>时间，保障平台内的车辆信息真实可靠
                    </div>
                </div>
            </div>
            <div class="Citem">
                <div class="in-b n_ab03">
                    <!--<div class="pic"><i class="i-2"></i></div>-->
                    <p class="p-t">卖车更轻松</p>

                    <div class="btm-t">
                        专业的车辆鉴定师，公开的二手车交易平台，<br/>让用户可以更快卖出手中的二手车
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="AboutContact">
    <div class="mid-block">
        <div class="title">联系我们</div>
        <div class="info clearfix">
            <div class="M">
                <span class="li loc">47 W. Polk St. Ste. 208, Chicago, IL 60605</span>
                <span class="li phone">312-929-8499</span>
                <span class="li email">help@wheelsrnr.com</span>
            </div>
        </div>
        <p class="start-time">上班时间：Monday - Thursday: 10am - 6pm</p>
        <p class="ck-map-b"><a href="javascript:void(0);">查看地图位置</a></p>
    </div>
</div>


<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
