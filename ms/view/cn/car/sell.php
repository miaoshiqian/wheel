<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>sell</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css" />

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/fun.js"></script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo2.png" class="my_logo" /></div>
<div class="sellTop">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>
        <div class="t-txt-box">
            <img src="http://sta.wheelsrnr.com/ms/images/sell-t-txt.png">
        </div>
        <div class="sellT-m radius-5">
            <form action="#" method="post">
                <div class="sellTm-in">
                    <div class="in-itm">
                        <div class="sellTm-in-1">
                            <dl class="l select">
                                <!--title用于存储选值-->
                                <dt title="value">品牌</dt>
                                <div class="option">
                                    <dd value="1">Acura</dd>
                                    <dd value="2">BMW</dd>
                                    <dd value="3">Cadillac</dd>
                                    <dd value="4">Chevrolet</dd>
                                    <dd value="5">Dodge</dd>
                                </div>
                            </dl>
                            <dl class="l select s2">
                                <!--title用于存储选值-->
                                <dt title="value">车系</dt>
                                <div class="option">
                                    <dd value="1">one</dd>
                                    <dd value="2">two</dd>
                                    <dd value="3">third</dd>
                                </div>
                            </dl>
                            <dl class="l select">
                                <!--title用于存储选值-->
                                <dt title="value">车型</dt>
                                <div class="option">
                                    <dd value="1">Sedan</dd>
                                    <dd value="2">Coupe</dd>
                                    <dd value="3">Hatchback</dd>
                                    <dd value="4">SUV</dd>
                                    <dd value="5">Mini Van</dd>
                                </div>
                            </dl>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="in-itm">
                        <div class="sellTm-in-2">
                            <div class="itemb">
                                <span class="tip">行驶里程：</span>
                                <input type="text" name="" class="radius-5 sellT-inp-s1"/>
                                <span class="tip">公里</span>
                            </div>
                            <div class="itemb">
                                <span class="tip">上牌时间：</span>
                                <input type="text" name="" class="radius-5 sellT-inp-s2"/>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="in-itm">
                        <div class="sellTm-in-3">
                            <span class="l tip">所在城市：</span>
                            <dl class="l select">
                                <!--title用于存储选值-->
                                <dt title="value">福建</dt>
                                <div class="option">
                                    <dd value="1">福建</dd>
                                    <dd value="2">广东</dd>
                                    <dd value="3">广西</dd>
                                    <dd value="4">湖南</dd>
                                    <dd value="5">云南</dd>
                                </div>
                            </dl>
                            <dl class="l select">
                                <!--title用于存储选值-->
                                <dt title="value">福州</dt>
                                <div class="option">
                                    <dd value="1">福州</dd>
                                </div>
                            </dl>
                            <dl class="l select">
                                <!--title用于存储选值-->
                                <dt title="value">福清</dt>
                                <div class="option">
                                    <dd value="1">福清</dd>
                                </div>
                            </dl>
                            <input type="text" name="" placeholder="输入手机号码" class="radius-5 sellT-inp-s3"/>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="sellT-btm-op clearfix">
                        <input type="submit" value="预约" class="book"/>
                        <input type="submit" value="估价" class="val"/>
                    </div>
                </div>
            </form>
            <div class="opac radius-5"></div>
        </div>
    </div>
</div>

<div class="sell-mn">
    <div class="mid-block">
        <div class="tspl"></div>
        <div class="sell-mn-in clearfix">
            <div class="item-b">
                <div class="sell-mni-b s0">
                    <!--<img src="images/sell-mn-ico-1.png"></i>-->
                    <div class="des-b">
                        <p class="tit">迅速成交</p>
                        <p class="btm-t">在这里平均每5分钟成交一辆车</p>
                    </div>
                </div>
            </div>
            <div class="item-b">
                <div class="sell-mni-b s1">
                    <!--<img src="images/sell-mn-ico-1.png"></i>-->
                    <div class="des-b">
                        <p class="tit">流程简单</p>
                        <p class="btm-t">只需一步 填写，专业顾问为您全程服务</p>
                    </div>
                </div>
            </div>
            <div class="item-b">
                <div class="sell-mni-b s2">
                    <!--<img src="images/sell-mn-ico-1.png"></i>-->
                    <div class="des-b">
                        <p class="tit">卖出好价</p>
                        <p class="btm-t">全城帮您找到出价最高的买主</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sell-mserv">
    <div class="mid-block">
        <div class="Top">
            <p class="p-1">【 方便 省心 】只需6步 给您100分的服务</p>
            <p class="p-2">Convenient save worry  100 points of service to you</p>
        </div>
        <div class="sell-ms-main clearfix">
            <div class="item">
                <i class="i-0"></i>
                <div class="des">
                    <p class="p-1">预约</p>
                    <p class="p-2">60秒预约专属卖车服务</p>
                </div>
            </div>
            <div class="item">
                <i class="i-1"></i>
                <div class="des">
                    <p class="p-1">检测</p>
                    <p class="p-2">评估师免费上门检测</p>
                </div>
            </div>
            <div class="item">
                <i class="i-2"></i>
                <div class="des">
                    <p class="p-1">自主定价</p>
                    <p class="p-2">海量数据助你精准定价</p>
                </div>
            </div>
            <div class="item">
                <i class="i-3"></i>
                <div class="des">
                    <p class="p-1">成交</p>
                    <p class="p-2">个人买家快速支付现金</p>
                </div>
            </div>
            <div class="item">
                <i class="i-4"></i>
                <div class="des">
                    <p class="p-1">过户</p>
                    <p class="p-2">平台专员帮你一站过户</p>
                </div>
            </div>
            <div class="item">
                <i class="i-5"></i>
                <div class="des">
                    <p class="p-1">尊享售后</p>
                    <p class="p-2">专业团队无忧后服务链</p>
                </div>
            </div>
        </div>
        <div class="sell-ms-btm">
            <a href="javascript:void(0);" class="radius-5 sell-ms-btn">提交卖车</a>
        </div>
    </div>
</div>

<?php $this->load('/common/footer.php', array()) ?>

</body>
</html>
