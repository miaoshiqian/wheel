<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>buy2</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/top_fade.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css" />

    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/ion.rangeSlider.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/ion.rangeSlider.skin.css" />

    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/custom.css" />

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/fade.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/fun.js"></script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
    <style>
        .select .option dd{
            color: #aaa;
        }
    </style>
</head>

<body>
<script type="text/javascript">
    $(window).scroll(function(event){
        var winPos = $(window).scrollLeft();
        $("#tp").css("left",winPos);
    });
    function inputFocus(el, defaultVal){
        if (el.value == defaultVal){
            el.value = '';
        }
    }
</script>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo.png" class="my_logo" /></div>
<div id="tp">
    <div style="width:100%; position:relative;">
        <div class="sousuo">
            <a class="prevT" href="javascript:;" stat="prev1001"><img src="http://sta.wheelsrnr.com/ms/images/l-btn.png" /></a>
            <a class="nextT" href="javascript:;" stat="next1002"><img src="http://sta.wheelsrnr.com/ms/images/r-btn.png" /></a>
            <form action="/car/" method="get">
                <ul class="sel1">
                    <li >
                        <input name="lan" type="hidden" value="<?php echo MsController::$LANGUAGE ?>" />
                        <input name="keyword" type="text" value="<?php echo isset($this->requestParams['keyword']) ? $this->requestParams['keyword'] : '搜索车辆...' ?>" onclick="inputFocus(this, '搜索车辆...')" autocomplete="off" class="sel2" />
                        <font>
                            <input class="button" type="submit" value="">
                        </font></li>
                    <span><a href="javascript:;">热门搜索</a><a href="/car/?keyword=suv">SUV</a><a href="/car/?keyword=manual">手动挡</a><a href="/car/?keyword=automatic">自动挡</a></span>
                </ul>
            </form>
        </div>
    </div>
</div>


<link href="http://sta.wheelsrnr.com/ms/css/lrtk.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/lrtk.js"></script>
<!--[if lte IE 6]>
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/belatedPNG.js"></script>
<script type="text/javascript">
    var __IE6=true;
    DD_belatedPNG.fix('.logo img,.prev img,.next img,img');
</script>
<![endif]-->

<div id="top" class="buy-t2" style="position:absolute">
    <!--<a class="prevT" href="javascript:void(0);" stat="prev1001"><img src="images/l-btn.png" /></a>
    <ul class="slideTop-box">
        <li class="slideTop" style="display:block;background:url(images/ershouche_1.jpg) no-repeat center top;"></li>
        <li class="slideTop" style="background:url(images/ershouche_1.jpg) no-repeat center top;"></li>
        <li class="slideTop" style="background:url(images/ershouche_1.jpg) no-repeat center top;"></li>
    </ul>
    <a class="nextT" href="javascript:void(0);" stat="next1002"><img src="images/r-btn.png" /></a>-->

    <!--
    <div class="itemT">
        <li class="on" stat="item1001" href="javascript:;"></li>
        <li href="javascript:void(0);" stat="item1002"></li>
        <li href="javascript:void(0);" stat="item1003"></li>
    </div>
    -->
    <div class="zhengti">
        <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
        <div style="clear:both;"></div>
        <!--<div class="sousuo">
        <form action="#" method="post">
        <ul class="sel1"><li ><input type="text" value="搜索车辆..." class="sel2" /><font>
        <input class="button" type="submit" name="submit" value=""></font></li>
        <span><a href="#">热门搜索</a><a href="#">SUV</a><a href="#">手动挡</a><a href="#">自动挡</a></span></ul>
        </form>
        </div>-->
    </div>

</div>


<div class="slide-main" id="touchMain">
    <!--<a class="prev" href="javascript:;" stat="prev1001"><img src="images/l-btn.png" /></a>-->
    <div class="slide-box" id="slideContent">

        <div class="slide" id="bgstylec">
            <img src="http://sta.wheelsrnr.com/ms/images/ershouche_1.jpg" width="100%" />
        </div>
        <div class="slide" id="bgstylec">
            <img src="http://sta.wheelsrnr.com/ms/images/ershouche_1.jpg" width="100%" />
        </div>
        <div class="slide" id="bgstylec">
            <img src="http://sta.wheelsrnr.com/ms/images/ershouche_1.jpg" width="100%" />
        </div>
        <img src="http://sta.wheelsrnr.com/ms/images/111.jpg" width="100%" style="visibility:visible; z-index:-1" />
    </div>
    <!--<a class="next" href="javascript:;" stat="next1002"><img src="images/r-btn.png" /></a>-->
</div>

<div class="ng-scope">
    <div class="mid-block">
        <form action="#" method="post">
            <div class="drop-row">
                <dl class="l select">
                    <!--title用于存储选值-->
                    <dt title="value"><?php echo $this->brandInfo ? $this->brandInfo['name'] : '品牌' ?></dt>
                    <div class="option">
                        <?php if ($this->queryOptions['brand_options']) { ?>
                            <?php foreach ($this->queryOptions['brand_options'] as $option) { ?>
                                <a href="/car/?<?php echo $option['url']?>"><dd value="1"><?php echo $option['name']?></dd></a>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </dl>
                <span class="l spl">.&nbsp;.&nbsp;.&nbsp;.</span>
                <dl class="l select">
                    <!--title用于存储选值-->
                    <dt title="value"><?php echo $this->seriesInfo ? $this->seriesInfo['name'] : '车系' ?></dt>
                    <div class="option">
                        <?php if ($this->queryOptions['series_options']) { ?>
                            <?php foreach ($this->queryOptions['series_options'] as $option) { ?>
                                <a href="/car/?<?php echo $option['url']?>"><dd value="1"><?php echo $option['name']?></dd></a>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </dl>
                <span class="l spl">.&nbsp;.&nbsp;.&nbsp;.</span>
                <dl class="l select">
                    <!--title用于存储选值-->
                    <dt title="value">车龄<?php echo isset($this->requestNames['age']) ? $this->requestNames['age'] : '' ?></dt>
                    <div class="option">
                        <?php if ($this->queryOptions['age_options']) { ?>
                            <?php foreach ($this->queryOptions['age_options'] as $option) { ?>
                                <a href="/car/?<?php echo $option['url']?>"><dd value="1"><?php echo $option['name']?></dd></a>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </dl>
                <span class="l spl">.&nbsp;.&nbsp;.&nbsp;.</span>
                <dl class="l select">
                    <!--title用于存储选值-->
                    <dt title="value"><?php echo isset($this->requestNames['gearbox']) ? $this->requestNames['gearbox'] : '变速箱' ?></dt>
                    <div class="option">
                        <?php if ($this->queryOptions['gearbox_options']) { ?>
                            <?php foreach ($this->queryOptions['gearbox_options'] as $option) { ?>
                                <a href="/car/?<?php echo $option['url']?>"><dd value="1"><?php echo $option['name']?></dd></a>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </dl>
                <div class="clear"></div>
            </div>

            <div class="buy-tHid" style="margin-bottom: 50px;">
                <div class="lc-row">
                    <div class="colum">
                        <span class="l lcrow-tip">里程数</span>
                        <div class="l range-bar">
                            <!--<div><img src="http://sta.wheelsrnr.com/ms/images/slide-s1.png"/></div>-->
                            <div class="range_1"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="colum">
                        <span class="l lcrow-tip">价格</span>
                        <div class="l range-bar">
                            <div class="range_2"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/ion.rangeSlider.js"></script>
                <script type="text/javascript">
                    $(".range_1").ionRangeSlider({
                        min: 0,
                        max: 1000000,
                        from:<?php echo isset($this->requestParams['min_mile']) ? $this->requestParams['min_mile'] : '0'?>,
                        to: <?php echo isset($this->requestParams['max_mile']) && $this->requestParams['max_mile'] > 0 ? $this->requestParams['max_mile'] : '1000000'?>,
                        type: 'double',//设置类型
                        step: 1000,
                        prefix: "",//设置数值前缀
                        postfix: "Miles",//设置数值后缀
                        prettify: true,
                        hasGrid: false,
                        onFinish: function(ret) {
                            $.post('/ajax/genListUrl/',{
                                origin_url : "<?php echo Request::getCurrentUrl() ?>",
                                min_mile : ret.fromNumber,
                                max_mile : ret.toNumber
                            },function(data){
                                if (data.code == 0) {
                                    location.href = '/car/?' + data.data.url;
                                } else {
                                    location.reload();
                                }
                            });
                        }
                    });
                    $(".range_2").ionRangeSlider({
                        min: 0,
                        max: 1000000,
                        from:<?php echo isset($this->requestParams['min_price']) ? $this->requestParams['min_price'] : '0'?>,
                        to: <?php echo isset($this->requestParams['max_price']) && $this->requestParams['max_price'] > 0 ? $this->requestParams['max_price'] : '1000000'?>,
                        type: 'double',//设置类型
                        step: 1000,
                        prefix: "$",//设置数值前缀
                        postfix: "",//设置数值后缀
                        maxPostfix:"+",
                        prettify: true,
                        hasGrid: false,
                        onFinish: function(ret) {
                            $.post('/ajax/genListUrl/',{
                                origin_url : "<?php echo Request::getCurrentUrl() ?>",
                                min_price : ret.fromNumber,
                                max_price : ret.toNumber
                            },function(data){
                                if (data.code == 0) {
                                    location.href = '/car/?' + data.data.url;
                                } else {
                                    location.reload();
                                }
                            });
                        }
                    });
                </script>

                <!--<div class="car-tpf-row clearfix">
                    <div class="colum clearfix">
                        <div class="l dr-fea-box">
                            <div class="dr-fea-item">
                                <span>驱动类型</span>
                                <div class="list">
                                    <a href="javascript:void(0);" class="active">全部</a>
                                    <a href="javascript:void(0);">四驱</a>
                                    <a href="javascript:void(0);">前驱</a>
                                    <a href="javascript:void(0);">后驱</a>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="dr-fea-item">
                                <span>车门数</span>
                                <div class="list">
                                    <a href="javascript:void(0);" class="active">全部</a>
                                    <a href="javascript:void(0);">2D</a>
                                    <a href="javascript:void(0);">3D</a>
                                    <a href="javascript:void(0);">4+</a>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="dr-fea-item">
                                <span>颜色</span>
                                <div class="color-ls">
                                    <a href="javascript:void(0);">全部</a>
                                    <a href="javascript:void(0);" style="background-color:#FFF;"></a>
                                    <a href="javascript:void(0);" style="background-color:#000000;"></a>
                                    <a href="javascript:void(0);" style="background-color:#c0c0c0;"></a>
                                    <a href="javascript:void(0);" style="background-color:#999999;"></a>
                                    <a href="javascript:void(0);" style="background-color:#b5651e;"></a>
                                    <a href="javascript:void(0);" style="background-color:#6599ff;"></a>
                                    <a href="javascript:void(0);" style="background-color:#fe0000;"></a>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="colum clearfix">
                        <div class="r dr-fea-box" id="fea">

                            <div class="dr-fea-item fea-ils">
                                <span>附加功能</span>
                                <div class="list">
                                    <a href="javascript:void(0);" class="active">全部</a>
                                    <a href="javascript:void(0);">天窗</a>
                                    <a href="javascript:void(0);">全景天窗</a>
                                    <a href="javascript:void(0);">真皮座椅</a>
                                    <a href="javascript:void(0);">座椅加热</a>
                                    <a href="javascript:void(0);">GPS导航</a>
                                    <a href="javascript:void(0);">倒车影像</a>
                                    <a href="javascript:void(0);">倒车雷达</a>
                                    <a href="javascript:void(0);">USB</a>
                                    <a href="javascript:void(0);">AUX</a>
                                    <a href="javascript:void(0);">车载蓝牙</a>
                                    <a href="javascript:void(0);">无匙进入</a>
                                    <a href="javascript:void(0);">高级音响</a>
                                    <a href="javascript:void(0);">LED灯</a>
                                </div>
                                <div class="clear"></div>
                            </div>

                        </div>
                    </div>-->
                </div>
            </div>

            <div class="row advanced-box">
                <!--<span class="advanced-tip active"><i class="i-1"></i><span class="txt">高级搜索</span><i class="i-2"></i></span>-->
                <dl class="select">
                    <!--title用于存储选值-->
                    <dt title="value"><?php echo $this->sortTitle ? $this->sortTitle : '默认排序' ?></dt>
                    <div class="option">
                        <?php foreach ($this->queryOptions['sort_options'] as $option) { ?>
                            <a href="/car/?<?php echo $option['url']?>"><dd><?php echo $option['name']?></dd></a>
                        <?php } ?>
                    </div>
                </dl>
            </div>
        </form>
    </div>
</div>

<div class="buy-car-show">
    <div class="mid-block">
    <?php if ($this->carList) { ?>
        <ul class="buy-car-list">
        <?php foreach ($this->carList as $car) { ?>
            <li>
                <div class=" clearfix buy-car-picB">
                    <div class="L">
                        <a href="<?php echo $car['detail_url'];?>" class="dv dv1"><img src="<?php echo $car['image_list'][1]['small_url']?>"/></a>
                        <a href="<?php echo $car['detail_url'];?>" class="dv dv2"><img src="<?php echo $car['image_list'][2]['small_url']?>"/></a>
                    </div>
                    <div class="R">
                        <a href="<?php echo $car['detail_url'];?>"><img src="<?php echo $car['image_list'][0]['big_url']?>"/></a>
                    </div>
                </div>
                <div class="buy-car-txt clearfix">
                    <div class="l">
                        <a target="_blank" href="<?php echo $car['detail_url'];?>"><p class="t1 cs-text-white"><?php echo $car['title'] ?></p></a>
                        <p class="t2"><?php echo $car['vehicle_year'] ?>年入手<span class="dot">.</span><?php echo $car['mile'] ?>英里<span class="dot">.</span><?php echo $car['transmission_text'] ?><span class="dot">.</span><?php echo $car['body_style_text'] ?></p>
                    </div>
                    <div class="r">
                        <span class="buy-car-mon"><span class="tip">$</span><?php echo $car['listing_price']?></span>
                    </div>
                </div>
            </li>
        <?php } ?>
        </ul>
        <?php } else { ?>
            <div><p>没有数据</p></div>
        <?php } ?>

        <!--分页-->
        <?php echo $this->helper('pager', array('pager' => $this->pager, 'requestParams' => $this->requestParams, 'urlUtil' => $this->urlUtil)); ?>

    </div>
</div>

<?php echo $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>

</body>
</html>
