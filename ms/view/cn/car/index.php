<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>二手车</title>
    <link href="http://sta.wheelsrnr.com/ms/css/css.css" rel="stylesheet" type="text/css"/>
    <SCRIPT src="http://sta.wheelsrnr.com/ms/js/slider.js" type=text/javascript></SCRIPT>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/lrtk.js"></script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/custom.css" />
</head>

<body>
<script type="text/javascript">
    $(window).scroll(function(event){
        var winPos = $(window).scrollLeft();
        $("#tp").css("left",winPos);
    });
</script>
<style>
    .my_logo_out{ width:1200px; position:relative; margin:0 auto;}
    .my_logo{
        position:absolute;
        left:0;
        top:0;
        z-index:9999;
    }
</style>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo.png" class="my_logo" /></div>

<div id="tp">
    <div style="width:100%; position:relative;">
        <div class="sousuo">
            <a class="prevT" href="javascript:;" stat="prev1001"><img src="http://sta.wheelsrnr.com/ms/images/l-btn.png" /></a>
            <a class="nextT" href="javascript:;" stat="next1002"><img src="http://sta.wheelsrnr.com/ms/images/r-btn.png" /></a>
            <form action="/car/" method="get">
                <ul class="sel1">
                    <li >
                        <input name="lan" type="hidden" value="<?php echo MsController::$LANGUAGE ?>" />
                        <input name="keyword" type="text" value="搜索车辆..." onclick="this.value=''" onblur="this.value='输入用户名'" class="sel2" />
                        <font>
                            <input class="button" type="submit" value="">
                        </font></li>
                    <span><a href="javascript:;">热门搜索</a><a href="/car/?keyword=suv">SUV</a><a href="/car/?keyword=manual">手动挡</a><a href="/car/?keyword=automatic">自动挡</a></span>
                </ul>
            </form>
        </div>
    </div>
</div>

<div id="top">
    <ul class="slideTop-box">
        <li class="slideTop"  style="background:url(http://sta.wheelsrnr.com/ms/images/index-bg-1.jpg) no-repeat center center; background-size:cover;"></li>
        <li class="slideTop"  style="background:url(http://sta.wheelsrnr.com/ms/images/index-bg-2.jpg) no-repeat center center; background-size:cover"></li>
        <li class="slideTop"  style="background:url(http://sta.wheelsrnr.com/ms/images/index-bg-3.jpg) no-repeat center center; background-size:cover;"></li>
    </ul>

    <div class="itemT">
        <li class="on" stat="item1001" href="javascript:;"></li>
        <li href="javascript:;" stat="item1002"></li>
        <li href="javascript:;" stat="item1003"></li>
    </div>
    <div class="zhengti">
        <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
        <div style="clear:both;"></div>
    </div>
</div>

<div style="position:relative; z-index:888;">

<div id="zuoyoutu">
    <div class="jiantou">
        <span><img src="http://sta.wheelsrnr.com/ms/images/ershouche_6.png" /></span>
        <span class="youb"><img src="http://sta.wheelsrnr.com/ms/images/ershouche_7.png" /></span>
        <li><a href="/car/?lan=cn">
                <div class="zzsc">
                    <canvas class="loader" id="loader-1"></canvas>
                    <div class="zzsc-inner"><img src="http://sta.wheelsrnr.com/ms/images/ershouche_3.png"></div>
                    <div class="zzsc-xu">
                        <img src="http://sta.wheelsrnr.com/ms/images/ershouche_4_out.png">
                    </div>
                </div>
                <div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';"></div></a></li>
        <li class="tuyou"><a href="/sell/?lan=cn">
                <!--<div class="zzscs"></div>-->
                <div class="zzsc" style="margin-left:0;">
                    <canvas class="loader" id="loader-2"></canvas>
                    <div class="zzsc-inner"><img src="http://sta.wheelsrnr.com/ms/images/ershouche_4.png"></div>
                    <div class="zzsc-xu"><img src="http://sta.wheelsrnr.com/ms/images/ershouche_4_out.png"></div>
                </div>
                <div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';"></div></a></li>
    </div>
    <font><img src="http://sta.wheelsrnr.com/ms/images/ershouche_7.jpg" /></font>
</div>
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.classyloader.min.js"></script>

<div id="youshi">
    <div class="youshiweizi">
        <font class="ztdx">我们的优势</font>
        <font class="ztdx1">OUR ADVANTAGE<br />
            <img src="http://sta.wheelsrnr.com/ms/images/ershouche_5.jpg" /></font>
        <li class="zuotu"><img src="http://sta.wheelsrnr.com/ms/images/ershouche_1.png" /></li>
        <li class="youweizi"><h1>专业检测团队</h1><h1 class="ywei">PROFESSIONAL TESTING TEAM</h1><h1 class="ywei2">团队由汽车专业媒体从业人员和职业车手组成，测试追求“专业、客观、公正”的准则，一经推出就获得了网民、厂商以及同行的一致认可，并表现出汽车网络媒体中汽车测试的绝对领先性，让大家可以看到，汽车网络媒体也可以做出世界一流的汽车测试出来。</h1><h1><img src="http://sta.wheelsrnr.com/ms/images/ershouche_4.jpg" /></h1></li>
    </div>
</div>
<div style="clear:both;"></div>



<div id="moshi">
    <div class="moshiwd">
        <li class="youweizi">
            <h1>全新模式普惠人人</h1>
            <font class="ywei">GET PREFERENTIAL</font>
            <font class="ywei2">每单交易都有多重惊喜，服务模式的创新不仅仅是经营模式的创新,还应该包括盈利模式,服务理念,服务产品和技术手段上等方面的创新。在着力普惠金融发展的过程中,无论是现有的互</font>
        </li>
        <li class="zuoweizi">
            <h1>7天100mils 免费退换</h1>
            <font class="ywei">FREE REPLACEMENT</font>
            <font class="ywei2">每单交易都有多重惊喜，服务模式的创新不仅仅是经营模式的创新,还应该包括盈利模式,服务理念,服务产品和技术手段上等方面的创新。在着力普惠金融发展的过程中,无论是现有的互</font>
        </li>
    </div>
</div>


<div id="feedback">
    <div id="fuceng">
        <div class="fuwu">
            <div class="fuwuzt">
                <font>我们的服务</font>
                <span>提供最省心，最直接的二手车交易平台</span>
            </div>



            <div class="tubi">
                <li><span><img src="http://sta.wheelsrnr.com/ms/images/ershouche_8.png" /><br />汽车质检 & 精确定价</span>
                    <font>专业的汽车质检师上门服务,对汽车进行整体检测，准确定价。</font>
                </li>
                <li><span><img src="http://sta.wheelsrnr.com/ms/images/ershouche_9.png" /><br />量子化卖车</span>
                    <font>在我们的线上平台展示的同时，您可以继续开着她。薛定谔车，边卖边开！</font>
                </li>
                <li><span><img src="http://sta.wheelsrnr.com/ms/images/ershouche_10.png" /><br />一击买车</span>
                    <font>买家可以直接在平台看到最真实的车辆信息，指尖一点，爱车到家门口。</font>
                </li>
                <li><span><img src="http://sta.wheelsrnr.com/ms/images/ershouche_11.png" /><br />门前试驾，无忧购车</span>
                    <font>在签收新车前，30分钟试驾；购买车辆后，7天/100公里无条件退货。</font>
                </li>
            </div>
        </div>
    </div>


    <div class="slide-main" id="touchMain">
        <a class="prev" href="javascript:;" stat="prev1001"><img src="http://sta.wheelsrnr.com/ms/images/l-btn.png" /></a>
        <div class="slide-box" id="slideContent">
            <div class="slide" id="bgstylec">
                <div class="fuh"><font>”</font>
                    <ul><li>It was a pleasure to work with the guys at WheelRNR Studio. They made sure we were well fed and drunk all the time!</li></ul>
                    <span><img src="http://sta.wheelsrnr.com/ms/images/ershouche_5_1.png" /><br />Milk.kwok</span>
                </div>
            </div>
            <div class="slide" id="bgstylea">
                <div class="fuh"><font>”</font>
                    <ul><li>It was a pleasure to work with the guys at WheelRNR Studio. They made sure we were well fed and drunk all the time!</li></ul>
                    <span><img src="http://sta.wheelsrnr.com/ms/images/ershouche_5_2.png" /><br />Milk.kwok</span>
                </div>
            </div>
            <div class="slide" id="bgstyleb">
                <div class="fuh"><font>”</font>
                    <ul><li>It was a pleasure to work with the guys at WheelRNR Studio. They made sure we were well fed and drunk all the time!</li></ul>
                    <span><img src="http://sta.wheelsrnr.com/ms/images/ershouche_5.png" /><br />Milk.kwok</span>
                </div>
            </div>
        </div>
        <a class="next" href="javascript:;" stat="next1002"><img src="http://sta.wheelsrnr.com/ms/images/r-btn.png" /></a>
        <div class="item">
            <a class="cur" stat="item1001" href="javascript:;"></a><a href="javascript:;" stat="item1002"></a><a href="javascript:;" stat="item1003"></a>
        </div>
    </div>
</div>
<div style="clear:both;"></div>

<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>

<script type="text/javascript">
    $(function(){
        var win_height=$(window).height();

        /*唱片效果*/
        var loader_0=$("#loader-1").ClassyLoader({
            width:368,
            height:368,
            animate: false,
            percentage: 0,
            speed: 20,
            showText: false,
            start:'top',
            diameter:180, //圆形进度条直径
            lineColor: 'rgba(3,3,3,1)', //圆形进度条线条颜色
            remainingLineColor: 'rgba(212,212,212,1)', //剩余百分比的线条颜色
            lineWidth: 3, //线条宽度
        });
        var loader_1=$("#loader-2").ClassyLoader({
            width:368,
            height:368,
            animate: false,
            percentage: 0,
            speed: 20,
            showText: false,
            start:'top',
            diameter:180, //圆形进度条直径
            lineColor: 'rgba(3,3,3,1)', //圆形进度条线条颜色
            remainingLineColor: 'rgba(212,212,212,1)', //剩余百分比的线条颜色
            lineWidth: 3, //线条宽度
        });
        $(".zzsc").hover(function(){
            var index=$(".zzsc").index(this);

            if(index==0){
                loader_0.draw(100);
            }else{
                loader_1.draw(100);
            }
        },function(){
            var index=$(".zzsc").index(this);

            if(index==0){
                loader_0.setPercent(0).draw();
            }else{
                loader_1.setPercent(0).draw();
            }
        })

    })
</script>
</div>
</body>
</html>
