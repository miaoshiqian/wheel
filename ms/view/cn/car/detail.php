<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>买车详情页</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/focus-slide.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/custom.css" />

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.SuperSlide.2.1.2.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/bxq-fun.js"></script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo.png" class="my_logo" /></div>
<div class="process-T">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>
    </div>
</div>

<link href="http://sta.wheelsrnr.com/ms/css/lrtk.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/lrtk.js"></script>
<!--[if lte IE 6]>
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/belatedPNG.js"></script>
<script type="text/javascript">
    var __IE6=true;
    DD_belatedPNG.fix('.logo img,.prev img,.next img,img');
</script>
<![endif]-->

<div class="slide-main" id="touchMain">
    <a class="prev" href="javascript:;" stat="prev1001"><img src="http://sta.wheelsrnr.com/ms/images/l-btn.png" /></a>
    <div class="slide-box" id="slideContent">
        <?php foreach ($this->carInfo['image_list'] as $image) { ?>
            <div class="slide" id="bgstylec">
                <img src="<?php echo $image['url'] ?>" width="100%" />
            </div>
        <?php } ?>
        <img src="http://sta.wheelsrnr.com/ms/images/000.jpg" width="100%" style="visibility:visible; z-index:-1" />
    </div>
    <a class="next" href="javascript:;" stat="next1002"><img src="http://sta.wheelsrnr.com/ms/images/r-btn.png" /></a>
</div>

<div class="bgc-f9f9f9">

<div class="bxq-slide">

    <div id="slideBox" class="slideBox">
        <div class="fbox">
            <div class="bxq-sd-fin clearfix">
                <a href="javascript:void(0);" class="bxq-open-pho">打开相册</a>
                <span class="bxq-sd-rt"><em style="color: #fd5200">$<?php echo number_format($this->carInfo['listing_price'], 2);?></em><span></span><em style="font-size: 20px"> <?php echo $this->carInfo['mile']?>英里</em></span>
            </div>
            <div class="opa"></div>
        </div>
    </div>
</div>
<div class="mid-block">

<div class="bxq-carval-b clearfix">
    <div class="bxq-carval-L">
        <div class="out-itm">
            <p class="Tit">
                <span class="ils">保证服务</span>
                <span class="ils bao">保修服务</span>
                <span class="ils pro">90天承诺退车</span>
            </p>

            <div class="car-n">
                <span class="nt"><?php echo $this->carInfo['title']?></span>
            </div>

            <div class="bxq-car-zb clearfix">
                <div class="imb">
                    <div class="pic">
                        <img src="http://sta.wheelsrnr.com/ms/images/bxq-ico-5.png">
                    </div>
                    <div class="dv-2">车身结构</div>
                    <div class="dv-3"><?php echo $this->carInfo['structure_text']?></div>
                    <div class="split"></div>
                </div>
                <div class="imb">
                    <div class="pic">
                        <img src="http://sta.wheelsrnr.com/ms/images/bxq-ico-6.png">
                    </div>
                    <div class="dv-2">变速箱</div>
                    <div class="dv-3"><?php echo $this->carInfo['gearbox_text']?></div>
                    <div class="split"></div>
                </div>
                <div class="imb">
                    <div class="pic">
                        <img src="http://sta.wheelsrnr.com/ms/images/bxq-ico-7.png">
                    </div>
                    <div class="dv-2">发动机</div>
                    <div class="dv-3"><?php echo $this->carInfo['model_trim_name']?></div>
                    <div class="split"></div>
                </div>
                <div class="imb">
                    <div class="pic">
                        <img src="http://sta.wheelsrnr.com/ms/images/bxq-ico-8.png">
                    </div>
                    <div class="dv-2">油耗</div>
                    城市: <?php echo $this->carInfo['model_info']['model_lkm_city'] ? $this->carInfo['model_info']['model_lkm_city'] : '-'?> <br> 高速: <?php echo $this->carInfo['model_info']['model_lkm_hwy'] ? $this->carInfo['model_info']['model_lkm_hwy'] : '-'?>
                </div>
            </div>

        </div>
        <div class="out-itm">

            <div class="car-pay-type">
                <span class="inline">分期付款</span>
                <span class="inline f">首付：<em>$141,350</em></span>
                <span class="inline m">月供：<em>$7831</em></span>
            </div>

        </div>
        <div class="out-itm">

            <div class="city-local">
                <span class="inline tip">所&nbsp;在&nbsp;城&nbsp;市&nbsp;：</span>
                <span class="inline dz"><?php echo $this->carInfo['state_name'] . ' ' . $this->carInfo['city_name']  ?></span>
            </div>

        </div>
        <div class="out-itm">

            <div class="bxq-cvl-sug">
                <p class="T">评估师建议:</p>

                <div class="C">
                    车况优秀，无补漆，无事故。2012年8月份出厂，10月份落
                    牌。保险到2016年10月。过了两次户但是一直是车主一个人开
                </div>
            </div>

        </div>
    </div>
    <div class="bxq-carval-R">
        <a href="/user/pay/?lng=<?php echo MsController::$LANGUAGE;?>&car_id=<?php echo $this->carInfo['id'];?>" class="radius-5 ykc">立即预约看车</a>

        <div class="bxq-cvl-man radius-5">
            <p class="t-1">认证检测师</p>

            <p class="t-2">Vitoria</p>

            <div class="v-pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bxq-upic.jpg">
            </div>
            <div class="v-dg clearfix">
                <span class="tag">高级鉴定评估师</span>
                <span class="tag">待人热情有责任心</span>
            </div>
            <div class="v-case">
                <div class="dv-1 clearfix">
                    <div class="inb no"><span class="i">已评车辆：15辆</span></div>
                    <div class="inb"><span class="i">所属地方：长春</span></div>
                </div>
                <div class="dv-2">
                    评估时间：2015-10-25
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bxq-ci-mn">
    <ul class="bxq-ci-list clearfix">
        <li class="active">
            <a href="javascript:void(0);">亮点配置</a>
        </li>
        <li>
            <a href="javascript:void(0);">车辆档案</a>
        </li>
        <li>
            <a href="javascript:void(0);">检测报告</a>
        </li>
        <li>
            <a href="javascript:void(0);">车辆参数</a>
        </li>
    </ul>
</div>


<!--亮点配置-->
<div class="bxq-ci-itm bxq-ldpz" id="bxq-cim-0">
    <div class="Top">
        <h3>亮点配置</h3>
    </div>
    <div class="in">
        <ul class="bxq-ld-in-ls clearfix">
            <?php foreach($this->carInfo['high_lights_text'] as $key => $text) {?>
                <li>
                    <div class="bxq-ldpz-lsin">
                        <i class="i-<?php echo $key >17 ? 17 : $key ?>"></i>
                        <p class="btm-tt"><?php echo $text?></p>
                        <div class="spl"></div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<!--end-->
<!--车辆档案-->
<div class="bxq-ci-itm bxq-carfile" id="bxq-cim-1">
    <div class="Top">
        <h3>车辆档案</h3>
    </div>
    <div class="bxq-cf-info">
        <div class="base-b">
            <span class="bxq-flag-s1">基本信息</span>

            <div class="mbox">
                【外部颜色】：<span class="n"><?php echo $this->carInfo['exterior_color']?></span>；&nbsp;
                【价格】：<span class="n">$<?php echo number_format($this->carInfo['listing_price'], 2);?></span>；&nbsp;
                【里程】：<span class="n"><?php echo $this->carInfo['mile']?> miles</span>；&nbsp;
                【发布时间】：<span class="n"><?php echo date('Y-m-d', $this->carInfo['create_time'])?></span>；
            </div>
        </div>
        <div class="import-b">
            <span class="bxq-flag-s1">重要配置</span>

            <div class="mbox">
                <span class="bxq-cfg-tag i-0">标配</span>
                <span class="bxq-cfg-tag i-1">加配</span>
                <span class="bxq-cfg-tag i-2">无</span>
                <span class="bxq-cfg-tag i-3">正常</span>
                <span class="bxq-cfg-tag i-4">异常</span>
            </div>
            <div class="bxq-cfg-mls clearfix">
                <ul class="l bxq-cfg-list">
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">一键启动</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">前后电动升降窗</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">电动折叠后视镜</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">电动调节前座椅</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">氙气大灯</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">安全气囊</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">大灯清洗</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">防抱死系统</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                </ul>
                <ul class="l bxq-cfg-list">
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">一键启动</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">前后电动升降窗</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">电动折叠后视镜</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">电动调节前座椅</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">氙气大灯</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">安全气囊</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">大灯清洗</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">防抱死系统</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--end-->
<!--检测报告-->
<div class="bxq-ci-itm" id="bxq-cim-2">
<div class="Top">
    <h3>检测报告</h3>
</div>
<div class="bxq-ts-report">
<ul class="bxq-tsr-mn clearfix">
    <li class="active"><a href="javascript:void(0);">事故排查</a></li>
    <li><a href="javascript:void(0);">判别火烧车</a></li>
    <li><a href="javascript:void(0);">判别水泡车</a></li>
    <li><a href="javascript:void(0);">安全性检测</a></li>
    <li><a href="javascript:void(0);">外观检测</a></li>
    <li><a href="javascript:void(0);">内饰检测</a></li>
    <li><a href="javascript:void(0);">发动机舱检测</a></li>
</ul>
<!--事故排查-->
<div class="bxq-tsr-mbox acc" style="display: block;">
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-1.jpg">
    <br/>

    <p>底盘：无撞击事故</p><br/>
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-2.jpg">
    <br/>

    <p>车头：无撞击事故</p><br/>
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-3.jpg">
    <br/>

    <p>左侧车身：无撞击事故</p><br/>
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-4.jpg">
    <br/>

    <p>右侧车身：无撞击事故</p><br/>
</div>
<!--end-->
<!--判别火烧车-->
<div class="bxq-tsr-mbox fire">
    <h3 class="bxq-tsr-mh-1">如何排查火烧车痕迹？</h3>

    <p class="bxq-tsr-tdes-1"><span>火烧车是指符合以下任意一条描述的车辆:</span></p>

    <div class="bxq-tsr-qes clearfix">
        <div class="cL">
            <p class="p-1">1.发动机线束和车身线束更换新件及局部地方有火烧痕迹</p>

            <p class="p-2">3.车身各夹层内有火烧熏黑的痕迹</p>
        </div>
        <div class="cR">
            <p class="p-1">2.发动机线束和车身线束有火烧熔线及局部地方有火烧痕迹</p>

            <p class="p-2">4.发动机舱、车厢内或尾箱内有烧黑痕迹</p>
        </div>
    </div>
    <div class="bxq-tsr-qshow">
        <div class="title">
            火烧排查检测<span class="tip">已通过,无火烧痕迹</span>
        </div>
        <ul class="qlist clearfix">
            <li>
                <span class="t">安全带/车内地毯机底板检测 :</span>
                <span class="c i-3">无淤泥 无锈迹 无潮湿</span>
            </li>
            <li>
                <span class="t">散热片机引擎旁零件/水箱及水箱前板检测 :</span>
                <span class="c i-3">无淤泥 无锈迹</span>
            </li>
        </ul>
    </div>
</div>
<!--end-->
<!--判别水泡车-->
<div class="bxq-tsr-mbox water">
    <h3 class="bxq-tsr-mh-1">如何排查水泡车痕迹?</h3>

    <p class="bxq-tsr-tdes-1"><span>水泡车是指同时符合以下任意两条或两条以上描述的车辆:</span></p>

    <div class="bxq-tsr-qes clearfix">
        <div class="cL">
            <p class="p-1">1.座椅底部的金属支架和滑轨有非自然锈蚀</p>

            <p class="p-2">3.车底板上有一层用来隔音隔热的棉毡,棉毡上有水渍或泥沙</p>

            <p class="p-3">5.备胎座有水渍或污泥</p>
        </div>
        <div class="cR">
            <p class="p-1">2.安全带抽到底,有水迹或霉斑</p>

            <p class="p-2">4.悬挂组件的固定螺丝及刹车挡板有非自然锈蚀</p>

            <p class="p-3">6.驾驶舱内地胶或地毯有拆卸痕迹并有水渍、污渍</p>
        </div>
    </div>
    <div class="bxq-tsr-qshow">
        <div class="title">
            火烧排查检测<span class="tip">已通过,无火烧痕迹</span>
        </div>
        <ul class="qlist clearfix">
            <li>
                <span class="t">安全带/车内地毯机底板检测 :</span>
                <span class="c i-3">无淤泥 无锈迹 无潮湿</span>
            </li>
            <li>
                <span class="t">散热片机引擎旁零件/水箱及水箱前板检测 :</span>
                <span class="c i-3">无淤泥 无锈迹</span>
            </li>
            <li>
                <span class="t">发动机线束及橡胶制品/座椅弹簧和内套绒布 :</span>
                <span class="c i-3">无淤泥 无异味</span>
            </li>
        </ul>
    </div>
</div>
<!--end-->
<!--安全性检测-->
<style>
    .safe .pic{
        width: 298px;
        height: 269px;
        padding: 32px 20px;
        background-color: #fbfbfb;
    }
    .item2{
        float:left;
        margin-left:50px
    }
</style>
<div class="bxq-tsr-mbox safe">
    <div class="bxq-ts-sbox clearfix">
        <div class="item2">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-sf1.png">
            </div>
            <div class="bxq-tsf-ibtm">
                <p class="t">防冻液<span class="r qr">有两项问题</span></p>

                <p class="t-t">防冻液冰点在25°C~35°C之间即为安全</p>

                <p class="warm"><span class="txt">-35°C</span></p>
            </div>
        </div>
        <div class="item2">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-sf2.png">
            </div>
        </div>
        <div class="item2">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-sf3.png">
            </div>
        </div>
    </div>
</div>
<!--end-->
<!--外观检测-->
<div class="bxq-tsr-mbox appear">
    <div class="mpic"></div>
</div>
<!--end-->
<!--内饰检测-->
<div class="bxq-tsr-mbox deco">
    <ul class="bxq-tsde-ls clearfix">
        <li>
            <div class="inn"><span class="txt">仪表盘<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">安全带<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">车顶灯<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">大灯开关<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">中央门锁<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">车顶拉手<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">转向灯开关<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">后视镜<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">扬声器<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">雨刮器<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">车门饰板<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">A柱饰件<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">空调<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">车门灯<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">B柱饰件<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">影音系统及导航<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">天窗<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">C珠饰件<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">前排座椅<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">车窗<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">安全气囊<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">后排座椅<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">扶手<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">车门密封条<i class="i-3"></i></span></div>
        </li>
        <li></li>
        <li>
            <div class="inn"><span class="txt">遮阳板<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">行李箱密封条<i class="i-3"></i></span></div>
        </li>
    </ul>
</div>
<!--end-->
<!--发动机舱检测-->
<div class="bxq-tsr-mbox motor">
    <div class="tpic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-pic-2.jpg">
    </div>
    <p class="ibtm">经认证发动机舱无明显瑕疵</p>
</div>
</div>
</div>
<!--end-->
<!--车辆参数-->
<div class="bxq-ci-itm" id="bxq-cim-3">
    <div class="Top">
        <h3>车辆参数</h3>
    </div>
    <div class="bxq-car-param">
        <?php
        $carConfig = [
            [
                'title' => 'Basic Parameters',
                'items' => [
                    ['key' => 'Price','val' => 'listing_price'],
                    ['key' => 'Mileage'        ,'val' => 'mile'],
                    ['key' => 'Exterior Color'      ,'val' => 'exterior_color'],
                    ['key' => 'Interior Color'      ,'val' => 'interior_color'],
                    ['key' => 'Title'  ,'val' => 'title'],
                    ['key' => 'Doors'    ,'val' => 'model_doors'],
                    ['key' => 'Body Type'    ,'val' => 'structure_text'],
                    ['key' => 'Gearbox','val' => 'gearbox_text'],
                    ['key' => 'Fuel','val' => 'model_engine_fuel'],
                    ['key' => 'Fuel Tank Capacity','val' => 'model_fuel_cap_l'],
                    ['key' => 'Stock','val' => 'id'],
                ]
            ],
            [
                'title' => 'Engine',
                'items' => [
                    ['key' => 'Engine Model'  ,'val' => 'model_trim_name'],
                    ['key' => 'Horsepower'     ,'val' => 'model_engine_power_ps'],
                    ['key' => 'Torque'    ,'val' => 'model_engine_torque_nm'],
                    ['key' => 'Engine Type'  ,'val' => 'model_engine_type'],
                    ['key' => 'Drive Type'      ,'val' => 'model_drive'],
                    ['key' => 'Transmission Type','val' => 'model_transmission_type'],
                    ['key' => 'Engine Position'    ,'val' => 'model_engine_position'],
                    ['key' => 'Top Speed（kph）','val' => 'model_top_speed_kph'],
                    ['key' => '0-100 kph'    ,'val' => 'model_0_to_100_kph'],
                    ['key' => 'Mixed MPG'    ,'val' => 'model_lkm_mixed'],
                ]
            ]
        ]
        ?>
        <?php for ($i = 0; $i < round(count($carConfig)/2); $i++ ) { ?>
            <table border="0" cellpadding="0" cellspacing="0" class="bxq-cpar-tb">
                <thead>
                <tr>
                    <td colspan="2">
                        <span class="bxq-flag-s1"><?php echo $carConfig[$i*2]['title'] ?></span>
                    </td>
                    <?php if (isset($carConfig[$i*2+1])) { ?>
                        <td colspan="2">
                            <span class="bxq-flag-s1"><?php echo $carConfig[$i*2 + 1]['title'] ?></span>
                        </td>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php for ($j = 0; $j < max(count($carConfig[$i]['items']), count($carConfig[$i+1]['items'])); $j++) { ?>
                    <tr>
                        <td class="td-1"><span class="tip"><?php echo $carConfig[$i*2]['items'][$j]['key']?></span></td>
                        <td><?php echo  $this->carInfo[$carConfig[$i*2]['items'][$j]['val']]?></td>
                        <?php if (isset($carConfig[$i+1]['items'][$j])) { ?>
                            <td class="td-1"><span class="tip"><?php echo $carConfig[$i*2+1]['items'][$j]['key']?></span></td>
                            <td><?php echo $this->carInfo[$carConfig[$i*2+1]['items'][$j]['val']]?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
</div>
<!--end-->

<!--可能喜欢-->
<div class="bxq-like">
<div class="Top">
    <b class="t">你可能会喜欢</b>
    <a href="javascript:void(0);" class="c-oth">换一批</a>
</div>
<div class="bxq-like-lsb">
<ul class="bxq-like-list clearfix">
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-1.jpg">
    </a>

    <p class="car-name">高尔夫 2012款 1.6 自动舒适型</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">时间</p>

            <p class="p-2">2012-12</p>
        </div>
        <div class="im">
            <p class="p-1">行走公里数</p>

            <p class="p-2">30000 km</p>
        </div>
        <div class="im">
            <p class="p-1">变速箱</p>

            <p class="p-2">手动</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-2.jpg">
    </a>

    <p class="car-name">马自达62011款2.0L自动时尚型</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">时间</p>

            <p class="p-2">2011-9</p>
        </div>
        <div class="im">
            <p class="p-1">行走公里数</p>

            <p class="p-2">56000 km</p>
        </div>
        <div class="im">
            <p class="p-1">变速箱</p>

            <p class="p-2">自动</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;5,8625</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-3.jpg">
    </a>

    <p class="car-name">捷达2010款1.6L伙伴</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">时间</p>

            <p class="p-2">2010-12</p>
        </div>
        <div class="im">
            <p class="p-1">行走公里数</p>

            <p class="p-2">980000 km</p>
        </div>
        <div class="im">
            <p class="p-1">变速箱</p>

            <p class="p-2">自动</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-4.jpg">
    </a>

    <p class="car-name">奥迪A6L2014款TFSI标准型</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">时间</p>

            <p class="p-2">2009-11</p>
        </div>
        <div class="im">
            <p class="p-1">行走公里数</p>

            <p class="p-2">72000 km</p>
        </div>
        <div class="im">
            <p class="p-1">变速箱</p>

            <p class="p-2">手动</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-5.jpg">
    </a>

    <p class="car-name">高尔夫 2012款 1.6 自动舒适型</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">时间</p>

            <p class="p-2">2012-12</p>
        </div>
        <div class="im">
            <p class="p-1">行走公里数</p>

            <p class="p-2">30000 km</p>
        </div>
        <div class="im">
            <p class="p-1">变速箱</p>

            <p class="p-2">手动</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-6.jpg">
    </a>

    <p class="car-name">马自达62011款2.0L自动时尚型</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">时间</p>

            <p class="p-2">2011-9</p>
        </div>
        <div class="im">
            <p class="p-1">行走公里数</p>

            <p class="p-2">56000 km</p>
        </div>
        <div class="im">
            <p class="p-1">变速箱</p>

            <p class="p-2">自动</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;5,8625</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-7.jpg">
    </a>

    <p class="car-name">捷达2010款1.6L伙伴</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">时间</p>

            <p class="p-2">2010-12</p>
        </div>
        <div class="im">
            <p class="p-1">行走公里数</p>

            <p class="p-2">980000 km</p>
        </div>
        <div class="im">
            <p class="p-1">变速箱</p>

            <p class="p-2">自动</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-8.jpg">
    </a>

    <p class="car-name">奥迪A6L2014款TFSI标准型</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">时间</p>

            <p class="p-2">2009-11</p>
        </div>
        <div class="im">
            <p class="p-1">行走公里数</p>

            <p class="p-2">72000 km</p>
        </div>
        <div class="im">
            <p class="p-1">变速箱</p>

            <p class="p-2">手动</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
</ul>
</div>
</div>
<!--end-->

</div>

</div>

<!--相册-->

<link href="http://sta.wheelsrnr.com/ms/css/grids.css" rel="stylesheet" type="text/css" media="all" />
<link href="http://sta.wheelsrnr.com/ms/css/album.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/carousel.js"></script>
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/album.js"></script>

<div class="car-gallery">
    <div class="pho-close">x</div>
    <div class="album" id="album">
        <div class="album-image-md" id="album-image-md">
            <p class="album-image-bd"><img src="http://sta.wheelsrnr.com/ms/images/buy-show-1-3.jpg" class="album-image" id="album-image" alt="图片1" /></p>

            <ul class="album-image-nav hide" id="album-image-nav">
                <li class="album-image-nav-left-block" id="album-image-nav-left-block"><a href="#prev-image" class="album-image-btn-prev" id="album-image-btn-prev">‹</a></li>
                <li class="album-image-nav-right-block" id="album-image-nav-right-block"><a href="#next-image" class="album-image-btn-next" id="album-image-btn-next">›</a></li>
            </ul>

        </div>
        <div class="album-carousel" id="album-carousel">
            <a href="#prev-group" class="album-carousel-btn-prev" id="album-carousel-btn-prev">‹</a>
            <div class="album-carousel-zone" id="album-carousel-zone">
                <ul class="album-carousel-list" id="album-carousel-list">
                    <?php foreach ($this->carInfo['image_list'] as $key => $image) { ?>
                        <li class="album-carousel-thumb <?php echo $key==0 ? 'album-carousel-thumb-selected' : ''?>"><a href="<?php echo $image['url']?>"><img src="<?php echo $image['url']?>" alt="相册图片-示例图片（17）" /></a></li>
                    <?php } ?>
                </ul>
            </div>
            <a href="#next-group" class="album-carousel-btn-next" id="album-carousel-btn-next">›</a>
        </div>
    </div>
</div>
<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
<script>
    $(document).ready(function(){
        var Album = new jQuery.Album();
        $(".bxq-open-pho").click(function(){
            $("body").css({"overflow":"hidden"});
            $(".car-gallery").show();
        })
        $(".pho-close").click(function(){
            $("body").css({"overflow":"auto"});
            $(".car-gallery").hide();
        })
    });
</script>
</html>
