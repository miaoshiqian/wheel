<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>车辆检测页面</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <!--<link rel="stylesheet" type="text/css" href="css/dlselect.css" />-->
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <!--<script type="text/javascript" src="js/dlselect.js"></script>-->
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo.png" class="my_logo" /></div>
<div class="car-check-T">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>

        <div class="main-box">
            <p class="t">The wheelsrnr Inspection Checklist</p>
            <div class="des">
                When is a long, boring list a great thing? When it's a gigundo list of dozens of individual steps we take to make sure <br/>
                that every car is up to Beepi's elevated standards before we offer it to our community.
            </div>
            <div class="btm">Every car that we offer for sale is not only certified - it's Beepi Certified!</div>
        </div>

    </div>
</div>

<div class="car-check-ts">
    <div class="tip-box"></div>
</div>

<div class="car-check-info">
    <div class="top-img" style="margin-bottom:80px;height:357px;
	background:url('http://sta.wheelsrnr.com/ms/images/car_check_pic_1.png') no-repeat center center;"></div>
    <div class="mid-block">
        <span class="car-check-spl"></span>
        <div class="pic" style="margin-top:18px;margin-bottom:62px;">
            <img src="http://sta.wheelsrnr.com/ms/images/car_check_pic_2.png"/>
        </div>
        <span class="car-check-spl"></span>
        <div class="pic" style="margin-top:100px;margin-bottom:80px;">
            <img src="http://sta.wheelsrnr.com/ms/images/car_check_pic_3.png"/>
        </div>
        <span class="car-check-spl"></span>
        <div class="pic" style="margin-top:80px;margin-bottom:68px;">
            <img src="http://sta.wheelsrnr.com/ms/images/car_check_pic_4.png"/>
        </div>
        <span class="car-check-spl"></span>
        <div class="pic" style="margin-top:70px;margin-bottom:100px;">
            <img src="http://sta.wheelsrnr.com/ms/images/car_check_pic_5.png"/>
        </div>
        <span class="car-check-spl"></span>
        <div class="pic" style="margin-top:102px;margin-bottom:150px;">
            <img src="http://sta.wheelsrnr.com/ms/images/car_check_pic_6.png"/>
        </div>
        <span class="car-check-spl"></span>
        <div class="pic" style="margin-top:30px;margin-bottom:182px;">
            <img src="http://sta.wheelsrnr.com/ms/images/car_check_pic_7.png"/>
        </div>

        <div class="pic">
            <img src="http://sta.wheelsrnr.com/ms/images/car_check_pic_8.png"/>
        </div>

    </div>
</div>

<div class="car-check-bot">
    <div class="mid-block">
        <div class="cck-bot-list">
            <a href="javascript:void(0);">How wheelsrnr Works</a>
            <a href="javascript:void(0);">Inspection Checklist</a>
            <a href="javascript:void(0);">Love Stories</a>
            <a href="javascript:void(0);">Coverage Area</a>
            <a href="javascript:void(0);">FAQ</a>
        </div>
    </div>
</div>


<!--底部-->
<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
