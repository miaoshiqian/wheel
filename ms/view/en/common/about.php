<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>About Us</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <!--<link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css" />-->
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <!--<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>-->
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo2.png" class="my_logo" /></div>
<div class="AboutTop">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>
        <div class="about-serv">
            <div class="item iR itm-0">
                <p class="t"><span class="txt">Buy a Car in One Click</span></p>

                <div class="ct">
                    Buyers can see real car details and with one click, <br/>your favorite car will be delivered to your front door
                </div>
            </div>
            <div class="item iL itm-1">
                <p class="t"><span class="txt">Quality Vehicles & Accurate Pricing</span></p>

                <div class="ct">
                    Professional car inspectors at your service, <br/>testing the quality of vehicles and giving accurate prices.
                </div>
                <div class="f-ico"></div>
            </div>
            <div class="item iR itm-2">
                <p class="t"><span class="txt">Worry free testing on your very own driveway</span></p>

                <div class="ct">
                    30 minute test driving before you decide if you want the car；after your buy the car<br/>，you will have 7 day/100km unconditional refund
                </div>
            </div>
            <div class="item iL itm-3">
                <p class="t"><span class="txt">Quantum Car Dealers</span></p>

                <div class="ct">
                    When your car is in our database, you can still<br/>drive it. Schrodinger Car，Sell and Drive simultaneously
                </div>
                <div class="f-ico"></div>
            </div>
        </div>
        <span class="dp-ico"></span>
    </div>
    <div class="o-opa"></div>
</div>

<div class="AboutModel">
    <div class="mid-block">
        <div class="T">
            <p class="t-1"><img src="http://sta.wheelsrnr.com/ms/images/aboutUs-mdl-txt-en.jpg"></p>

            <p class="t-2">Simplifying second hand car transactions, clear pricing, letting you feel at ease buying a second hand car, <br/>and quickly finding your favorite car</p>
        </div>
        <div class="C clearfix abbg">
            <div class="Citem">
                <div class="in-b n_ab01">
                    <p class="p-t">Clear pricing, fair negotiations</p>
                    <div class="btm-t">
                        Doesn't matter if you're selling or buying, second hand car prices are clearly displayed publicly, <br/>making our site a platform for fair trade
                    </div>
                </div>
            </div>
            <div class="Citem">
                <div class="in-b n_ab02">
                    <p class="p-t">Simplify the Purchase Process</p>
                    <div class="btm-t">
                        Customers no longer need to go to a store, saving a lot of time, <br/>and all vehicle information is real and reliable.
                    </div>
                </div>
            </div>
            <div class="Citem">
                <div class="in-b n_ab03">
                    <p class="p-t">Selling is Easier</p>
                    <div class="btm-t">
                        Professional appraisers, public car trades, <br/>allows our customers to quickly sell their cars.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="AboutContact">
    <div class="mid-block">
        <div class="title">Contact Us</div>
        <div class="info clearfix">
            <div class="M">
                <span class="li loc">47 W. Polk St. Ste. 208, Chicago, IL 60605</span>
                <span class="li phone">312-929-8499</span>
                <span class="li email">help@wheelsrnr.com</span>
            </div>
        </div>
        <p class="start-time">Business Hours：Monday - Thursday: 10am - 6pm</p>
        <p class="ck-map-b"><a href="javascript:void(0);">Location on Map</a></p>
    </div>
</div>

<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>

</body>
</html>
