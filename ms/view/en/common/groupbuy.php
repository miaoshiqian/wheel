<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Group Purchase</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <!--<link rel="stylesheet" type="text/css" href="css/dlselect.css" />-->
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <!--<script type="text/javascript" src="js/dlselect.js"></script>-->
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
    <style>
        .daohang li a,.my_nav div a,.my_nav div li a:visited{
            color:#3b3a3a; border-color:#3b3a3a;
        }
    </style>
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo.png" class="my_logo" /></div>
<div class="carTuan-T">
    <div class="mid-block" style="position:absolute; left:0; right:0">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>
    </div>
    <img src="http://sta.wheelsrnr.com/ms/images/tuan-top-bg.jpg" width="100%" />
</div>

<div class="carTuan-two">
    <div class="mid-block">
        <div class="carT-tip" style="background: url(http://sta.wheelsrnr.com/ms/images/tuan-two-en.png) no-repeat center top;"></div>
    </div>
</div>

<div class="carTuan-is">
    <div class="mid-block">
        <div class="tuan-is-tbox">
            <div class="btm">
                <p class="txt">What is Group Puchase?</p>
                <a class="dp-ico"></a>
            </div>
        </div>
        <div class="txt-box">
            <p class="item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Group purchase is purchasing a group of products，increasing the business efficiency and achieving the best price. Sellers can lower the price and get discounts and perks that you can't get with single purchases. Group Purchase is a new electronic business model that enhances the user's bargaining power and balances the interests of the buyers and the sellers.</p>
            <p class="item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;For the customers who are busy, Group Purchase is cost efficient and convenient.</p>
            <p class="item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Group Purchase is different and adds flavor to a business transaction.</p>
        </div>
    </div>
</div>

<div class="carTuan-list-b">
    <div class="mid-block">
        <div class="T-t" style="background: url(http://sta.wheelsrnr.com/ms/images/tuan-car-tt-en.png) no-repeat center center;"></div>
        <div class="carTuan-ls">
            <ul class="clearfix">
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-1.jpg"/></a>
                        <div class="pic-txt">Kandi K10</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">Costs at most 12,000</div>
                            <a href="javascript:void(0);" class="r join-now">Offer<br/>Now</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-2.jpg"/></a>
                        <div class="pic-txt">Outlander ( imported ) 2.4L four-wheel drive models 2016 Elite Edition</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">Costs at most 12,000</div>
                            <a href="javascript:void(0);" class="r join-now">Offer<br/>Now</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-3.jpg"/></a>
                        <div class="pic-txt">A Tezi Mazda 2015 models 2.0L Sky Deluxe Edition</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">Costs at most 22,000</div>
                            <a href="javascript:void(0);" class="r join-now">Offer<br/>Now</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-4.jpg"/></a>
                        <div class="pic-txt">Shanghai Volkswagen Tiguan 2016 models 300TSI</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">Costs at most 36,000</div>
                            <a href="javascript:void(0);" class="r join-now">Offer<br/>Now</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-1.jpg"/></a>
                        <div class="pic-txt">Kandi K10</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">Costs at most 12,000</div>
                            <a href="javascript:void(0);" class="r join-now">Offer<br/>Now</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-2.jpg"/></a>
                        <div class="pic-txt">Outlander ( imported ) 2.4L four-wheel drive models 2016 Elite Edition</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">Costs at most 12,000</div>
                            <a href="javascript:void(0);" class="r join-now">Offer<br/>Now</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-3.jpg"/></a>
                        <div class="pic-txt">A Tezi Mazda 2015 models 2.0L Sky Deluxe Edition</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">Costs at most 22,000</div>
                            <a href="javascript:void(0);" class="r join-now">Offer<br/>Now</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="o-box">
                        <a href="javascript:void(0);" class="picB"><img src="http://sta.wheelsrnr.com/ms/images/tuan-car-ls-4.jpg"/></a>
                        <div class="pic-txt">Shanghai Volkswagen Tiguan 2016 models 300TSI</div>
                        <div class="btm-b clearfix">
                            <div class="l blue-tip">Costs at most 36,000</div>
                            <a href="javascript:void(0);" class="r join-now">Offer<br/>Now</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="AboutContact">
    <div class="mid-block">
        <div class="title">Contact Us</div>
        <div class="info clearfix">
            <div class="M">
                <span class="li loc">47 W. Polk St. Ste. 208, Chicago, IL 60605</span>
                <span class="li phone">312-929-8499</span>
                <span class="li email">help@wheelsrnr.com</span>
            </div>
        </div>
        <p class="start-time">Business Hours：Monday - Thursday: 10am - 6pm</p>
        <p class="ck-map-b"><a href="javascript:void(0);">Location on Map</a></p>
    </div>
</div>


<!--底部-->
<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
