<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Other Services</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <!--<link rel="stylesheet" type="text/css" href="css/dlselect.css" />-->
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <!--<script type="text/javascript" src="js/dlselect.js"></script>-->
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo2.png" class="my_logo" /></div>
<div class="related-serv-T" style="background: url(http://sta.wheelsrnr.com/ms/images/services-bg-1-en.jpg) repeat-x center top;">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>

        <div class="part">
            <p class="t">Car Services</p>
            <p class="des">Nothing fake or secretive, we only provide the best products</p>
        </div>

    </div>
</div>

<div class="related-serv-two">
    <div class="mid-block">
        <div class="related-serv-list">
            <ul class="clearfix">
                <li class="item-0">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">Loan Services</p>
                        <div class="des">We provide a range of services after you purchase the car<br/>No hidden information.</div>
                    </div>
                </li>
                <li class="item-1">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">Insurance Services</p>
                        <div class="des">Every car we sell <br/>has good quality</div>
                    </div>
                </li>
                <li class="item-2">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">Car Accident Advisory</p>
                        <div class="des">We provide a range of services after you purchase the car<br/>No hidden information.</div>
                    </div>
                </li>
                <li class="item-3">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">Change of Address</p>
                        <div class="des">Every car we sell<br/>has good quality</div>
                    </div>
                </li>
                <div style="clear:both"></div>
                <li class="item-4">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">Driving School</p>
                        <div class="des">We provide a range of services after you purchase the car<br/>No hidden information.</div>
                    </div>
                </li>
                <li class="item-5">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">Car Maintenance</p>
                        <div class="des">We provide a range of services after you purchase the car<br/>No hidden information.</div>
                    </div>
                </li>
                <li class="item-6">
                    <div class="picB"></div>
                    <div class="btm">
                        <p class="t">Car Beautification, Upgrades</p>
                        <div class="des">We provide a range of services after you purchase the car<br/>No hidden information.</div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="AboutContact">
    <div class="mid-block">
        <div class="title">Contact Us</div>
        <div class="info clearfix">
            <div class="M">
                <span class="li loc">47 W. Polk St. Ste. 208, Chicago, IL 60605</span>
                <span class="li phone">312-929-8499</span>
                <span class="li email">help@wheelsrnr.com</span>
            </div>
        </div>
        <p class="start-time">Business Hours：Monday - Thursday: 10am - 6pm</p>
        <p class="ck-map-b"><a href="javascript:void(0);">Location on Map</a></p>
    </div>
</div>


<!--底部-->
<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
