<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>My Cart</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/usc.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/input_hide.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/usc.js"></script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="payTop">
    <div class="mid-block  clearfix">
        <div class="payTopR">
            <span>Hi，thinkteam</span>
            <span class="spl">|</span>
            <a href="javascript:void(0);" target="_blank">My Cart</a>
            <span class="spl">|</span>
            <a href="javascript:void(0);" target="_blank">Help Center</a>
            <span class="spl">|</span>
            <a href="javascript:void(0);" target="_blank">Quit</a>
        </div>
    </div>
</div>

<div class="my-order bgc-f5f5f5">
    <div class="mid-block clearfix">
        <div class="usc-menu">
            <dl class="usc-menu-item">
                <dt>Personalization</dt>
                <dd><a href="javacript:void(0);">My Cart</a></dd>
                <dd><a href="javacript:void(0);">My Account</a></dd>
                <dd><a href="javacript:void(0);">My Affiliates</a></dd>
                <dd><a href="javacript:void(0);">Delivery Address</a></dd>
            </dl>
            <dl class="usc-menu-item">
                <dt>Service</dt>
                <dd><a href="javacript:void(0);">Return Form</a></dd>
                <dd><a href="javacript:void(0);">Exchange Form</a></dd>
                <dd><a href="javacript:void(0);">Repair Form</a></dd>
            </dl>
            <dl class="usc-menu-item">
                <dt>Account Settings</dt>
                <dd><a href="javacript:void(0);">My Account</a></dd>
                <dd><a href="javacript:void(0);">Add Money</a></dd>
                <dd><a href="javacript:void(0);">Payment History</a></dd>
                <dd><a href="javacript:void(0);">Change Password</a></dd>
            </dl>
        </div>
        <div class="usc-Rbox bgc-transparent">
            <!--用户相关-->
            <div class="usc-u-info">
                <div class="TopMsg">
                    <div class="ac-msg clearfix">
                        <a href="javascript:void(0);" class="upic">
                            <img src="http://sta.wheelsrnr.com/ms/images/usc-upic-1.png">
                        </a>

                        <div class="l col"></div>
                        <div class="l ac-msg-main">
                            <p class="u-t">User thinktem<span class="hello">Good Morning!</span></p>

                            <div class="acc-has">
                                Account Balance：<span class="num">3,000</span>&nbsp;Dollars&nbsp;&nbsp;
                                <a href="javascript:void(0);" class="recharge">[Recharge]</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="usc-cfg-box clearfix">
                    <div class="item eamil">
                        <a href="javascript:void(0);" class="txt"><i></i>Inbox</a>
                    </div>
                    <div class="item phone">
                        <a href="javascript:void(0);" class="txt"><i></i>Phone</a>
                    </div>
                    <div class="item addr">
                        <a href="javascript:void(0);" class="txt"><i></i>Address</a>
                    </div>
                </div>
            </div>
            <!--我的订单-->
            <div class="myOrder-contain">
                <h3>我的订单</h3>

                <div class="myOrder-menu-box clearfix">
                    <div class="Item active">
                        <a href="javascript:void(0);">Order All</a>
                    </div>
                    <div class="Item">
                        <a href="javascript:void(0);">Schedule an Appointment</a>
                    </div>
                    <div class="Item">
                        <a href="javascript:void(0);">Reserve Car</a>
                    </div>
                </div>
                <table border="0" cellpadding="0" cellspacing="0" class="myOrder-cTb" style="margin-top: 30px;">
                    <thead>
                    <tr align="center">
                        <td class="td-1">Car</td>
                        <td>Model</td>
                        <td>Receiver</td>
                        <td>Status</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="itm-1">
                        <td colspan="4">
                            <span class="order-time">Order Time：09/25/2015</span>
                            <span class="pay-status">Payment Status：Paid Deposit</span>
                            <span class="book-time">( Car Appointment：2015/12/30  08:30 )</span>
                        </td>
                    </tr>
                    <tr align="center" class="itm-2">
                        <td>
                            <div class="picB">
                                <img src="http://sta.wheelsrnr.com/ms/images/usc-order-pic-1.jpg">
                            </div>
                        </td>
                        <td>
                            <span class="style-1">2012 Volkswagen Polo 1.6 Comfortline</span>
                        </td>
                        <td>
                            <span class="style-1">TOM</span>
                        </td>
                        <td>
                            <p class="deposit">
                                <span class="tip">Deposit</span>
                                <font class="flag">$</font>
                                <span class="pri">
                                    <span class="num">500</span>Dollars
                                </span>
                            </p>
                            <a href="javascript:void(0);" class="myOrder-dtl-btn">Order Details</a>
                        </td>
                    </tr>

                    <tr class="itm-1">
                        <td colspan="4">
                            <span class="order-time">Order Time：09/25/2015</span>
                            <span class="pay-status">Payment Status：Paid Deposit</span>
                            <span class="book-time">( Car Appointment：2015/12/30  08:30 )</span>
                        </td>
                    </tr>
                    <tr align="center" class="itm-2">
                        <td>
                            <div class="picB">
                                <img src="http://sta.wheelsrnr.com/ms/images/usc-order-pic-1.jpg">
                            </div>
                        </td>
                        <td>
                            <span class="style-1">2012 Volkswagen Polo 1.6 Comfortline</span>
                        </td>
                        <td>
                            <span class="style-1">TOM</span>
                        </td>
                        <td>
                            <p class="deposit">
                                <span class="tip">Deposit</span>
                                <font class="flag">$</font>
                                <span class="pri">
                                    <span class="num">500</span>Dollars
                                </span>
                            </p>
                            <a href="javascript:void(0);" class="myOrder-dtl-btn">Order Details</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!--订单详情弹出框-->
<div class="myOrder-dtl-mask">
    <div class="close-btn"></div>
    <div class="Top">
        <ul class="t-menu clearfix">
            <li>
                <span class="txt-b">Favorite Cars</span>
                <span class="split"></span>
            </li>
            <li>
                <span class="txt-b active">Deposit</span>
                <span class="split"></span>
            </li>
            <li>
                <span class="txt-b">Car Appointment</span>
                <span class="split"></span>
            </li>
            <li>
                <span class="txt-b">Confirm Order</span>
            </li>
        </ul>
    </div>
    <!--支付订金-->
    <div class="Order-dtl-mask-main">
        <div class="pay-item">
            <p class="status-b">
                Order Status： <span class="has">You have paid the $500 deposit and can now make an appointment with us to view your car.</span>
            </p>
            <p class="warm-tip">
                If you have not been contacted after 24 hours after you pay the deposit, you may directly contact us.<a href="javascript:void(0);" class="feedback">Feedback</a>
            </p>
        </div>
        <div class="x-info">
            <h3>Detailed Information</h3>
            <div class="mc-info">
                <div class="i-item">
                    <span class="tip">Appointment Location：</span>
                    <span class="t-c">—</span>
                </div>
                <div class="i-item">
                    <span class="tip">Delivery Staff：</span>
                    <span class="t-c">—</span>
                </div>
                <div class="i-item">
                    <span class="tip">Appointment Time：</span>
                    <span class="t-c">—</span>
                </div>
            </div>
        </div>
    </div>

</div>

<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
