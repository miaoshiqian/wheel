<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Login Page</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css" />

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/fun.js"></script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="login-mbox">
    <div class="mid-block">
        <div class="l login-picL">
            <img src="http://sta.wheelsrnr.com/ms/images/login-pic-1.png"/>
        </div>
        <div class="r login-frb">
            <div class="logo"><img src="http://sta.wheelsrnr.com/ms/images/logo.png"/></div>
            <div class="login-fr-inB radius-5">
                <div class="tpB">
                    <div class="login-fr-in">
                        <form action="#" method="post">
                            <div class="top">
                                <b class="t">Login</b>
                                <span class="txt">Login</span>
                            </div>
                            <div class="item">
                                <input id="js_username" type="text" placeholder="Username/Phone Number" class="radius-5 login-inp"/>
                            </div>
                            <div class="item">
                                <input id="js_password" type="password" placeholder="Password" class="radius-5 login-inp"/>
                            </div>
                            <div class="item">
                                <input id="js_submit" type="button" value="Login" class="radius-5 login-sub"/>
                            </div>
                            <div class="item clearfix">
                                <a href="javascript:void(0);" class="fw-s-1">Forgot your Password？</a>
                        <span class="r login-qes">
                        Questions?<a href="javascript:void(0);" class="online-kf">Chat with Us</a>
                        </span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="btm-B">
                    <div class="login-spl"></div>
                    <a href="/user/register/" class="radius-5 lreg-btn">Register</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#js_submit').click(function(){
            var username = $('#js_username').val();
            var password = $('#js_password').val();
            if (!username || !password) {
                alert('Please enter username or password');
                return false;
            }
            var $this = $(this);
            $this.val('Signing in...');
            $this.attr('disabled', true);
            $.ajax({
                url : '/user/ajaxLogin/',
                type : 'post',
                data : {username : username, password : password},
                dataType : 'json',
                success:function(ret){
                    if (ret.code == 0) {
                        location.href = '/user/order/';
                    } else if (ret.message) {
                        alert(ret.message);
                    }
                    $this.removeAttr('disabled');
                    $this.val('Login');
                },
                error:function(ret){
                    $this.removeAttr('disabled');
                    $this.val('Login');
                }
            });
        });
    });
</script>
</body>
</html>
