<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Pay（1 Confirm Car Details）New Bank Payment</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css"/>
    <!--<link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/focus-slide.css"/>-->
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/input_hide.js"></script>
    <!-- <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/pay-fun.js"></script>-->
    <!--<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.SuperSlide.2.1.2.js"></script>-->
    <script type="text/javascript">
        $(function () {
            $(".pay-s-tpls .pt-rd").click(function () {
                var index=$(".pay-s-tpls .pt-rd").index(this);
                $(".pay-s-tpls .pt-rd").removeClass("active");
                $(this).toggleClass("active");
                $(".pay-s-tpm").eq(index).show().siblings(".pay-s-tpm").hide();
            })
            $(".pay-sbk-ls li .pay-bk-rd").click(function () {
                $(".pay-sbk-ls li .pay-bk-rd").removeClass("active");
                $(this).toggleClass("active");
            })
        })
    </script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="payTop">
    <div class="mid-block  clearfix">
        <div class="payTopR">
            <span>Hello，<?php echo $this->userInfo['real_name'];?></span>
            <span class="spl">|</span>
            <a href="javascript:void(0);" target="_blank">My Cart</a>
            <span class="spl">|</span>
            <a href="javascript:void(0);" target="_blank">Customer Service</a>
            <span class="spl">|</span>
            <a href="javascript:void(0);" target="_blank">Quit</a>
        </div>
    </div>
</div>

<div class="mid-block">
<div class="pay-s-m1 clearfix">
    <div class="pay-s-m1i clearfix">
        <div class="Lb">
            <div class="pic">
                <img src="<?php echo $this->cover;?>">
            </div>
            <div class="pdes">
                <p><?php echo $this->carInfo['title'];?></p>

                <p>Price：$<?php echo $this->carInfo['seller_price'];?></p>

                <p>VIN:<?php echo $this->carInfo['vin_code'];?></p>

                <p>Miles：<?php echo $this->carInfo['miles'];?></p>
            </div>
        </div>
        <div class="osp"></div>
    </div>
    <div class="pay-s-m1i clearfix">
        <div class="Rb">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/pay-upic.jpg">
            </div>
            <div class="pdes">
                <p>Name：mii</p>

                <p>Address：apt 1212 401E 32nd St</p>

                <p>Phone Number：3125963596</p>

                <p>Email：milljif@gmail.com</p>
            </div>
        </div>
    </div>
</div>
<div class="pay-s-info">

<div class="priB">
    <span class="tip">Down Payment：</span>
    <span class="num">0</span>
    <span class="unit">元</span>
</div>

<div class="pay-s-typeB">
    <ul class="pay-s-tpls clearfix">
        <li class="clearfix">
                    <span class="l pt-rd">
                        <input type="radio" name="pay_type"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/pay-type-1.png">
            </div>
            <div class="spl"></div>
        </li>
        <li class="clearfix">
                    <span class="l pt-rd">
                        <input type="radio" name="pay_type"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/pay-type-2.png">
            </div>
            <div class="spl"></div>
        </li>
        <li class="clearfix">
                    <span class="l pt-rd active">
                        <input type="radio" name="pay_type" checked/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/pay-type-3.png">
            </div>
        </li>
    </ul>
</div>
<!--支付宝-->
<div class="pay-s-tpm">
    <div class="Top clearfix">
        <a href="javascript:void(0);"><span>Bank Card</span></a>
        <a href="javascript:void(0);"><span>Credit Card</span></a>
    </div>
    <div class="zfb-pay">
        <form action="" method="post">
            <p class="warm">You are in a safe environment, please use at ease!</p>
            <div class="pwd-box">
                <p class="tip-1">Password：</p>
                <div class="mItem clearfix">
                    <input type="password" name="zfb_pwd" class="l zfb-pwd-inp"/>
                    <a href="javascript:void(0);" class="l fpwd">Forgot Password?</a>
                </div>
                <p class="tip-2">Please enter a 6 character password</p>
            </div>
            <div class="yes-box">
                <input type="submit" value="确认付款" class="zfb-yes"/>
            </div>
        </form>
    </div>
</div>
<!--微信支付-->
<div class="pay-s-tpm">
</div>
<!--银行卡-->
<div class="pay-s-tpm" style="display: block;">
    <div class="Top clearfix">
        <a href="javascript:void(0);"><span>Bank Card</span></a>
        <a href="javascript:void(0);"><span>Credit Card</span></a>
    </div>
    <ul class="pay-sbk-ls clearfix">
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-1.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-2.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-3.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-4.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-5.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-6.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-7.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-8.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-9.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-10.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-11.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-12.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-13.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-14.png">
            </div>
        </li>
        <li class="clearfix">
                    <span class="l pay-bk-rd">
                        <input type="radio" name="pay_bank"/>
                    </span>

            <div class="l pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bank-ico-15.png">
            </div>
        </li>
    </ul>
</div>

</div>
<div class="pay-s-checkout">
    <div class="title">
        <a href="javascript:void(0);" class="back" id="js-back">Back First</a>
        <input type="hidden" name="js-step-val" value="1">

        <div class="pay-scar-step clearfix">
            <div class="step-itm active">
                <div class="t-num">1</div>
                <p class="txt">Confirm Car Details</p>

                <div class="spl-b"></div>
            </div>
            <div class="step-itm">
                <div class="t-num">2</div>
                <p class="txt">Fill Out Details</p>

                <div class="spl-b"></div>
            </div>
            <div class="step-itm">
                <div class="t-num">3</div>
                <p class="txt">Pay Deposit</p>
            </div>
        </div>
    </div>

    <!--确认车辆信息-->
    <div class="ps-cko-m s-1 clearfix" style="display: block;">
        <div class="Lb">
            <a href="javascript:void(0);" class="pic">
                <img src="<?php echo $this->bigCover;?>">
            </a>

            <div class="btm-txt">
                <p><?php echo $this->carInfo['title'];?></p>

                <p>Miles：<?php echo $this->carInfo['miles'];?></p>
            </div>
        </div>
        <div class="Rb">
            <div class="T-total">
                <span class="tip">Total（Pre-Tax）：</span>
                <span class="t-c">$<?php echo $this->carInfo['seller_price'];?></span>
            </div>
            <div class="fee clearfix">
                <div class="item">
                    <span class="tip">Documentation Fees：</span>
                    <span class="t-c">$20</span>
                    <span class="spl"></span>
                </div>
                <div class="item">
                    <span class="tip">Transfer Fee：</span>
                    <span class="t-c">$160</span>
                </div>
            </div>
            <a href="javascript:void(0);" class="info-yes">Confirm Details</a>
            <ul class="tip-ls clearfix">
                <li><span>203 Vehicle Quality</span></li>
                <li><span>Ten Day Test Drive</span></li>
                <li><span>100 Day Warranty</span></li>
                <li><span>DVM Transfer</span></li>
                <li><span>One Free Inspection</span></li>
                <li><span>One Free Gas Fill</span></li>
                <li><span>One Free Car Wash and Waxing</span></li>
                <li><span>Delivered to Your Door</span></li>
            </ul>
        </div>
    </div>

    <!--填写配件信息-->
    <div class="ps-cko-m s-2 clearfix" style="display: none;">
        <div class="ps-cko-m2L clearfix">
            <div class="pic">
                <img src="<?php echo $this->cover;?>">
            </div>
            <div class="pdes">
                <p><?php echo $this->carInfo['title'];?></p>

                <p>Price：$<?php echo $this->carInfo['seller_price'];?></p>

                <p>VIN:<?php echo $this->carInfo['vin_code'];?></p>

                <p>Miles：<?php echo $this->carInfo['miles'];?></p>
            </div>
        </div>
        <div class="ps-cko-m2R clearfix">
            <!--<form action="" method="post">-->
            <div class="o-item name clearfix">
                <span class="l tip">Name：</span>
                <input type="text" class="ps-inp-1 radius-5" name="real_name"/>
            </div>
            <div class="o-item addr clearfix">
                <span class="l tip">Email：</span>
                <input type="text" class="ps-inp-1 radius-5" name="email"/>
            </div>
            <div class="o-item email clearfix">
                <span class="l tip">Phone：</span>
                <input type="text" class="ps-inp-1 radius-5" name="phone"/>
            </div>
            <div class="o-item phone clearfix">
                <span class="l tip">Phone2：</span>
                <input type="text" class="ps-inp-1 radius-5" name="phone2"/>
            </div>
            <div class="o-item city clearfix">
                <span class="l tip">Address：</span>
                <input type="text" class="ps-inp-1 radius-5" value="Chicago" name="city"/>
            </div>
            <div class="o-item clearfix">
                <dl class="l select radius-5">
                    <!--title用于存储选值-->
                    <dt title="value">1L</dt>
                    <div class="option">
                        <dd value="1">1L</dd>
                        <dd value="2">2L</dd>
                        <dd value="3">3L</dd>
                    </div>
                </dl>
            </div>
            <div class="o-item cd clearfix">
                <span class="l tip">Zip：</span>
                <input type="text" class="ps-inp-1 radius-5" value="60616"/>
            </div>
            <div class="clear"></div>
            <div class="sub-box">
                <input type="button" value="Next" class="radius-5 submit-btn"/>
            </div>
            <!--</form>-->
        </div>
    </div>
    
    <!--支付完成-->
    <div class="ps-cko-m s-3 clearfix" style="display: none;">
    </div>

</div>
</div>

<script type="text/javascript">
    $(".ps-cko-m.s-1 .Rb .info-yes").click(function(){
        $(".pay-scar-step .step-itm").eq(1).addClass("active").siblings(".step-itm").removeClass("active");
        $(".ps-cko-m").eq(1).show().siblings(".ps-cko-m").hide();
    });
    /*$(".ps-cko-m2R .submit-btn").click(function(){
        $(".pay-scar-step .step-itm").eq(2).addClass("active").siblings(".step-itm").removeClass("active");
        $(".ps-cko-m").eq(2).show().siblings(".ps-cko-m").hide();
    });*/  //暂时不用第三步。点击第二部的按钮直接下单
    //反回按钮
    $("#js-back").click(function(){
        $(".pay-scar-step .step-itm").eq(0).addClass("active").siblings(".step-itm").removeClass("active");
        $(".ps-cko-m").eq(0).show().siblings(".ps-cko-m").hide();
    })
</script>

<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
