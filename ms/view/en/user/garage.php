<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Garage</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/focus-slide.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.SuperSlide.2.1.2.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/bxq-fun.js"></script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo.png" class="my_logo" /></div>
<div class="process-T">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>

    </div>
</div>

<div>
    <div class="slide-item" style="display:block"><img src="http://sta.wheelsrnr.com/ms/images/cart-picFocus-1.jpg" width="100%" id="myslid"></div>
    <div class="thumb1">
        <img src="images/car-focus-litter-1.jpg" onClick="myslide('http://sta.wheelsrnr.com/ms/images/cart-picFocus-1.jpg',1)" class="mysl1 curr">
        <img src="images/car-focus-litter-2.jpg" onClick="myslide('http://sta.wheelsrnr.com/ms/images/cart-picFocus-1.jpg',2)" class="mysl2">
        <img src="images/car-focus-litter-3.jpg" onClick="myslide('http://sta.wheelsrnr.com/ms/images/cart-picFocus-1.jpg',3)" class="mysl3">
        <img src="images/car-focus-litter-4.jpg" onClick="myslide('http://sta.wheelsrnr.com/ms/images/cart-picFocus-1.jpg',4)" class="mysl4">
        <img src="images/car-focus-litter-5.jpg" onClick="myslide('http://sta.wheelsrnr.com/ms/images/cart-picFocus-1.jpg',5)" class="mysl5">
        <img src="images/car-focus-litter-6.jpg" onClick="myslide('http://sta.wheelsrnr.com/ms/images/cart-picFocus-1.jpg',6)" class="mysl6">
        <img src="images/car-focus-litter-7.jpg" onClick="myslide('http://sta.wheelsrnr.com/ms/images/cart-picFocus-1.jpg',7)" class="mysl7">
        <img src="images/car-focus-litter-1.jpg" onClick="myslide('http://sta.wheelsrnr.com/ms/images/cart-picFocus-1.jpg',8)" class="mysl8">
        <img src="images/car-focus-litter-2.jpg" onClick="myslide('http://sta.wheelsrnr.com/ms/images/cart-picFocus-1.jpg',9)" class="mysl9">
        <img src="images/car-focus-litter-3.jpg" onClick="myslide('http://sta.wheelsrnr.com/ms/images/cart-picFocus-1.jpg',10)" class="mysl10">
        <div style="clear:both"></div>
    </div>
</div>
<script>
    function myslide(a,b){
        $("#myslid").animate({opacity: '0'},0);
        $('#myslid').attr('src',a);
        setTimeout("$('#myslid').animate({opacity: '1'},1000)",100);
        $(".thumb1 img").removeClass('curr');
        xx = ".mysl"+b;
        $(xx).addClass('curr');
        /*$("#myslid").animate({opacity: '1'},500);
         $("#myslid").attr('src',a);
         $("#myslid").animate({opacity: '0'},500);*/

    }
</script>

<div class="park-collect">
    <div class="mid-block">
        <div class="Tbox">
            <p class='t-1'><span>My Garage</span><i></i></p>
            <p class='t-2'>MY COLLECTION</p>
        </div>
        <div class="picScroll-left">
            <div class="bd">
                <ul class="picList">
                    <li>
                        <div class="pic"><a href="javascript:void(0);" target="_blank">
                                <img src="http://sta.wheelsrnr.com/ms/images/parking-tpic-1.jpg"/>

                                <div class="title">2012 Volkswagen Golf 1.6 Automatic Comfort</div>
                            </a></div>
                    </li>
                    <li>
                        <div class="pic"><a href="javascript:void(0);" target="_blank">
                                <img src="http://sta.wheelsrnr.com/ms/images/parking-tpic-2.jpg"/>

                                <div class="title">2012 Volkswagen Golf 1.6 Automatic Comfort</div>
                            </a></div>
                    </li>
                    <li>
                        <div class="pic"><a href="javascript:void(0);" target="_blank">
                                <img src="http://sta.wheelsrnr.com/ms/images/parking-tpic-3.jpg"/>

                                <div class="title">2012 Volkswagen Golf 1.6 Automatic Comfort</div>
                            </a></div>
                    </li>
                    <li>
                        <div class="pic"><a href="javascript:void(0);" target="_blank">
                                <img src="http://sta.wheelsrnr.com/ms/images/parking-tpic-1.jpg"/>

                                <div class="title">2012 Volkswagen Golf 1.6 Automatic Comfort</div>
                            </a></div>
                    </li>
                    <li>
                        <div class="pic"><a href="javascript:void(0);" target="_blank">
                                <img src="http://sta.wheelsrnr.com/ms/images/parking-tpic-2.jpg"/>

                                <div class="title">2012 Volkswagen Golf 1.6 Automatic Comfort</div>
                            </a></div>
                    </li>
                    <li>
                        <div class="pic"><a href="javascript:void(0);" target="_blank">
                                <img src="http://sta.wheelsrnr.com/ms/images/parking-tpic-3.jpg"/>

                                <div class="title">2012 Volkswagen Golf 1.6 Automatic Comfort</div>
                            </a></div>
                    </li>
                    <li>
                        <div class="pic"><a href="javascript:void(0);" target="_blank">
                                <img src="http://sta.wheelsrnr.com/ms/images/parking-tpic-1.jpg"/>

                                <div class="title">2012 Volkswagen Golf 1.6 Automatic Comfort</div>
                            </a></div>
                    </li>
                    <li>
                        <div class="pic"><a href="javascript:void(0);" target="_blank">
                                <img src="http://sta.wheelsrnr.com/ms/images/parking-tpic-2.jpg"/>

                                <div class="title">2012 Volkswagen Golf 1.6 Automatic Comfort</div>
                            </a></div>
                    </li>
                    <li>
                        <div class="pic"><a href="javascript:void(0);" target="_blank">
                                <img src="http://sta.wheelsrnr.com/ms/images/parking-tpic-3.jpg"/>

                                <div class="title">2012 Volkswagen Golf 1.6 Automatic Comfort</div>
                            </a></div>
                    </li>
                </ul>
            </div>
            <div class="hd">
                <ul></ul>
            </div>
        </div>
        <script type="text/javascript">
            $(".picScroll-left").slide({
                titCell: ".hd ul",
                mainCell: ".bd ul",
                autoPage: true,
                effect: "left",
                autoPlay: true,
                scroll: 3,//滚动个数
                vis: 3,//可视个数
                trigger: "click",
            });
        </script>
    </div>
</div>

<div class="bgc-f9f9f9">

<div class="mid-block">

<div class="bxq-ci-mn">
    <ul class="bxq-ci-list clearfix">
        <li class="active">
            <a href="javascript:void(0);">Configure Highlights</a>
        </li>
        <li>
            <a href="javascript:void(0);">Vehicle File</a>
        </li>
        <li>
            <a href="javascript:void(0);">Inspection Report</a>
        </li>
        <li>
            <a href="javascript:void(0);">Vehicle Parameters</a>
        </li>
    </ul>
</div>


<!--亮点配置-->
<div class="bxq-ci-itm bxq-ldpz" id="bxq-cim-0">
    <div class="Top">
        <h3>Configure Highlights</h3>
    </div>
    <div class="in">
        <ul class="bxq-ld-in-ls clearfix">
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-0"></i>

                    <p class="btm-tt">Electronic Stability (ESP)</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-1"></i>

                    <p class="btm-tt">External Sources</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-2"></i>

                    <p class="btm-tt">Power Windows</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-3"></i>

                    <p class="btm-tt">Rear Seat Air Conditioning</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-4"></i>

                    <p class="btm-tt">Side-Impact Airbags</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-5"></i>

                    <p class="btm-tt">Power Mirrors</p>

                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-6"></i>

                    <p class="btm-tt">DVD</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-7"></i>

                    <p class="btm-tt">GPS Navigation</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-8"></i>

                    <p class="btm-tt">Leater Seat</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-9"></i>

                    <p class="btm-tt">Color Console</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-10"></i>

                    <p class="btm-tt">Cruise Control</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-11"></i>

                    <p class="btm-tt">Automatic Air Conditioning</p>

                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-12"></i>

                    <p class="btm-tt">Automatic Heating</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-13"></i>

                    <p class="btm-tt">Bluetooth Phone</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-14"></i>

                    <p class="btm-tt">Push-Button Start</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-15"></i>

                    <p class="btm-tt">Reversing Radar</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-16"></i>

                    <p class="btm-tt">Rear View Camera</p>

                    <div class="spl"></div>
                </div>
            </li>
            <li>
                <div class="bxq-ldpz-lsin">
                    <i class="i-17"></i>

                    <p class="btm-tt">Headlights</p>

                </div>
            </li>
        </ul>
    </div>
</div>
<!--end-->
<!--车辆档案-->
<div class="bxq-ci-itm bxq-carfile" id="bxq-cim-1">
    <div class="Top">
        <h3>Vehicle File</h3>
    </div>
    <div class="bxq-cf-info">
        <div class="base-b">
            <span class="bxq-flag-s1">Basic Information</span>

            <div class="mbox">
                [Insurance Expiration Date]：<span class="n">2016-10-01</span>；&nbsp;
                [Number of Transfers]：<span class="n">2</span>；&nbsp;
                [Vehicle Inspection Date]：<span class="n">2016-10-01</span>；&nbsp;
                [Number of Keys]：<span class="n">2</span>；
            </div>
        </div>
        <div class="import-b">
            <span class="bxq-flag-s1">Important Configuration</span>

            <div class="mbox">
                <span class="bxq-cfg-tag i-0">Standard</span>
                <span class="bxq-cfg-tag i-1">Plus</span>
                <span class="bxq-cfg-tag i-2">None</span>
                <span class="bxq-cfg-tag i-3">Normal</span>
                <span class="bxq-cfg-tag i-4">Abnormal</span>
            </div>
            <div class="bxq-cfg-mls clearfix">
                <ul class="l bxq-cfg-list">
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Push-Button Start</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Front and Rear Window Lift</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Electric Folding Mirrors</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Electronically Adjustable Front Seats</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Headlights</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Airbags</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Headlight Washer</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Anti-lock Braking System (ABS)</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                </ul>
                <ul class="l bxq-cfg-list">
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Push-button Start</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Front and Rear Window Lift</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Electric Folding Mirrors</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Electronically Adjustable Front Seats</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Headlights</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Airbags</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Headlight Washer</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Anti-lock Braking System (ABS)</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--end-->
<!--检测报告-->
<div class="bxq-ci-itm" id="bxq-cim-2">
<div class="Top">
    <h3>Inspection Report</h3>
</div>
<div class="bxq-ts-report">
<ul class="bxq-tsr-mn clearfix">
    <li class="active"><a href="javascript:void(0);">Accident Investigation</a></li>
    <li><a href="javascript:void(0);">Car Fire</a></li>
    <li><a href="javascript:void(0);">Sinking Car</a></li>
    <li><a href="javascript:void(0);">Safety Test</a></li>
    <li><a href="javascript:void(0);">External Test</a></li>
    <li><a href="javascript:void(0);">Internal Test</a></li>
    <li><a href="javascript:void(0);">Engine Compartment Test</a></li>
</ul>
<!--事故排查-->
<div class="bxq-tsr-mbox acc" style="display: block;">
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-1.jpg">
    <br/>

    <p>Chassis: No Collision Accident</p><br/>
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-2.jpg">
    <br/>

    <p>Front: No Collision Accident</p><br/>
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-3.jpg">
    <br/>

    <p>Left Body: No Collision Accident</p><br/>
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-4.jpg">
    <br/>

    <p>Right Body: No Collision Accident</p><br/>
</div>
<!--end-->
<!--判别火烧车-->
<div class="bxq-tsr-mbox fire">
    <h3 class="bxq-tsr-mh-1">How do you know if a car has been in a fire accident?</h3>

    <p class="bxq-tsr-tdes-1"><span>A car is classified as a "Burnt Car" when it meets any of the following:</span></p>

    <div class="bxq-tsr-qes clearfix">
        <div class="cL">
            <p class="p-1">1. Engine Harness, Body Harness, and replacement parts have traces of fire burn marks.</p>

            <p class="p-2">3. Car compartments have traces of fire burn marks.</p>
        </div>
        <div class="cR">
            <p class="p-1">2. Engine Harness, Body Harness, and replacements parts have traces of fire burn marks.</p>

            <p class="p-2">4. Engine compartment, car interior, or trunk have traces of fire burn marks</p>
        </div>
    </div>
    <div class="bxq-tsr-qshow">
        <div class="title">
            Fire Accident Detection<span class="tip">Passed (No Burn Marks)</span>
        </div>
        <ul class="qlist clearfix">
            <li>
                <span class="t">Seat Belt/Carpet Bed Plate Inspection:</span>
                <span class="c i-3">No Slit No Rust No Dampness</span>
            </li>
            <li>
                <span class="t">Engine/Tank Inspection:</span>
                <span class="c i-3">No Silt No Rust</span>
            </li>
        </ul>
    </div>
</div>
<!--end-->
<!--判别水泡车-->
<div class="bxq-tsr-mbox water">
    <h3 class="bxq-tsr-mh-1">How do you detect a flood-damaged car?</h3>

    <p class="bxq-tsr-tdes-1"><span>A car is flood-damaged if it meets two or more of the following criteria:</span></p>

    <div class="bxq-tsr-qes clearfix">
        <div class="cL">
            <p class="p-1">1. The metal stents and slides under the bottom of the seat have unnatural rust.</p>

            <p class="p-2">3.The car floor has cotton blankets for noise and heat insulation, but the carpets have sediment or traces of water logging. </p>

            <p class="p-3">5.Seats have sediment or traces of water logging.</p>
        </div>
        <div class="cR">
            <p class="p-1">2.Seat belts have water stains or mildew.</p>

            <p class="p-2">4.The brake and screws in the suspension parts have unnatural corrosion.</p>

            <p class="p-3">6.The cockpick interior or carpet are disassembled or destroyed, plus stains and traces of waterlogging.</p>
        </div>
    </div>
    <div class="bxq-tsr-qshow">
        <div class="title">
            Fire Investigation and Detection<span class="tip">Passed (No Burn Marks)</span>
        </div>
        <ul class="qlist clearfix">
            <li>
                <span class="t">Seat belt/Carpet Inspection:</span>
                <span class="c i-3">No Silt No Rust No Dampness</span>
            </li>
            <li>
                <span class="t">Engine/Tank Inspection:</span>
                <span class="c i-3">No Silt No Rust</span>
            </li>
            <li>
                <span class="t">Engine Wiring Harness and Rubber Products/Seat Springs :</span>
                <span class="c i-3">No Silt No Odor</span>
            </li>
        </ul>
    </div>
</div>
<!--end-->
<!--安全性检测-->
<div class="bxq-tsr-mbox safe">
    <div class="bxq-ts-sbox clearfix">
        <div class="item">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-sf1.png">
            </div>
            <div class="bxq-tsf-ibtm">
                <p class="t">Anti-Freeze<span class="r qr">has two questions</span></p>

                <p class="t-t">Anti-Freeze point at 25°C~35°C is safe</p>

                <p class="warm"><span class="txt">-35°C</span></p>
            </div>
        </div>
        <div class="item">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-sf2.png">
            </div>
        </div>
        <div class="item">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-sf3.png">
            </div>
        </div>
    </div>
</div>
<!--end-->
<!--外观检测-->
<div class="bxq-tsr-mbox appear">
    <div class="mpic"></div>
</div>
<!--end-->
<!--内饰检测-->
<div class="bxq-tsr-mbox deco">
    <ul class="bxq-tsde-ls clearfix">
        <li>
            <div class="inn"><span class="txt">Dash Board<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Seat Belt<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Dome Light<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Headlight Switch<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Central Locking<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Roof Handles<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Turn Signals<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Rearview Mirro<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Speakers<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Wiper<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Door Panel<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">A-Pillars<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Air Conditioner<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Door Light<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">B-Pillars<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Audio/Video Systems and Navigation<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Dormer<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">C-Pillars<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Front Seats<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Window<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Airbags<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Rear Seats<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Armrests<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Door Seals<i class="i-3"></i></span></div>
        </li>
        <li></li>
        <li>
            <div class="inn"><span class="txt">Visor<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Trunk Seal<i class="i-3"></i></span></div>
        </li>
    </ul>
</div>
<!--end-->
<!--发动机舱检测-->
<div class="bxq-tsr-mbox motor">
    <div class="tpic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-pic-2.jpg">
    </div>
    <p class="ibtm">Certified that Engine Compartment has No Flaws</p>
</div>
</div>
</div>
<!--end-->
<!--车辆参数-->
<div class="bxq-ci-itm" id="bxq-cim-3">
    <div class="Top">
        <h3>Vhicle Parameters</h3>
    </div>
    <div class="bxq-car-param">
        <table border="0" cellpadding="0" cellspacing="0" class="bxq-cpar-tb">
            <thead>
            <tr>
                <td colspan="2">
                    <span class="bxq-flag-s1">Basic Parameters</span>
                </td>
                <td colspan="2">
                    <span class="bxq-flag-s1">Engine Parameters</span>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="td-1"><span class="tip">Model</span></td>
                <td>2013 Volkswagen Golf 2.9TSI Luxury Model</td>
                <td><span class="tip">Hundred Kilometers(L)</span></td>
                <td>-</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Color</span></td>
                <td>Black</td>
                <td><span class="tip">Displacement(L)</span></td>
                <td>2.0</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Gearbox</span></td>
                <td>6-Speed Dual-Clutch</td>
                <td><span class="tip">Fuel Tank Capacity(L)</span></td>
                <td>-</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Gearbox</span></td>
                <td>6 Gear</td>
                <td><span class="tip">Intake Form</span></td>
                <td>Turbo</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Number of Doors</span></td>
                <td>4</td>
                <td><span class="tip">Max. Torque(N.m)</span></td>
                <td>280</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Number of Seats</span></td>
                <td>5</td>
                <td><span class="tip">Fuel</span></td>
                <td>Gasoline</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">L*W*H(mm)</span></td>
                <td>4865*1820*1475</td>
                <td><span class="tip">Fuel Grade</span></td>
                <td>97(Beijing No.95)</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Trunk Volume(L)</span></td>
                <td>-</td>
                <td><span class="tip">Emission Standards</span></td>
                <td>State IV (State V)</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Wheel Hub Material</span></td>
                <td>Aluminum</td>
                <td><span class="tip">Power(kw)</span></td>
                <td>147</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Wheelbase(mm)</span></td>
                <td>2812</td>
                <td><span class="tip">Maximum Speed(km/h)</span></td>
                <td>230</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Curb Weight(kg)</span></td>
                <td>-</td>
                <td><span class="tip">Official 0-100km/h Acceleration(s)</span></td>
                <td>-</td>
            </tr>
            </tbody>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" class="bxq-cpar-tb">
            <thead>
            <tr>
                <td colspan="2">
                    <span class="bxq-flag-s1">Chassis Parameters</span>
                </td>
                <td colspan="2">
                    <span class="bxq-flag-s1">Wheel Brake</span>
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="td-1"><span class="tip">Drive Mode</span></td>
                <td>FF</td>
                <td><span class="tip">Front Brake</span></td>
                <td>Ventilated Disk Brakes</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Steering System</span></td>
                <td>Electric Power</td>
                <td><span class="tip">Rear Brake</span></td>
                <td>Disc</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Front Suspension</span></td>
                <td>MacPherson Strut</td>
                <td><span class="tip">Front Tire Specs(mm)</span></td>
                <td>15/55 R16</td>
            </tr>
            <tr>
                <td class="td-1"><span class="tip">Rear Suspension</span></td>
                <td>Multi-link Suspension</td>
                <td><span class="tip">Rear Tire Size(mm)</span></td>
                <td>215/55 R16</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<!--end-->

</div>

</div>

<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
