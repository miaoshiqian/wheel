<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Register Page</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css" />

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/fun.js"></script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="reg-tbox clearfix">
    <div class="mid-block">
        <div class="l L-1">
            <img src="http://sta.wheelsrnr.com/ms/images/logo-g.png"/>
        </div>
        <div class="l L-2">
            User Registration
        </div>
    </div>
</div>

<div class="reg-mbox">
    <div class="mid-block">
        <!--左-->
        <div class="reg-mL">
            <form action="#" method="post">
                <div class="item">
                    <span class="itip">Last Name&nbsp;&nbsp;&nbsp;&nbsp;First Name</span>
                    <div class="l reg-L-ct">
                        <input id="js_name" type="text" placeholder="Please enter your name" class="reg-inp-s1"/>
                    </div>
                    <div class="l reg-l-tip">
                        <span class="tp">Please Enter Your Name</span>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="item">
                    <span class="itip">Cell Phone Number</span>
                    <div class="l reg-L-ct">
                        <input id="js_mobile" type="text" placeholder="Please enter phone number" class="reg-inp-s1"/>
                        <!--<div class="btm">
                            没有手机？<a href="javascript:void(0);" class="yx-c">点击这里用邮箱注册</a>
                        </div>-->
                    </div>
                    <div class="l reg-l-tip">
                        <span class="tp">Please Enter Phone Number</span>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="item">
                    <span class="itip">Username</span>
                    <div class="l reg-L-ct">
                        <input id="js_username" type="text" placeholder="Please enter username" class="reg-inp-s1"/>
                    </div>
                    <div class="l reg-l-tip">
                        <span class="tp">Please Enter Username</span>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="item">
                    <span class="itip">Email</span>
                    <div class="l reg-L-ct">
                        <input id="js_mail" type="text" placeholder="Please enter email" class="reg-inp-s1"/>
                    </div>
                    <div class="l reg-l-tip">
                        <span class="tp">Please Enter Email Address</span>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="item">
                    <span class="itip">Change Password</span>
                    <div class="l reg-L-ct">
                        <input id="js_password" type="password" class="reg-inp-s1"/>
                    </div>
                    <div class="l reg-l-tip s2">
                        <span class="tp">Please enter 6-16 characters, case sensitive</span>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="item">
                    <span class="itip">Confirm Password</span>
                    <div class="l reg-L-ct">
                        <input id="js_repassword" type="password" class="reg-inp-s1"/>
                    </div>
                    <div class="l reg-l-tip s2">
                        <span class="tp">Please enter 6-16 characters, case sensitive</span>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="item">
                    <span class="itip">Code</span>
                    <div class="l reg-L-ct clearfix">
                        <input id="js_code_value" value="" type="text" class="l reg-code-is1"/>
                        <img src="/ajax/showCode/" class="l reg-cd-pic js_code_img"/>
                        <a href="javascript:void(0);" class="l reg-cd-c js_change_code">Change Code</a>
                    </div>
                    <div class="l reg-l-tip s2">
                        <span class="tp">Please Enter the Code</span>
                    </div>
                    <div class="clear"></div>
                </div>
                <!--<div class="item">
                    <span class="itip">短信验证</span>
                    <div class="l reg-L-ct clearfix">
                        <input type="text" class="l reg-msg-ck js_input_code"/>
                        <a id="js_btn_code" href="javascript:void(0);" class="r reg-msg-get">免费获取短信验证码</a>
                    </div>
                    <div class="clear"></div>
                </div>-->
                <div class="item">
                    <div class="T">
                        <span class="itip"></span>
                        <input id="js_submit" type="button" value="Register Now" class="reg-submit"/>
                        <div class="clear"></div>
                    </div>
                    <!--<p class="has-btm-tip clearfix">
                        <input type="checkbox" name="agree"/>
                        <span class="txt">我已阅读并接受<a href="javascript:void(0);">wheelsrnr注册条款</a></span>
                    </p>-->
                </div>
            </form>
        </div>
        <!--右-->
        <div class="reg-mR">
            <div class="reg-mR-in">
                <div class="tBox">
                    <a href="javascript:void(0);">Account Already Exists</a>
                </div>
                <p class="t-t">
                    Connect through a third party————
                </p>
                <div class="reg-oth-box">
                    <div class="itmB">
                        <a href="javascript:void(0);" class="i-0"></a>
                    </div>
                    <div class="itmB">
                        <a href="javascript:void(0);" class="i-1"></a>
                    </div>
                    <div class="itmB">
                        <a href="javascript:void(0);" class="i-2"></a>
                    </div>
                    <div class="itmB">
                        <a href="javascript:void(0);" class="i-3"></a>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
<script type="text/javascript">

    $(function(){
        $('.js_province dd').click(function(){
            var $this = $(this);
            if ($this.html() == $('.js_province dt').html()) {
                return false;
            }
            var provinceId = $this.attr('value');
            if (!provinceId) {
                return false;
            }
            $.post('/ajax/city/', {province_id:provinceId}, function(ret){
                if (ret.code == 0) {
                    $('.js_city .option').html('');
                    for (var index in ret.data) {
                        $('.js_city dt').html('City').attr('title', '');
                        $('.js_city .option').append('<dd value="' + ret.data[index]['id'] + '">' + ret.data[index]['short_name'] + '</dd>');
                    }
                }
            });
        });
        $('.js_city').delegate('dd', 'click', function(){
            var $this = $(this);
            if ($this.html() == $('.js_city dt').html()) {
                return false;
            }
            var cityId = $this.attr('value');
            if (!cityId) {
                return false;
            }
            $.post('/ajax/district/', {city_id:cityId}, function(ret){
                if (ret.code == 0) {
                    $('.js_district .option').html('');
                    for (var index in ret.data) {
                        $('.js_district dt').html('District').attr('title', '');
                        $('.js_district .option').append('<dd value="' + ret.data[index]['id'] + '">' + ret.data[index]['short_name'] + '</dd>');
                    }
                }
            });
        });

        $('.js_change_code').click(function(){
            $('.js_code_img').attr('src', '/ajax/showCode/?r=' + Math.random());
        });

        $('.item').find('input').focus(function(){
            hideErrorTip($(this).parents('.item'));
        });

        $('.js_province,.js_city,.js_district').click(function(){
            hideErrorTip($(this).parents('.item'));
        });

        $('#js_submit').click(function(){
            if (validateForm()) {
                var $this = $(this);
                var name = $.trim($('#js_name').val());
//                var province = $.trim($('#js_province_value').attr('title'));
//                var city = $.trim($('#js_city_value').attr('title'));
//                var district = $.trim($('#js_district_value').attr('title'));
                var phone = $.trim($('#js_mobile').val());
                var mail = $.trim($('#js_mail').val());
                var username = $.trim($('#js_username').val());
                var password = $.trim($('#js_password').val());
                var code = $.trim($('#js_code_value').val());
                $this.attr("disabled",true);
                $this.val('Registering...');
                $.ajax({
                    url : '/user/ajaxRegister/',
                    type : 'post',
                    data : {name:name,phone:phone,username:username,mail:mail,password:password,code:code},
                    dataType : 'json',
                    success : function(ret) {
                        if (ret.code == 0) {
                            alert('Registered Successfully, Please Log In');
                            location.href = '/user/login/';
                        } else if (ret.code > 0 && ret.message) {
                            alert(ret.message);
                        }
                        $this.removeAttr("disabled");
                        $this.val('Register Now');
                    },
                    error : function(ret) {
                        if (ret.code > 0 && ret.message) {
                            alert(ret.message);
                        }
                        $this.removeAttr("disabled");
                        $this.val('Register Now');
                    }
                });
            } else {
                return false;
            }
        });

        function validateForm() {
            if (!validateName() || !validatePhone() || !validateUsername() || !validateMail()
                || !validatePassword() || !validateRePassword() || !validateImgCode() ) {
                return false;
            }
            return true;
        }
        function validateName () {
            var name = $.trim($('#js_name').val());
            if (!name) {
                showError($('#js_name').parents('.item'), 'Please enter your name');
                return false;
            } else {
                hideErrorTip($('#js_name').parents('.item'));
                return true;
            }
        }
        function validateLocation () {
            var province = $.trim($('#js_province_value').attr('title'));
            var city = $.trim($('#js_city_value').attr('title'));
            var district = $.trim($('#js_district_value').attr('title'));

            if (!province || !city || !district) {
                showError($('#js_province_value').parents('.item'), 'Please enter the city');
                return false;
            } else {
                hideErrorTip($('#js_province_value').parents('.item'));
                return true;
            }
        }
        function validatePhone () {
            var phone = $.trim($('#js_mobile').val());
            if (!phone) {
                showError($('#js_mobile').parents('.item'), 'Please enter phone number');
                return false;
            } else if (!isTel(phone)){
                showError($('#js_mobile').parents('.item'), 'Please check phone number');
                return false;
            } else {
                hideErrorTip($('#js_mobile').parents('.item'));
                return true;
            }
        }

        function validateUsername() {
            var username = $.trim($('#js_username').val());
            var patrn = /^[a-zA-Z]{1}([a-zA-Z0-9]|[_]){4,19}$/; //以字母或数字开头，6-20个字母、数字、下划线
            if (!username) {
                showError($('#js_username').parents('.item'), 'Please enter username');
                return false;
            } else if (!patrn.test(username)) {
                showError($('#js_username').parents('.item'), 'Username must contain 6-20 characters (including numbers or underscores)');
                return false;
            } else {
                hideErrorTip($('#js_username').parents('.item'));
                return true;
            }
        }

        function validateMail () {
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var mail = $.trim($('#js_mail').val());
            if (!mail) {
                showError($('#js_mail').parents('.item'), 'Please enter email');
                return false;
            } else if (!filter.test(mail)){
                showError($('#js_mail').parents('.item'), 'Please check email');
                return false;
            } else {
                hideErrorTip($('#js_mail').parents('.item'));
                return true;
            }
        }

        function validatePassword () {
            var password = $.trim($('#js_password').val());
            var patrn = /^([0-9a-zA-Z\-_]){6,20}$/; //6-20个字母、数字、下划线
            if (!password) {
                showError($('#js_password').parents('.item'), 'Please enter password');
                return false;
            } else if (!patrn.test(password)) {
                showError($('#js_password').parents('.item'), 'Password must be 6-20 characters (including numbers or underscores)');
                return false;
            } else {
                hideErrorTip($('#js_password').parents('.item'));
                return true;
            }
        }
        function validateRePassword () {
            var password = $.trim($('#js_password').val());
            var repassword = $.trim($('#js_repassword').val());
            if (!repassword) {
                showError($('#js_repassword').parents('.item'), 'Please enter your password again');
                return false;
            } else if (password != repassword) {
                showError($('#js_repassword').parents('.item'), 'Passwords do not match');
                return false;
            } else {
                hideErrorTip($('#js_repassword').parents('.item'));
                return true;
            }
        }
        function validateImgCode() {
            var code = $.trim($('#js_code_value').val());
            if (!code) {
                showError($('#js_code_value').parents('.item'), 'Please enter code');
                return false;
            } else {
                hideErrorTip($('#js_code_value').parents('.item'));
                return true;
            }

        }

        function showError($item, $text) {
            $item.find('input').addClass('red');
            $item.find('.reg-l-tip').removeClass('s2');
            $item.find('.tp').html($text);
            $item.find('.reg-l-tip').show();
        }
        function showTip($item, $text) {
            $item.find('input').removeClass('red');
            $item.find('.reg-l-tip').addClass('s2');
            $item.find('.tp').html($text);
            $item.find('.reg-l-tip').show();
        }
        function hideErrorTip($item) {
            $item.find('input').removeClass('red');
            $item.find('.reg-l-tip').hide();
        }

        var $btnCode =  $('#js_btn_code');
        var $mobile = $('#js_mobile');
        var sendCode = false;

        $btnCode.click(function(){
            var mobileVal = $mobile.val();
            if (!isTel(mobileVal)) {
                alert('Please enter a correct phone number');
                return false;
            }
            if (sendCode) {
                return false;
            }
            $btnCode.attr("disabled", true);
            sendCode = true;
            $.ajax({
                url : '/ajax/sendMsg/',
                type : 'POST',
                data : {mobile : mobileVal},
                dataType : 'json',
                success : function(data) {
                    if (data.code > 0) {
                        if (data.message) {
                            alert(data.message);
                        } else {
                            alert('Error');
                        }
                        $btnCode.removeAttr("disabled");
                        sendCode = false;
                    } else {
                        var time = 60;
                        $btnCode.text('Try again(' + time + ')');
                        var timer = setInterval(function(){
                            time--;
                            $btnCode.text('Try again in(' + time + ')');
                            if (time == 0) {
                                $btnCode.text('Get Verification Code');
                                clearInterval(timer);
                                $btnCode.removeAttr("disabled");
                                sendCode = false;
                            }
                        }, 1000);
                    }
                },
                error : function(data) {
                    alert('Error');
                    $btnCode.removeAttr("disabled");
                    sendCode = false;
                }
            });
        });

        var isTel = function(tel){
            var regx = /^1[34587]\d{9}$/;
            if (!regx.test(tel)) {
                var regx = /^[0-9]{3}[-]?[0-9]{3}[-]?[0-9]{4}$/;
                if (regx.test(tel)) {
                    return true;
                }
                return false;
            } else {
                return true;
            }
        };
    });
</script>
</body>
</html>
