<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Compare Cars</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/focus-slide.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.SuperSlide.2.1.2.js"></script>

    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo.png" class="my_logo" /></div>
<div class="process-T">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>

    </div>
</div>

<div class="bgc-f9f9f9">

<div class="mid-block">
<div class="car-compar">
<ul class="car-compar-ls clearfix">
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-1.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;1,2655</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
            <li class="clearfix">
                <span class="l tip">Domestic Price:</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Dealer Price: </span>
                <span class="l tc">15000 (You save 3000)</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Year:</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Mileage：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Emissions：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Formerly Used As：</span>
                <span class="l tc">Private Car</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Bluetooth：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Car Seat Heating：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Leather Seats：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Remote Starter:</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Automatic Air Conditioning:</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Cruise:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">LWH:</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Weight:</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
                <span class="l">Comments from our Quality Inspectors:</span>&nbsp;
                <span class="l">Very worth buying, you can't find a better price.</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">Buy</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-2.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;3,1956</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
            <li class="clearfix">
                <span class="l tip">Domestic Price:</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Dealer Price: </span>
                <span class="l tc">15000 (You save 3000)</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Year：</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Mileage：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Emissions：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Formerly Used As：</span>
                <span class="l tc">Private Car</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Bluetooth：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Car Seat Heating：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Leather Seat：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Remote Starter：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Automatic Air Conditioning：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Cruise：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">LWH：</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Weight：</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
              <span class="l">Comments from our Quality Inspectors:</span>&nbsp;
              <span class="l">Very worth buying, you can't find a better price.</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">Buy</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-3.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;2,3645</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
              <li class="clearfix">
                <span class="l tip">Domestic Price:</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Dealer Price: </span>
                <span class="l tc">15000 (You save 3000)</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Year：</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Mileage：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Emissions：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Formerly Used As：</span>
                <span class="l tc">Private Car</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Bluetooth：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Car Seat Heating：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Leather Seat：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Remote Starter：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Automatic Air Conditioning：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Cruise：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">LWH：</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Weight：</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
              <span class="l">Comments from our Quality Inspectors:</span>&nbsp;
              <span class="l">Very worth buying, you can't find a better price.</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">Buy</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-1.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;1,2655</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
              <li class="clearfix">
                <span class="l tip">Domestic Price:</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Dealer Price: </span>
                <span class="l tc">15000 (You save 3000)</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Year：</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Mileage：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Emissions：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Formerly Used As：</span>
                <span class="l tc">Private Car</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Bluetooth：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Car Seat Heating：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Leather Seat：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Remote Starter：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Automatic Air Conditioning：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Cruise：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">LWH：</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Weight：</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
              <span class="l">Comments from our Quality Inspectors:</span>&nbsp;
              <span class="l">Very worth buying, you can't find a better price.</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">Buy</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-2.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;3,1956</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
              <li class="clearfix">
                <span class="l tip">Domestic Price:</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Dealer Price: </span>
                <span class="l tc">15000 (You save 3000)</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Year：</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Mileage：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Emissions：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Formerly Used As：</span>
                <span class="l tc">Private Car</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Bluetooth：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Car Seat Heating：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Leather Seat：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Remote Starter：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Automatic Air Conditioning：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Cruise：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">LWH：</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Weight：</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
              <span class="l">Comments from our Quality Inspectors:</span>&nbsp;
              <span class="l">Very worth buying, you can't find a better price.</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">Buy</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
<li class="car-compar-lsi">
    <div class="T">
        <div class="tpic">
            <img src="http://sta.wheelsrnr.com/ms/images/car-compar-ipic-3.jpg">
        </div>
        <div class="prib">
            <span>$&nbsp;2,3645</span>
        </div>
    </div>
    <div class="C">
        <ul class="car-cpar-ic">
              <li class="clearfix">
                <span class="l tip">Domestic Price:</span>
                <span class="l tc">20000</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Dealer Price: </span>
                <span class="l tc">15000 (You save 3000)</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Year：</span>
                <span class="l tc">2005</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Mileage：</span>
                <span class="l tc">870077</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Emissions：</span>
                <span class="l tc">2L</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Formerly Used As：</span>
                <span class="l tc">Private Car</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Bluetooth：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Car Seat Heating：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Leather Seat：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">GPS:</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Remote Starter：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Automatic Air Conditioning：</span>
                <span class="l tc">NO</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Cruise：</span>
                <span class="l tc">YES</span>
            </li>
            <li class="clearfix">
                <span class="l tip">LWH：</span>
                <span class="l tc">400*230*1500</span>
            </li>
            <li class="clearfix">
                <span class="l tip">Weight：</span>
                <span class="l tc">1800kg</span>
            </li>
            <li class="clearfix">
              <span class="l">Comments from our Quality Inspectors:</span>&nbsp;
              <span class="l">Very worth buying, you can't find a better price.</span>
            </li>
        </ul>
    </div>
    <div class="buy-b">
        <a href="javascript:void(0);" class="buy-btn">Buy</a>
        <div class="fbox">
            <a href="javascript:void(0);" class="del-btn"><img src="http://sta.wheelsrnr.com/ms/images/car-compar-bico.png"></a>
        </div>
        <div class="r-opa"></div>
    </div>
</li>
</ul>
</div>
</div>
</div>

<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.masonry.min.js"></script>
<script type="text/javascript">
    $(function(){
        var $container = $(".car-compar-ls");
        $container.imagesLoaded(function(){
            $container.masonry({
                itemSelector:".car-compar-lsi",
                columnWidth:0 //每两列之间的间隙(像素)
            });
        });
    })
</script>

<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
</html>
