<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Buy Car Page</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/focus-slide.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/custom.css" />

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.SuperSlide.2.1.2.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/bxq-fun.js"></script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo.png" class="my_logo" /></div>
<div class="process-T">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>
    </div>
</div>

<link href="http://sta.wheelsrnr.com/ms/css/lrtk.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/lrtk.js"></script>
<!--[if lte IE 6]>
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/belatedPNG.js"></script>
<script type="text/javascript">
    var __IE6=true;
    DD_belatedPNG.fix('.logo img,.prev img,.next img,img');
</script>
<![endif]-->

<div class="slide-main" id="touchMain">
    <a class="prev" href="javascript:;" stat="prev1001"><img src="http://sta.wheelsrnr.com/ms/images/l-btn.png" /></a>
    <div class="slide-box" id="slideContent">
        <?php foreach ($this->carInfo['image_list'] as $image) { ?>
            <div class="slide" id="bgstylec">
                <img src="<?php echo $image['url'] ?>" width="100%" />
            </div>
        <?php } ?>
        <img src="http://sta.wheelsrnr.com/ms/images/000.jpg" width="100%" style="visibility:visible; z-index:-1" />
    </div>
    <a class="next" href="javascript:;" stat="next1002"><img src="http://sta.wheelsrnr.com/ms/images/r-btn.png" /></a>
</div>

<div class="bgc-f9f9f9">

<div class="bxq-slide">

    <div id="slideBox" class="slideBox">
        <div class="fbox">
            <div class="bxq-sd-fin clearfix">
                <a href="javascript:void(0);" class="bxq-open-pho">Browse Gallery</a>
                <span class="bxq-sd-rt"><em style="color: #fd5200">$<?php echo number_format($this->carInfo['listing_price'], 2);?></em><span></span><em style="font-size: 20px"> <?php echo $this->carInfo['mile']?>Miles</em></span>
            </div>
            <div class="opa"></div>
        </div>
    </div>
</div>
<div class="mid-block">

<div class="bxq-carval-b clearfix">
    <div class="bxq-carval-L">
        <div class="out-itm">
            <p class="Tit">
                <span class="ils">Quality Service</span>
                <span class="ils bao">Warranty Service</span>
                <span class="ils pro">90 Day Return</span>
            </p>

            <div class="car-n">
                <span class="nt"><?php echo $this->carInfo['title']?></span>
            </div>

            <div class="bxq-car-zb clearfix">
                <div class="imb">
                    <div class="pic">
                        <img src="http://sta.wheelsrnr.com/ms/images/bxq-ico-5.png">
                    </div>
                    <div class="dv-2">Structure</div>
                    <div class="dv-3"><?php echo $this->carInfo['structure_text'] ?></div>
                    <div class="split"></div>
                </div>
                <div class="imb">
                    <div class="pic">
                        <img src="http://sta.wheelsrnr.com/ms/images/bxq-ico-6.png">
                    </div>
                    <div class="dv-2">Gearbox</div>
                    <div class="dv-3"><?php echo $this->carInfo['gearbox_text']?></div>
                    <div class="split"></div>
                </div>
                <div class="imb">
                    <div class="pic">
                        <img src="http://sta.wheelsrnr.com/ms/images/bxq-ico-7.png">
                    </div>
                    <div class="dv-2">Engine</div>
                    <div class="dv-3"><?php echo $this->carInfo['model_trim_name']?></div>
                    <div class="split"></div>
                </div>
                <div class="imb">
                    <div class="pic">
                        <img src="http://sta.wheelsrnr.com/ms/images/bxq-ico-8.png">
                    </div>
                    <div class="dv-2">MPG</div>
                    <div class="dv-3">
                        City: <?php echo $this->carInfo['model_info']['model_lkm_city'] ? $this->carInfo['model_info']['model_lkm_city'] : '-'?> <br> Highway: <?php echo $this->carInfo['model_info']['model_lkm_hwy'] ? $this->carInfo['model_info']['model_lkm_hwy'] : '-'?>
                    </div>
                </div>
            </div>

        </div>
        <div class="out-itm">

            <div class="car-pay-type">
                <span class="inline">Finance</span>
                <span class="inline f">Initial Deposit：<em>￥141,350</em></span>
                <span class="inline m">Monthly Payment：<em>￥7831</em></span>
            </div>

        </div>
        <div class="out-itm">

            <div class="city-local">
                <span class="inline tip">&nbsp;Current&nbsp;City：</span>
                <span class="inline dz"><?php echo $this->carInfo['state_name'] . ' ' . $this->carInfo['city_name']  ?></span>
            </div>

        </div>
        <div class="out-itm">

            <div class="bxq-cvl-sug">
                <p class="T">Appraiser Recommends:</p>

                <div class="C">
                   Excellent condition. No paint fixes. No accidents. Released from factory in August, 2012. Released to the public in October. Insurance expires in October 2016. Driven by two owners, but it's always been one person driving.
                </div>
            </div>

        </div>
    </div>
    <div class="bxq-carval-R">
        <a href="/user/pay/?lng=<?php echo MsController::$LANGUAGE;?>&car_id=<?php echo $this->carInfo['id'];?>" class="radius-5 ykc">Appointment Available Now</a>

        <div class="bxq-cvl-man radius-5">
            <p class="t-1">Car Inspector</p>

            <p class="t-2">Vitoria</p>

            <div class="v-pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bxq-upic.jpg">
            </div>
            <div class="v-dg clearfix">
                <span class="tag">Senior Appraiser</span>
                <span class="tag">Compassionate and responsible</span>
            </div>
            <div class="v-case">
                <div class="dv-1 clearfix">
                    <div class="inb no"><span class="i">Certified Cars：15 Cars</span></div>
                    <div class="inb"><span class="i">Located: Changchun</span></div>
                </div>
                <div class="dv-2">
                    Evaluation Time：2015-10-25
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bxq-ci-mn">
    <ul class="bxq-ci-list clearfix">
        <li class="active">
            <a href="javascript:void(0);">Highlights Configuration</a>
        </li>
        <li>
            <a href="javascript:void(0);">Vehicle File</a>
        </li>
        <li>
            <a href="javascript:void(0);">Test Report</a>
        </li>
        <li>
            <a href="javascript:void(0);">Vehicle Parameters</a>
        </li>
    </ul>
</div>


<!--亮点配置-->
<div class="bxq-ci-itm bxq-ldpz" id="bxq-cim-0">
    <div class="Top">
        <h3>Highlights Configuration</h3>
    </div>
    <div class="in">
        <ul class="bxq-ld-in-ls clearfix">
            <?php foreach($this->carInfo['high_lights_text'] as $key => $text) {?>
                <li>
                    <div class="bxq-ldpz-lsin">
                        <i class="i-<?php echo $key >17 ? 17 : $key ?>"></i>
                        <p class="btm-tt" title="<?php echo $text?>"><?php echo $text?></p>
                        <div class="spl"></div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
<!--end-->
<!--车辆档案-->
<div class="bxq-ci-itm bxq-carfile" id="bxq-cim-1">
    <div class="Top">
        <h3>Vehicle File</h3>
    </div>
    <div class="bxq-cf-info">
        <div class="base-b">
            <span class="bxq-flag-s1">Basic Information</span>

            <div class="mbox">
                【Exterior Color】：<span class="n"><?php echo $this->carInfo['exterior_color']?></span>；&nbsp;
                【Price】：<span class="n">$<?php echo number_format($this->carInfo['listing_price'], 2);?></span>；&nbsp;
                【Miles】：<span class="n"><?php echo $this->carInfo['mile']?> miles</span>；&nbsp;
                【Publish Time】：<span class="n"><?php echo date('Y-m-d', $this->carInfo['create_time'])?></span>；
            </div>
        </div>
        <div class="import-b">
            <span class="bxq-flag-s1">Important Configuration</span>

            <div class="mbox">
                <span class="bxq-cfg-tag i-0">Standard</span>
                <span class="bxq-cfg-tag i-1">Additional Ration</span>
                <span class="bxq-cfg-tag i-2">None</span>
                <span class="bxq-cfg-tag i-3">Normal</span>
                <span class="bxq-cfg-tag i-4">Abnormal</span>
            </div>
            <div class="bxq-cfg-mls clearfix">
                <ul class="l bxq-cfg-list">
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Push-Button Start</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Front and Rear Window</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Electric Folding Mirrors</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Electrically Adjustable Front Seats</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Headlights</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Airbag</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Headlight Washer</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">ABS</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                </ul>
                <ul class="l bxq-cfg-list">
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Push-Button Start</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Front and Rear Window</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Electric Folding Mirrors</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Electrically Adjustable Front Seats</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Headlights</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Airbag</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">Headlight Washer</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                    <li class="bxq-cfg-mlsI clearfix">
                        <span class="ttl">ABS</span>

                        <div class="bxq-cfg-icoB clearfix">
                            <i class="i-0"></i>
                            <i class="i-3"></i>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--end-->
<!--检测报告-->
<div class="bxq-ci-itm" id="bxq-cim-2">
<div class="Top">
    <h3>Inspection Report</h3>
</div>
<div class="bxq-ts-report">
<ul class="bxq-tsr-mn clearfix">
    <li class="active"><a href="javascript:void(0);">Accident Investigation</a></li>
    <li><a href="javascript:void(0);">Fire Damage Inspection</a></li>
    <li><a href="javascript:void(0);">Flood Damage Inspection</a></li>
    <li><a href="javascript:void(0);">Safety Inspection</a></li>
    <li><a href="javascript:void(0);">Exterior Inspection</a></li>
    <li><a href="javascript:void(0);">Interior Inspection</a></li>
    <li><a href="javascript:void(0);">Engine Inspection</a></li>
</ul>
<!--事故排查-->
<div class="bxq-tsr-mbox acc" style="display: block;">
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-1-en.jpg">
    <br/>

    <p>Chassis: No Collision Damage</p><br/>
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-2-en.jpg">
    <br/>

    <p>Front: No Collision Damage</p><br/>
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-3-en.jpg">
    <br/>

    <p>Left Side: No Collision Damage</p><br/>
    <img src="http://sta.wheelsrnr.com/ms/images/bxq-ts-pic-4-en.jpg">
    <br/>

    <p>Right Side: No Collision Damage</p><br/>
</div>
<!--end-->
<!--判别火烧车-->
<div class="bxq-tsr-mbox fire">
    <h3 class="bxq-tsr-mh-1">How to detect traces of fire? </h3>

    <p class="bxq-tsr-tdes-1"><span>A car has fire damage if it applies to any of the following:</span></p>

    <div class="bxq-tsr-qes clearfix">
        <div class="cL">
            <p class="p-1">1. The engine wiring harness and/or body harness replacement parts have burn marks</p>

            <p class="p-2">3. Car compartments have burn marks</p>
        </div>
        <div class="cR">
            <p class="p-1">2. The engine wiring harness and/or car fuse have traces of burn marks</p>

            <p class="p-2">4. Engine compartment, interior, or trunk have traces of burn marks</p>
        </div>
    </div>
    <div class="bxq-tsr-qshow">
        <div class="title">
            Fire Inspection and Detection<span class="tip"> Passed, No Burn Marks</span>
        </div>
        <ul class="qlist clearfix">
            <li>
                <span class="t">Safety Belt/Carpet Floor Mats Inspection</span>
                <span class="c i-3">No Rust No Leak No Damage</span>
            </li>
            <li>
                <span class="t">散热片机引擎旁零件/Tank Inspection :</span>
                <span class="c i-3">No Rust No Leak</span>
            </li>
        </ul>
    </div>
</div>
<!--end-->
<!--判别水泡车-->
<div class="bxq-tsr-mbox water">
    <h3 class="bxq-tsr-mh-1">How do you detect flood damage?</h3>

    <p class="bxq-tsr-tdes-1"><span>A car has water damage if it applies to two or more of the following: </span></p>

    <div class="bxq-tsr-qes clearfix">
        <div class="cL">
            <p class="p-1">1. Seat bottom and surrounding metals have unnatural rust.</p>

            <p class="p-2">3. Carpet floor and mats have muddy or moist insulation blankets</p>

            <p class="p-3">5. Seats have stains or mud</p>
        </div>
        <div class="cR">
            <p class="p-1">2. When drawn to the end, seat belts have stains or mildew</p>

            <p class="p-2">4. Suspension components have rust, leaks, or damage</p>

            <p class="p-3">6. Cockpit interor or carpet have traces of destruction, moisture, or rust</p>
        </div>
    </div>
    <div class="bxq-tsr-qshow">
        <div class="title">
            Fire Inspection<span class="tip">Passed, No traces of fire</span>
        </div>
        <ul class="qlist clearfix">
            <li>
                <span class="t">Safety Belt/Carpet Floor Mats Inspection</span>
                <span class="c i-3">No Rust No Leak No Damage</span>
            </li>
            <li>
                <span class="t">散热片机引擎旁零件/Tank Inspection:</span>
                <span class="c i-3">No Rust No Leak</span>
            </li>
            <li>
                <span class="t">Engine wiring and rubber components/Seat springs and inner sleeve flannel:</span>
                <span class="c i-3">No Silt No Odor</span>
            </li>
        </ul>
    </div>
</div>
<!--end-->
<!--安全性检测-->
<style>
    .safe .pic{
        width: 298px;
        height: 269px;
        padding: 32px 20px;
        background-color: #fbfbfb;
    }
    .item2{
        float:left;
        margin-left:50px
    }
</style>
<div class="bxq-tsr-mbox safe">
    <div class="bxq-ts-sbox clearfix">
        <div class="item2">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-sf1-en.png">
            </div>
            <div class="bxq-tsf-ibtm">
                <p class="t">Anti-Freeze<span class="r qr">has two questions</span></p>

                <p class="t-t">Freezing point between 25°C~35°C is safe</p>

                <p class="warm"><span class="txt">-35°C</span></p>
            </div>
        </div>
        <div class="item2">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-sf2-en.png">
            </div>
        </div>
        <div class="item2">
            <div class="pic">
                <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-sf3-en.png">
            </div>
        </div>
    </div>
</div>
<!--end-->
<!--外观检测-->
<div class="bxq-tsr-mbox appear">
    <div class="mpic" style="background: url(http://sta.wheelsrnr.com/ms/images/bxq-tsr-pic-1-en.jpg) no-repeat center top;"></div>
</div>
<!--end-->
<!--内饰检测-->
<div class="bxq-tsr-mbox deco">
    <ul class="bxq-tsde-ls clearfix">
        <li>
            <div class="inn"><span class="txt">Dashboard<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Seat Belt<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Roof light<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Headlight Switch<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Central Locks<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Roof Handles<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Turn Signals<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Rear Mirror<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Speakers<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Wiper<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Door Panel<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">A-Pillar<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Air Conditioning<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Door Light<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">B-Pillar<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Audio/Video Systems and Navigation<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Sunroof<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">C珠饰件<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Front Seats<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Windows<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Airbags<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Backseats<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Armrest<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Door Seals<i class="i-3"></i></span></div>
        </li>
        <li></li>
        <li>
            <div class="inn"><span class="txt">Visor<i class="i-3"></i></span></div>
        </li>
        <li>
            <div class="inn"><span class="txt">Trunk Seal<i class="i-3"></i></span></div>
        </li>
    </ul>
</div>
<!--end-->
<!--发动机舱检测-->
<div class="bxq-tsr-mbox motor">
    <div class="tpic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-tsr-pic-2.jpg">
    </div>
    <p class="ibtm">Engine compartment certified with no obvious defects</p>
</div>
</div>
</div>
<!--end-->
<!--车辆参数-->
<div class="bxq-ci-itm" id="bxq-cim-3">
    <div class="Top">
        <h3>Vehicle Parameters</h3>
    </div>
    <div class="bxq-car-param">
        <?php
        $carConfig = [
            [
                'title' => 'Basic Parameters',
                'items' => [
                    ['key' => 'Price','val' => 'listing_price'],
                    ['key' => 'Mileage'        ,'val' => 'mile'],
                    ['key' => 'Exterior Color'      ,'val' => 'exterior_color'],
                    ['key' => 'Interior Color'      ,'val' => 'interior_color'],
                    ['key' => 'Title'  ,'val' => 'title'],
                    ['key' => 'Doors'    ,'val' => 'model_doors'],
                    ['key' => 'Body Type'    ,'val' => 'structure_text'],
                    ['key' => 'Gearbox','val' => 'gearbox_text'],
                    ['key' => 'Fuel','val' => 'model_engine_fuel'],
                    ['key' => 'Fuel Tank Capacity','val' => 'model_fuel_cap_l'],
                    ['key' => 'Stock','val' => 'id'],
                ]
            ],
            [
                'title' => 'Engine',
                'items' => [
                    ['key' => 'Engine Model'  ,'val' => 'model_trim_name'],
                    ['key' => 'Horsepower'     ,'val' => 'model_engine_power_ps'],
                    ['key' => 'Torque'    ,'val' => 'model_engine_torque_nm'],
                    ['key' => 'Engine Type'  ,'val' => 'model_engine_type'],
                    ['key' => 'Drive Type'      ,'val' => 'model_drive'],
                    ['key' => 'Transmission Type','val' => 'model_transmission_type'],
                    ['key' => 'Engine Position'    ,'val' => 'model_engine_position'],
                    ['key' => 'Top Speed（kph）','val' => 'model_top_speed_kph'],
                    ['key' => '0-100 kph'    ,'val' => 'model_0_to_100_kph'],
                    ['key' => 'Mixed MPG'    ,'val' => 'model_lkm_mixed'],
                ]
            ]
        ]
        ?>
        <?php for ($i = 0; $i < round(count($carConfig)/2); $i++ ) { ?>
            <table border="0" cellpadding="0" cellspacing="0" class="bxq-cpar-tb">
                <thead>
                <tr>
                    <td colspan="2">
                        <span class="bxq-flag-s1"><?php echo $carConfig[$i*2]['title'] ?></span>
                    </td>
                    <?php if (isset($carConfig[$i*2+1])) { ?>
                        <td colspan="2">
                            <span class="bxq-flag-s1"><?php echo $carConfig[$i*2 + 1]['title'] ?></span>
                        </td>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php for ($j = 0; $j < max(count($carConfig[$i]['items']), count($carConfig[$i+1]['items'])); $j++) { ?>
                    <tr>
                        <td class="td-1"><span class="tip"><?php echo $carConfig[$i*2]['items'][$j]['key']?></span></td>
                        <td><?php echo  $this->carInfo[$carConfig[$i*2]['items'][$j]['val']]?></td>
                        <?php if (isset($carConfig[$i+1]['items'][$j])) { ?>
                            <td class="td-1"><span class="tip"><?php echo $carConfig[$i*2+1]['items'][$j]['key']?></span></td>
                            <td><?php echo $this->carInfo[$carConfig[$i*2+1]['items'][$j]['val']]?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
</div>
<!--end-->

<!--可能喜欢-->
<div class="bxq-like">
<div class="Top">
    <b class="t">You may like</b>
    <a href="javascript:void(0);" class="c-oth">Next set</a>
</div>
<div class="bxq-like-lsb">
<ul class="bxq-like-list clearfix">
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-1.jpg">
    </a>

    <p class="car-name">2012 Golf 1.6 automatic comfort</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">Time</p>

            <p class="p-2">2012-12</p>
        </div>
        <div class="im">
            <p class="p-1">Kilometers driven</p>

            <p class="p-2">30000 km</p>
        </div>
        <div class="im">
            <p class="p-1">Gearbox</p>

            <p class="p-2">Manual</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-2.jpg">
    </a>

    <p class="car-name">Mazda models 2.0L automatic fashion model 62011</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">Time</p>

            <p class="p-2">2011-9</p>
        </div>
        <div class="im">
            <p class="p-1">Kilometers driven</p>

            <p class="p-2">56000 km</p>
        </div>
        <div class="im">
            <p class="p-1">Gearbox</p>

            <p class="p-2">Automatic</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;5,8625</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-3.jpg">
    </a>

    <p class="car-name">2010 Jetta 1.6L partner</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">Time</p>

            <p class="p-2">2010-12</p>
        </div>
        <div class="im">
            <p class="p-1">Kilometers driven</p>

            <p class="p-2">980000 km</p>
        </div>
        <div class="im">
            <p class="p-1">Gearbox</p>

            <p class="p-2">Automatic</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-4.jpg">
    </a>

    <p class="car-name">Standard models TFSI Audi A6L2014</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">Time</p>

            <p class="p-2">2009-11</p>
        </div>
        <div class="im">
            <p class="p-1">Kilometers driven</p>

            <p class="p-2">72000 km</p>
        </div>
        <div class="im">
            <p class="p-1">Gearbox</p>

            <p class="p-2">Manual</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-5.jpg">
    </a>

    <p class="car-name">2012 Golf 1.6 automatic comfort</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">Time</p>

            <p class="p-2">2012-12</p>
        </div>
        <div class="im">
            <p class="p-1">Kilometers driven</p>

            <p class="p-2">30000 km</p>
        </div>
        <div class="im">
            <p class="p-1">Gearbox</p>

            <p class="p-2">Manual</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-6.jpg">
    </a>

    <p class="car-name">Mazda models 2.0L automatic fashion model 62011</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">Time</p>

            <p class="p-2">2011-9</p>
        </div>
        <div class="im">
            <p class="p-1">Kilometer Driven</p>

            <p class="p-2">56000 km</p>
        </div>
        <div class="im">
            <p class="p-1">Gearbox</p>

            <p class="p-2">Automatic</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;5,8625</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-7.jpg">
    </a>

    <p class="car-name">2010 Jetta 1.6L partner</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">Time</p>

            <p class="p-2">2010-12</p>
        </div>
        <div class="im">
            <p class="p-1">Kilometers driven</p>

            <p class="p-2">980000 km</p>
        </div>
        <div class="im">
            <p class="p-1">Gearbox</p>

            <p class="p-2">Automatic</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
<li>
    <a href="javascript:void(0);" class="car-pic">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-pic-8.jpg">
    </a>

    <p class="car-name">Standard models TFSI Audi A6L2014</p>

    <div class="car-cfg-spl">
        <img src="http://sta.wheelsrnr.com/ms/images/bxq-like-ispl.png">
    </div>
    <div class="car-cfg-m clearfix">
        <div class="im">
            <p class="p-1">Time</p>

            <p class="p-2">2009-11</p>
        </div>
        <div class="im">
            <p class="p-1">Kilometers driven</p>

            <p class="p-2">72000 km</p>
        </div>
        <div class="im">
            <p class="p-1">Gearbox</p>

            <p class="p-2">Manual</p>
        </div>
    </div>
    <p class="car-pri"><span class="un">$</span>&nbsp;1,2655</p>
    <a href="javascript:void(0);"><img src="http://sta.wheelsrnr.com/ms/images/bxq-like-buy.png"></a>
</li>
</ul>
</div>
</div>
<!--end-->

</div>

</div>

<link href="http://sta.wheelsrnr.com/ms/css/grids.css" rel="stylesheet" type="text/css" media="all" />
<link href="http://sta.wheelsrnr.com/ms/css/album.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/carousel.js"></script>
<script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/album.js"></script>

<div class="car-gallery">
    <div class="pho-close">x</div>
    <div class="album" id="album">
        <div class="album-image-md" id="album-image-md">
            <p class="album-image-bd"><img src="http://sta.wheelsrnr.com/ms/images/buy-show-1-3.jpg" class="album-image" id="album-image" alt="图片1" /></p>

            <ul class="album-image-nav hide" id="album-image-nav">
                <li class="album-image-nav-left-block" id="album-image-nav-left-block"><a href="#prev-image" class="album-image-btn-prev" id="album-image-btn-prev">‹</a></li>
                <li class="album-image-nav-right-block" id="album-image-nav-right-block"><a href="#next-image" class="album-image-btn-next" id="album-image-btn-next">›</a></li>
            </ul>

        </div>
        <div class="album-carousel" id="album-carousel">
            <a href="#prev-group" class="album-carousel-btn-prev" id="album-carousel-btn-prev">‹</a>
            <div class="album-carousel-zone" id="album-carousel-zone">
                <ul class="album-carousel-list" id="album-carousel-list">
                    <?php foreach ($this->carInfo['image_list'] as $key => $image) { ?>
                        <li class="album-carousel-thumb <?php echo $key==0 ? 'album-carousel-thumb-selected' : ''?>"><a href="<?php echo $image['url']?>"><img src="<?php echo $image['url']?>" alt="相册图片-示例图片（17）" /></a></li>
                    <?php } ?>
                </ul>
            </div>
            <a href="#next-group" class="album-carousel-btn-next" id="album-carousel-btn-next">›</a>
        </div>
    </div>
</div>
<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>
</body>
<script>
    $(document).ready(function(){
        var Album = new jQuery.Album();
        $(".bxq-open-pho").click(function(){
            $("body").css({"overflow":"hidden"});
            $(".car-gallery").show();
        })
        $(".pho-close").click(function(){
            $("body").css({"overflow":"auto"});
            $(".car-gallery").hide();
        })
    });
</script>
</html>
