<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>sell</title>
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/common.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/head_foot.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/dlselect.css" />
    <link rel="stylesheet" type="text/css" href="http://sta.wheelsrnr.com/ms/css/style.css" />

    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/jquery.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/dlselect.js"></script>
    <script type="text/javascript" src="http://sta.wheelsrnr.com/ms/js/fun.js"></script>
    <!--[if IE]>
    <script src="http://sta.wheelsrnr.com/ms/js/html5shiv.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="my_logo_out"><img src="http://sta.wheelsrnr.com/ms/images/logo2.png" class="my_logo" /></div>
<div class="sellTop">
    <div class="mid-block">
        <div class="tBox">
            <?php $this->load('/' . MsController::$LANGUAGE . '/common/daohang.php', array()) ?>
            <div class="clear"></div>
        </div>
        <div class="t-txt-box">
            <img src="http://sta.wheelsrnr.com/ms/images/sell-t-txt.png">
        </div>
        <div class="sellT-m radius-5">
            <form action="#" method="post">
                <div class="sellTm-in">
                    <div class="in-itm">
                        <div class="sellTm-in-1">
                            <dl class="l select">
                                <!--title用于存储选值-->
                                <dt title="value">Brand</dt>
                                <div class="option">
                                    <dd value="1">Acura</dd>
                                    <dd value="2">BMW</dd>
                                    <dd value="3">Cadillac</dd>
                                    <dd value="4">Chevrolet</dd>
                                    <dd value="5">Dodge</dd>
                                </div>
                            </dl>
                            <dl class="l select s2">
                                <!--title用于存储选值-->
                                <dt title="value">Series</dt>
                                <div class="option">
                                    <dd value="1">one</dd>
                                    <dd value="2">two</dd>
                                    <dd value="3">third</dd>
                                </div>
                            </dl>
                            <dl class="l select">
                                <!--title用于存储选值-->
                                <dt title="value">Type</dt>
                                <div class="option">
                                    <dd value="1">Sedan</dd>
                                    <dd value="2">Coupe</dd>
                                    <dd value="3">Hatchback</dd>
                                    <dd value="4">SUV</dd>
                                    <dd value="5">Mini Van</dd>
                                </div>
                            </dl>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="in-itm">
                        <div class="sellTm-in-2">
                            <div class="itemb">
                                <span class="tip">Milage：</span>
                                <input type="text" name="" class="radius-5 sellT-inp-s1"/>
                                <span class="tip">Kilometer</span>
                            </div>
                            <div class="itemb">
                                <span class="tip">Release Date：</span>
                                <input type="text" name="" class="radius-5 sellT-inp-s2"/>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="in-itm">
                        <div class="sellTm-in-3">
                            <span class="l tip">Location：</span>
                            <dl class="l select">
                                <!--title用于存储选值-->
                                <dt title="value">Fujian Province</dt>
                                <div class="option">
                                    <dd value="1">Fujian Province</dd>
                                    <dd value="2">Guangdong</dd>
                                    <dd value="3">Guangxi</dd>
                                    <dd value="4">Hunan</dd>
                                    <dd value="5">Yunnan</dd>
                                </div>
                            </dl>
                            <dl class="l select">
                                <!--title用于存储选值-->
                                <dt title="value">Fuzhou</dt>
                                <div class="option">
                                    <dd value="1">Fuzhou</dd>
                                </div>
                            </dl>
                            <dl class="l select">
                                <!--title用于存储选值-->
                                <dt title="value">Fuqing</dt>
                                <div class="option">
                                    <dd value="1">Fuqing</dd>
                                </div>
                            </dl>
                            <input type="text" name="" placeholder="Enter Phone Number" class="radius-5 sellT-inp-s3"/>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="sellT-btm-op clearfix">
                        <input type="submit" value="Book" class="book"/>
                        <input type="submit" value="Price" class="val"/>
                    </div>
                </div>
            </form>
            <div class="opac radius-5"></div>
        </div>
    </div>
</div>

<div class="sell-mn">
    <div class="mid-block">
        <div class="tspl"></div>
        <div class="sell-mn-in clearfix">
            <div class="item-b">
                <div class="sell-mni-b s0">
                    <!--<img src="images/sell-mn-ico-1.png"></i>-->
                    <div class="des-b">
                        <p class="tit">Rapid Transactions</p>
                        <p class="btm-t">Here, we trade a car every 5 minutes.</p>
                    </div>
                </div>
            </div>
            <div class="item-b">
                <div class="sell-mni-b s1">
                    <!--<img src="images/sell-mn-ico-1.png"></i>-->
                    <div class="des-b">
                        <p class="tit">Simplified Process</p>
                        <p class="btm-t">In only one step, professional services will assist you.</p>
                    </div>
                </div>
            </div>
            <div class="item-b">
                <div class="sell-mni-b s2">
                    <!--<img src="images/sell-mn-ico-1.png"></i>-->
                    <div class="des-b">
                        <p class="tit">Sell at a Good Price</p>
                        <p class="btm-t">We will help you find the highest bidder.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sell-mserv">
    <div class="mid-block">
        <div class="Top">
            <p class="p-1">【 Convenient, Worry-Free 】In 6 steps, we'll give you 100% service.</p>
            <p class="p-2">Convenient Worry-Free  100% Service</p>
        </div>
        <div class="sell-ms-main clearfix">
            <div class="item">
                <i class="i-0"></i>
                <div class="des">
                    <p class="p-1">Reserveation</p>
                    <p class="p-2">Reserve Professional Service in 60 second</p>
                </div>
            </div>
            <div class="item">
                <i class="i-1"></i>
                <div class="des">
                    <p class="p-1">Detect</p>
                    <p class="p-2">Inspectors will come to you for free</p>
                </div>
            </div>
            <div class="item">
                <i class="i-2"></i>
                <div class="des">
                    <p class="p-1">Independent Pricing</p>
                    <p class="p-2">Help you find accurate pricing</p>
                </div>
            </div>
            <div class="item">
                <i class="i-3"></i>
                <div class="des">
                    <p class="p-1">Trade</p>
                    <p class="p-2">Individual Buyers Pay Cash Fast</p>
                </div>
            </div>
            <div class="item">
                <i class="i-4"></i>
                <div class="des">
                    <p class="p-1">Transfer</p>
                    <p class="p-2">Specialists simplify your transfer process</p>
                </div>
            </div>
            <div class="item">
                <i class="i-5"></i>
                <div class="des">
                    <p class="p-1">Exclusive Sale</p>
                    <p class="p-2">Worry-free service with our professional team</p>
                </div>
            </div>
        </div>
        <div class="sell-ms-btm">
            <a href="javascript:void(0);" class="radius-5 sell-ms-btn">Buy Car</a>
        </div>
    </div>
</div>

<?php $this->load('/' . MsController::$LANGUAGE . '/common/footer.php', array()) ?>

</body>
</html>
