<?php
return [
    [
        'title' => '基本参数',
        'items' => [
            '厂商指导价'    => 'quoto_price',
            '级别'          => 'level',
            '发动机'        => 'engine',
            '变速箱'        => 'transmission',
            '长宽高(mm)'    => 'lwh',
            '轴距(mm)'      => 'wheel_base',
            '车身结构'      => 'structure_all',
            '整备质量(kg)'  => 'quality',
            '行李箱容积(L)' => 'boot'
        ]
    ],
    [
        'title' => '发动机',
        'items' => [
            '发动机型号'    => 'engine_model',
            '排量(L)'       => 'emissions_l',
            '进气形式'      => 'intake_form',
            '汽缸数(个)'    => 'cylinder_num',
            '压缩比'        => 'compression_ratio',
            '最大马力(Ps)'  => 'horsepower',
            '最大扭矩(N*m)' => 'max_torque',
            '燃油标号'      => 'fuel_label',
            '供油方式'      => 'fuel_feed_mode'
        ]
    ],
    [
        'title' => '底盘和制动',
        'items' => [
            '驱动方式'      => 'driving_mode',
            '前悬架类型'    => 'front_suspension',
            '后悬架类型'    => 'back_suspension',
            '助力类型'      => 'assist_mode',
            '前制动器类型'  => 'front_brake',
            '后制动器类型'  => 'back_brake',
            '铝合金轮圈'    => 'alloy_wheel',
            '前轮胎规格'    => 'front_wheel',
            '后轮胎规格'    => 'back_wheel'
        ]
    ],
    [
        'title' => '安全装置',
        'items' => [
            '主/副驾驶座安全气囊'   => 'driver_airbag',
            '前/后排侧气囊'         => 'side_airbag',
            '前/后排头部气囊'       => 'head_airbag',
            '儿童座椅接口'          => 'isofix',
            '无钥匙启动'            => 'start_keyless',
            '无钥匙进入'            => 'enty_keyless',
            'ABS 防抱死'            => 'abs',
            '车身稳定控制(ESP)'     => 'esc',
            '上坡辅助'              => 'hac'
        ]
    ],
    [
        'title' => '外部配置',
        'items' => [
            '电动天窗'              => 'roof',
            '全景天窗'              => 'panoramic_roof',
            '氙气大灯'              => 'xenon_light',
            'LED 大灯'              => 'led_light',
            '日间行车灯'            => 'day_light',
            '前雾灯'                => 'fog_light',
            '后视镜电动调节'        => 'electric_variable_mirror',
            '前/后电动车窗'         => 'electric_window',
            '感应雨刷'              => 'auto_wiper'
        ]
    ],
    [
        'title' => '内部配置',
        'items' => [
            '真皮/仿皮座椅'         => 'leather_seat',
            '主/副驾驶座电动调节'   => 'electric_variable_seat',
            '前/后排座椅加热'       => 'heated_seat',
            '后排座椅放倒方式'      => 'rear_seat_fold_mode',
            '真皮方向盘'            => 'leather_wheel',
            '多功能方向盘'          => 'multi_function_wheel',
            '方向盘换挡'            => 'shifters_wheel',
            '定速巡航'              => 'ccs',
            '空调控制方式'          => 'air_conditioning_mode'
        ]
    ]
];