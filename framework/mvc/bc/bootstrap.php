<?PHP
require_once dirname(__FILE__) . '/Router.class.php';
require_once dirname(__FILE__) . '/Dispatch.class.php';
require_once dirname(__FILE__) . '/../../util/http/Request.class.php';
require_once dirname(__FILE__) . '/../../util/http/Response.class.php';
if (defined('CHARSET')) {
    Response::setCharset(CHARSET);
} 

class SysError {
    public static function run() {
        $args = func_get_args();
        if (is_int($args[0])) {
            $errorCode    = $args[0];
            $errorMessage = $args[1];
        } else {
            $errorCode    = $args[0]->getCode();
            $errorMessage = $args[0]->getMessage();
        }
	    // 程序运行中出错,或各种异常 记录下
	    if ($args[0] instanceof UserException) { // 用户级异常
		    Log::fatal('sys.userException', $args[0]);
	    } elseif($args[0] instanceof SysException) { // 系统级异常
		    $errorMessage = '对不起！系统错误，请稍后再试。';
		    Log::fatal('sys.sysException', $args[0]);
	    } elseif($args[0] instanceof Exception) {
		    Log::fatal('sys.Exception', $args[0]);
	    } else { // Exception 或者 系统错误
		    Log::fatal('sys.error', $errorMessage.'|'.$errorCode);
		    $errorMessage = '对不起！系统错误，请稍后再试。';
	    }

        if (!empty($_SERVER['DEBUG'])) {
	        echo "<pre>";
	        if($args[0] instanceof Exception){
		        echo Util::formatStackTrace($args[0]);
	        } else {
		        echo $errorMessage . "\n";
		        print_r(debug_backtrace());
	        }
            exit;
        }

	    // 统一在错误信息页面输出
		$jumpUrl = Request::getRefer();
	    false !== strpos($jumpUrl, '/index/error/') && $jumpUrl = '/';
	    Response::redirect(Router::createUrl('index', 'error', array(), Router::getModule(), array(
		    'code' => $errorCode, 'message' => $errorMessage, 'url' => $jumpUrl
	    )));
    }
}

set_error_handler(array('SysError', 'run'), E_USER_ERROR);
set_exception_handler(array('SysError', 'run'));
