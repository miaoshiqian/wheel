<?php
require_once dirname(__FILE__) . '/View.class.php';

abstract class BaseController {
    // 前端变量
    protected $staticVars = array();

    // 模板引擎
    protected $view;

    // 构造函数
    public function __construct() {
        $this->view = new View();
    }

    public function init() {
        
    }

	abstract public function errorAction();
	abstract public function successAction();
}
