<?php
require_once dirname(__FILE__) . '/Helper.class.php';
require_once dirname(__FILE__) . '/../../util/http/Response.class.php';
require_once dirname(__FILE__) . '/Router.class.php';

class View {
    // 模板目录
    private $_dirs = array();

    public function __construct() {
        self::addDir(PROJECT_PATH . '/view');
        if (Router::getModule() != '') {
            self::addDir(Router::getModuleDir() . '/view');
        }
    }
    
    public function addDir($dir) {
        if (empty($dir)) return false;
        
        if (is_string($dir) && is_dir($dir) && !in_array($dir, $this->_dirs)) {
            array_unshift($this->_dirs, $dir);  // 后加的优先搜索
            return true;
        } 
        
        return false;
    }

    /**
     * 给模板赋值
     * @param string|array $key
     * @param mixed $val
     */
    public function assign($key, $val='') {
        if (is_array($key)) {
            foreach ((array)$key as $k => $v){
                $this->$k = $v;
//	             extract($key);
            }
        } else {
//	        extract(array($key => $val));
            $this->$key = $val;
        }
    }

	public function put($value, $raw = false){
		echo $raw ? $value : htmlspecialchars($value);
	}

    public function fetch($templateFile, $values = array()) {
        if (is_array($values) && count($values) > 0) {
            $this->assign($values);
        }

        ob_start();

        $exists = 0;
        foreach ($this->_dirs as $tplPath) {
	        $includeFile = $tplPath . '/' . ltrim($templateFile, '/');
            if (is_file($includeFile)) {
                $exists = 1;
                include $includeFile;
                break;
            }
        }
        
        $ret = ob_get_contents();
	    $ret = $this->replaceTag($ret, 'css');
	    $ret = $this->replaceTag($ret, 'js');
        ob_end_clean();

        if (!$exists) {
            throw new Exception('模块文件不存在[' . $templateFile . ']');
        }

        return $ret;
    }

	/**
	 * 替换标签,把正文中 <!--xxx:start-->(.+?)<!--xxx:end--> 的内容放到 <!--content:xxx--> 中
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $content
	 * @param $tagName
	 *
	 * @return mixed
	 */
	public function replaceTag($content, $tagName) {
		$pattern = sprintf('#<!--%s:start-->(.+?)<!--%s:end-->#s', $tagName, $tagName);
		preg_match_all($pattern, $content, $matches);
		$content = preg_replace($pattern, '', $content);
		$replaceStr = '';
		foreach ($matches[1] as $value) {
			$replaceStr .= $value;
		}
		return str_replace(sprintf('<!--%s:content-->', $tagName), $replaceStr, $content);
	}
    
    /**
     * 使用插件
     * @param 插件名称 $funcName 对应plugin/helper/$funcName_helper.php中的$funcName_helper方法
     * @param mix $params
     * @return mix
     */
    public function helper($funcName, $params=array()) {
        return Helper::call($funcName, $params);
    }

    /**
     * 载入子模板，在模板中使用
     * @param string $fileName 子模板文件名，相对于模板目录指定
     * @param array $params 要传给子模板的变量
     */
    protected function load($fileName, $params=array()) {
        foreach ($this->_dirs as $tplPath) {
            if (is_file($tplPath . '/' . $fileName)) {
                extract((array)$params);
                include $tplPath . '/' . $fileName;
                return;
            }
        }
        throw new Exception('文件"'.$fileName.'"不存在');
    }

	public function __call($func, $arguments) {
		if ($func == 'date') {
			$format = empty($arguments[1]) ? 'Y-m-d H:i:s' : $arguments[1];
			echo empty($arguments[0]) ? '' : date($format, (int)$arguments[0]);
			return;
		}

		echo call_user_func_array($func, $arguments);
		return;
	}
}
