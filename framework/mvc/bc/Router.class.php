<?PHP

class Router {
	protected static $_module = '';
	protected static $_controller = '';
	protected static $_action = '';
	protected static $_params = array();
	protected static $_moduleDir = '';
	protected static $_lastUrl = '';

	public static function getModule() {
		if (empty(self::$_controller)) {
			self::init();
		}
		return self::$_module;
	}

	public static function getModuleDir() {
		if (empty(self::$_controller)) {
			self::init();
		}
		return self::$_moduleDir;
	}

	public static function getController() {
		if (empty(self::$_controller)) {
			self::init();
		}
		return self::$_controller;
	}

	public static function getAction() {
		if (empty(self::$_controller)) {
			self::init();
		}
		return self::$_action;
	}

	public static function getParams() {
		if (empty(self::$_controller)) {
			self::init();
		}
		return self::$_params;
	}

	public static function createUrl($controller = 'index', $action = 'default', $params = array(), $module = '', $get = array()) {
		$str = '/';
		if (!empty($module)) { // 把模块加到子域名上
			$requestUrl = Request::getCurrentUrl();
			$parseUrl   = parse_url($requestUrl);
			$host       = $parseUrl['host'];
			$hostArr    = explode('.', $host);
			$scheme     = $parseUrl['scheme'];
			if (count($hostArr) === 3) { //
				array_unshift($hostArr, $module);
			} else if (count($hostArr) === 4) {
				$hostArr[0] = $module;
			}

			$str = $scheme . '://' . join('.', $hostArr) . '/';
		}
		if (is_array($params) && count($params) > 0) {
			if (empty($controller)) {
				$controller = 'index';
			}
			if (empty($action)) {
				$action = 'default';
			}
		}
		if (!empty($controller)) {
			$str .= $controller . '/';
		}
		if (!empty($action)) {
			$str .= $action . '/';
		}
		if (is_array($params) && count($params) > 0) {
			foreach ($params as $val) {
				$str .= rawurlencode($val) . '/';
			}
		}
		if ($get && is_array($get)) {
			$str .= '?' . http_build_query($get);
		}
		return $str;
	}

	public static function getInfo($requestUrl = '') {
		if (empty($requestUrl) && empty(self::$_controller)) {
			$requestUrl = Request::getCurrentUrl();
		}
		if ($requestUrl) {
			self::init($requestUrl);
		}

		return array(
			'module'     => self::$_module,
			'controller' => self::$_controller,
			'action'     => self::$_action,
			'params'     => self::$_params,
			'module_dir' => self::$_moduleDir,
		);
	}

	protected static function init($requestUrl = '') {
		if (empty($requestUrl)) {
			$requestUrl = Request::getCurrentUrl();
		}
		if (self::$_lastUrl == $requestUrl) {
			return ;
		}
		self::$_lastUrl = $requestUrl;

		$parseUrl = parse_url($requestUrl);
		$host     = $parseUrl['host'];
		$hostArr  = explode('.', $host);

		if (count($hostArr) === 4) { // 有三级域名  log.bc.mt.com
			self::$_module = $hostArr[0];
			self::$_moduleDir =  ModuleConfig::getModuleDir(self::$_module);
		}

		$url    = preg_replace("/[#\?].*$/", '', $parseUrl['path']);
		$_paths = explode('/', $url);

		$paths = array();
		foreach ($_paths as $path) {
			if ($path !== '' && $path != '.' && $path != '..') {
				$paths[] = $path;
			}
		}
		if (count($paths) == 0) {
			$paths[] = 'index';
		}
		if (count($paths) == 1) {
			$paths[] = 'default';
		}

		self::$_controller = !empty($paths[0]) ? array_shift($paths) : 'index';
		self::$_action     = !empty($paths[0]) ? array_shift($paths) : 'default';

		if (count($paths) > 0) {
			foreach ($paths as $path) {
				self::$_params[] = urldecode($path);
			}
		}

		return ;
	}
}