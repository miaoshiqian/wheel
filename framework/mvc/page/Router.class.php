<?PHP

class Router {
    protected static $_modules = array();
    protected static $_module = '';
    protected static $_controller = '';
    protected static $_action = '';
    protected static $_params = array();
    protected static $_lastUrl = '';
    public static $rules = array();

    public static function setModules($modules = array()) {
        self::$_modules = $modules;
    }

    public static function getModule() {
        if (empty(self::$_controller)) {
            self::init();
        }
        return self::$_module;
    }

    public static function getController() {
        if (empty(self::$_controller)) {
            self::init();
        }
        return self::$_controller;
    }

    public static function getAction() {
        if (empty(self::$_controller)) {
            self::init();
        }
        return self::$_action;
    }

    public static function getParams() {
        if (empty(self::$_controller)) {
            self::init();
        }
        return self::$_params;
    }

    public static function getParamByIndex($index, $default = null){
        $params = self::getParams();
        return isset($params[$index]) ? $params[$index] : $default;
    }

    public static function createUrl($controller = 'index', $action = 'default', $params = array(), $module = '', $get = array()) {
        $str = '/';
        if (!empty($module)) {
            if (empty(self::$_modules[$module])) {
                throw new Exception("模块{$module}未定义");
            }
            $str .= $module . '/';
        }
        if (is_array($params) && count($params) > 0) {
            if (empty($controller)) {
                $controller = 'index';
            }
            if (empty($action)) {
                $action = 'default';
            }
        }
        if (!empty($controller)) {
            $str .= $controller . '/';
        }
        if (!empty($action)) {
            $str .= $action . '/';
        }
        if (is_array($params) && count($params) > 0) {
            foreach ($params as $val) {
                $str .= rawurlencode($val) . '/';
            }
        }
        if ($get && is_array($get)) {
            $str .= '?' . http_build_query($get);
        }
        return $str;
    }

    public static function getInfo($requestUrl = '') {
        if (empty($requestUrl) && empty(self::$_controller)) {
            $requestUrl = Request::getCurrentUrl();
        }
        if ($requestUrl) {
            self::init($requestUrl);
        }

        return array(
            'module'     => self::$_module,
            'controller' => self::$_controller,
            'action'     => self::$_action,
            'params'     => self::$_params,
        );
    }

    protected static function init($requestUrl = '') {
        if (empty($requestUrl)) {
            $requestUrl = Request::getCurrentUrl();
        }
        if (self::$_lastUrl == $requestUrl) {
            return ;
        }
        self::$_lastUrl = $requestUrl;

        $parseUrl = parse_url($requestUrl);
        $url    = self::getFormatPath($parseUrl['path']);
        $_paths = explode('/', $url);
        $paths  = array();
        foreach ($_paths as $path) {
            if ($path !== '' && $path != '.' && $path != '..') {
                $paths[] = $path;
            }
        }

        if (!empty($paths[0]) && in_array($paths[0], self::$_modules)) {
            self::$_module = array_shift($paths);
        } else {
            self::$_module = null;
        }

        if (count($paths) == 0) {
            $paths[] = 'index';
        }
        if (count($paths) == 1) {
            $paths[] = 'default';
        }

        self::$_controller = !empty($paths[0]) ? array_shift($paths) : 'index';
        self::$_action     = !empty($paths[0]) ? array_shift($paths) : 'default';

        if (count($paths) > 0) {
            foreach ($paths as $path) {
                self::$_params[] = rawurldecode($path);
            }
        }

        return ;
    }

    protected static function getFormatPath($url) {
        if (!preg_match("/^\/([^\/_]*)/", $url, $reg)) {
            return $url;
        }
        if (empty(self::$rules[$reg[1]])) {
            return $url;
        }
        if (is_string(self::$rules[$reg[1]])) {
            return self::$rules[$reg[1]];
        }
        if (is_array(self::$rules[$reg[1]])) {
            foreach (self::$rules[$reg[1]] as $path => $rule) {
                $ret = preg_replace($path, $rule, $url);
                if ($ret != $url) {
                    return $ret;
                }
            }
        }
        return $url;
    }
}