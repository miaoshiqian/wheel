<?PHP
if (!empty($_SERVER['DEBUG'])) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

require_once FRAMEWORK_PATH . '/mvc/page/Router.class.php';
require_once FRAMEWORK_PATH . '/mvc/page/Dispatch.class.php';
require_once FRAMEWORK_PATH . '/util/http/Request.class.php';
require_once FRAMEWORK_PATH . '/util/http/Response.class.php';
require_once FRAMEWORK_PATH . '/common/AutoLoader.class.php';

if (defined('CHARSET')) {
    Response::setCharset(CHARSET);
} 

class SysError {
    public static function run() {
        $args = func_get_args();
        if (is_int($args[0])) {
            $errorCode    = $args[0];
            $errorMessage = $args[1];
        } else {
            $errorCode    = $args[0]->getCode();
            $errorMessage = $args[0]->getMessage();
        }
        if (!empty($_SERVER['DEBUG'])) {
            echo $errorMessage . "\n";
            print_r(debug_backtrace());
            exit;
        }

	    $errorMessage = '对不起！系统错误，请稍后再试。';
	    Response::redirect(Router::createUrl(Router::getController(), 'error', array(), Router::getModule(), array(
		    'code' => $errorCode, 'message' => $errorMessage, 'url' => Request::getRefer()
	    )));
    }
}

set_error_handler(array('SysError', 'run'), E_USER_ERROR);
set_exception_handler(array('SysError', 'run'));
