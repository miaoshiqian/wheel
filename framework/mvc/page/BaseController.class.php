<?php
require_once dirname(__FILE__) . '/View.class.php';

class BaseController {
    // 前端变量
    protected $staticVars = array();

	/** @var View  模板引擎 */
    protected $view;

    // 构造函数
    public function __construct() {
        $this->view = new View();
    }

    public function init() {
        
    }

}
