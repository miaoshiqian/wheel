<?php

$_SERVER['DEBUG'] = true;

define('FRAMEWORK_PATH', __DIR__ . '/..');
require_once FRAMEWORK_PATH . '/common/AutoLoader.class.php';
require_once FRAMEWORK_PATH . '/mvc/bootstrap.php';

AutoLoader::setAutoDir(FRAMEWORK_PATH . '/util');
AutoLoader::setAutoDir(FRAMEWORK_PATH . '/../conf/conf');
AutoLoader::setAutoDir(FRAMEWORK_PATH . '/../conf/common');
