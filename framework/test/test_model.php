<?php

require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/TestAModelV5.class.php';
require_once __DIR__ . '/TestBModelV5.class.php';


// 如果不需要事务，Model的用法和平时一样；如果需要事务，那么需要新建一个临时连接去初始化Model

// 事务主要是读写，需要在主库上新建连接句柄；true参数代表临时连接，是为了防止事务嵌套
$handle1 = new MysqliClient(DBConfig::$SERVER_MASTER, true);
$handle2 = new MysqliClient(DBConfig::$SERVER_MASTER, true);

// 开启两个事务
$handle1->begin();
$handle2->begin();

// 用临时连接去初始化Model, AB建立在不同的连接上，执行sql时互不干扰
$modelA = new TestAModelV5($handle1);
$modelB = new TestBModelV5($handle2);

$modelA->insert(array(
    'name'  => date('Y-m-d H:i:s', time()),
));

$modelB->insert(array(
    'name'  => date('Y-m-d H:i:s', time()),
));

$handle1->rollback();   // A回滚
$handle2->commit();     // B提交

// 如果php正常退出，事务没提交，那么会自动回滚，并且记录日志；
// 如果在同一个mysql连接上发生事务嵌套，那么会回滚，并且抛出异常；

