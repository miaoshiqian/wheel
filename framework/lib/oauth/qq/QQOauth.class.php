<?php

/**
 *  QQ联合登录
 * 官方链接：http://wiki.connect.qq.com/%E7%BD%91%E7%AB%99%E6%8E%A5%E5%85%A5%E6%A6%82%E8%BF%B0
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/12/1
 * Time: 20:06
 */
class QQOauth extends BaseOauth {

	/** 获取Authorization Code */
	const GET_AUTH_CODE_URL = 'https://graph.qq.com/oauth2.0/authorize';
	/** 通过Authorization Code获取Access Token */
	const GET_ACCESS_TOKEN_URL = 'https://graph.qq.com/oauth2.0/token';
	/** 获取openId */
	const GET_OPENID_URL = 'https://graph.qq.com/oauth2.0/me';

	public function __construct() {
		parent::__construct();
		$this->getConfig();
		$this->_clientId     = $this->_config['clientId'];
		$this->_clientSecret = $this->_config['clientSecret'];
	}

	public function getConfig() {
		if (!$this->_config) {
			$this->_config = include __DIR__ . '/conf/oauth.conf.php';
		}
		return $this->_config;
	}

	/**
	 * 获取请求CODE的url
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string
	 * @throws SysException
	 */
	public function getAuthCodeUrl() {
		$params = array(
			'response_type' => 'code',
			'client_id'     => $this->_clientId,
			'redirect_uri'  => $this->getRedirectUrl(),
			'state'         => $this->genState(),
			'scope'         => 'get_user_info',
		);
		// http://wiki.connect.qq.com/%E4%BD%BF%E7%94%A8authorization_code%E8%8E%B7%E5%8F%96access_token
		// 如果用户成功登录并授权，则会跳转到指定的回调地址，并在redirect_uri地址后带上Authorization Code和原始的state值
		return self::GET_AUTH_CODE_URL . '?' . http_build_query($params);
	}

	/**
	 * 获取
	 * @author 李登科<lidengke@xiongying.com>
	 * @return bool
	 * @throws UserException
	 */
	public function getAccessToken() {
		$code  = Request::getStr('code');
		$state = Request::getStr('state');
		if (!$code) {
			return false;
		}
		$this->checkState($state);
		$params     = array(
			'grant_type'    => 'authorization_code',
			'client_id'     => $this->_clientId,
			'client_secret' => $this->_clientSecret,
			'code'          => $code,
			'redirect_uri'  => $this->getRedirectUrl(),
		);
		$requestUrl = self::GET_ACCESS_TOKEN_URL . '?' . http_build_query($params);
		// 如果成功返回，即可在返回包中获取到Access Token。 如：access_token=FE04************************CCE2&expires_in=7776000&refresh_token=88E4************************BE14
		$response = $this->spider->get($requestUrl);
		parse_str($response, $data);
		if (isset($data['access_token'])) {
			$this->tokenInfo = $data;
			return $data['access_token'];
		}
		Log::fatal('oauth.qq.getAccessToken', $response);
		return false;
	}

	/**
	 * 获取用户openid
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string | bool
	 */
	public function getOpenId() {
		// openid	授权用户唯一标识 unionid	只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
		if (!empty($tokenInfo['access_token'])) {
			$requestUrl = self::GET_OPENID_URL . '?access_token=' . $this->tokenInfo['access_token'];
			$response   = $this->spider->get($requestUrl);
			$data       = $this->parseCallbackData($response);
			if (!empty($data['openid'])) {
				return $data['openid'];
			}
			Log::fatal('oauth.qq.getOpenId', $response);
			return false;
		}
		return false;
	}

	/**
	 * 对带callback的数据处理
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string $response
	 *
	 * @return mixed|string
	 */
	public function parseCallbackData($response = '') {
		$ret = '';
		if (strpos($response, "callback") !== false) {
			$lpos     = strpos($response, "(");
			$rpos     = strrpos($response, ")");
			$response = substr($response, $lpos + 1, $rpos - $lpos - 1);
			$ret      = json_decode($response, 1);
		}
		return $ret;
	}

}