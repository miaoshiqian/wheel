<?php

/**
 *
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/12/1
 * Time: 19:02
 */
abstract class BaseOauth {

	protected $_clientId;
	protected $_clientSecret;
	protected $_config;
	protected $tokenInfo = array();

	protected $_saltCode = '%*&^12321^&*%'; // 加密时混淆

	/** @var  Spider */
	protected $spider;
	/** @var  string 当前是哪个平台类型 */
	protected $platformType;

	public function __construct() {
		$this->spider = new Spider();
		$this->getConfig();
		$this->_clientId     = $this->_config['clientId'];
		$this->_clientSecret = $this->_config['clientSecret'];
	}

	public function getTokenInfo() {
		return $this->tokenInfo;
	}


	/**
	 * 获取 授权后的跳转url
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string
	 * @throws SysException
	 */
	public function getRedirectUrl() {
		$url = $this->_config['redirectUri'];
		$url = str_replace('www.qingchuangba.com', 'sim.qingchuangba.com', $url); // todo remove online

		return $url;
	}

	/**
	 * 生成state 验证请求合法性 防止csrf
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string
	 */
	public function genState() {
		$now = time();
		return $this->_genState($now) . '|' . $now;
	}

	protected function _genState($requestTime) {
		if ($requestTime) {
			return md5($this->_saltCode . $requestTime);
		}
		return false;
	}


	/**
	 * 检查state
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $stateStr
	 *
	 * @return bool
	 * @throws UserException
	 */
	public function checkState($stateStr) {
		if (is_string($stateStr)) {
			$tmpArr    = explode('|', $stateStr);
			$code      = isset($tmpArr[0]) ? $tmpArr[0] : '';
			$timestamp = isset($timestamp[1]) ? $tmpArr[1] : '';
			$genCode   = $this->_genState($timestamp);
			if ($code != $genCode) {
				Log::fatal(sprintf('oauth.%s.checkSate', $this->getPlatformType()), array($stateStr, $genCode));
				return false;
			}

		}

		return true;
	}

	/**
	 *  简介:
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string $type 平台类型
	 *
	 * @return mixed
	 */
	public function setPlatformType($type) {
		$this->platformType = $type;
	}

	public function getPlatformType() {
		return $this->platformType;
	}

	abstract function getConfig();

	/**
	 * 获取网站
	 * @author 李登科 <lidengke@xiongying.com>
	 * @return string
	 */
	abstract function getAuthCodeUrl();

	/**
	 * 获取access token
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string
	 */
	abstract function getAccessToken();

	/**
	 * 获取openId
	 * @author 李登科<lidengke@xiongying.com>
	 * @return mixed
	 */
	abstract function getOpenId();
}