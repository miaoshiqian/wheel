<?php

/**
 *  新浪微博登录
 * 官方链接:http://open.weibo.com/wiki/%E6%8E%88%E6%9D%83%E6%9C%BA%E5%88%B6%E8%AF%B4%E6%98%8E
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/12/1
 * Time: 20:06
 */
class SinaOauth extends BaseOauth {

	/** 获取Authorization Code */
	const GET_AUTH_CODE_URL = 'https://api.weibo.com/oauth2/authorize';
	/** 通过Authorization Code获取Access Token */
	const GET_ACCESS_TOKEN_URL = 'https://api.weibo.com/oauth2/access_token';
	/** 获取openId */
	const GET_OPENID_URL = 'https://graph.qq.com/oauth2.0/me';

	public function __construct() {
		parent::__construct();
	}

	public function getConfig() {
		if (!$this->_config) {
			$this->_config = include __DIR__ . '/conf/oauth.conf.php';
		}
		return $this->_config;
	}

	/**
	 * 获取请求CODE的url
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string
	 * @throws SysException
	 */
	public function getAuthCodeUrl() {
		$params = array(
			'client_id'     => $this->_clientId,
			'redirect_uri'  => $this->getRedirectUrl(),
			'response_type' => 'code',
			'state'         => $this->genState(),
//			'scope'         => '',
		);
		// http://open.weibo.com/wiki/Oauth2/authorize
		return self::GET_AUTH_CODE_URL . '?' . http_build_query($params);
	}

	/**
	 * 获取
	 * @author 李登科<lidengke@xiongying.com>
	 * @return bool
	 * @throws UserException
	 */
	public function getAccessToken() {
		$code  = Request::getStr('code');
		$state = Request::getStr('state');
		if (!$code) {
			return false;
		}
		$this->checkState($state);
		$params     = array(
			'client_id'     => $this->_clientId,
			'client_secret' => $this->_clientSecret,
			'grant_type'    => 'authorization_code',
			'code'          => $code,
			'redirect_uri'  => $this->getRedirectUrl(), // 回调地址，需需与注册应用里的回调地址一致。
		);
		$requestUrl = self::GET_ACCESS_TOKEN_URL . '?' . http_build_query($params);
		$response   = $this->spider->get($requestUrl);
		$data       = json_decode($response, 1);
		if (isset($data['access_token'])) {
			$this->tokenInfo = $data;
			return $data['access_token'];
		}
		Log::fatal('oauth.sina.getAccessToken', $response);
		return false;
	}

	/**
	 * 获取用户openid
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string | bool
	 */
	public function getOpenId() {
		if (!empty($this->tokenInfo['uid'])) { // 当前授权用户的UID。// todo 确认sina用的openid是否为uid
			return $this->tokenInfo['uid'];
		}

		Log::fatal('oauth.sina.getOpenId', $this->tokenInfo);
		return false;
	}


}