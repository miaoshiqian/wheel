<?php

/**
 *  微信登录
 * 官方链接：https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1419316505&token=&lang=zh_CN
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/12/1
 * Time: 20:06
 */
class WeixinOauth extends BaseOauth {

	/** 获取Authorization Code */
	const GET_AUTH_CODE_URL = 'https://open.weixin.qq.com/connect/qrconnect';
	/** 通过Authorization Code获取Access Token */
	const GET_ACCESS_TOKEN_URL = 'https://api.weixin.qq.com/sns/oauth2/access_token';

	public function __construct() {
		parent::__construct();
	}

	public function getConfig() {
		if (!$this->_config) {
			$this->_config = include __DIR__ . '/conf/oauth.conf.php';
		}
		return $this->_config;
	}

	/**
	 * 获取请求CODE的url
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string
	 * @throws SysException
	 */
	public function getAuthCodeUrl() {
		$params = array(
			'appid'         => $this->_clientId,
			'redirect_uri'  => $this->getRedirectUrl(),
			'response_type' => 'code',
			'scope'         => 'snsapi_login',
			'state'         => $this->genState(),
		);
		// 用户允许授权后，将会重定向到redirect_uri的网址上，并且带上code和state参数 redirect_uri?code=CODE&state=STATE
		return self::GET_AUTH_CODE_URL . '?' . http_build_query($params);
	}

	/**
	 * 获取
	 * @author 李登科<lidengke@xiongying.com>
	 * @return bool
	 * @throws UserException
	 */
	public function getAccessToken() {
		$code  = Request::getStr('code');
		$state = Request::getStr('state');
		if (!$code) {
			return false;
		}
		$this->checkState($state);
		// https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1419316505&token=&lang=zh_CN
		$params     = array(
			'appid'      => $this->_clientId,
			'secret'     => $this->_clientSecret,
			'code'       => $code,
			'grant_type' => 'authorization_code',
		);
		$requestUrl = self::GET_ACCESS_TOKEN_URL . '?' . http_build_query($params);
		$response   = $this->spider->get($requestUrl);
		$data       = json_decode($response, 1);
		if (isset($data['access_token'])) {
			$this->tokenInfo = $data;
			return $data['access_token'];
		}
		Log::fatal('oauth.weixin.getAccessToken', $response);
		return false;
	}

	/**
	 * 获取用户openid
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string | bool
	 */
	public function getOpenId() {
		// openid	授权用户唯一标识 unionid	只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
		if (!empty($this->tokenInfo['openid'])) {
			return $this->tokenInfo['openid'];
		}

		Log::fatal('oauth.weixin.getOpenId', $this->tokenInfo);
		return false;
	}

}