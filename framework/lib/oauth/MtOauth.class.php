<?php
/**
 *
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/12/1
 * Time: 21:36
 */

class MtOauth {
	const TYPE_WEIXIN = 'weixin';
	const TYPE_QQ = 'qq';
	const TYPE_SINA = 'sina';
	const TYPE_BAIDU = 'baidu';
	const TYPE_RENREN = 'renren';

	/** @var BaseOauth */
	protected static $_instance = null;

	public static function getInstance($oauthType) {
		switch($oauthType){
			case self::TYPE_WEIXIN :
				include __DIR__.'/weixin/WeixinOauth.class.php';
				self::$_instance = new WeixinOauth();
				break;

			case self::TYPE_QQ :
				include __DIR__.'/qq/QQOauth.class.php';
				self::$_instance = new QQOauth();
				break;

			case self::TYPE_SINA :
				include __DIR__.'/sina/SinaOauth.class.php';
				self::$_instance = new SinaOauth();
				break;

			case self::TYPE_BAIDU :
				include __DIR__.'/baidu/BaiduOauth.class.php';
				self::$_instance = new BaiduOauth();
				break;

			case self::TYPE_RENREN :
				include __DIR__.'/renren/RenrenOauth.class.php';
				self::$_instance = new RenrenOauth();
				break;

			default :
				return false;
		}
		self::$_instance->setPlatformType($oauthType);
		return self::$_instance;
	}

	public static function getTypeList(){
		return array(
			self::TYPE_WEIXIN, self::TYPE_QQ, self::TYPE_SINA, self::TYPE_BAIDU, self::TYPE_RENREN
		);
	}
}