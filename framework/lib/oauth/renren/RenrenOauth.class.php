<?php

/**
 *  人人网登录
 * 官方链接:http://open.renren.com/wiki/OAuth2.0
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/12/1
 * Time: 20:06
 */
class RenrenOauth extends BaseOauth {

	/** 获取Authorization Code */
	const GET_AUTH_CODE_URL = 'https://graph.renren.com/oauth/authorize';
	/** 通过Authorization Code获取Access Token */
	const GET_ACCESS_TOKEN_URL = 'https://graph.renren.com/oauth/token';
	/** 获取openId */
	const GET_OPENID_URL = 'https://openapi.baidu.com/rest/2.0/passport/users/getLoggedInUser';

	public function __construct() {
		parent::__construct();
	}

	public function getConfig() {
		if (!$this->_config) {
			$this->_config = include __DIR__ . '/conf/oauth.conf.php';
		}
		return $this->_config;
	}

	/**
	 * 获取请求CODE的url
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string
	 * @throws SysException
	 */
	public function getAuthCodeUrl() {
		$params = array(
			'client_id'     => $this->_clientId,
			'response_type' => 'code',
			'redirect_uri'  => $this->getRedirectUrl(),
			'state'         => $this->genState(),
//			'scope'         => '',
		);
		// http://open.renren.com/wiki/OAuth2.0
		return self::GET_AUTH_CODE_URL . '?' . http_build_query($params);
	}

	/**
	 * 获取
	 * @author 李登科<lidengke@xiongying.com>
	 * @return bool
	 * @throws UserException
	 */
	public function getAccessToken() {
		$code  = Request::getStr('code');
		$state = Request::getStr('state');
		if (!$code) {
			return false;
		}
		$this->checkState($state);
		$params     = array(
			'grant_type'    => 'authorization_code',
			'client_id'     => $this->_clientId,
			'client_secret' => $this->_clientSecret,
			'redirect_uri'  => $this->getRedirectUrl(), // 回调地址，需需与注册应用里的回调地址一致。
			'code'          => $code,
		);
		$requestUrl = self::GET_ACCESS_TOKEN_URL . '?' . http_build_query($params);
		$response   = $this->spider->get($requestUrl);
		$data       = json_decode($response, 1);
		if (isset($data['access_token'])) {
			$this->tokenInfo = $data;
			return $data['access_token'];
		}
		Log::fatal('oauth.renren.getAccessToken', $response);
		return false;
	}

	/**
	 * 获取用户openid
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string | bool
	 */
	public function getOpenId() {
		if (!empty($this->tokenInfo['user']['id'])) { // 当前授权用户的UID。
			return $this->tokenInfo['user']['id'];
		}

		Log::fatal('oauth.renren.getOpenId', $this->tokenInfo);
		return false;
	}


}