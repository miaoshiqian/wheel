<?php
/**
 * 微信支付
 * @author guochaohui
 */
include_once("WxPayPubHelper/WxPayPubHelper.php");

class Tenpay2 {

    /**
     * 微信支付调用接口
     */
    public static function jsApiCall($params) {
        //第一步获取openid
        $openid = Util::getFromArray('open_id', $params, '');
        if (empty($openid)) {
            $openid = self::_getOpenId($params);
        }

        //第二步,获取prepay_id
        $prepay_id = self::_getPrePayId($openid, $params);
        //第三部获取支付参数,并渲染到js中
        $jsApiParameters = self::_getPayParams($prepay_id);
        return $jsApiParameters;
    }

    /**
     * 微信支付第一步:网页授权获取openid
     */
    private static function _getOpenId($params) {
        $jsApi = new JsApi_pub();
        if (!isset($_GET['code'])) {
            //触发微信返回code码
            $url = $jsApi->createOauthUrlForCode(WxPayConf_pub::JS_API_CALL_URL . '?pay_id=' . Util::getFromArray('pay_id', $params) . '&wx_pay_id=' . Util::getFromArray('wx_pay_id', $params) . '&money=' . Util::getFromArray('money', $params, 0));
            Header("Location: $url");
        } else {
            //获取code码，以获取openid
            $code = $_GET['code'];
            $jsApi->setCode($code);
            $openid = $jsApi->getOpenId();
        }
        return $openid;
    }

    /**
     * 第二步:使用统一支付接口,获取prepay_id,
     */

    private static function _getPrePayId($openid, $params) {
        $unifiedOrder = new UnifiedOrder_pub();
        $unifiedOrder->setParameter("openid", "$openid");//商品描述
        $unifiedOrder->setParameter("body", Util::getFromArray('body', $params));//商品描述
        $unifiedOrder->setParameter("out_trade_no", Util::getFromArray('pay_id', $params));//商户订单号
        $unifiedOrder->setParameter("total_fee", Util::getFromArray('money', $params));//总金额
        $unifiedOrder->setParameter("notify_url", WxPayConf_pub::NOTIFY_URL);//通知地址
        $unifiedOrder->setParameter("trade_type", "JSAPI");//交易类型
        $prepay_id = $unifiedOrder->getPrepayId();
        return $prepay_id;
    }

    /**
     * 第三步:使用jsapi调起支付,返回支付参数
     */
    private static function _getPayParams($prepay_id) {
        $jsApi = new JsApi_pub();
        $jsApi->setPrepayId($prepay_id);
        $jsApiParameters = $jsApi->getParameters();
        return $jsApiParameters;
    }
}