<?php
/**
 * 接受微信回调信息,记录日志,回复微信服务器
 */
include_once("WxPayPubHelper/WxPayPubHelper.php");
include_once("demo/log_.php");
class Notify {
    /**
     * 执行整个流程,(对外接口)
     */
    public static function init() {
        //流程一:接受微信消息,验证签名,并回复微信
        self::_checkSign();

        //流程二:记录日志
        self::_log();
    }

    /**
     * 流程一:接受微信消息,验证签名,并回复微信
     */
    private static function _checkSign() {
        //使用通用通知接口
        $notify = new Notify_pub();
        //存储微信的回调
        $xml = $GLOBALS['HTTP_RAW_POST_DATA'];
        $notify->saveData($xml);
        //验证签名，并回应微信。
        //对后台通知交互时，如果微信收到商户的应答不是成功或超时，微信认为通知失败，
        //微信会通过一定的策略（如30分钟共8次）定期重新发起通知，
        //尽可能提高通知的成功率，但微信不保证通知最终能成功。
        if($notify->checkSign() == FALSE){
            $notify->setReturnParameter("return_code","FAIL");//返回状态码
            $notify->setReturnParameter("return_msg","签名失败");//返回信息
        }else{
            $notify->setReturnParameter("return_code","SUCCESS");//设置返回码
        }
        $returnXml = $notify->returnXml();
        echo $returnXml;
    }

    /**
     * 记录日志
     */
    private static function _log() {
        $xml = $GLOBALS['HTTP_RAW_POST_DATA'];
        $notify = new Notify_pub();
        //以log文件形式记录回调信息
        $log_ = new Log_();
        $log_name="/tmp/notify_url.log";//log文件路径
        $log_->log_result($log_name,"【接收到的notify通知】:\n".$xml."\n");

        if($notify->checkSign() == TRUE)
        {
            if ($notify->data["return_code"] == "FAIL") {
                //此处应该更新一下订单状态，商户自行增删操作
                $log_->log_result($log_name,"【通信出错】:\n".$xml."\n");
            }
            elseif($notify->data["result_code"] == "FAIL"){
                //此处应该更新一下订单状态，商户自行增删操作
                $log_->log_result($log_name,"【业务出错】:\n".$xml."\n");
            }
            else{
                //此处应该更新一下订单状态，商户自行增删操作
                $log_->log_result($log_name,"【支付成功】:\n".$xml."\n");
            }

            //商户自行增加处理流程,
            //例如：更新订单状态
            //例如：数据库操作
            //例如：推送支付完成信息
        }
    }
}