<?php
/**
 *  发送邮件工具类
 * @author 缪石乾<miaoshiqian@iyuesao.com>
 * @date   2015年2月10日 上午11:11:17
 */
require_once dirname(__FILE__) . '/../../../conf/common/SmtpConfig.class.php';
require_once dirname(__FILE__) . '/Smtp.class.php';

class Email {
	/**
	 * 发送邮件方法
	 *
	 * @param $title 邮件标题
	 * @param $content 邮件内容
	 * @param $mailList 收件箱、可以一个、可以是数组发送多个
	 * @return bool
	 */
	public static function send($title, $content, $mailList) {
		$requireParams = array(
			'title' => $title,
			'content' => $content,
			'mailList' => $mailList
		);

		Util::checkEmpty($requireParams);

		if (extension_loaded('gearman')) {
			include_once(dirname(__FILE__) . '/../gearman/Gearman.class.php');
			Gearman::call('email', $params);
			return false;
		} else {
			return self::smtpSend($params);
		}
	}

	public static function smtpSend($params) {
		$smtp        = new Smtp(
			SmtpConfig::$smtpServer,
			SmtpConfig::$smtpServerport,
			true,
			SmtpConfig::$smtpUser,
			base64_decode(SmtpConfig::$smtpPass)
		);
		$smtp->debug = false;

		if (is_array($params['mailList'])) {
			$mailList = implode(',', $params['mailList']);
		} elseif (is_string($params['mailList'])) {
			$mailList = $params['mailList'];
		} else {
			return;
		}

		if (isset($params['mailType']) && in_array($params['mailType'], array('TEXT', 'HTML'))) {
			$mailType = $params['mailType'];
		} else {
			$mailType = 'HTML';
		}

		if (isset($params['charset']) && in_array($params['charset'], array('gb2312', 'gbk', 'utf-8'))) {
			$charset = $params['charset'];
		} else {
			$charset = 'utf-8';
		}

		return $smtp->sendmail(
			$mailList,
			SmtpConfig::$smtpUsermail,
			$params['title'],
			$params['content'],
			$mailType,
			$charset
		);
	}
}