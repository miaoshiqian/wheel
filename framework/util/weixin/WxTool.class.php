<?php
if (!defined('CONF_PATH')) {
    define('CONF_PATH', dirname(__FILE__) . '/../../../conf/conf');
}

require_once dirname(__FILE__) . '/../http/Request.class.php';

class WxTool {

    public static function createOAuthUrlForCode($params) {
        $scope = isset($params['scope']) && $params['scope'] == 'snsapi_userinfo' ? 'snsapi_userinfo' : 'snsapi_base';
        $state = 1;
        if ($scope == 'snsapi_userinfo') {
            $state = 2;
        }
        $urlObj["appid"]         = $params['appid'];
        $urlObj["redirect_uri"]  = urlencode($params['redirect_uri']);
        $urlObj["response_type"] = "code";
        $urlObj["scope"]         = $scope;
        $urlObj["state"]         = $state . "#wechat_redirect";
        $bizString               = self::_formatBizQueryParaMap($urlObj, false);
        return "https://open.weixin.qq.com/connect/oauth2/authorize?" . $bizString;
    }

    /**
     *    作用：格式化参数，签名过程需要使用
     */
    private static function _formatBizQueryParaMap($paraMap, $urlencode) {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }
            //$buff .= strtolower($k) . "=" . $v . "&";
            $buff .= $k . "=" . $v . "&";
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

    /**
     *    作用：通过curl向微信提交code，以获取openid
     */
    public static function getOpenid($code, $appId, $appSecret) {
        if (empty($code)) {
            return '';
        }
        $url = self::createOauthUrlForOpenid($code, $appId, $appSecret);
        //初始化curl
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOP_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //运行curl，结果以jason形式返回
        $res = curl_exec($ch);
        curl_close($ch);
        //取出openid
        $data   = json_decode($res, true);
        $openId = $data['openid'];
        return $openId;
    }

    /**
     *    作用：生成可以获得openid的url
     */
    public static function createOauthUrlForOpenid($code, $appId, $appSecret) {
        $urlObj["appid"]      = $appId;
        $urlObj["secret"]     = $appSecret;
        $urlObj["code"]       = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString            = self::_formatBizQueryParaMap($urlObj, false);
        return "https://api.weixin.qq.com/sns/oauth2/access_token?" . $bizString;
    }

    public static function createSnsapiUserinfoUrl($accessToken, $openId) {
        $urlObj['access_token'] = $accessToken;
        $urlObj['openid'] = $openId;
        $urlObj['lang'] = 'zh_CN';
        $bizString = self::_formatBizQueryParaMap($urlObj, false);
        return 'https://api.weixin.qq.com/sns/userinfo?' . $bizString;
    }
}