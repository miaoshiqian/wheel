<?PHP
//中文分词

class Scws {

	private $cws;

    // $dictExtFilePath 自定义词典路径
	public function __construct($charset = 'utf8') {
		if ($charset != 'utf8' && $charset != 'gbk') {
			throw new SysException('编码不正确');
		}
		$ext = $charset == 'utf8' ? '.utf8' : '';
		$this->cws = scws_new(); 
		$this->cws->set_charset($charset); 
		$this->cws->set_rule("/usr/local/etc/rules{$ext}.ini"); 
		$this->cws->set_dict("/usr/local/etc/dict{$ext}.xdb"); 
		$this->cws->add_dict(__DIR__ . '/user_dict.txt', SCWS_XDICT_TXT); //自定义词库
		$this->cws->set_multi(true);
	}

	//  $ignore 是否过滤标点符号
	public function exec($text, $ignore = false) {
		$this->cws->set_ignore((bool) $ignore);
		$this->cws->send_text($text); 
		$ret = array(); 
		while ($keys = $this->cws->get_result()) { 
			 if (!empty($keys)) foreach ($keys as $word) { 
				  $ret[] = $word['word']; 
			 } 
		}
		return($ret);
	}
	
}