<?php

/**
 *  封装mysqli
 * @author longwg
 */
class DBMysqli {

	// 编码定义
	const ENCODING_GBK = 0;
	const ENCODING_UTF8 = 1;
	const ENCODING_LATIN = 2;
	const ENCODING_UTF8MB4 = 3;

	// 数据库句柄需要ping
	const HANDLE_PING = 100;

	const SLOW_QUERY_1 = 500;
	const SLOW_QUERY_2 = 2000;

	private static $_lastSql = '';
	/**
	 * @biref 数据库句柄是否需要ping
	 */
	private static $_HANDLE_PING = false;

	/**
	 * 已打开的db handle
	 *
	 * @var array
	 */
	private static $_HANDLE_ARRAY = array();

	private static function _getHandleKey($params) {
		ksort($params);
		return md5(implode('_', $params));
	}

	/**
	 * 设置 handle ping的属性
	 *
	 * @param $value
	 *
	 * @example
	 *        如果是crontabe 建议在开始是增加
	 *        DBMysql::setHandlePing(self::HANDLE_PING);
	 *        如果关闭可以设置 false, 默认是不开启的
	 *        DBMysql::setHandlePing();
	 */
	public static function setHandlePing($value = false) {
		self::$_HANDLE_PING = $value;
	}

	/**
	 * 根据数据库表述的参数获取数据库操作句柄
	 *
	 * @param array  $db_config_array , 是一个array类型的数据结构，必须有host, username, password 三个熟悉, port为可选属性， 缺省值分别为3306
	 * @param string $db_name         , 数据库名称
	 * @param int    $encoding        , 从数据库编码相关的常量定义获取, 有缺省值ENCODING_UTF8
	 *
	 * @return false|object 非FALSE表示成功获取hadnle， 否则返回FALSE
	 */
	public static function createDBHandle($db_config_array, $db_name, $encoding = self::ENCODING_UTF8) {

		$db_config_array['db_name']  = $db_name;
		$db_config_array['encoding'] = $encoding;
		$handle_key                  = self::_getHandleKey($db_config_array);

		//有存在连接句柄的情况，并且是命令行请求进行ping
		if (isset(self::$_HANDLE_ARRAY[$handle_key])) {
			// MSC-2077 命令行模式
			if ((PHP_SAPI === 'cli' || self::$_HANDLE_PING === true) && method_exists(self::$_HANDLE_ARRAY[$handle_key], 'ping')) {
				if (self::$_HANDLE_ARRAY[$handle_key]->ping()) {
					return self::$_HANDLE_ARRAY[$handle_key];
				} else {
					mysqli_close(self::$_HANDLE_ARRAY[$handle_key]);
				}
			} else {
				return self::$_HANDLE_ARRAY[$handle_key];
			}
		}

		$port = 3306;
		do {
			if (!is_array($db_config_array)) {
				break;
			}
			if (!is_string($db_name)) {
				break;
			}
			if (strlen($db_name) == 0) {
				break;
			}
			if (!array_key_exists('host', $db_config_array)) {
				break;
			}
			if (!array_key_exists('username', $db_config_array)) {
				break;
			}
			if (!array_key_exists('password', $db_config_array)) {
				break;
			}
			if (array_key_exists('port', $db_config_array)) {
				$port = (int) ($db_config_array['port']);
				if (($port < 1024) || ($port > 65535)) {
					break;
				}
			}
			$host = $db_config_array['host'];
			if (strlen($host) == 0) {
				break;
			}
			$username = $db_config_array['username'];
			if (strlen($username) == 0) {
				break;
			}
			$password = $db_config_array['password'];
			if (strlen($password) == 0) {
				break;
			}

			$handle = @mysqli_connect($host, $username, $password, $db_name, $port);
			// 如果连接失败，再重试2次
			for ($i = 1; ($i < 3) && (false === $handle); $i++) {
				// 重试前需要sleep 50毫秒
				usleep(50000);
				$handle = @mysqli_connect($host, $username, $password, $db_name, $port);
			}
			if (false === $handle) {
				break;
			}

			$is_encoding_set_success = true;
			switch ($encoding) {
				case self::ENCODING_UTF8 :
					$is_encoding_set_success = mysqli_set_charset($handle, "utf8");
					break;
				case self::ENCODING_GBK :
					$is_encoding_set_success = mysqli_set_charset($handle, "gbk");
					break;
				case self::ENCODING_UTF8MB4 :
					$is_encoding_set_success = mysqli_set_charset($handle, "utf8mb4");
					break;
				default:
			}
			if (false === $is_encoding_set_success) {
				mysqli_close($handle);
				break;
			}
			self::$_HANDLE_ARRAY[$handle_key] = $handle;
			return $handle;
		} while (false);

		$password_part        = substr($password, 0, 5) . '...';
		$logArray             = $db_config_array;
		$logArray['password'] = $password_part;
		Log::fatal('mysql.error.connect', sprintf("Connect failed:db_config_array=%s", var_export($logArray, true)));

		return false;
	}

	/**
	 * 释放通过getDBHandle或者getDBHandleByName 返回的句柄资源
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param resource $handle 句柄资源
	 *
	 * @return int
	 */
	public static function releaseDBHandle($handle = null) {
		//if (!self::_checkHandle($handle)) {
		//return;
		//}
		if (empty($handle)) {
			foreach (self::$_HANDLE_ARRAY as $handle_key => $handleObj) {
				mysqli_close($handleObj);
			}
			self::$_HANDLE_ARRAY = array();
			return 0;
		}
		foreach (self::$_HANDLE_ARRAY as $handle_key => $handleObj) {
			if ($handleObj->thread_id == $handle->thread_id) {
				mysqli_close($handle);
				unset(self::$_HANDLE_ARRAY[$handle_key]);
			}
		}
	}

	/**
	 * 执行sql语句， 该语句必须是insert, update, delete, create table, drop table等更新语句
	 *
	 * @param resource $handle , 操作数据库的句柄
	 * @param string   $sql    , 具体执行的sql语句
	 *
	 * @return bool TRUE:表示成功， FALSE:表示失败
	 */
	public static function execute($handle, $sql) {
		if (!self::_checkHandle($handle)) {
			return false;
		}

		if (self::_query($handle, $sql)) {
			return true;
		}

		return false;
	}

	/**
	 * 执行insert sql语句，并获取执行成功后插入记录的id
	 *
	 * @param [in] handle $handle, 操作数据库的句柄
	 * @param [in] string $sql, 具体执行的sql语句
	 *
	 * @return false|int FALSE表示执行失败， 否则返回insert的ID
	 */
	public static function insertAndGetID($handle, $sql) {
		if (!self::_checkHandle($handle)) {
			return false;
		}
		do {
			if (false === self::_query($handle, $sql)) {
				break;
			}
			if ((false === $result = mysqli_query($handle, 'select LAST_INSERT_ID() AS LastID'))) {
				break;
			}
			$row    = mysqli_fetch_assoc($result);
			$lastId = $row['LastID'];
			mysqli_free_result($result);
			return $lastId;
		} while (false);

		return false;
	}

	/**
	 * 将所有结果存入数组返回
	 *
	 * @param [in] handle $handle, 操作数据库的句柄
	 * @param [in] string $sql, 具体执行的sql语句
	 *
	 * @return false|array FALSE表示执行失败， 否则返回执行的结果, 结果格式为一个数组，数组中每个元素都是mysqli_fetch_assoc的一条结果
	 */
	public static function queryAll($handle, $sql) {
		if (!self::_checkHandle($handle)) {
			return false;
		}
		do {
			if ((false === $result = self::_query($handle, $sql))) {
				break;
			}

			$res = array();
			while ($row = mysqli_fetch_assoc($result)) {
				$res[] = $row;
			}
			mysqli_free_result($result);
			return $res;
		} while (false);

		return false;
	}

	/**
	 * 将查询的第一条结果返回
	 *
	 * @param [in] handle $handle, 操作数据库的句柄
	 * @param [in] string $sql, 具体执行的sql语句
	 *
	 * @return false|array FALSE表示执行失败， 否则返回执行的结果, 执行结果就是mysqli_fetch_assoc的结果
	 */
	public static function queryRow($handle, $sql) {
		if (!self::_checkHandle($handle)) {
			return false;
		}
		do {
			if ((false === $result = self::_query($handle, $sql))) {
				break;
			}

			$row = mysqli_fetch_assoc($result);
			mysqli_free_result($result);
			return $row;
		} while (false);

		return false;
	}

	/**
	 * 查询第一条结果的第一列
	 *
	 * @param resource $handle
	 * @param string   $sql
	 *
	 * @return array|false
	 */
	public static function queryOne($handle, $sql) {
		$row = self::queryRow($handle, $sql);
		if (is_array($row)) {
			return current($row);
		}
		return $row;
	}

	/**
	 * 得到最近一次操作影响的行数
	 *
	 * @param [in] handle $handle, 操作数据库的句柄
	 *
	 * @return bool|int FALSE表示执行失败， 否则返回影响的行数
	 */
	public static function lastAffected($handle) {
		$affected_rows = mysqli_affected_rows($handle);
		if ($affected_rows < 0) {
			return false;
		}
		return $affected_rows;
	}

	/**
	 * 得到最近一次操作错误的信息
	 *
	 * @param [in] handle $handle, 操作数据库的句柄
	 *
	 * @return bool false表示执行失败， 否则返回 'errorno: errormessage'
	 */
	public static function getLastError($handle) {
		if (!self::_checkHandle($handle)) {
			return false;
		}
		if (mysqli_errno($handle)) {
			return mysqli_errno($handle) . ': ' . mysqli_error($handle);
		}
		return false;
	}

	/**
	 * 检查handle
	 *
	 * @param [in] handle $handle, 操作数据库的句柄
	 *
	 * @return boolean true|成功, false|失败
	 */
	private static function _checkHandle($handle) {
		if (!is_object($handle)) {
			Log::fatal('mysql.error.handle', sprintf("handle Error: handle='%s'", var_export($handle, true)));
			return false;
		}

		// MSC-2077 命令行模式 或者 手工指定 handle_ping
		if ((PHP_SAPI === 'cli' || self::$_HANDLE_PING === self::HANDLE_PING)
			&& method_exists($handle, 'ping')
		) {
			// 做3次ping
			$tryCount = 3;
			$pingRet  = false;
			do {
				$pingRet = $handle->ping();
				if ($pingRet) {
					break;
				}
				$tryCount--;
			} while ($tryCount > 0);
			if (!$pingRet) {
				Log::fatal('mysql.error.handle.ping', sprintf("handle_ping Error: handle='%s'", var_export($handle, true)));
				return false;
			}
		}
		return true;
	}

	protected static function getMicrosecond() {
		//list($usec, $sec) = explode(" ", microtime());
		return microtime(true);
	}

	private static function _setLastSql($sql) {
		self::$_lastSql = $sql;
	}

	public static function getLastSql() {
		return self::$_lastSql;
	}

	/**
	 *  简介:封装一下mysqli_query，方便统一修改
	 *
	 * @param resource $handel
	 * @param string   $sql
	 * @param null|int $resultMode MYSQLI_STORE_RESULT or MYSQLI_STORE_RESULT
	 *
	 * @return bool|mysqli_result
	 */
	private static function _query($handel, $sql, $resultMode = MYSQLI_STORE_RESULT) {
		self::_setLastSql($sql);
		$tm = self::getMicrosecond();
		try {
			$result = @mysqli_query($handel, $sql, $resultMode); // Returns FALSE on failure. For successful SELECT, SHOW, DESCRIBE or EXPLAIN queries mysqli_query() will return a mysqli_result object. For other successful queries mysqli_query() will return TRUE.

			if ($result === false) {
				Log::fatal('mysql.error.sql', $sql . self::getLastError($handel));
			}
		} catch (Exception $e) {
			Log::exception('mysql.error.sql', $e);
			throw $e;
		}

		$tmUsed  = self::getMicrosecond() - $tm;
		$tmp     = intval($tmUsed * 1000);
		$logData = "spend={$tmUsed}毫秒, SQL={$sql}";
		if ($tmp > self::SLOW_QUERY_2) {
			Log::warn('mysql.slowest', $logData);
		} elseif ($tmp >= self::SLOW_QUERY_1) {
			Log::warn('mysql.slow', $logData);
		}

		return $result;
	}
}

function debugBackTrace($filters = array()) {
	$debug = debug_backtrace();

	$result = $debug;
	if (!empty($debug) && !empty($filters)) {
		foreach ($debug as $k => $arr) {
			foreach ($filters as $filter) {
				if (array_key_exists($filter, $arr)) {
					unset($result[$k][$filter]);
				}
			}
		}
	}
	//处理格式
	$res = array();
	if ($result) {
		foreach ($result as $key => &$value) {
			if (array_key_exists('file', $value)) {
				if (strstr($value['file'], 'DBMysqli.class.php') ||
					strstr($value['file'], 'BaseModel.class.php') ||
					strstr($value['file'], 'Dispatch.class.php') ||
					strstr($value['file'], 'Bootstrap.class.php') ||
					strstr($value['file'], 'index.php')
				) {
					unset($result[$key]);
				} else {
					$value = $value['file'] . '[' . $value['line'] . ']';
				}

			}
		}

		foreach ($result as $rkey => $rvalue) {
			$res[] = $rvalue;
		}
	}
	return $res;
}
