<?php

/**
 *  model基类
 *
 * @author longwg
 */

require_once dirname(__FILE__) . '/DBMysqli.class.php';
require_once dirname(__FILE__) . '/SqlBuilder.class.php';

abstract class BaseModel {

	// 以下成员变量必须在子类中指定
	protected $dbName = '';
	protected $tableName = '';
	protected $fieldTypes = [];
	protected $dbMasterConfig = [];
	protected $dbSlaveConfig = [];

	protected $idFieldName = 'id';

	// 基类成员变量
	protected $sqlBuilder;

	static private $masterConns = array();
	static private $slaveConns = array();
	static private $sqlBuilders = array();
	static private $key = 0;
	protected $myKey = 0;

	// 编码定义
	const ENCODING_GBK = 0;
	const ENCODING_UTF8 = 1;
	const ENCODING_LATIN = 2;
	const ENCODING_UTF8MB4 = 3;

	protected $charset;

	public function __construct() {
		self::$key++;
		$this->myKey = self::$key;

		$this->init();

		if (empty($this->dbName)) {
			trigger_error('未指定库名', E_USER_ERROR);
		}
		if (empty($this->tableName)) {
			trigger_error('未指定表名', E_USER_ERROR);
		}
		if (empty($this->fieldTypes)) {
			trigger_error('未指定字段类型', E_USER_ERROR);
		}
	}

	/**
	 * 获取当前model实例
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 * @return BaseModel
	 */
	public static function getInstance() {
		static $instances = array();

		$class = get_called_class();
		if (!isset($instances[$class])) {
			$instances[$class] = new $class();
		}
		return $instances[$class];
	}

	/**
	 * 子类实现init方法，类实例化后调用
	 * 在子类中实现，必须指定以下变量：
	 * this->dbName,
	 * $this->dbName,
	 * $this->tableName,
	 * $this->fieldTypes,
	 * $this->dbMasterConfig,
	 * $this->dbSlaveConfig
	 */
	abstract protected function init();

	/**
	 * 获取主库链接
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 * @return false|object
	 * @throws Exception
	 */
	protected function getMasterConn() {
		if (empty(self::$masterConns[$this->myKey])) {
			if (empty($this->dbMasterConfig)) {
				trigger_error('未指定主库连接配置', E_USER_ERROR);
			}
			$this->charset      = empty($this->charset) ? 1 : $this->charset;
			self::$masterConns[$this->myKey] = DBMysqli::createDBHandle($this->dbMasterConfig, $this->dbName, $this->charset);
			if (self::$masterConns[$this->myKey] === false) {
				throw new Exception("数据库:{$this->dbName} master 连接失败");
			}
		}
		return self::$masterConns[$this->myKey];
	}

	/**
	 * 获取从库连接
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 * @return false|object 失败|连接对象
	 * @throws Exception
	 */
	protected function getSlaveConn() {
		if (empty(self::$slaveConns[$this->myKey])) {
			if (empty($this->dbSlaveConfig)) {
				trigger_error('未指定从库连接配置', E_USER_ERROR);
			}
			$this->charset     = empty($this->charset) ? 1 : $this->charset;
			self::$slaveConns[$this->myKey] = DBMysqli::createDBHandle($this->dbSlaveConfig, $this->dbName, $this->charset);
			if (self::$slaveConns[$this->myKey] === false) {
				throw new Exception("数据库:{$this->dbName} slave 连接失败");
			}
		}
		return self::$slaveConns[$this->myKey];
	}

	public static function releaseDbConn() {
		foreach (self::$masterConns as $key => $handle) {
			DBMysqli::releaseDBHandle($handle);
			self::$masterConns[$key] = null;
		}
		self::$masterConns = array();
		foreach (self::$slaveConns as $key => $handle) {
			DBMysqli::releaseDBHandle($handle);
			self::$slaveConns[$key] = null;
		}
		self::$slaveConns = array();
		self::$sqlBuilders = array();
	}

	/**
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 * @return SqlBuilder
	 * @throws Exception
	 */
	protected function getSqlBuilder() {
		if (empty(self::$sqlBuilders[$this->myKey])) {
			self::$sqlBuilders[$this->myKey] = new SqlBuilder($this->getSlaveConn(), $this->dbName, $this->tableName, $this->fieldTypes);
			self::$sqlBuilders[$this->myKey]->setDbName($this->dbName);
		}
		return self::$sqlBuilders[$this->myKey];
	}

	/**
	 * 执行Sql
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string     $sql
	 * @param bool|false $fromMaster
	 *
	 * @return bool true成功 false失败
	 * @throws Exception
	 */
	public function execute($sql, $fromMaster = false) {
		$conn = $fromMaster ? $this->getMasterConn() : $this->getSlaveConn();
		$this->writeSqlLog($sql);
		return DBMysqli::execute($conn, $sql);
	}

	/**
	 * 查询所有结果集
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string     $sql
	 * @param bool|false $fromMaster
	 *
	 * @return array|false
	 * @throws Exception
	 */
	public function queryAll($sql, $fromMaster = false) {
		$conn = $fromMaster ? $this->getMasterConn() : $this->getSlaveConn();
		$this->writeSqlLog($sql);
		return DBMysqli::queryAll($conn, $sql);
	}

	/**
	 * 查询第一条结果的第一列
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string     $sql
	 * @param bool|false $fromMaster
	 *
	 * @return array|false
	 * @throws Exception
	 */
	public function queryRow($sql, $fromMaster = false) {
		$conn = $fromMaster ? $this->getMasterConn() : $this->getSlaveConn();
		$this->writeSqlLog($sql);
		return DBMysqli::queryRow($conn, $sql);
	}

	/**
	 * 查询第一条结果的第一列
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param            $sql
	 * @param bool|false $fromMaster
	 *
	 * @return array|false
	 * @throws Exception
	 */
	public function queryOne($sql, $fromMaster = false) {
		$conn = $fromMaster ? $this->getMasterConn() : $this->getSlaveConn();
		$this->writeSqlLog($sql);
		return DBMysqli::queryOne($conn, $sql);
	}

	/**
	 * 插入数据
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param array $columnsAndValues 要插入的数据 例如 array('id'=>1, 'name'=>'a') 或者 array(array('id'=>1, 'name'=>'a'), array('id'=>2, 'name'=>'b'));
	 *
	 * @return false|int
	 * @throws Exception
	 */
	public function insert($columnsAndValues) {
		$sql = $this->getSqlBuilder()->createInsertSql($columnsAndValues);
		$conn = $this->getMasterConn();
		$this->writeSqlLog($sql);
		return DBMysqli::insertAndGetID($conn, $sql);
	}

	/**
	 * 更新数据
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param array $columnsAndValues
	 * @param array $filters
	 *
	 * @return bool|int
	 * @throws Exception
	 */
	public function update($columnsAndValues, $filters) {
		$sql  = $this->getSqlBuilder()->createUpdateSql($columnsAndValues, $filters);
		$conn = $this->getMasterConn();
		$this->writeSqlLog($sql);
		if (DBMysqli::execute($conn, $sql)) {
			return DBMysqli::lastAffected($conn);
		}
		return false;
	}

	/**
	 * 删除数据
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $filters
	 *
	 * @return bool|int 失败|影响行数
	 * @throws Exception
	 */
	public function delete($filters) {
		$sql  = $this->getSqlBuilder()->createDeleteSql($filters);
		$conn = $this->getMasterConn();
		$this->writeSqlLog($sql);
		if (DBMysqli::execute($conn, $sql)) {
			return DBMysqli::lastAffected($conn);
		}
		return false;
	}

	/**
	 * 查询所有
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string     $field
	 * @param array      $filters array 数组，不要拼sql 可能会被注入。格式 array('字段名', '逻辑条件 > < = !=', 值)eg:<br>
	 *                            1. array(array('id', '=', 1)) 转成 id=1。<br>
	 *                            2. array(array('id', '=', 1), array('name', '=', 'foo')) 转成 id=1 and name='foo'。<br>
	 *                            3. array(array('id', '=', 1), array('or' => array(array('id','>', 3), array('id','<', 4)))) <br>
	 *                            4. array(array('id', 'in', array(1,2,3)))  转成 id in (1,2,3) in or not in
	 * @param array     $orderBy array('id' => 'DESC', 'name' => 'ASC')
	 * @param int        $limit
	 * @param int        $offset
	 * @param bool|false $fromMaster
	 * @param string     $useIndex
	 *
	 * @return array|false
	 * @throws Exception
	 */
	public function getAll($field = '*', $filters = array(), $orderBy = array(), $limit = 0, $offset = 0, $fromMaster = false, $useIndex = '') {
		$sql  = $this->getSqlBuilder()->createSelectSql($field, $filters, $orderBy, $limit, $offset, $useIndex);
		$conn = $fromMaster ? $this->getMasterConn() : $this->getSlaveConn();
		$this->writeSqlLog($sql);
		return DBMysqli::queryAll($conn, $sql);
	}

	/**
	 * 获取一行
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string     $field
	 * @param array      $filters
	 * @param string     $orderBy
	 * @param int        $offset
	 * @param bool|false $fromMaster
	 *
	 * @return array|false
	 * @throws Exception
	 */
	public function getRow($field = '*', $filters = array(), $orderBy = '', $offset = 0, $fromMaster = false, $forUpdate = false) {
		$sql  = $this->getSqlBuilder()->createSelectSql($field, $filters, $orderBy, 1, $offset, '', $forUpdate);
		$conn = $fromMaster ? $this->getMasterConn() : $this->getSlaveConn();
		$this->writeSqlLog($sql);
		return DBMysqli::queryRow($conn, $sql);
	}

	/**
	 * 根据id锁定行
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $id
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function lockById($id) {
		$sql  = $this->getSqlBuilder()->createSelectSql($this->idFieldName, array(
			array($this->idFieldName, '=', $id)
		), array(), 0, 0, '', true);
		$conn = $this->getMasterConn();
		$this->writeSqlLog($sql);
		return DBMysqli::execute($conn, $sql);
	}

	/**
	 * 获取一个数据
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string     $field
	 * @param array      $filters
	 * @param bool|false $fromMaster
	 * @param string     $useIndex
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function getOne($field = '*', $filters = array(), $fromMaster = false, $useIndex = '', $forUpdate = false) {
		$conn = $fromMaster ? $this->getMasterConn() : $this->getSlaveConn();
		$sql  = $this->getSqlBuilder()->createSelectSql($field, $filters, [], 0, 0, $useIndex, $forUpdate);
		$this->writeSqlLog($sql);
		return DBMysqli::queryOne($conn, $sql);
	}

	/**
	 * 在某个字段的基础上增加相应值
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string    $fieldName 字段名
	 * @param array     $filters   where 条件
	 * @param int|float $num       增量
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function incr($fieldName, $filters, $num = 1) {
		$sql  = $this->getSqlBuilder()->createIncrSql($fieldName, $filters, $num);
		$conn = $this->getMasterConn();
		$this->writeSqlLog($sql);
		return DBMysqli::execute($conn, $sql);
	}

	/**
	 * 在某个字段的基础上减少相应值
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string $fieldName 字段名
	 * @param array  $filters   where 条件
	 * @param int    $num       递减量
	 *
	 * @return bool
	 * @throws Exception
	 */
	public function decr($fieldName, $filters, $num = 1) {
		$sql  = $this->getSqlBuilder()->createDecrSql($fieldName, $filters, $num);
		$conn = $this->getMasterConn();
		$this->writeSqlLog($sql);
		return DBMysqli::execute($conn, $sql);
	}

	/**
	 * 开启事务
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 * @return bool
	 * @throws Exception
	 */
	public function begin() {
		$sql  = 'BEGIN';
		$conn = $this->getMasterConn();
		return DBMysqli::execute($conn, $sql);
	}

	/**
	 * 提交事务
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 * @return bool
	 * @throws Exception
	 */
	public function commit() {
		$sql  = 'COMMIT';
		$conn = $this->getMasterConn();
		return DBMysqli::execute($conn, $sql);
	}

	/**
	 * 回滚事务
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 * @return bool
	 * @throws Exception
	 */
	public function rollback() {
		$sql  = 'ROLLBACK';
		$conn = $this->getMasterConn();
		return DBMysqli::execute($conn, $sql);
	}

	/**
	 * 获取所有字段
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 * @return array
	 * @throws Exception
	 */
	public function getFields() {
		$conn   = $this->getMasterConn();
		$result = DBMysqli::queryAll($conn, 'SHOW FULL COLUMNS FROM `' . $this->tableName.'`');
		$info   = [];
		if ($result) {
			foreach ($result as $key => $val) {
				$val['type'] = preg_replace('/\([^\(\)]+\)\s?\w*/', '', $val['Type']);
				$info[]      = $val;
			}
		}
		return $info;
	}

	/**
	 * 获取最后一条sql
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 * @return string
	 */
	public function getLastSql() {
		return DBMysqli::getLastSql();
	}

	/**
	 * 获取条数
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $filters
	 *
	 * @return mixed
	 */
	public function getCount($filters) {
		return $this->getOne('count(*)', $filters);
	}

	/**
	 * 根据id获取信息
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param        $id
	 * @param string $field
	 *
	 * @return array|bool|false
	 */
	public function getById($id, $field = '*') {
		if ($id && $field) {
			return $this->getRow($field, array(
				array($this->idFieldName, '=', $id)
			));
		}
		return false;
	}

	/**
	 * 根据id修改
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param       $id
	 * @param array $columnsAndValues
	 *
	 * @return bool|int
	 */
	public function updateById($id, $columnsAndValues = array()) {
		if ($id && $columnsAndValues) {
			return $this->update($columnsAndValues, array(
				array($this->idFieldName, '=', $id)
			));
		}

		return false;
	}

	/**
	 * 根据id删除
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $id
	 *
	 * @return bool|int
	 */
	public function deleteById($id) {
		if ($id) {
			return $this->delete(array(
				array($this->idFieldName, '=', $id)
			));
		}
		return false;
	}

	/**
	 * 根据id批量更新
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param array $ids
	 * @param array $columnsAndValues
	 *
	 * @return bool|int
	 */
	public function updateByIds($ids = array(), $columnsAndValues = array()) {
		if ($ids && $columnsAndValues) {
			return $this->update($columnsAndValues, array(
				array($this->idFieldName, 'in', $ids)
			));
		}
		return false;
	}

	/**
	 * 用户对条目的删除
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param array $ids
	 *
	 * @return bool|int
	 */
	public function deleteByUser($ids) {
		if ($ids && is_array($ids)) {
			return $this->updateByIds($ids, array('status' => GlobalConfig::STATUS_USER_DELETE));
		}
		return false;
	}

	/**
	 * 管理员对条目的删除
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param array $ids
	 *
	 * @return bool|int
	 */
	public function deleteByAdmin($ids) {
		if ($ids && is_array($ids)) {
			return $this->updateByIds($ids, array('status' => GlobalConfig::STATUS_ADMIN_DELETE));
		}
		return false;
	}

	/**
	 * 管理员对条目禁用
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param $ids
	 * @param $data
	 *
	 * @return bool|int
	 */
	public function disableByAdmin($ids, $data = array()){
		if ($ids && is_array($ids)) {
			$params = array('status' => GlobalConfig::STATUS_ADMIN_DISABLE);
			if ($data && is_array($data)) {
				$params = array_merge($data, $params);
			}
			return $this->updateByIds($ids, $params);
		}
		return false;
	}

	/**
	 * 管理员对条目启用
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param array $ids
	 *
	 * @return bool|int
	 */
	public function enableByAdmin($ids){
		if ($ids && is_array($ids)) {
			return $this->updateByIds($ids, array('status' => GlobalConfig::STATUS_NORMAL));
		}
		return false;
	}

	public function genWhere($conditions = array(), $relations = 'AND'){
		return $this->getSqlBuilder()->dfsWhere($conditions, $relations);
	}

	private function writeSqlLog($sql) {
		if (!GlobalConfig::inOnline() && $this->dbName != 'log') {
			Log::debug('sql.trace.' . $this->tableName, $sql);
			$file = GlobalConfig::SQL_LOG_DIR . '/' . date('Y-m-d').'.log';
			if (is_file($file) && !is_writable($file)) {
				rename($file, $file . '.' . time());;
			}
			file_put_contents($file, join(' ', array(date('Y-m-d H:i:s'), $sql, "\r\n")), FILE_APPEND);
		}
	}

}

