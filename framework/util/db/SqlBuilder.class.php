<?php

/**
 *  生成sql语句的方法，只适用于一个数据表的操作，不适合联表查询
 * @author longwg
 */

class SqlBuilder {

    // 默认的边界符
    const MODIFIER_DEFAULT = "'"; 
    
    // update中set时的关键字以及分割符
    private static $_hashSet = array(
        ' SET ', 
        ', '
    );

    // update中set时的关键字以及分割符
    private static $_hashWhere = array(
        ' WHERE ', 
        ' AND '
    );

    // where时的关键字以及分割符
    private static $_noModType = array( ///< 不用分割符号的数据类型(全大写)，如：INT
        'TINYINT',
        'SMALLINT',
        'MEDIUMINT',
        'INT',
        'INTEGER',
        'BIGINT',
        'FLOAT',
        'DOUBLE',
        'DECIMAL',
    );

    // 你懂得
    protected $mysqli = null;
    protected $dbName = '';
    protected $tableName = '';
    protected $fieldTypes = array();
    
    public function __construct($mysqli, $dbName, $tableName, $fieldTypes) {
        $this->mysqli = $mysqli;
        $this->dbName = $dbName;
        $this->tableName = $tableName;
        $this->fieldTypes = $fieldTypes;
    }

    /**
     * 取得数据库名
     * @return string
     */
    public function getDbName() {
        return $this->dbName;
    }

    public function setDbName($dbName) {
        $this->dbName = $dbName;
    }

    /**
     * 取得数据表名
     * @return string
     */
    public function getTableName() {
        return $this->tableName;
    }

    /**
     * 检查字段的类型，确定要不要用边界符
     * @param String $fieldName
     * @return boolean
     */
    private function _getFieldTypeMod($fieldName) {
        $pos = strrpos($fieldName, '.');
        if (false !== $pos) {
            $fieldName = substr($fieldName, $pos + 1);
        }
        if (isset($this->fieldTypes[$fieldName]) && in_array(strtoupper($this->fieldTypes[$fieldName]), self::$_noModType)) {
            return '';
        }
        return self::MODIFIER_DEFAULT;
    }

    /**
     * 可以获取一个表的数据。
     * @param String $field 字段
     * @param array $where 条件
     * @param array $orderBy 排序
     * @param int $limit 结果集每页纪录数
     * @param int $offset 结果集的偏移
     * @return String SQL
     */
    public function createSelectSql($field = '*', $where = array(), $orderBy = array(), $limit = 0, $offset = 0, $useIndex='', $forUpdate = false) {
        if ($limit > 0) {
            $offset = (int) $offset;
            $limit = " LIMIT {$offset}, $limit";
        } else {
            $limit = '';
        }
        // select增加group_by
        $groupBy = '';
        if (array_key_exists('group_by', $where)) {
            $groupBy = ' GROUP BY ' . $where['group_by'];
            unset($where['group_by']);
        }
	    $forUpdateStr = $forUpdate ? ' FOR UPDATE ' : '';
        return 'SELECT ' . $field . ' FROM ' . $this->_checkTableName() . $useIndex . $this->createWhereSql($where) .$forUpdateStr . $groupBy . $this->createOrderbySql($orderBy) . $limit;
    
    }

    /**
     * 数据插入
     * @param Array $data 要插入的数据 例如 array('id'=>1, 'name'=>'a') 或者 array(array('id'=>1, 'name'=>'a'), array('id'=>2, 'name'=>'b'));
     * @return String sql
     */
    public function createInsertSql($data) {
        if (isset($data[0]) && is_array($data[0])) {
            $insertValues = array();
            foreach ($data as $item) {
                $this->filterFields($item);
                $arrKeys = array_keys($item);
                $arrVals = array();
                foreach ($arrKeys as $f) {
                    $mod = $this->_checkEmptyValue($f, $item[$f]);
                    if (false === $mod) {
                        unset($arrKeys[array_search($f, $arrKeys)]);
                        continue;
                    }
                    $arrVals[] = $mod . addslashes($item[$f]) . $mod;
                }
                $field  = implode('`, `', $arrKeys);
                $values = implode(', ', $arrVals);
                $values = '(' . $values . ')';
                $insertValues[] = $values;
            }
            if (!empty($insertValues)) {
                $insertValues = implode(',', $insertValues);
                return 'INSERT INTO ' . $this->_checkTableName() . ' (`' . $field . '`) VALUES ' . $insertValues;
            }
        } else {
            $this->filterFields($data);
            $arrKeys = array_keys($data);
            $arrVals = array();
            foreach ($arrKeys as $f) {
                $mod = $this->_checkEmptyValue($f, $data[$f]);
                if (false === $mod) {
                    unset($arrKeys[array_search($f, $arrKeys)]);
                    continue;
                }
                $arrVals[] = $mod . addslashes($data[$f]) . $mod;
            }
            $field = implode('`, `', $arrKeys);
            $values = implode(', ', $arrVals);
            $ret = 'INSERT INTO ' . $this->_checkTableName() . ' (`' . $field . '`) VALUES (' . $values . ')';
            return $ret;
        }
    }

    /**
     * 数据更新
     * @param String or Array $setData 要插入的数据
     * @param String or Array $where 条件
     * @return String sql
     */
    public function createUpdateSql($setData, $where) {
        return 'UPDATE ' . $this->_checkTableName() . $this->createSetSql($setData, true) . $this->createWhereSql($where);
    }

    /**
     * 更新字段数据增量 update XXX set xxx=xxx+1 where ....
     * @param type $fieldName
     * @param type $where
     * @param type $num
     * @return type string
     */
    public function createIncrSql($fieldName, $where, $num = 1) {
        $fieldName = $this->_checkField($fieldName);
        $num = (int) $num;
        return 'UPDATE ' . $this->_checkTableName() . " SET {$fieldName} = {$fieldName} + {$num} " . $this->createWhereSql($where);
    }

    /**
     * 更新字段数据减量 update XXX set xxx=xxx-1 where ....
     * @param type $fieldName
     * @param type $where
     * @param type $num
     * @return type string
     */
    public function createDecrSql($fieldName, $where, $num = 1) {
        $fieldName = $this->_checkField($fieldName);
        $num = (int) $num;
        return 'UPDATE ' . $this->_checkTableName() . " SET {$fieldName} = {$fieldName} - {$num} " . $this->createWhereSql($where);
    }

    /**
     * 删除数据
     * @param $where WHERE
     * @return String sql
     */
    public function createDeleteSql($where) {
        return "DELETE FROM " . $this->_checkTableName() . $this->createWhereSql($where);
    }

    /**
     * 生成Where数据集
     * @param String or Array $filters
     */
    public function createWhereSql($filters) {
        $ret = $this->dfsWhere($filters, 'AND');
        if (false === $ret) {
            trigger_error('构建Where条件失败！', E_USER_ERROR);
        }
        if ($ret) {
            return " WHERE " . $ret;
        }
        return '';
    }

    /**
     *   将查询数组转换成sql的where部分
     * @param   $where array 数组，不要拼sql 可能会被注入。格式 array('字段名', '逻辑条件 > < = !=', 值)eg:<br>
     *          1. array(array('id', '=', 1)) 转成 id=1。<br>
     *          2. array(array('id', '=', 1), array('name', '=', 'foo')) 转成 id=1 and name='foo'。<br>
     *          3. array(array('id', '=', 1), array('or' => array(array('id','>', 3), array('id','<', 4)))) 转成 id=1 and (id>3 or id<4)
     *          4. array(array('id', 'in', array(1,2,3)))  转成 id in (1,2,3) in or not in
     * @param   $relation string 连接符AND或者OR
     * @return  string
     */
    public function dfsWhere($where, $relation = 'AND') {
        if (empty($where)) {
            return '';
        }

        $ret = array();
        foreach ($where as $filter) {

            // 合法性判断
            if (empty($filter) || !is_array($filter)) {
                $msg = "转换条件失败，原子条件必须是非空数组！附加信息：\$filter = {$filter} ";
                trigger_error($msg, E_USER_WARNING);
                return false;
            }

            $filterOr = array();
            isset($filter['or']) && $filterOr = $filter['or'];
            isset($filter['OR']) && $filterOr = $filter['OR'];

            if ($filterOr) {
                // 如果是或条件，修改连接符后继续递归
	            $orRet = $this->dfsWhere($filterOr, 'OR');
                if ($orRet === false) {
                    return false;
                }
                $ret[] = "({$orRet})";

            } else if (is_array($filter[0])) {
                // 如果是且条件，继续递归

                $andRet = $this->dfsWhere($filter);
                if ($andRet === false) {
                    return false;
                }
                $ret[] = $andRet;

            } else if (is_string($filter[0]) &&  count($filter) == 1) {
                // 如果当前是一维数组，并且只有一个字符串元素

                $ret[] = $this->mysqli->real_escape_string($filter[0]);

            } else if (is_string($filter[0]) && count($filter) == 3) {
                // 如果当前是一维数组，并且数组元素为3个

                $field = $filter[0];
                $op    = strtoupper($filter[1]);
                $value = $filter[2];
                $glue  = $this->_getFieldTypeMod($field);

                // 校验字段是否存在
                if (!array_key_exists($field, $this->fieldTypes)) {
                    $msg = "转换条件失败，字段不存在！附加信息：\$field = {$field} ";
                    Log::fatal('mysqlns.sql', $msg);
                    trigger_error($msg, E_USER_WARNING);
                    return false;
                }

                if ($op == 'IN' || $op == 'NOT IN') {

                    // IN操作时，$value是数组
                    if (!is_array($value)) {
                        $msg = "转换条件失败，当操作符为IN时，value必须是数组！附加信息：\$field = {$field} ";
                        trigger_error($msg, E_USER_WARNING);
                        return false;
                    }

                    // 空数组处理
                    if (empty($value)) {
                        $ret[] = $op == 'IN' ? 'FALSE' : 'TRUE';
                        continue;
                    }

                    // 校验$value数组中的每个值，并且对每个值escape
                    $arr = array();
                    foreach ($value as $val) {
                        $arr[] = $glue . $this->mysqli->real_escape_string($val) . $glue;
                    }
                    $arr = implode(', ', $arr);
                    $ret[] = "{$field} {$op} ({$arr})";

                } else {

                    $value = $glue . $this->mysqli->real_escape_string($value) . $glue;
                    $ret[] = $field . ' ' . $op . ' ' . $value;
                }
            } else {
                $msg = "转换条件失败，因格式不正确，无法转换where条件！ ";
                trigger_error($msg, E_USER_WARNING);
                return false;
            }
        }

//	    print_r(array($relation, $ret));

        return implode(' ' . $relation . ' ', $ret);
    }

	/**
	 * orderBy 子句
	 * @author 李登科<lidengke@xiongying.com>
	 * @param array $orderBys array('id' => 'DESC', 'name' => 'ASC')
	 *
	 * @return string
	 */
    public function createOrderbySql($orderBys) {
        if (empty($orderBys) || !is_array($orderBys)) {
            return '';
        }
        $ret = array();
        foreach ($orderBys as $key => $val) {
            if (empty($key)) {
                continue;
            }
            $key = $this->_checkField($key);
            $val = strtoupper($val) == 'DESC' ? 'DESC' : 'ASC';
            $ret[] = "$key $val";
        }
        
        $sql = '';
        if (count($ret) > 0) {
            $sql = " ORDER BY  " . implode(", ", $ret);
        }

        return $sql;
    }

    /**
     * 生成Set数据集
     * @param String or Array $data
     */
    protected function createSetSql($data) {
        return $this->_parseHash($data, self::$_hashSet);
    }

    /**
     * 检查表名
     * @return String
     */
    protected function _checkTableName() {
        return "`" . $this->dbName . "`.`" . $this->tableName . "`";
    }

    /**
     * 过滤没用的字段
     * @param Array $fields
     */
    protected function filterFields(&$data) {
        $fields = array_keys($data);
        foreach ($fields as $f) {
            if (! array_key_exists($f, $this->fieldTypes)) {
                unset($data[$f]);
            }
        }
    }

    /**
     * 将数组数据生成键值对形式的字符串
     * @param String or Array $data
     * @param Array $ws Where 和 Set时的关键字和分割符
     * @return String
     */
    private function _parseHash($data, $ws, $filter = true) {
        
        $strHash = '';
        if (is_string($data)) {
            $strHash = $data;
        } else if (is_array($data)) {
            if ($filter) {
                $this->filterFields($data);
            }
            
            $arrHash = array();
            foreach ($data as $k => $v) {
                $mod = $this->_checkEmptyValue($k, $v);
                
                if ($filter && $mod === false) {
                    continue;
                }
                
                $arrHash[] = $this->_checkField($k) . "=" . $mod . addslashes($v) . $mod;
            }
            $strHash = implode($ws[1], $arrHash);
        }
        return $strHash ? " {$ws[0]} {$strHash}" : ' ';
    }

    /**
     * 检查字段名，过滤掉特殊字符（不包含.） 得到合法的字符
     * @param String $field
     */
    private function _checkField($field) {
        $field = preg_replace('/[^\w\.\+]/', '', $field);
        $pos = strpos($field, '.');
        $posPlus = strpos($field, '+');
        if ($pos > 0) {
            $field = str_replace('.', "`.`", $field);
        } else if ($pos === 0) {
            $field = substr($field, 1);
        }
        if ($posPlus > 0) {
            $field = str_replace('+', "`+`", $field);
        } else if ($posPlus === 0) {
            $field = substr($field, 1);
        }
        
        return "`" . $field . "`";
    }

    /**
     * 
     * 检查不使用边界符时的数据是否合法（要为数字）
     * @param String $field 字段
     * @param String $val	字段值
     * @return boolean or char
     */
    private function _checkEmptyValue($field, $val) {
        $mod = $this->_getFieldTypeMod($field);
        
        if (empty($mod) && ! preg_match('/\d+/', $val)) {
            return false;
        }
        return $mod;
    }
}
