<?php

/**
 *  用户级异常，此异常可以把异常信息展示给用户
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/25
 * Time: 9:33
 */
class UserException extends BaseException {
	public function __construct($message, $code = 999) {
		parent::__construct($message, $code);
	}
}