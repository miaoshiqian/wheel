<?php

/**
 * Class BaseException
 *
 * 异常基类
 * @author 李登科<lidengke@xiongying.com>
 */
abstract class BaseException extends Exception {

    public function __construct($message, $code) {
        parent::__construct($message, $code);
    }

    public function getStackTrace() {
        $trace_elems = array();

        $i = 0;
        $e = $this;
        do {
            $trace_elems[$i]['msg'] = $e->getMessage();
            $trace_elems[$i]['code'] = $e->getCode();
            $trace_elems[$i]['file'] = $e->getFile();
            $trace_elems[$i]['line'] = $e->getLine();
            $trace_elems[$i]['trace'] = $e->getTrace();
            ++$i;
            $e = $e->getPrevious();
        } while ($e);

        return $trace_elems;
    }

	/**
	 * 格式化异常追踪信息
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string $lf
	 *
	 * @return string
	 */
    public function formatStackTrace($lf = "\r\n") {
        $trace = '';

        $i = 0;
        foreach ($this->getStackTrace() as $trace_elem) {
            if ($i > 0) {
                $trace .= 'Caused by: ';
            }

            $trace .= get_class($this) . ": {$trace_elem['msg']}$lf";

            $j = 0;
            $thrown_at = "Thrown at [{$trace_elem['file']}:{$trace_elem['line']}]";
            $trace .= "\t#$j {$thrown_at}$lf";
            ++$j;

            foreach ($trace_elem['trace'] as $trace_item) {
                $class = empty($trace_item['class']) ? '' : $trace_item['class'];
                $type = empty($trace_item['type']) ? '' : $trace_item['type'];
                $pos = empty($trace_item['file']) ? '' : "[{$trace_item['file']}:{$trace_item['line']}]";

                $args = '';
                if (!empty($trace_item['args'])) {
                    $k = 0;
                    foreach ($trace_item['args'] as $arg) {
                        if ($k > 0) {
                            $args .= ',';
                        }

                        $args .= gettype($arg);

                        ++$k;
                    }
                }

                $called_at = "Called at {$class}{$type}{$trace_item['function']}($args) $pos";
                $trace .= "\t#$j {$called_at}$lf";

                ++$j;
            }
            ++$i;
        }

        return $trace;
    }

}