<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit','1024M');
set_time_limit(0);


if(!defined("ROOT_PATH")) {
    define('ROOT_PATH', dirname(__FILE__) . '/../../..');
}
if(!defined("FRAMEWORK_PATH")) {
    define('FRAMEWORK_PATH', ROOT_PATH . '/framework');
}
if(!defined("API_PATH")) {
    define('API_PATH', ROOT_PATH . '/yanglao/api');
}

//自动加载
require_once FRAMEWORK_PATH . '/common/AutoLoader.class.php';

AutoLoader::setAutoDir(API_PATH);
AutoLoader::setAutoDir(FRAMEWORK_PATH . '/util/');
AutoLoader::setAutoDir(ROOT_PATH . '/conf/conf/');
AutoLoader::setAutoDir(ROOT_PATH . '/conf/common/');
class YingxiaoMessage {
    public static function send() {
        try {
            $info = self::_formatParams();
            foreach($info as $key => $value) {
                $params = [
                    'mobile' => Util::getFromArray('mobile', $value),
                    'tpl_value' => "#customer_name#=". Util::getFromArray('customer_name', $value),
                ];
                Message::sendYingxiao($params);
                break;
                sleep(2);
            }
        }catch (Exception $e) {
            Log::fatal('YingxiaoMessage.send', $e);
        }
    }

    private static function _formatParams() {
        $contents = file_get_contents('include/phone.txt');
        $contents = explode("\n", $contents);
        $info = [];
        foreach($contents as $key => $value) {
            if(!empty($value)) {
                $tmp = explode("\t", $value);
                $tmp['customer_name'] = trim($tmp[0]);
                $tmp['mobile'] = trim($tmp[1]);
                unset($tmp[0]);
                unset($tmp[1]);
                $info[] = $tmp;
            }
        }
        return $info;
    }
}

YingxiaoMessage::send();
