<?php
/**
 * @通过微信服务器给用户发送模板信息
 */
//TODO:项目中由于自动require了下面的文件,不需要重新包好,如果要跑脚本等等的,需要require_once下面的文件
if (!defined('CONF_PATH')) {
    define('CONF_PATH', dirname(__FILE__) . '/../../../conf/conf');
}

require_once dirname(__FILE__) . '/../http/Request.class.php';
require_once dirname(__FILE__) . '/../../../conf/conf/cache/MemcacheConfig.class.php';
require_once dirname(__FILE__) . '/../cache/CacheNamespace.class.php';

class WxMessage {
    /**
     * 获取微信应用的accesstoken
     */
    public function getAccessToken($react = false) {

        $memHandle = CacheNamespace::createCache(CacheNamespace::MODE_MEMCACHE, MemcacheConfig::$GROUP_DEFAULT);
        //从缓存中读取，如果缓存中有数据
        $memKey      = 'WX_YANGLAO_ACCESS_TOKEN';

        if($react == false ) {
            $accessToken = $memHandle->read($memKey);
            if (!empty($accessToken)) {
                return $accessToken;
            }
        }
        $appid          = 'wx9a91feb5c0fa9547';
        $secret         = '37a80a130bae8c3836b857ab9d9bb78f';
        $accessTokenUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $appid . '&secret=' . $secret;
        $ret            = Request::RequestUrl($accessTokenUrl);
        $ret            = json_decode($ret, true);
        //加入缓存
        if (!empty($ret['access_token'])) {
            $memHandle->write($memKey, $ret['access_token'], 3600);
        }

        return $ret['access_token'];
    }


    /**
     * 发送模板信息
     */
    public function sendMessage($params) {

        if(empty($params)) {
            Log::fatal('WxMessage.sendMessage', '发送微信模板消息参数不能为空');
            return false;
        }
        $type = Util::getFromArray('type', $params, 0);
        $url = 'https://api.weixin.qq.com/cgi-bin/message/template/send';
        $sendRetArr = $this->callWexin($url, $params, $type);
        $msg = Util::getFromArray('errmsg', $sendRetArr, '');
        if ($msg != 'ok') {
            Log::fatal('WxMessage.sendMessage.error', $sendRetArr);
            return false;
        }

        Log::info('WxMessage.sendMessage.success', $sendRetArr);

        return true;
    }

    /**
     * 针对accessToken失效的问题解决方案
     * @return mixed
     */
    private function callWexin($url, $data, $type = 0) {
        if($type == 0) {
            $accessToken = $this->getAccessToken();
        }
        if($type == 1) {
            $accessToken = $this->getYuerAccessToken();//安心客的accesstoken
        }
        if($type == 101) {
            $accessToken = $this->getYuerNewAccessToken();
        }
        if($type = 102) {
            $accessToken = $this->getKeshiAccessToken();//安心科室的accesstoken
        }
        if(empty($accessToken)) {
            Log::fatal('WxMessage.callWeixin', [$url, $data, $type]);
            return false;
        }

        $url .= (strpos($url, "?") !== false) ? "&" : "?";
        $url1 = $url . "&access_token=$accessToken";
        $data = json_encode($data);
        $res  = json_decode(Request::RequestUrl($url1, $data  ,1), true);
        Log::info('Wxmessage.callWexin', $res);

        if (Util::getFromArray('errcode',$res, 0) == '40001') {
            if($type == 0) {
                $accessToken = $this->getAccessToken();
            }
            if($type == 1) {
                $accessToken = $this->getYuerAccessToken();
            }
            if($type == 101) {
                $accessToken = $this->getYuerNewAccessToken();
            }
            if($type = 102) {
                $accessToken = $this->getKeshiAccessToken();
            }
            $url2        = $url . "&access_token=$accessToken";
            $res         = json_decode(Request::RequestUrl($url2, $data, 1), true);
        }
        if (Util::getFromArray('errcode',$res, 0) != 0) {
            Log::fatal('Wxmessage.callWexin.accessToken.error', $res);
        }

        return $res;
    }

    /**
     * 获取安心客微信应用的accesstoken
     */
    public function getYuerAccessToken($react = false) {

        $memHandle = CacheNamespace::createCache(CacheNamespace::MODE_MEMCACHE, MemcacheConfig::$GROUP_DEFAULT);
        //从缓存中读取，如果缓存中有数据
        $memKey      = 'WX_ACCESS_TOKEN';

        if($react == false ) {
            $accessToken = $memHandle->read($memKey);
            if (!empty($accessToken)) {
                return $accessToken;
            }
        }
        $appid          = 'wx433336e9ca24dfe4';
        $secret         = '3ea84423e69f6f82a24d55797ee49c77';
        $accessTokenUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $appid . '&secret=' . $secret;
        $ret            = Request::RequestUrl($accessTokenUrl);
        $ret            = json_decode($ret, true);
        //加入缓存
        if (!empty($ret['access_token'])) {
            $memHandle->write($memKey, $ret['access_token'], 3600);
        }

        return $ret['access_token'];
    }

    /**
     * 获取微信应用的accesstoken
     */
    public function getYuerNewAccessToken($react = false) {

        $memHandle = CacheNamespace::createCache(CacheNamespace::MODE_MEMCACHE, MemcacheConfig::$GROUP_DEFAULT);
        //从缓存中读取，如果缓存中有数据
        $memKey      = 'WX_YUER_NEW_ACCESS_TOKEN';

        if($react == false ) {
            $accessToken = $memHandle->read($memKey);
            if (!empty($accessToken)) {
                return $accessToken;
            }
        }
        $appid          = 'wx907d0aa21bcd50ad';
        $secret         = '9204a2cc7fb6e466c6780473f4229982';
        $accessTokenUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $appid . '&secret=' . $secret;
        $ret            = Request::RequestUrl($accessTokenUrl);
        $ret            = json_decode($ret, true);
        //加入缓存
        if (!empty($ret['access_token'])) {
            $memHandle->write($memKey, $ret['access_token'], 3600);
        }

        return $ret['access_token'];
    }

    /**
     * 获取微信应用的accesstoken
     */
    public function getKeshiAccessToken($react = false) {

        $memHandle = CacheNamespace::createCache(CacheNamespace::MODE_MEMCACHE, MemcacheConfig::$GROUP_DEFAULT);
        //从缓存中读取，如果缓存中有数据
        $memKey      = 'WX_KESHI_ACCESS_TOKEN';

        if($react == false ) {
            $accessToken = $memHandle->read($memKey);
            if (!empty($accessToken)) {
                return $accessToken;
            }
        }
        $appid          = 'wx8e09459b83f57dd0';
        $secret         = '4f2a2cbd01774954b752631f646754db';
        $accessTokenUrl = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $appid . '&secret=' . $secret;
        $ret            = Request::RequestUrl($accessTokenUrl);
        $ret            = json_decode($ret, true);
        //加入缓存
        if (!empty($ret['access_token'])) {
            $memHandle->write($memKey, $ret['access_token'], 3600);
        }

        return $ret['access_token'];
    }


}