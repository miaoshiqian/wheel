<?php
/**
 * 异步发送消息接口
 * @desc : 如果有gearman 扩展,使用gearman;否则同步发送消息
 */
class Msg {

    /**
     * 发送微信消息
     *
     * @param $params touser 微信openID
     * @throws Exception
     * @return 消息是否发送成功
     */
    public static function sendWX($params) {
        if (extension_loaded('gearman')) {
            include_once(dirname(__FILE__) . '/../gearman/MessageGearman.class.php');
            $handle = MessageGearman::getInstance();
        } else {
            include_once(dirname(__FILE__) . '/WxMessage.class.php');
            $handle = new WxMessage();
        }

        return $handle->sendMessage($params);
    }

	/**
	 * 发短信
	 * @author 龙卫国<longweiguo@xiongying.com>
	 *
	 * @param $mobile
	 * @param $content
	 *
	 * @return bool
	 * @throws Exception
	 */
    public static function sendSMS($mobile, $content) {
	    $params = array('mobile' => $mobile, 'content' => $content);
	    Util::checkEmpty($params);

        //如果安装了gearman,使用gearman,否则使用同步发送
        if (extension_loaded('gearman')) {
            include_once(dirname(__FILE__) . '/../gearman/Gearman.class.php');
	        Gearman::call('sms', $params);
        } else {
	        Log::fatal('msg.sendSMS', 'gearman不可用');
	        return false;
        }

        return true;
    }
}