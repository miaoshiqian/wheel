<?php

/**
 * Created by PhpStorm.
 * User: water
 * Date: 15/1/26
 * Time: 上午11:23
 */
class YunPianServcie {
    /**
     * url 为服务的url地址
     * query 为请求串
     */
    private function sock_post($url, $query) {
        $data = "";
        $info = parse_url($url);
        $fp   = fsockopen($info["host"], 80, $errno, $errstr, 30);
        if (!$fp) {
            return $data;
        }
        $head = "POST " . $info['path'] . " HTTP/1.0\r\n";
        $head .= "Host: " . $info['host'] . "\r\n";
        $head .= "Referer: http://" . $info['host'] . $info['path'] . "\r\n";
        $head .= "Content-type: application/x-www-form-urlencoded\r\n";
        $head .= "Content-Length: " . strlen(trim($query)) . "\r\n";
        $head .= "\r\n";
        $head .= trim($query);
        $write  = fputs($fp, $head);
        $header = "";
        while ($str = trim(fgets($fp, 4096))) {
            $header .= $str;
        }
        while (!feof($fp)) {
            $data .= fgets($fp, 4096);
        }
        return $data;
    }

    /**
     * 模板接口发短信
     * apikey 为云片分配的apikey
     * tpl_id 为模板id
     * tpl_value 为模板值
     * mobile 为接受短信的手机号
     */
    public function tpl_send_sms($apikey, $tpl_id, $tpl_value, $mobile) {
        $url               = "http://yunpian.com/v1/sms/tpl_send.json";
        $encoded_tpl_value = urlencode("$tpl_value");
        $post_string       = "apikey=$apikey&tpl_id=$tpl_id&tpl_value=$encoded_tpl_value&mobile=$mobile";
        return $this->sock_post($url, $post_string);
    }

    /**
     * 普通接口发短信
     * apikey 为云片分配的apikey
     * text 为短信内容
     * mobile 为接受短信的手机号
     */
    public function send_sms($apikey, $text, $mobile) {
        $url          = "http://yunpian.com/v1/sms/send.json";
        $encoded_text = urlencode("$text");
        $post_string  = "apikey=$apikey&text=$encoded_text&mobile=$mobile";
        return $this->sock_post($url, $post_string);
    }

    public function sendMessage($params) {
        $mobile = Util::getFromArray('mobile', $params, []);
        $conf   = Util::getFromArray('conf', $params, []);
        $tplId  = Util::getFromArray('tpl_id', $conf, '');
        
        //检测手机号码和tpl_id是否正常
        if (empty($mobile) || empty($tplId)) {
            Log::fatal('YunPianServcie.sendMessage', '参数异常');
        }
        $sendParams              = [];
        $sendParams['tpl_value'] = Util::getFromArray('tpl_value', $params);
        $sendParams['tpl_id']    = $tplId;
        $sendParams['mobile']    = $mobile;
        $ret                     = $this->_send($sendParams);
        return $ret;
    }

    public function _send($params) {
        $tpl_value = Util::getFromArray('tpl_value', $params);
        $apikey    = SMSConfig::YUNPIANAPIKEY;
        $tpl_id    = Util::getFromArray('tpl_id', $params, 0);

        $mobile = Util::getFromArray('mobile', $params, 0);
        if (empty($apikey) || empty($tpl_id) || empty($tpl_value) || empty($mobile)) {
            Log::fatal('SMSmessage.param.error', $params);
            return false;
        }
        $ret = $this->tpl_send_sms($apikey, $tpl_id, $tpl_value, $mobile);
        $retArr = json_decode($ret, true);
        $msg = Util::getFromArray('msg', $retArr, 0);

        if($msg != 'OK') {
            Log::fatal('SMSmessage.fail', $retArr);
            return false;
        }
        Log::info('SMSmessage.success', $retArr);
        return $ret;
    }

    public function sendYingXiao($params) {
        $apiKey = SMSConfig::YINXIAO_API_KEY;
        $mobile = Util::getFromArray('mobile', $params, 0);
        $tplId = Util::getFromArray('tpl_id', $params, 819755);
        $tplValue = Util::getFromArray('tpl_value', $params, '');
        if(empty($tplId) || empty($mobile) || empty($tplValue)) {
            Log::fatal('YunPianService.sendYingxiao.paramserror', $params);
        }
        $ret = $this->tpl_send_sms($apiKey, $tplId, $tplValue, $mobile);
        $retArr = json_decode($ret, true);
        $msg = Util::getFromArray('msg', $retArr, 0);
        if($msg != 'OK') {
            Log::fatal('YunPianService.sendYingXiao.fail', $retArr);
            return false;
        }
        return $ret;
    }
}
