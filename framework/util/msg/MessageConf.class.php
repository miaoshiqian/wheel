<?php

/**
 * 消息全局配置(短信和微信)
 */
class MessageConf {

    //微信消息类型
    const TYPE_WEIXIN= 1;

    //短信消息类型
    const TYPE_SMS = 2;

}