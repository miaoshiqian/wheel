<?php
/**
 *
 * User: 龙卫国<longweiguo@xiongying.com>
 * Date: 2015/11/18
 */

class Gearman {
	private static $instances = [];

	public static function call($code, $params, $config = []) {
		if (empty($config)) {
			$config = [
				'server' => BaseGlobalConfig::GEARMAN_HOST,
				'port'   => BaseGlobalConfig::GEARMAN_PORT,
			];
		}

		$hash = json_encode($config);
		if (array_key_exists($hash, self::$instances)) {
			$gmc = self::$instances[$hash];
		} else {
			$gmc = new gearmanClient();
			$gmc->addServer($config['server'], $config['port']);
			self::$instances[$hash] = $gmc;
		}

		$gmc->doBackground($code, json_encode($params));
	}
}