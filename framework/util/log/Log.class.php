<?php

require_once __DIR__ . '/../gearman/Gearman.class.php';

class Log {
	/**
	 * 记录inifo日志
	 * @author 龙卫国<longweiguo@xiongying.com>
	 *
	 * @param $tag
	 * @param $data
	 */
    public static function info($tag, $data) {
        $params = array(
	        'level' => 1,
	        'tag' => $tag,
	        'data' =>  $data,
        );
        self::_log($params);
    }

	/**
	 * 记录debug日志
	 * @author 龙卫国<longweiguo@xiongying.com>
	 *
	 * @param $tag
	 * @param $data
	 */
	public static function debug($tag, $data) {
		$params = array(
			'level' => 2,
			'tag' => $tag,
			'data' =>  $data,
		);
		self::_log($params);
	}

	/**
	 * 记录报警日志
	 * @author 龙卫国<longweiguo@xiongying.com>
	 *
	 * @param $tag
	 * @param $data
	 */
    public static function warn($tag, $data) {
	    $params = array(
		    'level' => 3,
		    'tag' => $tag,
		    'data' =>  $data,
	    );
        self::_log($params);
    }

	/**
	 *  简介:记录错误日志
	 * @author 龙卫国<longweiguo@xiongying.com>
	 *
	 * @param $tag
	 * @param $data
	 */
    public static function fatal($tag, $data) {
	    $params = array(
		    'level' => 4,
		    'tag' => $tag,
		    'data' =>  $data,
	    );
        self::_log($params);
    }


	/**
	 *  简介:异常时记录错误日志
	 * @author 龙卫国<longweiguo@xiongying.com>
	 *
	 * @param $tag
	 * @param $e
	 */
	public static function exception($tag, Exception $e) {
		if($e instanceof UserException) {
			self::warn($tag, $e);
		} else {
			self::fatal($tag, $e);
		}
	}

    private static function _log($params) {
        $logParams = self::_formatParams($params);
        if (extension_loaded('gearman')) {
	        Gearman::call('log', $logParams);
	        return true;
        }

	    return false;
    }

    /**
     * 格式化记录日志的参数
     * @param $params
     * @return mixed
     * @throws Exception
     */
    private static function _formatParams($params) {
        $data  = Util::getFromArray('data', $params, '');
        $tag   = Util::getFromArray('tag', $params, '');
        $level = Util::getFromArray('level', $params, '');
        if (empty($tag) || empty($level)) { // data 的值有可能会空
            throw new Exception('记录日志时需要传递data和tag参数');
        }

        $trace = [];

        $ignoreConf = [
            'Dispatch.class.php',
//            'MessageGearmanWorker.class.php',
            'LogInterface.class.php',
	        'Log.class.php',
	        'DBMysqli.class.php',
	        'BaseModel.class.php',
	        'log/impl/LogImpl.class.php',
	        'gearman_worker/log.php'
        ];
        if(strpos('xhprof', $tag) === false) {//xhprof 日志不记录路径,太大记录不下来
            //日志路径
            $traceRet = debug_backtrace();
            foreach ($traceRet as $value) {
                //如果文件中有忽略记录的文件路径,break 2
                foreach($ignoreConf as $ignore) {
                    if(empty($value['file']) || strpos($value['file'], $ignore)) {
                        continue 2;
                    }
                }
                $trace[] = [
                    'file'   => $value['file'] . ' | ' . $value['line'] . ' | ' . $value['function'],
                    'object' => Util::getFromArray('object', $value, ''),
                    'args'   => $value['args'],
                ];
            }
        }

	    // 记录下data
	    if ($data instanceof Exception) {
		    $logData = Util::formatStackTrace($data);
	    } elseif (is_string($data)) {
		    $logData = $data;
	    } else {
		    $logData = json_encode($data);
	    }

        //构造日志参数
        $logParams['level']       = $level;
        $logParams['tag']         = $tag;
        $logParams['data']        = $logData;
        $logParams['trace']       = json_encode($trace);
        $logParams['url']         = !empty($_SERVER["HTTP_HOST"]) ? Request::getCurrentUrl() : $_SERVER["PHP_SELF"];
        $logParams['create_time'] = time();
        return $logParams;
    }

}