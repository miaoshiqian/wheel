<?php

class GeoUtil {

    private static function rad($d) {

        return $d * 3.1415926535898 / 180.0;
    }

    public static function getDistance($lng1, $lat1, $lng2, $lat2) {

        $EARTH_RADIUS = 6378137;
        $radLat1 = self::rad($lat1);
        //echo $radLat1;
        $radLat2 = self::rad($lat2);
        $a = $radLat1 - $radLat2;
        $b = self::rad($lng1) - self::rad($lng2);
        $s = 2 * asin(sqrt(pow(sin($a/2),2) +
                cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)));
        $s = $s *$EARTH_RADIUS;
        $s = round($s * 10000) / 10000;
        return $s;
    }

    public static function getAround($longitude, $latitude, $radius) {

        $pi = pi();

        $degree = (24901*1609)/360.0;
        $dpmLat = 1/$degree;
        $radiusLat = $dpmLat*$radius;
        $minLat = $latitude - $radiusLat;
        $maxLat = $latitude + $radiusLat;

        $mpdLng = $degree*cos($latitude * ($pi/180));
        $dpmLng = 1/$mpdLng;
        $radiusLng = abs($dpmLng*$radius);
        $minLng = $longitude - $radiusLng;
        $maxLng = $longitude + $radiusLng;
        return array(
            'min_longitude' => $minLng,
            'max_longitude' => $maxLng,
            'min_latitude'  => $minLat,
            'max_latitude'  => $maxLat,
        );
    }
}
