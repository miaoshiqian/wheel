<?php

class Time {

    // 结合p方法使用，用来监控时间，并存入$timeList
    public static $firstTime  = 0;
    public static $recordList = array();

    /**
     *   监控时间，将结果存入$record
     * @param   $name   string 记录时间点的名称
     * @return  float 当前时间-开始时间
     */
    public static function p($name) {

        if (empty(self::$firstTime)) {
            self::$firstTime = self::ms();
            $timeInfo = array(
                'name' => 'Init',
                'time' => date('Y-m-d H:i:s', time()),
            );
            self::$recordList[] = $timeInfo;
        }
        $time = sprintf('%.2lf', self::ms()-self::$firstTime);
        $timeInfo = array(
            'name' => $name,
            'time' => $time . 'ucenter',
        );
        self::$recordList[] = $timeInfo;
        return $time;
    }

    /**
     *   打印时间
     */
    public static function show() {

        foreach (self::$recordList as $timeInfo) {
            echo '<br/>';
            echo sprintf('<span style="width: 200px; display: inline-block;">%s</span>', $timeInfo['name'] . ' At: ');
            echo sprintf('<span style="width: 200px; display: inline-block;">%s</span>', $timeInfo['time']);
        }
    }

    /**
     * 返回当前毫秒级时间戳
     */
    public static function ms() {

        list($usec, $sec) = explode(' ', microtime());
        return intval(1000 * ($sec + $usec));
    }

    /**
     *   返回中文星期
     * @param   $week   int 0-6对应的星期，$week也可以是时间戳
     * @return  string  中文的星期几
     */
    public static function getWeekCn($week) {

        if (empty($week)) {
            return false;
        }
        $arr = array(
            1   => '星期一',
            2   => '星期二',
            3   => '星期三',
            4   => '星期四',
            5   => '星期五',
            6   => '星期六',
            7   => '星期日',
        );
        $key = $week > 7 ? intval(date('N', $week)) : $week;
        return array_key_exists($key, $arr) ? $arr[$key] : false;
    }
}