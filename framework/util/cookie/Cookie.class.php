<?php

/**
 *  Cookie 类
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/23
 * Time: 17:15
 */
class Cookie {

	public static function set($name, $value = '', $expire = 0, $path = '', $domain = '', $secure = false, $httpOnly = false) {
		$cookieName = self::getCookeName($name);
		$_COOKIE[$cookieName] = $value;
		return setcookie($cookieName, $value, $expire, $path, $domain, $secure, $httpOnly);
	}

	public static function get($cookieName) {
		$cookieName = self::getCookeName($cookieName);
		return isset($_COOKIE[$cookieName]) ? $_COOKIE[$cookieName] : null;
	}

	public static function getCookeName($cookieName) {
		if (GlobalConfig::inOnline()) {
			return $cookieName;
		} else {
			return 'dev_' . $cookieName;
		}
	}
}