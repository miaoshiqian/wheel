<?php
/**
 * @Copyright (c) 2011 Ganji Inc
 * @file          /code_base2/util/session/SessionNamespace.class.php
 * @author        dujian@ganji.com
 * @date          2011-03-31
 * session操作的封装，所有使用session的文件，必须require_one本文件
 */

/**
 * @class  : Session
 * @PURPOSE: Session是一个静态类，对session进行统计管理
 */
class Session {

	/**
	 * Session 初始化
	 *
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string $config [cookie_domain]  cookie域 默认会设置到主域
	 * @param int    $config [expire]  过期时间
	 */
	public static function init($config = array()) {
		static $initFinish = false;
		if ($initFinish) return;
		$initFinish = true;

		if (!empty($config['cookie_domain'])) {
			ini_set('session.cookie_domain', $config['cookie_domain']);
		} else {
			ini_set('session.cookie_domain', GlobalConfig::MS_COOKIE_DOMAIN); // 默认设置到主域上
		}
		if (isset($config['expire'])) {
			ini_set('session.cookie_lifetime', (int) $config['expire']);
		}

		$userInfo = Cookie::get('loginInfo');
		$userInfo = $userInfo ? json_decode($userInfo, 1) : false;
		$userId   = !empty($userInfo['user_id']) ? $userInfo['user_id'] : null;

		$sessionId = null;
		$needCopy  = false;
		if (!empty($config['session_id'])) { // 登录完后进行设置
			$sessionId = $config['session_id'];
			$needCopy  = true;
		} else if (!empty($userId)) { // 客户端有用户的user_id
			$sessionId = md5($userId);
		}

		// 是否copy数据
		if ($needCopy) {
			session_start();
			$oldData = $_SESSION;
			session_unset();
			session_destroy();
		}

		if ($sessionId) {
			session_id($sessionId);
		}

		session_start();

		if ($needCopy) {
			$_SESSION = array_merge($oldData, $_SESSION);
		}
	}

	/// 读取或写入session值，注意：仅存取一次,这个一般用于在redirect到某个页面前，先存入某个临时值，到新页面取出。
	/// @param $key  string  SESSION中的key
	/// @param $value    unknown_type    要存入SESSION的值 为空时，表示读取。
	/// @return unknown_type SESSION的值

	/**
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param      $key
	 * @param null $value
	 *
	 * @return null
	 */
	public static function flashData($key, $value = null) {
		self::init();
		if ($value === null) {
			if (isset($_SESSION[$key])) {
				$value = $_SESSION[$key];
				unset($_SESSION[$key]);
			}
		} else {
			$_SESSION[$key] = $value;
		}
		return $value;
	}

	/// 向session中存入某个值
	/// @param $key string SESSION中的key
	/// @param $value unknown_type 要存入SESSION的值
	/// @return unknown_type 返回当前key对应的旧值，如果原来key不存在，则返回NULL

	public static function setValue($key, $value) {
		self::init();
		$ret = null;
		if (isset($_SESSION[$key])) {
			$ret = $_SESSION[$key];
		}
		$_SESSION[$key] = $value;
		return $ret;
	}

	/// 从session中获取某个值
	/// @param $key string SESSION中的key
	/// @param $default 设置未找到SESSION的返回值，默认是NULL
	/// @return SESSION的值，未找到默认返回NULL

	public static function getValue($key, $default = null) {
		self::init();
		if (isset($_SESSION[$key])) {
			return $_SESSION[$key];
		}
		return $default;
	}

	/// 判断SESSION是否存在指定key
	/// @param $key string SESSION中的key
	/// @return bool 返回真或者假

	public static function isExists($key) {
		self::init();
		return isset($_SESSION[$key]);
	}

	/// 从session中删除某个值
	/// @param $key string SESSION中的key

	public static function delete($key) {
		self::init();
		if (isset($_SESSION[$key])) {
			unset($_SESSION[$key]);
		}
	}

	/// 清空Session

	public static function clear() {
		self::init();
		session_unset();
		session_destroy();
	}

}
