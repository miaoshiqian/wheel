<?php

class Util {

	public static $SPIDER_HOSTS = array(
		'img1', 'img2', 'img3', 'img4'
	);

	public static function getFromArray($key, $array, $default = null, $notEmpty = false) {
		if (empty($array) || !is_array($array) || !array_key_exists($key, $array) || ($notEmpty && empty($array[$key]))) {
			return $default;
		}
		return $array[$key];
	}

	/**
	 * 获取排除列表之外的数组
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param array $allArray
	 * @param array $exceptArray
	 *
	 * @return mixed
	 */
	public static function exceptArray($allArray, $exceptArray) {
		if ($exceptArray) foreach ($exceptArray as $v) {
			if (false !== ($key = array_search($v, $allArray))) {
				unset($allArray[$key]);
			}
		}
		return $allArray;
	}

	/**
	 * 检查参数是否都存在
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param array $checkList 检查的变量名 array('name', 'id')
	 * @param array $params    参数列表 array('name'=>1, 'id'=> 1)
	 *
	 * @return bool
	 * @throws Exception
	 */
	public static function checkParamsExist($checkList, $params = array()) {
		foreach ($checkList as $name) {
			if (!isset($params[$name])) {
				throw new SysException('缺少参数 ' . $name);
			}
		}
		return true;
	}

	/**
	 * 检查参数是否有为空的
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param array $checkList
	 * @param array $params
	 *
	 * @return bool
	 * @throws Exception
	 */
	public static function checkParamsEmpty($checkList, $params = array()) {
		foreach ($checkList as $name) {
			if (empty($params[$name])) {
				throw new SysException('参数 ' . $name . ' 不能为空');
			}
		}
		return true;
	}

	/**
	 * 检测变量是否为空
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param mixed $params
	 *
	 * @return bool
	 * @throws Exception
	 */
	public static function checkEmpty($params) {
		if (is_array($params)) foreach ($params as $key => $param) {
			if (empty($param)) {
				$msg = !is_int($key) ? "参数{$key}不能为空" : '参数不能为空';
				throw new SysException($msg, ExceptionCodeConfig::ERR_PARAMS_EMPTY);
			}
		} else if (empty($params)) {
			throw new SysException('参数不能为空', ExceptionCodeConfig::ERR_PARAMS_EMPTY);
		}
		return true;
	}

	/**
	 * 把字符串切割为数组，返回不为空的唯一集合
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param string $string
	 * @param string $split
	 *
	 * @return array
	 */
	public static function split2array($string, $split = ',') {
		$ret = array();
		foreach (explode($split, $string) as $v) {
			if ($t = trim($v)) {
				$ret[$t] = 1;
			}
		}
		return array_keys($ret);
	}


	/**
	 * 获取输出的异常信息
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param Exception $e
	 * @param string    $defaultMsg
	 *
	 * @return string
	 */
	public static function getUserExceptionMessage(Exception $e, $defaultMsg = ''){
		if($e instanceof UserException) {
			return $e->getMessage();
		}
		if($defaultMsg) {
			return $defaultMsg;
		}
		return '';
	}
	public static function formatStackTrace(Exception $e, $lf = "\r\n") {
		$trace = '';

		$i = 0;
		foreach (self::getStackTrace($e) as $trace_elem) {
			if ($i > 0) {
				$trace .= 'Caused by: ';
			}

			$trace .= get_class($e) . ": {$trace_elem['msg']}: {$trace_elem['code']}$lf";

			$j = 0;
			$thrown_at = "Thrown at [{$trace_elem['file']}:{$trace_elem['line']}]";
			$trace .= "\t#$j {$thrown_at}$lf";
			++$j;

			foreach ($trace_elem['trace'] as $trace_item) {
				$class = empty($trace_item['class']) ? '' : $trace_item['class'];
				$type = empty($trace_item['type']) ? '' : $trace_item['type'];
				$pos = empty($trace_item['file']) ? '' : "[{$trace_item['file']}:{$trace_item['line']}]";

				$args = '';
				if (!empty($trace_item['args'])) {
					$k = 0;
					foreach ($trace_item['args'] as $arg) {
						if ($k > 0) {
							$args .= ',';
						}

						$args .= gettype($arg);

						++$k;
					}
				}

				$called_at = "Called at {$class}{$type}{$trace_item['function']}($args) $pos";
				$trace .= "\t#$j {$called_at}$lf";

				++$j;
			}
			++$i;
		}

		return $trace;
	}

	protected static function getStackTrace(Exception $e) {
		$trace_elems = array();

		$i = 0;
		do {
			$trace_elems[$i]['msg'] = $e->getMessage();
			$trace_elems[$i]['code'] = $e->getCode();
			$trace_elems[$i]['file'] = $e->getFile();
			$trace_elems[$i]['line'] = $e->getLine();
			$trace_elems[$i]['trace'] = $e->getTrace();
			++$i;
			$e = $e->getPrevious();
		} while ($e);

		return $trace_elems;
	}

    /**
     * 编码转接 支持数组
     * @param type $fContents
     * @param type $from
     * @param type $to
     * @return type
     */
    public static function convertEncoding($fContents, $from = 'gbk', $to = 'utf-8') {
        $from = strtoupper($from) == 'UTF8' ? 'utf-8' : $from;
        $to = strtoupper($to) == 'UTF8' ? 'utf-8' : $to;
        if (strtoupper($from) === strtoupper($to) || empty($fContents) || (is_scalar($fContents) && !is_string($fContents))) {
            //如果编码相同或者非字符串标量则不转换
            return $fContents;
        }
        if (is_string($fContents)) {
            if (function_exists('mb_convert_encoding')) {
                return mb_convert_encoding($fContents, $to, $from);
            } elseif (function_exists('iconv')) {
                return iconv($from, $to, $fContents);
            } else {
                return $fContents;
            }
        } elseif (is_array($fContents)) {
            foreach ($fContents as $key => $val) {
                $_key = Util::convertEncoding($key, $from, $to);
                $fContents[$_key] = Util::convertEncoding($val, $from, $to);
                if ($key != $_key)
                    unset($fContents[$key]);
            }
            return $fContents;
        } else {
            return $fContents;
        }
    }

	public static function isDate($date) {
		return preg_match('/^\d{4}-\d{1,2}-\d{1,2}$/', $date);
	}

	public static function cny($ns) {
		static $cnums = array("零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"),
		$cnyunits = array("元", "角", "分"),
		$grees = array("拾", "佰", "仟", "万", "拾", "佰", "仟", "亿");
		list($ns1, $ns2) = explode(".", $ns, 2);
		$ns2 = array_filter(array($ns2[1], $ns2[0]));
		$ret = array_merge($ns2, array(implode("", self::_cny_map_unit(str_split
		($ns1), $grees)), ""));
		$ret = implode("", array_reverse(self::_cny_map_unit($ret, $cnyunits)));
		return str_replace(array_keys($cnums), $cnums, $ret);
	}

	private static function  _cny_map_unit($list, $units) {
		$ul = count($units);
		$xs = array();
		foreach (array_reverse($list) as $x) {
			$l = count($xs);
			if ($x != "0" || !($l % 4))
				$n = ($x == '0' ? '' : $x) . ($units[($l - 1) % $ul]);
			else $n = is_numeric($xs[0][0]) ? $x : '';
			array_unshift($xs, $n);
		}
		return $xs;
	}


	/**
	 * 格式化图片
	 *
	 * @param string $image  图片地址
	 * @param array  $option 配置参数
	 */
	public static function formatImageUrl($image, $option) {
		$replacement = '';
		if ($image) {
			//图片宽度
			$replacement .= '_' . $option['width'];
			//图片高度
			$replacement .= '-' . $option['height'];
			//图片是否剪裁
			if ($option['cut']) {
				$replacement .= 'c';
			}
			//图片质量
			$replacement .= '_' . $option['quality'];
			//是否锐化图片,1锐化，0不锐化
			$replacement .= '_' . $option['mark'];
			//版本号
			$replacement .= '_' . $option['version'];
			$pattern = "/(|_\d+-\d+[c|f]?_\d_\d_\d)(.[a-z]{3,})$/i";
			$replacement .= "\$2";
			$image = str_replace('_min', '', $image);
			$image = preg_replace($pattern, $replacement, $image);
			//字符串包含AD时，是否替换/AD/ 或/AD/AD/两种情况
			if ($option['replace_ad']) {
				if (strpos($image, '/AD/') !== false) {
					$image = str_replace('/AD/', '/CAD/', $image);
					if (strpos($image, '/AD/') !== false) {
						$image = str_replace('/AD/', '/CAD/', $image);
					}
				}
			}
		}
		if (strpos($image, 'http://') === false) {
			$image = 'http://img.273.com.cn/' . $image;
		}
		return $image;
	}

	public static function getNoWaterImageUrl($image) {
		$image = str_replace('http://img.273.com.cn/', '', $image);
		$time  = time();
		$token = md5('/' . $image . $time . 'M)!=tOz2)G7h$3x&yFMX539WjPYc#_UM');
		return $image . '?token=' . $token . '&time=' . $time;
	}

	public static function formatSpiderImage($path, $options) {
		$replacement = '';
		$hash        = crc32($path);
		if ($path) {
			//图片宽度
			$replacement .= '_' . $options['width'];
			//图片高度
			$replacement .= '-' . $options['height'];
			//图片是否剪裁
			if ($options['cut']) {
				$replacement .= 'c';
			}
			//图片质量
			$replacement .= '_' . $options['quality'];
			//是否锐化图片,1锐化，0不锐化
			$replacement .= '_' . ($options['mark'] ? 1 : 0);
			//版本号
			$replacement .= '_' . ($options['version'] ? $options['version'] : 0);
			$pattern = "/(|_\d+-\d+[c|f]?_\d_\d_\d)(.[a-z]{3,})$/i";
			$replacement .= "\$2";
			$path = str_replace('_min', '', $path);
			$path = preg_replace($pattern, $replacement, $path);
		}
		if (strpos($path, 'http://') === false) {
			$host = self::$SPIDER_HOSTS[$hash % count(self::$SPIDER_HOSTS)];
			$host = $host ? $host : 'img1';
			$path = 'http://' . $host . '.273.com.cn/' . $path;
		}
		return $path;
	}

	/**
	 * @desc 根据车况事故项生成车况展示图片
	 *
	 * @param $ckParams car_sale表中的condition_detai字段，需反序列化
	 */
	public static function getCkImageUrl($ckParams) {
		$host = 'http://img.273.com.cn/ckbimg/';
		$url  = $host . 'good.jpg';
		if (!empty($ckParams)) {
			include_once dirname(__FILE__) . '/../../../conf/common/check/CheckVars.class.php';
			$badItems = array();
			foreach (CheckVars::$FRAME_P2P as $key => $value) {
				if (in_array($value, $ckParams)) {
					$badItems[] = $key;
				}
			}
			if (!empty($badItems)) {
				$badItems = implode('_', $badItems);
				$url      = $host . $badItems . '.jpg';
			}
		}
		return $url;
	}

	/**
	 * @desc  判断uuid是否合法
	 * 规则根据js中的uuid生成规则反解析
	 *
	 * @param uuid
	 *
	 * @return true/false
	 * @refer /static/util/uuid_v2.js
	 */
	public static function isValidUuid($uuid) {
		//随机数位数
		$randomLen = 8;
		$len       = strlen($uuid);
		$ret       = false;
		do {
			//目前uuid必须是22位
			if ($len != 22) {
				break;
			}
			$random = substr($uuid, -$randomLen);
			$random = intval($random);
			// 10000000<$random<99999999
			if ($random < 10000000) {
				break;
			}
			$remain = substr($uuid, 0, $len - $randomLen);
			$remain = $remain - $random;
			//反转字符串
			$remain = strrev($remain);
			//得到的是毫秒时间戳，且包含1位随机数
			$remain = substr(0, -1);
			$time   = $remain / 1000;
			//时间戳必定小于当前时间戳
			if ($time < time()) {
				$ret = true;
			}
		} while (false);
		return $ret;
	}

	/**
	 * @desc   格式化输出数组
	 *
	 * @params array()
	 *
	 * @return string
	 * @refer  /log/bc/controller/IndexController.class.php ->getDetailAction
	 */
	public static function outputArray($params, $label = '', $return = false) {
		if (ini_get('html_errors')) {
			$content = "<pre>\n";
			if ($label != '') {
				$content .= "<strong>{$label}</strong>\n";
			}
			$content .= htmlspecialchars(print_r($params, true));
			$content .= "\n</pre>\n";
		} else {
			$content = $label . "\n" . print_r($params, true);
		}

		if ($return) {
			return $content;
		}
		echo $content;
	}

	/**
	 * 判断是否数组中指定的key是否都是不为空的。
	 *
	 * @param $key 要判断的key、可以是一个字符串、也可以是一个数组（里面包含多个要判断的key)
	 * @param $arr 目标数组
	 *
	 * @return 返回bool true：都不空符合条件、false：里面有空得、不满足条件
	 */
	public static function judgeEmpty($key, $arr) {
		$keys = array();
		if (!is_array($key)) {
			$keys[] = $key;
		} else {
			$keys = $key;
		}
		if (empty($keys) || !is_array($keys)) {
			return true;
		}
		foreach ($keys as $k) {
			if (empty($arr[$k])) {
				return false;
			}
		}
		return true;
	}

	/**
	 *   数组$params中拷贝一些字段
	 */
	public static function initFromArray($params, $fieldArr) {
		$data = array();
		foreach ($fieldArr as $field) {
			if (array_key_exists($field, $params)) {
				$data[$field] = $params[$field];
			}
		}
		return $data;
	}

	/**
	 *   从$list索引数组中，取出每行的一个字段作为键，然后返回关联数组
	 *          如果行中不存在这个键，那么会报warning，同时这行会被过滤；如果键重复，会覆盖之前的行
	 *
	 * @param   int|string $key      存在$fullList中的键
	 * @param   array      $fullList 完整的二维索引数组
	 *
	 * @return  array       返回一个新的关联数组
	 * @throws  SysException
	 */
	public static function listToAssoc($key, Array $fullList) {

		// 如果为空，返回
		if (empty($fullList)) {
			return array();
		}

		$retList = array();
		$warn    = false;
		foreach ($fullList as $row) {
			// 键不存在
			if (!is_array($row)) {
				throw new SysException('$fullList必须是二维数组！');
			}
			if (!array_key_exists($key, $row)) {
				$warn = true;
				continue;
			}
			$retList[$row[$key]] = $row;
		}

		// 只警告一次
		if ($warn) {
			Log::warn('user.error', "数组中不存在键：{$key}");
			trigger_error("数组中不存在键：{$key}", E_USER_WARNING);
		}
		return $retList;
	}

	/**
	 *   判断脚本是否是控制台运行
	 * @return  bool
	 */
	public static function isConsole() {
		if (ini_get('html_errors')) {
			return false;
		}
		return true;
	}

}
