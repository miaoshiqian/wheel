<?php
/**
 * Class Spider
 *
 * http 请求类
 * @author 李登科<lidengke@xiongying.com>
 * @example $spider = new Spider();
 *          $spider->timeout_ms = 1000*1000;
 *          $spider->refer = 'http://xxx.com';
 *          $spider->header = array('Content-type: text/plain', 'Cache-Control: no-cache');
 *          $result = $spider->get($url);
 *          $result = $spider->post($url, array('id'=>1, 'name'=>'n'));
 */
class Spider {
	private $option;
	private $curl;
	private $post;
	private $header;
	private $cookie;

	public function __construct() {

	}

	public function getHeader(){
		return $this->header;
	}

	public function getUrl(){
		return isset($this->option['url']) ? $this->option['url'] : null;
	}

	public function get_curl() {
		$this->_init_curl();
		return $this->curl;
	}

	public function soap($url = '') {
		$url && $this->__set('url', $url);
		return $this->_exec('soap');
	}

	public function post($url = '', $data = array()) {
		$url && $this->__set('url', $url);
		$data && $this->__set('post_data', $data);
		$this->post = true;
		return $this->_exec('post');
	}

	public function get($url, $data = array()) {
		if (empty($url)) return false;

		if (!empty($data)) {
			$dataStr = http_build_query($data);
			$url .=  strpos($url, '?') !== false ? '&' : '?';
			$url .= $dataStr;
		}

		$this->__set('url', $url);

		return $this->_exec('get');
	}

	/**
	 * 设置请求 参数
	 *
	 * @param string $key : 可用 key:<br>
	 *  <li>ssl 非空 时忽略ssl验证
	 *  <li>timeout_ms int 超时时间(毫秒)，默认为500
	 *  <li>port int 端口号
	 *  <li>refer string refer
	 *  <li>header array header
	 *  <li>cookie string cookie内容
	 * @param        $value
	 */
	public function __set($key, $value) {
		$this->option[$key] = $value;
	}

	private function _init_curl() {
		empty($this->curl) && $this->curl = curl_init();
		isset($this->option['post_data']) && $this->post = $this->_join_array($this->option['post_data']);
		isset($this->option['cookie']) && $this->cookie = $this->_join_array($this->option['cookie']);
		$this->header = empty($this->option['header']) ? array() : $this->option['header'];
	}

	private function _join_array($array) {
		$data = '';
		if (is_array($array)) {
			foreach ($array as $key => $value) {
				$data .= $data ? "&$key=$value" : "$key=$value";
			}
			return $data;
		}
		return $array;
	}

	private function _exec($type) {
		$this->_init_curl();
		curl_setopt($this->curl, CURLOPT_URL, $this->option['url']);
		// 禁用ssl
		if (!empty($this->option['ssl'])) {
			curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, false);
		}
		// 返回执行结果
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);

		$timeout_ms = 10*1000; // 默认超时时间(毫秒)
		if (isset($this->option['timeout_ms'])) {
			$timeout_ms = $this->option['timeout_ms'];
		}
		// 超时时间(毫秒)
		curl_setopt($this->curl, CURLOPT_TIMEOUT_MS, $timeout_ms);

		// 设置端口号
		if (isset($this->option['port'])) {
			curl_setopt($this->curl, CURLOPT_PORT, $this->option['port']);
		}
		// 设置refer
		if (isset($this->option['refer'])) {
			curl_setopt($this->curl, CURLOPT_REFERER, $this->option['refer']);
		}
		// soap 请求
		if ($type == 'soap') {
			$this->header[] = 'SOAPAction: "' . $this->option['url'] . '"';
			curl_setopt($this->curl, CURLOPT_HEADER, 0);
			curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 0);
		}
		// 设置 header
		if ($this->header) {
			$setHeader = array();
			foreach ($this->header as $key => $value ) {
				if (is_string($key)) { // 处理于未拼接的 传过来是 'Accept' => "application/json", 这种形式的
					$setHeader[] = sprintf('%s: %s', $key, $value);
				} else {
					$setHeader[] = $value;
				}
			}
			curl_setopt($this->curl, CURLOPT_HTTPHEADER, $setHeader);
		}
		// 设置cookie
		if ($this->cookie) {
			curl_setopt($this->curl, CURLOPT_HEADER, 0);
			curl_setopt($this->curl, CURLOPT_COOKIE, $this->cookie);
		}
		// 是否post请求
		if ($this->post) {
			curl_setopt($this->curl, CURLOPT_POST, 1);
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, $this->post);
			curl_setopt($this->curl, CURLOPT_BINARYTRANSFER, true);
		}
		// 权限验证
		if (isset($this->option['username']) && isset($this->option['password'])) {
			curl_setopt($this->curl, CURLOPT_USERPWD, "{$this->option['username']}:{$this->option['password']}");
		}
		$result = curl_exec($this->curl);
		curl_close($this->curl);
		$this->curl = null;
		return $result;
	}
}