<?php
class Request {
    public static function isAjax() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }
    
    public static function isPost() {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }
    
    public static function isGet() {
        return $_SERVER['REQUEST_METHOD'] == 'GET';
    }

    public static function getCurrentUrl() {
        $pageURL = 'http';
        if (!empty($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") $pageURL .= "s";
        $pageURL .= "://" . $_SERVER["HTTP_HOST"];
        if (!empty($_SERVER["SERVER_PORT"]) && $_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= ":" . $_SERVER["SERVER_PORT"];
        }
        $pageURL .= $_SERVER["REQUEST_URI"];
        return $pageURL;
    }

	public static function getCurrentDomain(){
		$currentUrl = self::getCurrentUrl();
		$parseUrl = parse_url($currentUrl);
		if(isset($parseUrl['scheme']) && isset($parseUrl['host'])) {
			return $parseUrl['scheme'].'://'.$parseUrl['host'];
		}
		return '';
	}

	private static function fixRequest(){
		// An associative array that by default contains the contents of $_GET, $_POST and $_COOKIE.
		if($_POST) {
			return $_REQUEST;
		} else {
			self::fixPost();
			$_REQUEST = array_merge($_REQUEST, $_POST);
			return $_REQUEST;
		}
	}

	private static function fixPost(){
		if($_POST) {
			return $_POST;
		} else {
			// POST时不提交 Content-Type: application/x-www-form-urlencoded;头部 $_POST里没东西
			$postRaw = file_get_contents("php://input");
			parse_str($postRaw, $postArr);
			$_POST = $postArr; // POST 修复下
			return $postArr;
		}
	}

    public static function getIp($useInt = true, $returnAll=false) {
        $ip = getenv('HTTP_CLIENT_IP');
		if($ip && strcasecmp($ip, "unknown") && !preg_match("/192\.168\.\d+\.\d+/", $ip)) {
            return self::_returnIp($ip, $useInt, $returnAll);
		} 
        
        $ip = getenv('HTTP_X_FORWARDED_FOR');
        if($ip && strcasecmp($ip, "unknown")) {
            return self::_returnIp($ip, $useInt, $returnAll);
        }

        $ip = getenv('REMOTE_ADDR');
        if($ip && strcasecmp($ip, "unknown")) {
            return self::_returnIp($ip, $useInt, $returnAll);
        }

        if (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
            if($ip && strcasecmp($ip, "unknown")) {
                return self::_returnIp($ip, $useInt, $returnAll);
            }
        }
        
		return false;
    }

	public static function getIpAddress($ip_raw = ''){
		empty($ip_raw) && $ip_raw = self::getIp(false);
		$QQwry = new QQWry($ip_raw);
		$country = $QQwry->Country;
		$address = $QQwry->Local;
		return array($country, $address);
	}

    private static function _returnIp($ip, $useInt, $returnAll) {
        if (!$ip) return false;

        $ips = preg_split("/[，, _]+/", $ip);
        if (!$returnAll) {
            $ip = $ips[count($ips)-1];
            return $useInt ? ip2long($ip) : $ip;
        }

        $ret = array();
        foreach ($ips as $ip) {
            $ret[] = $useInt ? ip2long($ip) : $ip;
        }
        return $ret;
    }

	public static function getRefer($default = ''){
		return empty($_SERVER['HTTP_REFERER']) ? $default : $_SERVER['HTTP_REFERER'];
	}
    
    public static function getCurrentQuery() {
        $current = Request::getCurrentUrl();
        $s = parse_url($current);
        return isset($s['query']) ? $s['query'] : null;
    }

	public static function getPostArr(){
		self::fixPost();
		return $_POST;
	}

	public static function getPostRaw(){
		return file_get_contents("php://input");
	}

	public static function getRequestArr(){
		self::fixRequest();
		return $_REQUEST;
	}

    public static function getPOST($key, $default = false, $enableHtml = false) {
	    self::fixPost();
        if (array_key_exists($key, $_POST)) {
            return !$enableHtml ? strip_tags($_POST[$key]) : $_POST[$key];
        }
        return $default;
    }

    public static function getGET($key, $default = false, $enableHtml = false) {
        if (array_key_exists($key, $_GET)) {
            return !$enableHtml ? strip_tags($_GET[$key]) : $_GET[$key];
        }
        return $default;
    }

    public static function getREQUEST($key, $default = false, $enableHtml = false) {
	    self::fixRequest();
        if (array_key_exists($key, $_REQUEST)) {
            return !$enableHtml ? strip_tags($_REQUEST[$key]) : $_REQUEST[$key];
        }
        return $default;
    }

    public static function getStr($key, $default = '', $enableHtml = false){
	    self::fixRequest();
        if (array_key_exists($key, $_REQUEST)) {
            return !$enableHtml ? strip_tags(trim($_REQUEST[$key])) : trim($_REQUEST[$key]);
        }
        return $default;
    }

    public static function getInt($key, $default = false, $enableHtml = false){
	    self::fixRequest();
        if (array_key_exists($key, $_REQUEST)) {
            $_REQUEST[$key] = (int)$_REQUEST[$key];
            return !$enableHtml ? strip_tags($_REQUEST[$key]) : $_REQUEST[$key];
        }
        return $default;
    }

    public static function getFloat($key, $precision = 4, $default = false, $enableHtml = false){
	    self::fixRequest();
        if (array_key_exists($key, $_REQUEST)) {
            $_REQUEST[$key] = sprintf('%.'.abs($precision).'f', $_REQUEST[$key]);
            return !$enableHtml ? strip_tags($_REQUEST[$key]) : $_REQUEST[$key];
        }
        return $default;
    }

    public static function getArray($key, $default = false, $enableHtml = false){
	    self::fixRequest();
        if (array_key_exists($key, $_REQUEST)) {
            $_REQUEST[$key] = (array) $_REQUEST[$key];
            !$enableHtml && array_walk_recursive($_REQUEST[$key], function(&$v, $k){$v = strip_tags($v);});
            return $_REQUEST[$key];
        }
        return $default;
    }


    public static function getCOOKIE($key, $default = false, $enableHtml = false) {
        if (isset ($_COOKIE[$key])) {
            return !$enableHtml ? strip_tags($_COOKIE[$key]) : $_COOKIE[$key];
        }
        return $default;
    }
    
    public static function socketPostJson($url, $post_string, $connectTimeout=3, $readTimeout=3) {
        $urlInfo = parse_url($url);
        $urlInfo["path"] = ($urlInfo["path"] == "" ? "/" : $urlInfo["path"]);
        $urlInfo["port"] = (!isset($urlInfo["port"]) ? 80 : $urlInfo["port"]);
        $hostIp = gethostbyname($urlInfo["host"]);
    
        $urlInfo["request"] =  $urlInfo["path"]    .
        (empty($urlInfo["query"]) ? "" : "?" . $urlInfo["query"]) .
        (empty($urlInfo["fragment"]) ? "" : "#" . $urlInfo["fragment"]);
    
        $fsock = fsockopen($hostIp, $urlInfo["port"], $errno, $errstr, $connectTimeout);
        if (false == $fsock) {
            throw new Exception('open socket failed!');
        }
        /* begin send data */
        $in = "POST " . $urlInfo["request"] . " HTTP/1.0\r\n";
        $in .= "Accept: */*\r\n";
        $in .= "User-Agent: ganji.com API PHP5 Client 1.0 (non-curl)\r\n";
        $in .= "Host: " . $urlInfo["host"] . "\r\n";
        $in .= "Content-type: application/json\r\n";
        $in .= "Content-Length: " . strlen($post_string) . "\r\n";
        $in .= "Connection: Close\r\n\r\n";
        $in .= $post_string . "\r\n\r\n";
    
        stream_set_timeout($fsock, $readTimeout);
        if (!fwrite($fsock, $in, strlen($in))) {
            fclose($fsock);
            throw new Exception('fclose socket failed!');
        }
        unset($in);
    
        //process response
        $out = "";
        while ($buff = fgets($fsock, 2048)) {
            $out .= $buff;
        }
        //finish socket
        fclose($fsock);
        $pos = strpos($out, "\r\n\r\n");
        $head = substr($out, 0, $pos);        //http head
        $status = substr($head, 0, strpos($head, "\r\n"));        //http status line
        $body = substr($out, $pos + 4, strlen($out) - ($pos + 4));        //page body
        if (preg_match("/^HTTP\/\d\.\d\s([\d]+)\s.*$/", $status, $matches)) {
            if (intval($matches[1]) / 100 == 2) {//return http get body
                return $body;
            } else {
                throw new Exception('http status not ok:' . $matches[1]);
            }
        } else {
            throw new Exception('http status invalid:' . $status . "\nOUT: " . var_export($out, true));
        }
    }

    //远程请求
    public static function RequestUrl($url, $data = [], $request = 0) {
        $ret = null;
        try {
            $this_header = array(
                "content-type: application/x-www-form-urlencoded;charset=UTF-8"
            );
            $ch = curl_init ();

            curl_setopt($ch, CURLOPT_HTTPHEADER,$this_header);
            curl_setopt ($ch, CURLOPT_URL, $url);            //定义表单提交地址
            curl_setopt ($ch, CURLOPT_POST, $request);       //定义提交类型 1：POST ；0：GET
            curl_setopt ($ch, CURLOPT_HEADER, 0);            //定义是否显示状态头 1：显示 ； 0：不显示
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);    //定义是否直接输出返回流
            curl_setopt ($ch, CURLOPT_POSTFIELDS, is_array($data) ? http_build_query($data) : $data);    //定义提交的数据

            $ret = curl_exec ($ch);
            curl_close ($ch);

        } catch (Exception $e) {

        }

        return $ret;
    }
}