<?php

class Pager {
	public $firstRow; // 起始行数
	public $listRows; // 列表每页显示行数
	public $totalRows; // 总行数
	public $totalPages; // 分页总页面数
	public $rollPage = 5;// 分页栏每页显示的页数
	public $url = '';

	private $nowPage = 1;

	// 分页显示定制
	private $config = array(
		'header' => '<li class=""><span>共 %TOTAL_ROW% 条记录</span></li>',
		'prev'   => '<<',
		'next'   => '>>',
		'first'  => '第一页',
		'last'   => '最后一页',
		'footer' => '<li class=""><span>共 %TOTAL_PAGE% 页</span></li>',
		'theme'  => '%HEADER% %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %FOOTER%',
	);

	/**
	 * 架构函数
	 *
	 * @param $totalRows |总的记录数
	 * @param $listRows  |每页显示记录数
	 * @param $nowPage   |当前页
	 */
	public function __construct($totalRows, $listRows, $nowPage) {
		$this->totalRows = $totalRows; //设置总记录数
		$this->listRows  = $listRows;  //设置每页显示行数
		$this->nowPage   = max($nowPage, 1);
		$this->firstRow  = $this->listRows * ($this->nowPage - 1);

		// 分页显示定制
		$this->config = array(
			'header' => '<li class=""><span>' . ('共') . ' %TOTAL_ROW% ' . ('条记录') . '</span></li>',
			'prev'   => '',
			'next'   => '',
			'first'  => '<<',
			'last'   => '>>',
			'footer' => '<li class=""><span>' . ('共') . ' %TOTAL_PAGE% ' . ('页') . '</span></li>',
			'theme'  => '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%',
		);
	}

	/**
	 * 组装分页链接
	 *
	 * @return string
	 */
	public function show() {
		if (0 == $this->totalRows) return '';
		/* 计算分页信息 */
		$this->totalPages = ceil($this->totalRows / $this->listRows); //总页数
		if (!empty($this->totalPages) && $this->nowPage > $this->totalPages) {
			$this->nowPage = $this->totalPages;
		}

		/* 计算分页零时变量 */
		$now_cool_page      = $this->rollPage / 2;
		$now_cool_page_ceil = ceil($now_cool_page);

		//上一页
		$up_row  = $this->nowPage - 1;
		$up_page = $up_row > 0 ? '<li><a class="prev" href="' . $this->createLink($up_row). '">' . $this->config['prev'] . '</a></li>' : '';

		//下一页
		$down_row  = $this->nowPage + 1;
		$down_page = ($down_row <= $this->totalPages) ? '<li><a class="next" href="'  . $this->createLink($down_row) . '">' . $this->config['next'] . '</a></li>' : '';

		//第一页
		$the_first = '';
		if ($this->totalPages > $this->rollPage && ($this->nowPage - $now_cool_page) >= 1) {
			$the_first = '<li><a class="first" href="' . $this->createLink(1) . '">' . $this->config['first'] . '</a></li>';
		}

		//最后一页
		$the_end = '';
		if ($this->totalPages > $this->rollPage && ($this->nowPage + $now_cool_page) < $this->totalPages) {
			$the_end = '<li><a class="last" href="' . $this->createLink($this->totalPages) . '">' . $this->config['last'] . '</a></li>';
		}

		//数字连接
		$link_page = "";
		for ($i = 1; $i <= $this->rollPage; $i++) {
			if (($this->nowPage - $now_cool_page) <= 0) {
				$page = $i;
			} elseif (($this->nowPage + $now_cool_page - 1) >= $this->totalPages) {
				$page = $this->totalPages - $this->rollPage + $i;
			} else {
				$page = $this->nowPage - $now_cool_page_ceil + $i;
			}
			if ($page > 0 && $page != $this->nowPage) {

				if ($page <= $this->totalPages) {
					$link_page .= '<li><a href="' . $this->createLink($page) . '">' . $page . '</a></li>';
				} else {
					break;
				}
			} else {
				if ($page > 0 && $this->totalPages != 1) {
					$link_page .= '<li class="active current"><span>' . $page . '</span></li>';
				}
			}
		}

		//替换分页内容
		$page_str = str_replace(
			array('%HEADER%', '%FOOTER%', '%NOW_PAGE%', '%UP_PAGE%', '%DOWN_PAGE%', '%FIRST%', '%LINK_PAGE%', '%END%', '%TOTAL_ROW%', '%TOTAL_PAGE%'),
			array($this->config['header'], $this->config['footer'], $this->nowPage, $up_page, $down_page, $the_first, $link_page, $the_end, $this->totalRows, $this->totalPages),
			$this->config['theme']);
		$page_str = trim($page_str);
		return !empty($page_str) ? "<ul class='pagination'>{$page_str}</ul>" : '';
	}

	private function createLink($pageNum) {
		if(false !== strpos($this->url, '%pn%')) {
			return str_replace('%pn%', $pageNum, $this->url);
		}
		return $this->url.$pageNum;
	}

	/**
	 * 获取分页数据和分页html
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param int $totalCount 总条数
	 * @param int $pageNum 当前页数
	 * @param int $pageSize 每页显示条数
	 * @param array $dataList 数据集
	 * @param string $pageUrl
	 *
	 * @return array array('dataList' => array(), 'pageHtml' =>'')
	 */
	public static function getPageList($totalCount, $pageNum, $pageSize, $dataList, $pageUrl){
		$pageModel = new self($totalCount, $pageSize, $pageNum);
		$pageModel->url = $pageUrl;
		return array(
			'dataList' => $dataList ? $dataList : array(),
			'totalCount' => $totalCount, // 总条数
			'pageCount' => ceil($totalCount / $pageSize), // 总页数
			'pageNum' => $pageNum, // 当前页
			'pageSize' => $pageSize, // 每页条数
			'pageHtml' => $pageModel->show(),
		);
	}

	/**
	 * 获取当前页数
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param int $offset 从0计算的 起始位置
	 * @param int $limit
	 *
	 * @return float
	 */
	public static function getPageNumByOffset($offset, $limit){
		$limit = max(1, $limit);
		return ceil(($offset + 1) / $limit);
	}

	/**
	 * 生成后台用datatable时输出的内容
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param array $pageList 调用Pager::getPageList 生成的结果
	 *
	 * @return array
	 */
	public static function getPageDataTable($pageList = array()){
		$data = array(
			'draw'            => Request::getInt('draw'),
			'recordsTotal'    => (int) $pageList['totalCount'],
			'recordsFiltered' => (int) $pageList['totalCount'],
			'data'            => $pageList['dataList'],
		);
		return $data;
	}

	/**
	 * 生成分页时的起点
	 * @author 李登科<lidengke@xiongying.com>
	 *
	 * @param int $pageNum 当前页数
	 * @param int $pageSize　每页显示条数
	 * @param int $type　1 返回起始页 2返回分页limit字符
	 *
	 * @return mixed|string
	 */
	public static function genPageOffset($pageNum, $pageSize, $type = 1){
		$pageNum = max(1, $pageNum);
		$startNum = ($pageNum - 1) * $pageSize;
		if ($type == 1) {
			return $startNum;
		} else {
			return sprintf('%d, %d', $startNum, $pageSize);
		}
	}

	/**
	 * 不需要分页时用的参数
	 * @author 李登科<lidengke@xiongying.com>
	 * @return array
	 */
	public static function genUnPageParams(){
		return array(
			'pageSize' => 10000,
			'pageNum' => 1,
		);
	}
}
