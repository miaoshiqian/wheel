<?php
/**
 *
 * @author aozhongxu
 * @date 6/11/15
 */
define('FRAMEWORK_PATH', __DIR__ . '/../../..');
require_once FRAMEWORK_PATH . '/../conf/conf/cache/RedisConfig.class.php';
require_once __DIR__ . '/../RedisClient3.class.php';

$redisClient = RedisClient3::getInstance(RedisConfig::$REDIS_NURSE);

$redisClient->hmset('abcd', array('123', 'dd' => 123, 345));


var_dump($redisClient->hgetall('abcd')); /* returns 2 */
