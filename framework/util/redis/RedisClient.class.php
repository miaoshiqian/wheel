<?php
/**
 *
 * @author aozhongxu
 * @date 6/11/15
 */

class RedisClientException extends Exception{
	public function __construct($code, $message){
		parent::__construct($message, $code);
	}
}


class RedisClient {

	// redis 配置项
	public $_config = array();

    // 缓存RedisClient对象
    private static $_instanceMaster = null;
    private static $_instanceSlave = null;

    // 缓存redis连接对象
    private static $_redisHandleList = array();

	// 只读的方法，从库访问
	private static $_readonlyFunc = array(
		'get', 'getbit', 'getrange', 'mget', 'getmultiple', 'strlen','setex',
		'dump', 'exists', 'keys', 'getkeys', 'scan', 'randomkey', 'type', 'ttl', 'pttl',
		'hexists', 'hget', 'hgetall', 'hkeys', 'hlen', 'hmget', 'hvals', 'hscan',
		'lindex', 'lget', 'llen', 'lsize', 'lrange', 'lgetrange',
		'scard', 'ssize', 'sdiff', 'sinter', 'sismember', 'scontains', 'smembers', 'sgetmembers',
		'zcard', 'zsize', 'zcount', 'zrange', 'zrangebyscore', 'zrevrangebyscore', 'zrangebylex', 'zrank', 'zrevrank', 'zrevrange', 'zscore', 'zscan',
	);

    /**
     * 获取Redis实例
     * @author 李登科<lidengke@xiongying.com>
     * @param array $config
     * @return Redis
     */
    public static function getInstanceMaster($config = array()) {
	    if(empty($config)) {
		    $config = RedisConfig::$REDIS_DEFAULT;
	    }

        if(!self::$_instanceMaster){
	        $o = new self($config, 'master');
	        self::$_instanceMaster = $o->_getRedis($config, 'master');
        }
	    return self::$_instanceMaster;
    }

    /**
     * 获取Redis实例
     * @author 李登科<lidengke@xiongying.com>
     * @param array $config
     * @return Redis
     */
    public static function getInstanceSlave($config = array()) {
	    if(empty($config)) {
		    $config = RedisConfig::$REDIS_DEFAULT;
	    }
        if(!self::$_instanceSlave){
	        $o = new self($config, 'slaves');
	        self::$_instanceSlave = $o->_getRedis($config, 'slaves');
        }
	    return self::$_instanceSlave;
    }

	/**
	 * @param array $config
	 * @param string $serviceType master|slave
	 *
	 * @throws RedisClientException
	 */
    private function __construct($config, $serviceType) {
    }

	private function _getRedis($config, $serviceType){
		if(empty($config) || empty($config[$serviceType]) || !count($config[$serviceType])){
			throw new RedisClientException(ExceptionCodeConfig::CUSTOM, 'redis配置错误');
		}
		$this->_config = $config;
		$randIndex = array_rand($config[$serviceType]);
		$server = $config[$serviceType][$randIndex];
		$hashKey = $this->getHashKey(array($server['host'], $server['port'], $server['timeout']));
		if(isset($_redisHandleList[$hashKey])){
			return $_redisHandleList[$hashKey];
		}

		for($i = 0; $i < 3; $i++){
			try{
				$redis = new Redis();
				if($redis->pconnect($server['host'], $server['port'], $server['timeout'], $hashKey)){
					$redis->select($server['db']);
					$_redisHandleList[$hashKey] = $redis;
					return $redis;
				} else {
					sleep(1);
				}
			}catch (Exception $e){
				sleep(1);
			}
		}

		throw new RedisClientException(ExceptionCodeConfig::CUSTOM, 'redis连接错误');
	}

    private function __clone() {}

    private function getHashKey($server) {
        ksort($server);
        return md5(json_encode($server));
    }

}
