<?php
class PushMessage {
    /**
     * 推送单条消息
     * @param $params['client_id'] 手机标识 必填项
     * @param $params['title'] 消息标题 必填项
     * @param $params['content'] 消息内容 必填项
     * @param $params['page_url'] 选填项
     * @param $params['conf'] 个推服务器配置,参考MessageConfig
     * @return Array
     * @throws Exception
     */
    public static function sendPush($params) {
        if (extension_loaded('gearman')) {
            include_once(dirname(__FILE__) . '/PushGearmanClient.class.php');
            $handle = PushGearmanClient::getInstance();
            $ret = $handle->sendOne($params);
        } else {
            include_once(dirname(__FILE__) . '/GeTuiMessage.class.php');
            $ret = GeTuiMessage::pushNoticeToSingle($params);
        }
        return $ret;
    }
}