<?php
class PushGearmanClient extends BaseGearman {


    /**
     * 构造函数
     */
    public function __construct($params) {
        return parent::__construct($params);
    }
    /**
     * 获取或创建（如果需要）一个日志实例
     */
    public static function getInstance() {
        $params = [
            'server' => '127.0.0.1',
            'port'   => 4730,
            'queue'  => 'sendOne',
        ];
        $hash = json_encode($params);
        if (!array_key_exists($hash, self::$instances)) {
            self::$instances[$hash] = new self ($params);
        }
        return self::$instances[$hash];
    }

    /**
     * 记录日志
     */
    public function sendOne($params) {
        $this->gmc->doBackground($this->queue, json_encode($params));
    }
}