<?php
/**
 * 消息推送
 * @author 陈朝阳<chenchaoyang@iyuesao.com>
 * @date 15/4/30
 * @copyright Copyright (c) 2015 安心客 Inc. (http://www.anxin365.cn)
 */

require_once dirname(__FILE__) . '/../../lib/getui/IGt.Push.php';
require_once dirname(__FILE__) . '/../../lib/getui/igetui/IGt.AppMessage.php';
require_once dirname(__FILE__) . '/../../lib/getui/igetui/template/IGt.BaseTemplate.php';

class GeTuiMessage {
    /**
     * 推送单条消息
     * @param $params['client_id'] 手机标识 必填项
     * @param $params['title'] 消息标题 必填项
     * @param $params['content'] 消息内容 必填项
     * @param $params['page_url'] 选填项
     * @param $params['conf'] 个推服务器配置
     * @return Array
     * @throws Exception
     */
    public static function pushNoticeToSingle($params) {
        $clientId = isset($params['client_id']) ? $params['client_id'] : 0;
        $title    = isset($params['title']) ? $params['title'] : '';
        $content  = isset($params['content']) ? $params['content'] : '';
        $pageUrl  = !empty($params['page_url']) ? $params['page_url'] : 'push';
        $conf     = isset($params['conf']) ? $params['conf'] : '';
        if (empty($clientId) || empty($title) || empty($content) || empty($conf)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 参数错误');
        }
        $appId        = isset($conf['app_id']) ? $conf['app_id'] : 0;
        $appSecret    = isset($conf['app_secret']) ? $conf['app_secret'] : '';
        $appKey       = isset($conf['app_key']) ? $conf['app_key'] : '';
        $masterSecret = isset($conf['master_secret']) ? $conf['master_secret'] : '';
        $gtHost       = isset($conf['gt_host']) ? $conf['gt_host'] : '';
        if (empty($appId) || empty($appSecret) || empty($appKey) || empty($masterSecret) || empty($gtHost)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 个推服务器配置错误');
        }

        $igt = new IGeTui($gtHost, $appKey, $masterSecret);
        $template =  new IGtNotificationTemplate();
        $template->set_appId($appId);
        $template->set_appkey($appKey);
        //用户点击消息后跳转的URL，默认启动app
        if (!empty($pageUrl)) {
            $template->set_transmissionType(2);
            $template->set_transmissionContent($pageUrl);
        } else {
            $template->set_transmissionType(1);
            $template->set_transmissionContent('');
        }
        //通知栏标题
        $template->set_title($title);
        //通知栏内容
        $template->set_text($content);
        //通知栏logo
        $template->set_logo("icon.png");
        //$template->set_logoURL("http://wwww.igetui.com/logo.png"); //通知栏logo链接
        //是否响铃
        $template->set_isRing(true);
        //是否震动
        $template->set_isVibrate(true);
        //通知栏是否可清除
        $template->set_isClearable(true);

        // iOS推送需要设置的pushInfo字段
        //$template ->set_pushInfo($actionLocKey,$badge,$message,$sound,$payload,$locKey,$locArgs,$launchImage);
        $template->set_pushInfo('anxin', 0, $content, '', $pageUrl, '', '', '');
        //$template ->set_pushInfo("test",1,"message","","","","","");

        $message = new IGtSingleMessage();
        //是否离线
        $message->set_isOffline(true);
        //离线时间
        $message->set_offlineExpireTime(3600 * 12 * 1000);
        //设置推送消息类型
        $message->set_data($template);
        //设置是否根据WIFI推送消息，1为wifi推送，0为不限制推送
        $message->set_PushNetWorkType(0);
        //接收方
        $target = new IGtTarget();
        $target->set_appId($appId);
        $target->set_clientId($clientId);
        $rep = $igt->pushMessageToSingle($message, $target);
        return $rep;
    }

    public static function pushMessageToList($params) {
        $clientIds = isset($params['client_ids']) ? $params['client_ids'] : array();
        $title     = isset($params['title']) ? $params['title'] : '';
        $content   = isset($params['content']) ? $params['content'] : '';
        $pageUrl   = isset($params['page_url']) ? $params['page_url'] : '';
        $conf      = isset($params['conf']) ? $params['conf'] : '';
        if (empty($clientIds) || empty($title) || empty($content) || empty($conf)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 参数错误');
        }
        $appId        = isset($conf['app_id']) ? $conf['app_id'] : 0;
        $appSecret    = isset($conf['app_secret']) ? $conf['app_secret'] : '';
        $appKey       = isset($conf['app_key']) ? $conf['app_key'] : '';
        $masterSecret = isset($conf['master_secret']) ? $conf['master_secret'] : '';
        $gtHost       = isset($conf['gt_host']) ? $conf['gt_host'] : '';
        if (empty($appId) || empty($appSecret) || empty($appKey) || empty($masterSecret) || empty($gtHost)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 个推服务器配置错误');
        }
        $rep = array();
        try {
            $igt = new IGeTui($gtHost, $appKey, $masterSecret);
            $template =  new IGtNotificationTemplate();
            $template->set_appId($appId);
            $template->set_appkey($appKey);
            //用户点击消息后跳转的URL，默认启动app
            if (!empty($pageUrl)) {
                $template->set_transmissionType(2);
                $template->set_transmissionContent($pageUrl);
            } else {
                $template->set_transmissionType(1);
                $template->set_transmissionContent('');
            }
            //通知栏标题
            $template->set_title($title);
            //通知栏内容
            $template->set_text($content);
            //通知栏logo
            $template->set_logo("icon.png");
            //$template->set_logoURL("http://wwww.igetui.com/logo.png"); //通知栏logo链接
            //是否响铃
            $template->set_isRing(true);
            //是否震动
            $template->set_isVibrate(true);
            //通知栏是否可清除
            $template->set_isClearable(true);

            // iOS推送需要设置的pushInfo字段
            //$template ->set_pushInfo($actionLocKey,$badge,$message,$sound,$payload,$locKey,$locArgs,$launchImage);
            $template->set_pushInfo('anxin', 0, $content, '', $pageUrl, '', '', '');
            //$template ->set_pushInfo("test",1,"message","","","","","");

            $message = new IGtListMessage();
            //是否离线
            $message->set_isOffline(true);
            //离线时间
            $message->set_offlineExpireTime(3600 * 12 * 1000);
            //设置推送消息类型
            $message->set_data($template);
            //设置是否根据WIFI推送消息，1为wifi推送，0为不限制推送
            $message->set_PushNetWorkType(0);
            $contentId = $igt->getContentId($message);
            $targetList = array();
            if (!empty($clientIds) && is_array($clientIds)) {
                foreach ($clientIds as $clientId) {
                    //接收方
                    $target = new IGtTarget();
                    $target->set_appId($appId);
                    $target->set_clientId($clientId);
                    $targetList[] = $target;
                }
            }
            $igt->needDetails = true;
            $rep = $igt->pushMessageToList($contentId, $targetList);
        } catch (Exception $e) {
            Log::fatal('GeTuiMessage_pushMessageToList', $params);
        }
        return $rep;
    }

    public static function iosPushToSingle($params){
        $clientId = isset($params['client_id']) ? $params['client_id'] : 0;
        $title    = isset($params['title']) ? $params['title'] : '';
        $content  = isset($params['content']) ? $params['content'] : '';
        $payload  = !empty($params['payload']) ? json_encode($params['payload']) : '';
        $conf     = isset($params['conf']) ? $params['conf'] : '';
        if (empty($clientId) || empty($title) || empty($content) || empty($conf)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 参数错误');
        }
        $appId        = isset($conf['app_id']) ? $conf['app_id'] : 0;
        $appSecret    = isset($conf['app_secret']) ? $conf['app_secret'] : '';
        $appKey       = isset($conf['app_key']) ? $conf['app_key'] : '';
        $masterSecret = isset($conf['master_secret']) ? $conf['master_secret'] : '';
        $gtHost       = isset($conf['gt_host']) ? $conf['gt_host'] : '';
        if (empty($appId) || empty($appSecret) || empty($appKey) || empty($masterSecret) || empty($gtHost)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 个推服务器配置错误');
        }

        $igt = new IGeTui($gtHost,$appKey,$masterSecret);

        $template =  new IGtTransmissionTemplate();
        //应用appid
        $template->set_appId($appId);
        //应用appkey
        $template->set_appkey($appKey);
        //透传消息类型
        $template->set_transmissionType(1);
        //透传内容
        $template->set_transmissionContent($payload);
        //$template ->set_pushInfo($actionLocKey,$badge,$message,$sound,$payload,$locKey,$locArgs,$launchImage);
        $template->set_pushInfo('anxin', 0, $content, '', $payload, '', '', '');

        $message = new IGtSingleMessage();
        //是否离线
        $message->set_isOffline(true);
        //离线时间
        $message->set_offlineExpireTime(3600 * 12 * 1000);
        //设置推送消息类型
        $message->set_data($template);
        //设置是否根据WIFI推送消息，1为wifi推送，0为不限制推送
        $message->set_PushNetWorkType(0);
        //接收方
        $target = new IGtTarget();
        $target->set_appId($appId);
        $target->set_clientId($clientId);
        $rep = $igt->pushMessageToSingle($message, $target);
        return $rep;
    }

    public static function iosPushToList($params) {
        $clientIds = isset($params['client_ids']) ? $params['client_ids'] : array();
        $title     = isset($params['title']) ? $params['title'] : '';
        $content   = isset($params['content']) ? $params['content'] : '';
        $payload   = !empty($params['payload']) ? json_encode($params['payload']) : '';
        $conf      = isset($params['conf']) ? $params['conf'] : '';
        if (empty($clientIds) || empty($title) || empty($content) || empty($conf)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 参数错误');
        }
        $appId        = isset($conf['app_id']) ? $conf['app_id'] : 0;
        $appSecret    = isset($conf['app_secret']) ? $conf['app_secret'] : '';
        $appKey       = isset($conf['app_key']) ? $conf['app_key'] : '';
        $masterSecret = isset($conf['master_secret']) ? $conf['master_secret'] : '';
        $gtHost       = isset($conf['gt_host']) ? $conf['gt_host'] : '';
        if (empty($appId) || empty($appSecret) || empty($appKey) || empty($masterSecret) || empty($gtHost)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 个推服务器配置错误');
        }
        $rep = array();
        try {
            $igt = new IGeTui($gtHost, $appKey, $masterSecret);
            $template =  new IGtTransmissionTemplate();
            //应用appid
            $template->set_appId($appId);
            //应用appkey
            $template->set_appkey($appKey);
            //透传消息类型
            $template->set_transmissionType(1);
            //透传内容
            $template->set_transmissionContent($payload);
            //$template ->set_pushInfo($actionLocKey,$badge,$message,$sound,$payload,$locKey,$locArgs,$launchImage);
            $template->set_pushInfo('anxin', 0, $content, '', $payload, '', '', '');

            $message = new IGtListMessage();
            //是否离线
            $message->set_isOffline(true);
            //离线时间
            $message->set_offlineExpireTime(3600 * 12 * 1000);
            //设置推送消息类型
            $message->set_data($template);
            //设置是否根据WIFI推送消息，1为wifi推送，0为不限制推送
            $message->set_PushNetWorkType(0);
            $contentId = $igt->getContentId($message);
            $targetList = array();
            if (!empty($clientIds) && is_array($clientIds)) {
                foreach ($clientIds as $clientId) {
                    //接收方
                    $target = new IGtTarget();
                    $target->set_appId($appId);
                    $target->set_clientId($clientId);
                    $targetList[] = $target;
                }
            }
            $igt->needDetails = true;
            $rep = $igt->pushMessageToList($contentId, $targetList);
        } catch (Exception $e) {
            Log::fatal('GeTuiMessage_iosPushToList', $params);
        }
        return $rep;
    }

    public static function ardPushToSingle($params) {
        $clientId = isset($params['client_id']) ? $params['client_id'] : 0;
        $title    = isset($params['title']) ? $params['title'] : '';
        $content  = isset($params['content']) ? $params['content'] : '';
        $payload  = !empty($params['payload']) ? json_encode($params['payload']) : '';
        $conf     = isset($params['conf']) ? $params['conf'] : '';
        if (empty($clientId) || empty($title) || empty($content) || empty($conf)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 参数错误');
        }
        $appId        = isset($conf['app_id']) ? $conf['app_id'] : 0;
        $appSecret    = isset($conf['app_secret']) ? $conf['app_secret'] : '';
        $appKey       = isset($conf['app_key']) ? $conf['app_key'] : '';
        $masterSecret = isset($conf['master_secret']) ? $conf['master_secret'] : '';
        $gtHost       = isset($conf['gt_host']) ? $conf['gt_host'] : '';
        if (empty($appId) || empty($appSecret) || empty($appKey) || empty($masterSecret) || empty($gtHost)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 个推服务器配置错误');
        }

        $igt = new IGeTui($gtHost, $appKey, $masterSecret);
        $template =  new IGtNotificationTemplate();
        $template->set_appId($appId);
        $template->set_appkey($appKey);
        //用户点击消息后跳转的URL，默认启动app
        if (!empty($payload)) {
            $template->set_transmissionType(2);
            $template->set_transmissionContent($payload);
        } else {
            $template->set_transmissionType(1);
            $template->set_transmissionContent('');
        }
        //通知栏标题
        $template->set_title($title);
        //通知栏内容
        $template->set_text($content);
        //通知栏logo
        $template->set_logo("icon.png");
        //$template->set_logoURL("http://wwww.igetui.com/logo.png"); //通知栏logo链接
        //是否响铃
        $template->set_isRing(true);
        //是否震动
        $template->set_isVibrate(true);
        //通知栏是否可清除
        $template->set_isClearable(true);

        // iOS推送需要设置的pushInfo字段
        //$template ->set_pushInfo($actionLocKey,$badge,$message,$sound,$payload,$locKey,$locArgs,$launchImage);
        //$template->set_pushInfo('anxin', 0, $content, '', $pageUrl, '', '', '');
        //$template ->set_pushInfo("test",1,"message","","","","","");

        $message = new IGtSingleMessage();
        //是否离线
        $message->set_isOffline(true);
        //离线时间
        $message->set_offlineExpireTime(3600 * 12 * 1000);
        //设置推送消息类型
        $message->set_data($template);
        //设置是否根据WIFI推送消息，1为wifi推送，0为不限制推送
        $message->set_PushNetWorkType(0);
        //接收方
        $target = new IGtTarget();
        $target->set_appId($appId);
        $target->set_clientId($clientId);
        $rep = $igt->pushMessageToSingle($message, $target);
        return $rep;
    }

    public static function ardPushToList($params) {
        $clientIds = isset($params['client_ids']) ? $params['client_ids'] : array();
        $title     = isset($params['title']) ? $params['title'] : '';
        $content   = isset($params['content']) ? $params['content'] : '';
        $payload   = !empty($params['payload']) ? json_encode($params['payload']) : '';
        $conf      = isset($params['conf']) ? $params['conf'] : '';
        if (empty($clientIds) || empty($title) || empty($content) || empty($conf)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 参数错误');
        }
        $appId        = isset($conf['app_id']) ? $conf['app_id'] : 0;
        $appSecret    = isset($conf['app_secret']) ? $conf['app_secret'] : '';
        $appKey       = isset($conf['app_key']) ? $conf['app_key'] : '';
        $masterSecret = isset($conf['master_secret']) ? $conf['master_secret'] : '';
        $gtHost       = isset($conf['gt_host']) ? $conf['gt_host'] : '';
        if (empty($appId) || empty($appSecret) || empty($appKey) || empty($masterSecret) || empty($gtHost)) {
            throw new Exception('GeTuiMessage::pushNoticeToSingle 个推服务器配置错误');
        }
        $rep = array();
        try {
            $igt = new IGeTui($gtHost, $appKey, $masterSecret);
            $template =  new IGtNotificationTemplate();
            $template->set_appId($appId);
            $template->set_appkey($appKey);
            //用户点击消息后跳转的URL，默认启动app
            if (!empty($payload)) {
                $template->set_transmissionType(2);
                $template->set_transmissionContent($payload);
            } else {
                $template->set_transmissionType(1);
                $template->set_transmissionContent('');
            }
            //通知栏标题
            $template->set_title($title);
            //通知栏内容
            $template->set_text($content);
            //通知栏logo
            $template->set_logo("icon.png");
            //$template->set_logoURL("http://wwww.igetui.com/logo.png"); //通知栏logo链接
            //是否响铃
            $template->set_isRing(true);
            //是否震动
            $template->set_isVibrate(true);
            //通知栏是否可清除
            $template->set_isClearable(true);

            // iOS推送需要设置的pushInfo字段
            //$template ->set_pushInfo($actionLocKey,$badge,$message,$sound,$payload,$locKey,$locArgs,$launchImage);
            //$template->set_pushInfo('anxin', 0, $content, '', $pageUrl, '', '', '');
            //$template ->set_pushInfo("test",1,"message","","","","","");

            $message = new IGtListMessage();
            //是否离线
            $message->set_isOffline(true);
            //离线时间
            $message->set_offlineExpireTime(3600 * 12 * 1000);
            //设置推送消息类型
            $message->set_data($template);
            //设置是否根据WIFI推送消息，1为wifi推送，0为不限制推送
            $message->set_PushNetWorkType(0);
            $contentId = $igt->getContentId($message);
            $targetList = array();
            if (!empty($clientIds) && is_array($clientIds)) {
                foreach ($clientIds as $clientId) {
                    //接收方
                    $target = new IGtTarget();
                    $target->set_appId($appId);
                    $target->set_clientId($clientId);
                    $targetList[] = $target;
                }
            }
            $igt->needDetails = true;
            $rep = $igt->pushMessageToList($contentId, $targetList);
        } catch (Exception $e) {
            Log::fatal('GeTuiMessage_pushMessageToList', $params);
        }
        return $rep;
    }

}