<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('memory_limit','1024M');
set_time_limit(0);


if(!defined("ROOT_PATH")) {
    define('ROOT_PATH', dirname(__FILE__) . '/../../..');
}
if(!defined("FRAMEWORK_PATH")) {
    define('FRAMEWORK_PATH', ROOT_PATH . '/framework');
}

//自动加载
require_once FRAMEWORK_PATH . '/common/AutoLoader.class.php';
AutoLoader::setAutoDir(ROOT_PATH . '/conf/conf/');
AutoLoader::setAutoDir(FRAMEWORK_PATH . '/util/');

class PushGearmanWorker {

    public function run() {
        $worker = new GearmanWorker();
        $worker->addServer();
        $worker->addFunction("sendOne", function (GearmanJob $job) {
            $workload = json_decode($job->workload(), true);
            try {
                GeTuiMessage::pushNoticeToSingle($workload);
                Log::info('getui.push.success', $workload);
            } catch(Exception $e) {
                Log::fatal('getui.push.fail', [
	                $workload,
	                $e,
                ]);
            }

        });

        while ($worker->work()) ;
    }


}

$PushGearmanWorker = new PushGearmanWorker();
$PushGearmanWorker->run();