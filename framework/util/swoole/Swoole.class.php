<?php
/**
 * swoole V2 使用静态方法,调用更方便,效率更高
 * @author guochaohui
 */
class Swoole {


    public static $server = [
        'host' => '127.0.0.1',
        'port' => 9501,
        'timeout' => -1,
    ];

    /**
     * 构造swoole客户端
     *
     * @param array $server
     *                  host,
     *                  port,
     *                  timeout
     *
     * @return swoole_client
     */
    public static function createSwoole($server = []) {
        $server = empty($server) ? self::$server : $server;
        $host    = Util::getFromArray('host', $server, '');
        $port    = Util::getFromArray('port', $server, 0);
        $timeout = Util::getFromArray('timeout', $server, 0);
        $swoole = new swoole_client(SWOOLE_SOCK_TCP);
        $swoole->connect($host, $port, $timeout);
        return $swoole;
    }

    /**
     * 使用swoole异步执行任务,没有返回值
     *
     * @param $module
     * @param $params
     * @param array $server
     *
     * @throws Exception
     */
    public static function task($module, $params, $server = []) {
        if(empty($module) || empty($params)) {
            throw new Exception('Swoole::task,参数不完整');
        }

        $sendParams['module'] = $module;
        $sendParams['params'] = $params;
        $sendParams['type'] = 2;//使用task,不必返回结果
        $swooler = self::createSwoole($server);
        //发送数据到服务器端
        $swooler->send(json_encode($sendParams));
    }

    /**
     * 使用swoole同步回调API,有返回值
     *
     * @param $module
     * @param $params
     * @param array $server
     * @throws Exception
     *
     * @return array api返回数组
     */
    public static function callApi($module, $params, $server = []) {
        if(empty($module) || empty($params)) {
            throw new Exception('Swoole::task,参数不完整');
        }

        $sendParams['module'] = $module;
        $sendParams['params'] = $params;
        $sendParams['type'] = 1;//使用task,不必返回结果
        $swooler = self::createSwoole($server);
        //发送数据到服务器端
        $swooler->send(json_encode($sendParams));

        //从服务器接收数据
        $ret = $swooler->recv();

        return json_decode($ret, true);
    }
}