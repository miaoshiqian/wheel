<?php

class CallApi {
    /**
     * 外部调用养老的INTERFACE
     *
     * @param $params 同养老的interface参数
     *
     * @throws Exception
     *
     * @return $ret
     */
    public static function yanglao($modules, $params) {

        //找到养老API路径,自动加载
        if (!defined('YANGLAO_INTERFACE')) {
            define('YANGLAO_INTERFACE', dirname(__FILE__) . '/../../../yanglao/api/interface');
        }
        AutoLoader::setAutoDir(YANGLAO_INTERFACE);

        //检查传入参数是否正确
        if (empty($modules)) {
            throw new Exception('调用养老API模块参数不能为空');
        }

        //格式化数据
        $moduleArr = explode(".", $modules);
        $interface = Util::getFromArray(0, $moduleArr) . 'Interface';
        $interface = ucfirst($interface);
        $func      = Util::getFromArray(1, $moduleArr);

        //检查调用参数是否正确
        if (empty($interface) || empty($func)) {
            throw new Exception($interface .'::'. $func. '不正确!');
        }

        //执行并返回结果
        try {
            $ret = $interface::$func($params);
        }catch (Exception $e) {
            Log::fatal('CallApi::yanglao.error', [
	            'modules' => $modules,
	            'params' => $params,
	            'error' => $e,
            ]);
            throw new Exception('调用养老API模块错误,请查看日志CallApi::yanglao.error');
        }
        return $ret;

    }

    /**
     * 外部调用育儿的INTERFACE
     *
     * @param $params 同育儿的interface参数
     *
     * @throws Exception
     *
     * @return $ret
     */
    public static function yuer($modules, $params) {
        if (!defined('YUER_INTERFACE')) {
            define('YUER_INTERFACE', dirname(__FILE__) . '/../../../yuer/api/interface');
        }
        AutoLoader::setAutoDir(YUER_INTERFACE);
        if (empty($modules)) {
            throw new Exception('调用养老API模块参数不能为空');
        }
        $moduleArr = explode(".", $modules);
        $interface = Util::getFromArray(0, $moduleArr) . 'Interface';
        $interface = ucfirst($interface);
        $func      = Util::getFromArray(1, $moduleArr);

        if (empty($interface) || empty($func)) {
            throw new Exception($interface .'::'. $func. '不正确!');
        }

        try {
            $ret = $interface::$func($params);
        }catch (Exception $e) {
            Log::fatal('CallApi::yuer.error', [
	            'modules' => $modules,
	            'params' => $params,
	            'error' => $e,
            ]);
            throw new Exception('调用养老API模块错误,请查看日志CallApi::yuer.error');
        }

        return $ret;
    }

}