<?php
/**
 * 七牛文件云存储工具类
 * @author 缪石乾<miaoshiqian@iyuesao.com>
 * @date 2015年1月26日 下午5:01:08
 */
require_once dirname(__FILE__) . '/../../lib/qiniu/rs.php';
require_once dirname(__FILE__) . '/../../lib/qiniu/fop.php';
require_once dirname(__FILE__) . '/../../lib/qiniu/io.php';
require_once __DIR__ . '/../../../conf/common/ThirdPartyConfig.class.php';

class QiNiu {
    private static $_ACCESS_KEY   = ThirdPartyConfig::QINIU_ACCESS_KEY;
    private static $_SECRET_KEY   = ThirdPartyConfig::QINIU_SECRET_KEY;
    private static $_QINIU_DOMAIN = ThirdPartyConfig::QINIU_DOMAIN;
    private static $_BUCKET       = ThirdPartyConfig::QINIU_BUCKET;

    //返回不超过指定宽高的图片（等比例缩放的前提下、w和h  哪个先到边界就停住）
    const MODE_LIMIT_W_H = 0;

    //剪裁正中部分,等比例缩小
    const MODE_CUT_W_H = 1;

    //宽度固定为指定值,高度等比例缩小
    const MODE_LIMIT_W = 2;

    //高度固定为指定值,宽度等比例缩小
    const MODE_LIMIT_H = 3;

    public static $MODE = [
        self::MODE_LIMIT_W_H => 2,
        self::MODE_CUT_W_H => 1,
        self::MODE_LIMIT_W => 1,
        self::MODE_LIMIT_H => 1,
    ];

    /**
     * 格式化图片、返回需要的外链url
     * @param string $path 图片的路径
     * @param array $option 图片的参数、即为、width、height这些参数
     * @param $option['mode'] （默认为0）的特殊说明：
     *          =0 ：即为返回不超过指定的宽高的图片。（等比例缩放的前提下、w和h  哪个先到边界就停住）
     *          =1 ：剪裁正中部分,等比例缩小
     * @return url
     */
    public static function formatImageUrl($path, $option = array()) {
        if (empty($path)) {
            return '';
        }
        
        //Qiniu_SetKeys(self::$_ACCESS_KEY, self::$_SECRET_KEY);

        $sign = '?';
        //有高级处理的单独处理
        if (strpos($path, '?imageMogr2') !== false) {
            //管道处理
            $sign = '|';
            $baseUrl = 'http://' . self::$_QINIU_DOMAIN . '/' . $path;
        } else {
            $baseUrl = Qiniu_RS_MakeBaseUrl(self::$_QINIU_DOMAIN, $path);
        }
        if (empty($option['width']) && empty($option['height'])) {
            return $baseUrl;
        } else {
            $mode = !empty($option['mode']) && in_array($option['mode'], array(0, 1, 2, 3)) ? $option['mode'] : 0;
            $extUrl = 'imageView2/' . Util::getFromArray($mode, self::$MODE);
            $width = !empty($option['width']) ? $option['width'] : '';
            $height = !empty($option['height']) ? $option['height'] : '';

            if($mode == 2) {
                $url = $baseUrl . $sign . $extUrl .'/w/'. $width;
            } else if($mode == 3) {
                $url = $baseUrl . $sign . $extUrl .'/h/' . $height;
            } else {
                $url = $baseUrl . $sign . $extUrl .'/w/'. $width . '/h/' . $height;
            }

            return $url;
        }
    }
    
    /**
     * 上传本地文件
     * @param $file 要上传的文件的本地路径。例如从表单上来的$_FILES 里面的temp_name
     * @param $pre 前缀。必须要。如果不传默认为 nopre。$pre 图片的key前缀。方便后面归类
     * @param $suffix 后缀。如果有需要的话、就传进来、默认没有
     */
    public static function upLocalFile($file, $pre = 'nopre', $suffix = '') {
        $bucket = self::$_BUCKET;
        $key1 = $pre . "-" . time() . "-" . rand(1000, 9999) . $suffix;
        Qiniu_SetKeys(self::$_ACCESS_KEY, self::$_SECRET_KEY);
        
        $putPolicy = new Qiniu_RS_PutPolicy($bucket);
        $upToken = $putPolicy->Token(null);
        $putExtra = new Qiniu_PutExtra();
        $putExtra->Crc32 = 1;
        list($ret, $err) = Qiniu_PutFile($upToken, $key1, $file, $putExtra);
        if ($err !== null) {
            $errorInfo = get_object_vars($err);
            Log::info('qiniu.upLocalFile', "上传资源失败，{$errorInfo['Err']}");
            throw new Exception($errorInfo['Err']);
        } else {
            return $ret;
        }
    }

    /**
     *   上传远程文件。七牛没有相应api，所以该方法通过本地转存的方式实现
     */
    public static function upRemoteFile($url, $pre = 'nopre', $suffix = '') {

        // 转存
        $curl = new Curl();
        $content = $curl->get($url);
        if (false === $content) {
            Log::info('qiniu.upRemoteFile', "远程下载资源失败，{$url}");
            throw new Exception("远程下载资源失败，{$url}");
        }
        if (empty($content)) {
            Log::info('qiniu.upRemoteFile', "资源不存在！{$url}");
            throw new Exception("资源不存在！{$url}");
        }
        $file = tempnam('/tmp', 'upRemote');
        $ret = file_put_contents($file, $content);
        if (false === $ret) {
            Log::info('qiniu.upRemoteFile', "保存文件失败！");
            throw new Exception("保存文件失败！");
        }

        $re = QiNiu::upLocalFile($file, $pre, $suffix);
        unlink($file);
        return $re;
    }

    /**
     * 获取七牛上传图片的Token
     */
    public static function getUpToken() {
        $time = strtotime("+1 day");
        $putPolicy = array(
            'scope'    => self::$_BUCKET,
            'deadline' => $time,
        );
        $putPolicy = json_encode($putPolicy);

        $encodedPutPolicy = base64_encode($putPolicy);
        $encodedPutPolicy = str_replace(array('+','/'),array('-','_'), $encodedPutPolicy);

        $signature = "";
        $key = self::$_SECRET_KEY;
        if (function_exists('hash_hmac')) {
            $data = hash_hmac("sha1", $encodedPutPolicy, $key, true);
            $data = base64_encode($data);
            $signature = str_replace(array('+','/'),array('-','_'), $data);
        } else {
            $blocksize = 64;
            $hashfunc = 'sha1';
            if (strlen($key) > $blocksize) {
                $key = pack('H*', $hashfunc($key));
            }
            $key  = str_pad($key, $blocksize, chr(0x00));
            $ipad = str_repeat(chr(0x36),$blocksize);
            $opad = str_repeat(chr(0x5c),$blocksize);
            $hmac = pack(
                'H*', $hashfunc(
                    ($key^$opad).pack(
                        'H*',$hashfunc(
                            ($key^$ipad).$encodedPutPolicy
                        )
                    )
                )
            );
            $signature = urlsafe_b64encode($hmac);
        }
        $uploadToken = self::$_ACCESS_KEY . ':' . $signature . ':' . $encodedPutPolicy;
        return $uploadToken;
    }
}