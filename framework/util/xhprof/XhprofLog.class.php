<?php

/**
 * xhprof安心可系统适配器,使用xhprof更方便
 * @author : 郭朝辉
 */
class XhprofLog {
    //保存随机取样是否使用xhprof
    public static $CAN_USE = false;

    //值为true时,强制使用xhprof记录
    public static $DEBUG = false;
    /**
     * 函数运行的地方就是xhprof分析程序的起点的起点
     */
    public static function start() {
        //随机取样,按比例使用xhprof
        $randMax = 5;
        if(mt_rand(1, $randMax) == 1 || self::$DEBUG) {
            if(function_exists('xhprof_enable')) {
                //xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
                xhprof_enable(XHPROF_FLAGS_NO_BUILTINS | XHPROF_FLAGS_CPU | XHPROF_FLAGS_MEMORY);
                self::$CAN_USE = true;
            }
        }
    }

    public static function end() {
        if(self::$CAN_USE == true) {
            self::$CAN_USE == false;
            $data = xhprof_disable();
            $wt = Util::getFromArray('wt', Util::getFromArray('main()', $data));
            if ($wt > 500000 || self::$DEBUG) {
                include_once FRAMEWORK_PATH . '/util/xhprof/xhprof_lib/utils/xhprof_lib.php';
                include_once FRAMEWORK_PATH . '/util/xhprof/xhprof_lib/utils/xhprof_runs.php';
                $objXhprofRun = new XHProfRuns_Default();
                $objXhprofRun->save_run($data, 1);
            }
        }
    }
}