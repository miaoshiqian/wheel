<?php

class Captcha {
    private $config = null;
    public function __construct() {
        if( !function_exists('gd_info') ) {
            throw new Exception('Required GD library is missing');
        }
        $this->config = include __DIR__.'/config.php';
    }

	public function getCode(){
		$code = '';
		$length = rand($this->config['min_length'], $this->config['max_length']);
		while( strlen($code) < $length ) {
			$code .= substr($this->config['characters'], rand() % (strlen($this->config['characters'])), 1);
		}
		return $code;
	}

    public function show($code) {
	    //ob_clean();

        // Pick random background, get info, and start captcha
        $background = $this->config['png_backgrounds'][rand(0, count($this->config['png_backgrounds']) -1)];

        list($bg_width, $bg_height, $bg_type, $bg_attr) = getimagesize($background);

        // Create captcha object
        $captcha = imagecreatefrompng($background);
        imagealphablending($captcha, true);
        imagesavealpha($captcha , true);

        $color = $this->hex2rgb($this->config['color']);
        $color = imagecolorallocate($captcha, $color['r'], $color['g'], $color['b']);

        // Determine text angle
        $angle = rand( $this->config['angle_min'], $this->config['angle_max'] ) * (rand(0, 1) == 1 ? -1 : 1);

        // Select font randomly
        $font = $this->config['fonts'][rand(0, count($this->config['fonts']) - 1)];

        // Verify font file exists
        if( !file_exists($font) ) throw new Exception('Font file not found: ' . $font);

        //Set the font size.
        $font_size = rand($this->config['min_font_size'], $this->config['max_font_size']);

        $text_box_size = imagettfbbox($font_size, $angle, $font, $code);

        // Determine text position
        $box_width = abs($text_box_size[6] - $text_box_size[2]);
        $box_height = abs($text_box_size[5] - $text_box_size[1]);
        $text_pos_x_min = 0;
        $text_pos_x_max = ($bg_width) - ($box_width);
        $text_pos_x = rand($text_pos_x_min, $text_pos_x_max);
        $text_pos_y_min = $box_height;
        $text_pos_y_max = ($bg_height) - ($box_height / 2);
        $text_pos_y = rand($text_pos_y_min, $text_pos_y_max);

        // Draw shadow
        if( $this->config['shadow'] ){
            $shadow_color = $this->hex2rgb($this->config['shadow_color']);
            $shadow_color = imagecolorallocate($captcha, $shadow_color['r'], $shadow_color['g'], $shadow_color['b']);
            imagettftext($captcha, $font_size, $angle, $text_pos_x + $this->config['shadow_offset_x'], $text_pos_y + $this->config['shadow_offset_y'], $shadow_color, $font, $code);
        }

        // Draw text
        imagettftext($captcha, $font_size, $angle, $text_pos_x, $text_pos_y, $color, $font, $code);

        // Output image
        $this->no_cache();
        header("Content-type: image/png");
        imagepng($captcha);
        imagedestroy($captcha);
    }

	private function hex2rgb($hex_str, $return_string = false, $separator = ',') {
		$hex_str = preg_replace("/[^0-9A-Fa-f]/", '', $hex_str); // Gets a proper hex string
		$rgb_array = array();
		if( strlen($hex_str) == 6 ) {
			$color_val = hexdec($hex_str);
			$rgb_array['r'] = 0xFF & ($color_val >> 0x10);
			$rgb_array['g'] = 0xFF & ($color_val >> 0x8);
			$rgb_array['b'] = 0xFF & $color_val;
		} elseif( strlen($hex_str) == 3 ) {
			$rgb_array['r'] = hexdec(str_repeat(substr($hex_str, 0, 1), 2));
			$rgb_array['g'] = hexdec(str_repeat(substr($hex_str, 1, 1), 2));
			$rgb_array['b'] = hexdec(str_repeat(substr($hex_str, 2, 1), 2));
		} else {
			return false;
		}
		return $return_string ? implode($separator, $rgb_array) : $rgb_array;
	}

    private function no_cache() {
        header( 'Expires: Sat, 26 Jul 1997 05:00:00 GMT' );
        header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
        header( 'Cache-Control: no-store, no-cache, must-revalidate' );
        header( 'Cache-Control: post-check=0, pre-check=0', false );
        header( 'Pragma: no-cache' );
    }
}
