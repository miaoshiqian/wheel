<?php
return array(
	'code' => '',
	'min_length' => 5,
	'max_length' => 5,
	'png_backgrounds' => array(
		__DIR__.'/verify.png',
		__DIR__.'/default.png',
	),
	'fonts' => array(
		__DIR__.'/times_new_yorker.ttf',
	),
	'characters' => 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789',
	'min_font_size' => 25,
	'max_font_size' => 26,
	'color' => '#1B0A0A',
	'angle_min' => 0,
	'angle_max' => 0,
	'shadow' => true,
	'shadow_color' => '#CCC',
	'shadow_offset_x' => 0,
	'shadow_offset_y' => 0
);