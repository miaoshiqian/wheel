<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 *
 * @modify by chenchaoyang<chenchaoyang@anxin365.com>
 */
require_once dirname(__FILE__) . '/../../../conf/common/Store.php';

class GatewayClient {
    /**
     * 向服务者发送消息
     * @param $wokerId，可以是单个用户id，也可以是用户id数组
     * @param $message
     * @throws Exception
     */
    public static function sendToWorker($wokerId, $message) {
        $clientId = self::getClientIdByUid($wokerId);
        if (!empty($clientId))  {
            if (is_array($clientId)) {
                self::sendToAll($message, $clientId);
            } else {
                self::sendToClient($clientId, $message);
            }
        }
    }

    /**
     * 向客户发送消息
     * @param $customerId，可以是单个用户id，也可以是用户id数组
     * @param $message
     * @throws Exception
     */
    public static function sendToCustomer($customerId, $message) {
        $clientId = self::getClientIdByUid($customerId);
        if (!empty($clientId))  {
            if (is_array($clientId)) {
                self::sendToAll($message, $clientId);
            } else {
                self::sendToClient($clientId, $message);
            }
        }
    }


    /**
     * 向所有客户端(或者client_id_array指定的客户端)广播消息
     * @param string $message 向客户端发送的消息（可以是二进制数据）
     * @param array $client_id_array 客户端id数组
     */
    protected static function sendToAll($message, $client_id_array = null) {
        $gateway_data = GatewayProtocol::$empty;
        $gateway_data['cmd'] = GatewayProtocol::CMD_SEND_TO_ALL;
        $gateway_data['body'] = $message;

        if ($client_id_array) {
            $params = array_merge(array('N*'), $client_id_array);
            $gateway_data['ext_data'] = call_user_func_array('pack', $params);
        } elseif (empty($client_id_array) && is_array($client_id_array)) {
            return;
        }

        $all_addresses = Store::instance('gateway')->get('GLOBAL_GATEWAY_ADDRESS');
        if (!is_array($all_addresses)) {
            throw new \Exception('bad gateway addresses ' . var_export($all_addresses, true));
        }
        foreach ($all_addresses as $address) {
            self::sendToGateway($address, $gateway_data);
        }
    }

    /**
     * 向某个客户端发消息
     * @param int $client_id
     * @param string $message
     */
    protected static function sendToClient($client_id, $message) {
        $cmd = GatewayProtocol::CMD_SEND_TO_ONE;
        $address = Store::instance('gateway')->get('client_id-' . $client_id);
        if (!$address) {
            return false;
        }
        $gateway_data = GatewayProtocol::$empty;
        $gateway_data['cmd'] = $cmd;
        $gateway_data['client_id'] = $client_id ? $client_id : Context::$client_id;
        $gateway_data['body'] = $message;

        return self::sendToGateway($address, $gateway_data);
    }

    /**
     * 发送数据到网关
     * @param string $address
     * @param string $buffer
     */
    protected static function sendToGateway($address, $gateway_data) {
        $gateway_buffer = GatewayProtocol::encode($gateway_data);
        $client = stream_socket_client("tcp://$address", $errno, $errmsg);
        return strlen($gateway_buffer) == stream_socket_sendto($client, $gateway_buffer);
    }

    /**
     * 用户id 与 workerman唯一id 的对应关系转换
     * @param $userId 用户id
     * @return array
     * @throws Exception
     */
    public static function getClientIdByUid($userId) {
        if (is_array($userId)) {
            $cidList = array();
            foreach ($userId as $uid) {
                $cid = Store::instance('wid2ClientId')->hGet('wid2ClientId', $uid);;
                if ($cid) {
                    $cidList[] = $cid;
                }
            }
            return $cidList;
        } else {
            return Store::instance('wid2ClientId')->hGet('wid2ClientId', $userId);
        }
    }

}

/**
 * Gateway与Worker间通讯的二进制协议
 *
 * struct GatewayProtocol
 * {
 *     unsigned int        pack_len,
 *     unsigned char     cmd,//命令字
 *     unsigned int        local_ip,
 *     unsigned short    local_port,
 *     unsigned int        client_ip,
 *     unsigned short    client_port,
 *     unsigned int        client_id,
 *     unsigned char      flag,
 *     unsigned int        ext_len,
 *     char[ext_len]        ext_data,
 *     char[pack_length-HEAD_LEN] body//包体
 * }
 *
 *
 * @author walkor <walkor@workerman.net>
 */
class GatewayProtocol {
    // 发给worker，gateway有一个新的连接
    const CMD_ON_CONNECTION = 1;

    // 发给worker的，客户端有消息
    const CMD_ON_MESSAGE = 3;

    // 发给worker上的关闭链接事件
    const CMD_ON_CLOSE = 4;

    // 发给gateway的向单个用户发送数据
    const CMD_SEND_TO_ONE = 5;

    // 发给gateway的向所有用户发送数据
    const CMD_SEND_TO_ALL = 6;

    // 发给gateway的踢出用户
    const CMD_KICK = 7;

    // 发给gateway，通知用户session更改
    const CMD_UPDATE_SESSION = 9;

    // 获取在线状态
    const CMD_GET_ONLINE_STATUS = 10;

    // 判断是否在线
    const CMD_IS_ONLINE = 11;

    // 包体是标量
    const FLAG_BODY_IS_SCALAR = 0x01;

    /**
     * 包头长度
     * @var integer
     */
    const HEAD_LEN = 26;

    public static $empty = array('cmd' => 0, 'local_ip' => '0.0.0.0', 'local_port' => 0, 'client_ip' => '0.0.0.0', 'client_port' => 0, 'client_id' => 0, 'flag' => 0, 'ext_data' => '', 'body' => '',);

    /**
     * 返回包长度
     * @param string $buffer
     * @return int return current package length
     */
    public static function input($buffer) {
        if (strlen($buffer) < self::HEAD_LEN) {
            return 0;
        }

        $data = unpack("Npack_len", $buffer);
        return $data['pack_len'];
    }

    /**
     * 获取整个包的buffer
     * @param array $data
     * @return string
     */
    public static function encode($data) {
        $flag = (int)is_scalar($data['body']);
        if (!$flag) {
            $data['body'] = serialize($data['body']);
        }
        $ext_len = strlen($data['ext_data']);
        $package_len = self::HEAD_LEN + $ext_len + strlen($data['body']);
        return pack("NCNnNnNNC", $package_len, $data['cmd'], ip2long($data['local_ip']), $data['local_port'], ip2long($data['client_ip']), $data['client_port'], $data['client_id'], $ext_len, $flag) . $data['ext_data'] . $data['body'];
    }

    /**
     * 从二进制数据转换为数组
     * @param string $buffer
     * @return array
     */
    public static function decode($buffer) {
        $data = unpack("Npack_len/Ccmd/Nlocal_ip/nlocal_port/Nclient_ip/nclient_port/Nclient_id/Next_len/Cflag", $buffer);
        $data['local_ip'] = long2ip($data['local_ip']);
        $data['client_ip'] = long2ip($data['client_ip']);
        if ($data['ext_len'] > 0) {
            $data['ext_data'] = substr($buffer, self::HEAD_LEN, $data['ext_len']);
            if ($data['flag'] & self::FLAG_BODY_IS_SCALAR) {
                $data['body'] = substr($buffer, self::HEAD_LEN + $data['ext_len']);
            } else {
                $data['body'] = unserialize(substr($buffer, self::HEAD_LEN + $data['ext_len']));
            }
        } else {
            $data['ext_data'] = '';
            if ($data['flag'] & self::FLAG_BODY_IS_SCALAR) {
                $data['body'] = substr($buffer, self::HEAD_LEN);
            } else {
                $data['body'] = unserialize(substr($buffer, self::HEAD_LEN));
            }
        }
        return $data;
    }
}

/**
 * 存储类
 * 这里用memcache实现
 * @author walkor <walkor@workerman.net>
 */
class Store {
    /**
     * 实例数组
     * @var array
     */
    protected static $instance = array();

    /**
     * 获取实例
     * @param string $config_name
     * @throws \Exception
     */
    public static function instance($config_name) {
            if (!isset(\Config\Store::$$config_name)) {
                echo "\\Config\\Store::$config_name not set\n";
                throw new \Exception("\\Config\\Store::$config_name not set\n");
            }
            if (!isset(self::$instance[$config_name])) {
                self::$instance[$config_name] = new Redisd();
                // 只选择第一个ip作为服务端
                $address = current(\Config\Store::$$config_name);
                list($ip, $port) = explode(':', $address);
                $timeout = 0;
                self::$instance[$config_name]->connect($ip, $port, $timeout);
                self::$instance[$config_name]->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_PHP);
            }
            return self::$instance[$config_name];
    }
}

class Redisd extends \Redis {
    public function increment($key) {
        return parent::incr($key);
    }
}
