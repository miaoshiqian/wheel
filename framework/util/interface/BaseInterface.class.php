<?php

class BaseInterface {

    const TYPE_INT      = 1;    // isInt
    const TYPE_NUMERIC  = 2;    // is_numeric
    const TYPE_ARRAY    = 4;    // is_array
    const TYPE_STRING   = 8;    // is_string
    const TYPE_BOOL     = 16;   // is_bool

    protected static function getType($n) {

        $ret = 0;
        if (self::isInt($n))    $ret |= self::TYPE_INT;
        if (is_numeric($n))     $ret |= self::TYPE_NUMERIC;
        if (is_array($n))       $ret |= self::TYPE_ARRAY;
        if (is_string($n))      $ret |= self::TYPE_STRING;
        if (is_bool($n))        $ret |= self::TYPE_BOOL;
        return $ret;
    }

    /**
     *   整数或者整数字符串
     */
    protected static function isInt($n) {

        return (is_numeric($n) && intval($n) == $n);
    }

    protected static function getFromArray($key, $arr, $default = '', $forceType = 0, $allowEmpty = true) {

        if (!$allowEmpty) {
            if (!array_key_exists($key, $arr) && empty($default)) {
                throw new InterfaceException("缺少参数：{$key}");
            } else if (array_key_exists($key, $arr) && empty($arr[$key])) {
                throw new InterfaceException("参数为空：{$key}");
            }
        }
        $ret = !array_key_exists($key, $arr) ? $default : $arr[$key];
        if ($forceType) {
            if (0 == ($forceType & self::getType($ret))) {
                throw new InterfaceException("参数类型错误：{$key}");
            }
        }
        return $ret;
    }

    protected static function judgeEmpty($keys, $arr) {
        foreach ($keys as $key) {
            if (!array_key_exists($key, $arr)) {
                throw new InterfaceException("缺少参数：{$key}");
            }
        }
        return true;
    }
}