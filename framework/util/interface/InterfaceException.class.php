<?php
/**
 * @author 陈朝阳<chenchaoy@icloud.com>
 *  接口异常类
 * @date   15/9/19
 * @time   下午4:38
 */

class InterfaceException extends Exception {
    //参数缺失
    const E_PARAM_MISS = -1;
    //数据已存在
    const E_DATA_EXIST = -2;
    //model层操作错误
    const E_MODEL_ERROR = -3;

    public function __construct($message, $code = E_USER_ERROR, $tag = 'exception.interface') {

        $message = $message . ' ' . $this->getFile() . ':' . $this->getLine();
        Log::info($tag, $message);
        parent::__construct($message, $code);
    }
}
