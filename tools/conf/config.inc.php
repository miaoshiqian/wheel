<?php
define('PROJECT_PATH', dirname(dirname(__FILE__)));
define('ROOT_PATH', dirname(PROJECT_PATH));
define('API_PATH', ROOT_PATH . '/api');
define('FRAMEWORK_PATH', ROOT_PATH . '/framework');
define('CONF_PATH', ROOT_PATH . '/conf/conf');
define('CONF_COMMON_PATH', ROOT_PATH . '/conf/common');
define('DATA_PATH', ROOT_PATH . '/data/');

require_once CONF_COMMON_PATH.'/GlobalConfig.class.php';
$_SERVER['DEBUG'] = !GlobalConfig::inOnline();


require_once FRAMEWORK_PATH . '/mvc/page/bootstrap.php';

//设置该模块自动加载的目录,可以设置多个,如果多个目录中有同一个类的话，先设置哪个目录，就自动加载这个目录下面的类
AutoLoader::setAutoDir(FRAMEWORK_PATH . "/util/");//工具类
AutoLoader::setAutoDir(CONF_PATH);                //平台配置类
AutoLoader::setAutoDir(CONF_COMMON_PATH); // 项目总配置公共文件
AutoLoader::setAutoDir(PROJECT_PATH . "/conf/");  //配置类
AutoLoader::setAutoDir(PROJECT_PATH . "/common/");//项目公共类
AutoLoader::setAutoDir(API_PATH); // Api接口
AutoLoader::setAutoDir(ROOT_PATH.'/cache_redis'); // 缓存
AutoLoader::setAutoDir(FRAMEWORK_PATH.'/lib/alioss');

//注册自定义的自动加载方法
spl_autoload_register (['AutoLoader', 'autoLoad']);
