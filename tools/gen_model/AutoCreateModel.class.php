<?php
/**
 * 根据数据表生成对应的model文件，生成到models里，可根据需要做模板的相应修改
 */

require_once ROOT_PATH.'/conf/conf/db/DBConfig.class.php';
require_once ROOT_PATH.'/framework/util/db/BaseModel.class.php';

$dbNames = array(
	'log' => '\DBConfig::DB_LOG',
	'rric_new' => '\DBConfig::DB_WHEEL',
    'us_car' => '\DBConfig::DB_CAR',
	'common' => '\DBConfig::DB_COMMON',
);
$dbName = '';
$tableName = '';
$dbMasterConfig = '\DBConfig::$SERVER_MASTER';
$dbSlaveConfig = '\DBConfig::$SERVER_SLAVE';

class AutoCreateModel extends \BaseModel {
    protected function init() {
        global $dbName,$tableName,$dbMasterConfig,$dbSlaveConfig;
        $this->dbName         = eval('return '.$dbName.';');
        $this->tableName      = $tableName;
        $this->dbMasterConfig = eval('return '.$dbMasterConfig.';');
        $this->dbSlaveConfig  = eval('return '.$dbSlaveConfig.';');
        $this->fieldTypes     = array('int');
    }

    public static function getClassName($tableName) {
        $fs = explode('_', $tableName);
        $name = '';
        foreach ($fs as $f) {
            $name .= ucfirst($f);
        }
        $name .= 'Model';
        return $name;
    }

}

function genModel($name){
    global $dbNames, $dbName;

	$names = explode(".", $name);
	if (empty($names[0]) || empty($dbNames[$names[0]])) {
		echo "输入错误\n";
		return;
	}
	$dbName = $dbNames[$names[0]];
	if (empty($names[1])) {
		$model = new AutoCreateModel();
		$res = $model->queryAll('show tables');
		if($res) foreach ($res as $v) {
			_genModel($names[0], $v['Tables_in_' . $names[0]]);
		}
		return;
	}

	_genModel($names[0], $names[1]);
}

function _genModel($db, $table){
	global $dbNames, $dbName, $tableName, $dbMasterConfig, $dbSlaveConfig;

	$dbName = $dbNames[$db];
	$tableName = $table;
	$className = AutoCreateModel::getClassName($table);

	$a = new AutoCreateModel();
	$fields = $a->getFields();

	$fieldCode = "array(\n";
	foreach ($fields as $field) {
		$fieldCode .= "            '{$field['Field']}' => '{$field['type']}', // {$field['Comment']}\n";
	}
	$fieldCode .= '        )';

	$tplFile = dirname(__FILE__) . '/TplModel.class.php';
	$code = file_get_contents($tplFile);
	$code = str_replace('{DIS}', $tableName.'表 model', $code);
	$code = str_replace('{DATE}', date('Y-m-d H:i:s'), $code);
	$code = str_replace('TplModel', $className, $code);
	$code = str_replace('\'{DB_NAME}\'', $dbName, $code);
	$code = str_replace('\'{DB_MASTER_CONFIG}\'', $dbMasterConfig, $code);
	$code = str_replace('\'{DB_SLAVE_CONFIG}\'', $dbSlaveConfig, $code);
	$code = str_replace('{TABLE_NAME_TPL}', $tableName, $code);
	$code = str_replace('\'{FILE_TYPES_TPL}\'', $fieldCode, $code);

	echo $outFile = __DIR__.'/../../api/models/'.$className . '.class.php', "\n";
	file_put_contents($outFile, $code);
}
