<?php
/**
 * {DIS}
 * @date {DATE}
 */

class TplModel extends BaseModel {

    protected function init() {
        $this->dbName         = '{DB_NAME}';
        $this->tableName      = '{TABLE_NAME_TPL}';
        $this->dbMasterConfig = '{DB_MASTER_CONFIG}';
        $this->dbSlaveConfig  = '{DB_SLAVE_CONFIG}';
        $this->fieldTypes     = '{FILE_TYPES_TPL}';
    }
}