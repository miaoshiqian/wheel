<?php
/**
 *  根据数据表生成对应的model文件，生成到models里，可根据需要做模板的相应修改
 * User: 李登科<lidengke@xiongying.com>
 * Date: 2015/11/17
 * Time: 16:25
 */

require_once __DIR__.'/../conf/config.inc.php';

include_once __DIR__.'/AutoCreateModel.class.php';

//genAllModel();
$tableName = isset($argv[1]) ? $argv[1] : '';
if($tableName){
	genModel($tableName);
}

